#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from result import RetCode
from variable import Variable

def coldwater(inputs, pvalues, dbcur):
    """
    outputs : alert, coldwater command
    """
    avgtemp = old_div((inputs["#bottomtemp1"].getvalue() + inputs["#middletemp1"].getvalue() + inputs["#uptemp1"].getvalue()), 3)
    now = int(time.time())

    alert = 0
    coldwater = 0
    print ("cold water inputs", inputs)
    if now >= inputs["#secondtime"].getvalue() and inputs["#secondtime"].getvalue() != 0:    # 2단
        print ("2단 담금입니다!!")
        if avgtemp > inputs["#coldwork2"].getvalue() + inputs["#diff2"].getvalue():
            alert = avgtemp
        if avgtemp > inputs["#coldwork2"].getvalue():
            coldwater = 201     # switch on 
        elif avgtemp > inputs["#alcwork1"].getvalue() and inputs["#alc1"].getvalue() > inputs["#st1alc"].getvalue():
            coldwater = 201
        elif avgtemp > inputs["#alcwork2"].getvalue() and inputs["#alc1"].getvalue() > inputs["#st2alc"].getvalue():
            coldwater = 201
        elif avgtemp > inputs["#alcwork3"].getvalue() and inputs["#alc1"].getvalue() > inputs["#st3alc"].getvalue():
            coldwater = 201

    elif now >= inputs["#firsttime"].getvalue() and inputs["#firsttime"].getvalue() != 0:   # 1단
        print ("1단 담금입니다!!")
        if inputs["#bottomtemp1"].getvalue() > inputs["#coldwork1"].getvalue() + inputs["#diff1"].getvalue():
            alert = inputs["#bottomtemp1"].getvalue()
        if avgtemp > inputs["#coldwork1"].getvalue():
            coldwater = 201

    return (RetCode.OK, [alert, coldwater])

def mix(inputs, pvalues, dbcur):
    """
    outputs : mixer command
    """
    now = int(time.time())
    cmd = 0
    print ("mix rule inputs", inputs)
    diff = (inputs["#mixstatus"].getobserved() - inputs["#mixstatus"].getmodified()).total_seconds()
    print ("mixer status is stable for ", diff/3600)

    if now >= inputs["#secondtime"].getvalue() and inputs["#secondtime"].getvalue() != 0:    # 2단
        print ("2단 담금입니다!!")
        if inputs["#mixstatus"].getvalue() == 0:
            if diff >= inputs["#period2"].getvalue():
                cmd = 201
        elif inputs["#mixstatus"].getvalue() == 201:
            if diff >= inputs["#worktime2"].getvalue():
                cmd = 0
        else:
            print("mixer status is bad.")
            cmd = 0
    elif now >= inputs["#firsttime"].getvalue() and inputs["#firsttime"].getvalue() != 0:   # 1단
        print ("1단 담금입니다!!")
        if inputs["#mixstatus"].getvalue() == 0:
            if diff >= inputs["#period1"].getvalue():
                cmd = 201
        elif inputs["#mixstatus"].getvalue() == 201:
            if diff >= inputs["#worktime1"].getvalue():
                cmd = 0
        else:
            print("mixer status is bad.")
            cmd = 0
    return (RetCode.OK, [cmd])

if __name__ == '__main__':
    inputs= {
        "#bottomtemp1" : Variable(10), 
        "#middletemp1" : Variable(11), 
        "#uptemp1" : Variable(12), 
        "#secondtime" : Variable(int(time.time()) + 10),
        "#firsttime" : Variable(int(time.time()) - 10),
        "#max1" : Variable(10),
        "#diff1" : Variable(2),
        "#coldwork1" : Variable(10)
    }
    ret = coldwater(inputs, [])
    print("result", ret)
