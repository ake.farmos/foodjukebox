#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime, timedelta

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

soiltbl = [
    [10, 20, 33, 50, 100, 1500],
    [8.2, 7.0, 6.3, 5.9, 5.0, 2.6],
    [12.5, 10.6, 9.6, 9.0, 7.74, 4.3],
    [22.3, 19.2, 17.0, 15.5, 13.1, 6.3],
    [29.4, 25.3, 22.5, 20.6, 17.3, 8.7],
    [35.1, 30.5, 27.1, 24.9, 21.3, 10.4],
    [27.3, 23.4, 20.6, 19.1, 16.8, 10.6],
    [33.0, 28.9, 26.2, 24.5, 21.4, 12.8],
    [37.8, 33.4, 30.9, 29.0, 26.0, 15.7],
    [42.5, 38.3, 35.7, 33.7, 31.2, 23.5],
    [44.0, 40.0, 38.1, 36.2, 33.7, 26.2],
]

def calculatesoiltension(inputs, pvalues, dbcur):
    """
    inputs : #solitype, #content1
    outputs : #tension
    """
    soiltype = int(inputs["#soiltype"].getvalue())
    content = inputs["#content1"].getvalue()
    tension = soiltbl[0]
    row = soiltbl[soiltype]
    if content > row[0]:
        return (RetCode.OK, [9])
    for i in range(1, 6):
        if row[i - 1] >= content > row[i]:
            ratio = (row[i-1] - content) / (row[i-1] - row[i])
            return (RetCode.OK, [tension[i-1] + (tension[i] - tension[i-1]) * ratio])
    return (RetCode.OK, [1501])

def antichill(inputs, pvalues, dbcur):
    """
    inputs: #maxt, #mint, #avgt, #ca
    outputs : #caacc, #ca, #germinated

    scheduled rule. no pvalues is normal.
    """
    nsec = inputs["#nsec"].getvalue()
    times = inputs["#table.time"].getvalue()
    for idx in range(len(times)):
        if nsec <= times[idx] < nsec + 60:
            break
    else:
        return (RetCode.NOT_PROPER_TIME, None)

    if "#caacc" in inputs:
        before = inputs["#caacc"].getvalue() 
    else:
        before = 0

    ltime = time.localtime(time.time())
    gtime = time.localtime(inputs["#germinated"].getvalue())
    if gtime.tm_year != ltime.tm_year:
        before = 0  # new year. reset caacc
    elif before > 155:
        return (RetCode.NOT_PROPER_TIME, None)

    if inputs["#mint"].getvalue() > 8:
        ca = inputs["#avgt"].getvalue() - 8
    else:
        maxt = inputs["#maxt"].getvalue()
        mint = inputs["#mint"].getvalue()
        if maxt > 8:
            ca = (maxt - 8) * (maxt - 8) / 2 / (maxt - mint)
        else:
            ca = 0
    caacc = before + ca

    if caacc > 155:
        germinated = time.time() - 86400
    else:
        if caacc == 0:
            nday = 155
        else:
            temp = 155 * ltime.tm_yday / caacc / 1.2     # 20%
            nday = 155 if temp > 200 else temp
        germinated = (datetime(ltime.tm_year, 1, 1, 0, 0) + timedelta(nday)).timestamp()

    return (RetCode.OK, [caacc, ca, germinated])

def growthperiod(inputs, pvalues, dbcur):
    """
    inputs: #germinated
    outputs : #period
    """
    nsec = inputs["#nsec"].getvalue()
    times = inputs["#table.time"].getvalue()
    for idx in range(len(times)):
        if nsec <= times[idx] < nsec + 60:
            break
    else:
        return (RetCode.NOT_PROPER_TIME, None)

    today = time.localtime(time.time()).tm_yday
    gday = time.localtime(inputs["#germinated"].getvalue()).tm_yday
    nday = today - gday

    prevp = inputs["#period"].getvalue()
    days = [-1 * gday, -7, 30, 40, 92, 132, 150]
    tmp = 0
    for i in range(len(days) - 1):
        if days[i] <= nday < days[i+1]:
            period = 21 + i
            break
    else:
        if today > 325: #11/20
            period = 21
        else:
            period = 27

    return (RetCode.OK, [period])

def irrigation(inputs, pvalues, dbcur):
    """
    inputs : day1min, day1prep, day1rain, day2prep, day2rain, day3prp, day3rain, tension, period, prep, rain, temp, isday
    outputs : cmd
    """

    stdtension = [30, 30, None, 30, 40, 50, 30]
    gp = int(inputs["#period"].getvalue()) - 21
    if stdtension[gp] == None:
        return (RetCode.OK, [CmdCode.OFF])

    if stdtension[gp] > inputs["#tension"].getvalue():
        return (RetCode.OK, [CmdCode.OFF])

    prep = inputs["#prep"].getvalue()
    rain = inputs["#rain"].getvalue()

    if inputs["#day1prep"].getvalue() >= prep and inputs["#day1rain"].getvalue() >= rain:
        return (RetCode.OK, [CmdCode.OFF])
    if inputs["#day2prep"].getvalue() >= prep and inputs["#day2rain"].getvalue() >= rain:
        return (RetCode.OK, [CmdCode.OFF])
    if inputs["#day3prep"].getvalue() >= prep and inputs["#day3rain"].getvalue() >= rain:
        return (RetCode.OK, [CmdCode.OFF])

    if inputs["#day1min"].getvalue() > inputs["#temp"].getvalue():
        if inputs["#isday"].getvalue() == 0:
            return (RetCode.OK, [CmdCode.TIMED_ON])
    else:
        if inputs["#isday"].getvalue() == 1:
            return (RetCode.OK, [CmdCode.TIMED_ON])
    return (RetCode.OK, [CmdCode.OFF])


if __name__ == '__main__':
    pass
