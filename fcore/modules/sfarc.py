#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import print_function
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode, StatCode
from result import RetCode
from variable import Variable

def heatpump(inputs, pvalues, dbcur):
    """
    inputs : intemp0, intemp1, pump0, starttemp, stoptemp, waitsec
    outputs : pumpcmd
    """

    cmd = None
    now = datetime.now()
    stoptime = None

    if inputs["#pump0"].getvalue() == StatCode.WORKING and inputs["#intemp1"].getvalue() > inputs["#stoptemp"].getvalue():
        cmd = CmdCode.OFF.value
        stoptime = now

    elif inputs["#pump0"].getvalue() == StatCode.READY and inputs["#intemp1"].getvalue() < inputs["#starttemp"].getvalue():
        if (pvalues is None or len(pvalues) == 0) or pvalues[1] is None or (now - pvalues[1]).total_seconds() > inputs["#waitsec"]:
            cmd = CmdCode.ON.value
        else:
            stoptime = pvalues[1]

    print("calculated cmd", cmd, stoptime)

    values = [cmd, stoptime]
    return (RetCode.OK, values)

def boiler(inputs, pvalues, dbcur):
    """
    inputs : tanktemp0, tanktemp1, pump0, boiler0, starttemp, stoptemp, waitsec
    outputs : pumpcmd
    """

    cmd = None
    now = datetime.now()
    stoptime = None

    if inputs["#pump0"].getvalue() == StatCode.WORKING and inputs["#tanktemp1"].getvalue() < inputs["#stoptemp"].getvalue():
        cmd = CmdCode.OFF.value
        stoptime = now

    elif inputs["#pump0"].getvalue() == StatCode.READY and inputs["#tanktemp1"].getvalue() < inputs["#starttemp"].getvalue():
        if (pvalues is None or len(pvalues) == 0) or pvalues[2] is None or (now - pvalues[2]).total_seconds() > inputs["#waitsec"]:
            cmd = CmdCode.ON.value
        else:
            stoptime = pvalues[2]

    print("calculated cmd", cmd, stoptime)

    values = [cmd, cmd, stoptime]
    return (RetCode.OK, values)

if __name__ == '__main__':
    inputs= {
        "#pump0" : Variable(0), 
        "#intemp0" : Variable(0),
        "#intemp1" : Variable(14),
        "#starttemp" : Variable(15),
        "#stoptemp" : Variable(17),
        "#waitsec" : Variable(600)
    }

    prev = heatpump(inputs, [], None)
    print("heatpump - on", inputs, prev) 
    time.sleep(3)
    inputs["#pump0"] = Variable(StatCode.WORKING.value)
    cmd = heatpump(inputs, prev[1], None)
    print("heatpump - do nothing", inputs, cmd)
    time.sleep(3)
    inputs["#intemp1"] = Variable(18)
    cmd = heatpump(inputs, prev[1], None)
    print("heatpump - stop", inputs, cmd)

