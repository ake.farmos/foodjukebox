#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
import socket
from datetime import datetime, timedelta
from collections import deque

import sys
sys.path.insert(0, "..")
from code import RetCode
from mblock import CmdCode
from variable import Variable

def movingaverage(inputs, pvalues, dbcur):
    """
    inputs: #sensor0, #sensor1, #number
    outputs : value
    """

    n = inputs["#number"].getvalue()
    v = inputs["#sensor1"].getvalue()
    if pvalues is None or len(pvalues) == 0:
        # first time
        values = [v, deque([v] * n)] 
    else:
        q = pvalues[1]
        q.popleft()
        q.append(v)
        values = [old_div(sum(q)*1.0,n), q]

    return (RetCode.OK, values)

def highhumidity(inputs, pvalues, dbcur):
    """
    inputs: #min, #inhum1, #hum, #msg, #receiver, #id
    outputs : value
    """

    n = inputs["#min"].getvalue()
    s = inputs["#hum"].getvalue()

    if pvalues is None or len(pvalues) == 0:
        # first time
        did = inputs["#inhum1"].getid()
        query = "select * from observations where data_id = %s order by obs_time desc limit %s"
        dbcur.execute(query, [did, n])
        values = deque()
        for row in dbcur.fetchall():
            values.append(row['nvalue'])
    else:
        v = inputs["#inhum1"].getvalue()
        values = pvalues[4]
        values.popleft()
        values.append(v)
        
    avg = sum(values) * 1.0 / n
    if s < avg:
        alertid = inputs["#id"].getvalue()
        msg = inputs["#msg"].getvalue() + " (호스트명: " + socket.gethostname() + ", " + str(n) + "분 동안 평균습도 : " + str(avg) + ")"
        rets = [CmdCode.ALERT.value, alertid, msg, inputs["#receiver"].getvalue(), values]
    else:
        rets = [None, None, None, None, values]
    return (RetCode.OK, rets)

def lowpassfilter(inputs, pvalues, dbcur):
    """
    inputs: #sensor0, #sensor1, #weight
    outputs : value
    """

    v = inputs["#sensor1"].getvalue()
    if pvalues is None or len(pvalues) == 0:
        # first time
        values = [v] 
    else:
        w = inputs["#weight"].getvalue()
        p = pvalues[0]
        values = [v * (1 - w) + p * w]

    return (RetCode.OK, values)

def temperatureguide(inputs, pvalues, dbcur):
    """
    inputs: #vtemp, #htemp
    outputs : venttemp, heattemp, stdtemp
    """
    v = inputs["#vtemp"].getvalue()
    h = inputs["#htemp"].getvalue()
    return (RetCode.OK, [v, h, old_div((v + h), 2.0)])

def radaccumulate(inputs, pvalues, dbcur):
    """
    inputs: #outrad0, #outrad1, #accrad
    outputs : acc, day
    """

    day = time.localtime(time.time()).tm_yday
    if pvalues is None or len(pvalues) == 0 or pvalues[1] == day:
        if inputs["#accrad"].isupdatedtoday():
            before = inputs["#accrad"].getlastestvalue()
        else:
            before = 0
    else:
        before = 0

    print (before, inputs["#outrad1"].getvalue())
    acc = before + inputs["#outrad1"].getvalue() * 0.0036

    return (RetCode.OK, [acc, day])

def diffdaynight(inputs, pvalues, dbcur):
    """
    inputs: #temp0, #temp1, sunrise, sunset
    outputs: day, night, diff
    """
    return (RetCode.OK, [])


if __name__ == '__main__':
    inputs= {"#sensor0" : Variable(0), "#sensor1" : Variable(10), "#number" : Variable(3)}
    ret, pv = movingaverage(inputs, None)
    print("result", pv)
    inputs= {"#sensor0" : Variable(0), "#sensor1" : Variable(11), "#number" : Variable(3)}
    ret, pv = movingaverage(inputs, pv)
    print("result", pv)
    inputs= {"#sensor0" : Variable(0), "#sensor1" : Variable(12), "#number" : Variable(3)}
    ret, pv = movingaverage(inputs, pv)
    print("result", pv)
    inputs= {"#sensor0" : Variable(0), "#sensor1" : Variable(13), "#number" : Variable(3)}
    ret, pv = movingaverage(inputs, pv)
    print("result", pv)





