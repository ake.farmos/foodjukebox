#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

def _resetload(value):
    if value > 100:
       return 100
    elif value < 0:
       return 0
    return value

def heatload(inputs, pvalues, dbcur):
    """
    inputs : tsidx, intemp0, intemp1, htemp, KpH, KdH, KpO, inoutdiff
    outputs : heatload
    """

    tsidx = inputs["tsidx"].getvalue()
    htemp = inputs["#intemp1"].getlastvalue() - (inputs["#vtemp"].getlastvalue() + inputs["#htemp"].getlastvalue()) / 2 
    if pvalues is None or len(pvalues) == 0:
        thd = Variable(htemp)
    else:
        thd = pvalues[1]
        thd.setvalue(htemp)
        
    dt = old_div(inputs["period"].getvalue(), 60)
    hl = inputs["#KpH"].getvalue(tsidx) * htemp + inputs["#KdH"].getvalue(tsidx) * thd.getdifferential(dt) + inputs["#KpO"].getvalue(tsidx) * inputs["#inoutdiff"].getvalue()

    print("calculated hl", hl)

    values = [_resetload(hl), thd]
    return (RetCode.OK, values)

def ventload(inputs, pvalues, dbcur):
    """
    inputs : tsidx, intemp0, intemp1, htemp, KpV, KdV, KiV, KpO, KdO, inoutdiff
    outputs : ventload
    """
    tsidx = inputs["tsidx"].getvalue()
    vtemp = inputs["#intemp1"].getlastvalue() - (inputs["#vtemp"].getlastvalue() + inputs["#htemp"].getlastvalue()) / 2
    if pvalues is None or len(pvalues) == 0:
        thd = Variable(vtemp)
    else:
        thd = pvalues[1]
        thd.setvalue(vtemp)

    dt = old_div(inputs["period"].getvalue(), 60)
    vl = inputs["#KpV"].getvalue(tsidx) * vtemp + inputs["#KdV"].getvalue(tsidx) * thd.getdifferential(dt) + inputs["#KiV"].getvalue(tsidx) * thd.getintegration(dt)  + inputs["#KpO"].getvalue(tsidx) * inputs["#inoutdiff"].getvalue() + inputs["#KdO"].getvalue(tsidx) * inputs["#inoutdiff"].getdifferential(dt)

    print("calculated vl", vl)

    values = [_resetload(vl), thd]
    return (RetCode.OK, values)

def ventloadcontrol(inputs, pvalues, dbcur):
    """
    inputs : ventload, rightwind, leftwind, israin
    outputs : leftcmd, leftpos, rightcmd, rightpos
    """
    print("inputs", inputs)
    if inputs["#israin1"].getvalue() > 0: # it's raining
        rpos = lpos = 0
    else:
        lpos = rpos = inputs["#ventload"].getvalue()
        if inputs["#rightwind"].getvalue() == 1:
            rpos = rpos * 0.7
        if inputs["#leftwind"].getvalue() == 1:
            lpos = lpos * 0.7

    values = [CmdCode.POSITION.value, lpos, CmdCode.POSITION.value, rpos]
    return (RetCode.OK, values)

def temperature_compensation(inputs, pvalues, dbcur):
    """
    outputs : tc1day, tc2day, tc3day
    """
    if pvalues is None or len(pvalues) == 0:
        now = datetime.now()
        n = int(old_div((now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds(), 60))
        oldtc = inputs["#tctoday"].getvalue()
        tc = inputs["#stdtemp"].getvalue() - inputs["#intemp1"].getvalue()
    else:
        n = pvalues[4] 
        oldtc = pvalues[0]
        tc = old_div((old_div(oldtc, n) + inputs["#stdtemp"].getvalue() - inputs["#intemp1"].getvalue()), 2.0)

    n = n + 1
    if inputs["#tc1day"].isupdatedtoday():
        return (RetCode.OK, [tc, None, None, None, n])

    else: # first time at a day
        tc2 = inputs["#tc1day"].getvalue() + oldtc
        tc3 = inputs["#tc2day"].getvalue() + oldtc
        values = [tc, oldtc, tc2, tc3, 1]

    return (RetCode.OK, values)

if __name__ == '__main__':
    inputs= {
        "tsidx" : Variable(0), 
        "#KpH" : Variable([-5]), 
        "#KdH" : Variable([-3]), 
        "#KpO" : Variable([-1]), 
        "#htemp" : Variable(15),
        "#intemp0" : Variable(0),
        "#intemp1" : Variable(14),
        "#inoutdiff" : Variable(4)
    }
    hl = heatload(inputs, [], None)
    vl = ventload(inputs, [], None)
    print("heatload", hl, "ventload", vl)

    inputs= {
        "tsidx" : Variable(0), 
        "#KpH" : Variable([-5]), 
        "#KdH" : Variable([-3]), 
        "#KpO" : Variable([-1]), 
        "#htemp" : Variable(15),
        "#intemp0" : Variable(0),
        "#intemp1" : Variable(13),
        "#inoutdiff" : Variable(3)
    }
    pvalues = hl[1]
    hl = heatload(inputs, pvalues, None)
    pvalues = vl[1]
    vl = ventload(inputs, pvalues, None)
    print("heatload", hl, "ventload", vl)

    inputs= {
        "#tctoday" : Variable(10), 
        "#tc1day" : Variable(10), 
        "#tc2day" : Variable(11), 
        "#tc3day" : Variable(12), 
        "#stdtemp" : Variable(25), 
        "#intemp0" : Variable(0),
        "#intemp1" : Variable(23)
    }
    tc = temperature_compensation(inputs, None, None)
    print("tc", tc)
    inputs["intemp1"] = Variable(24)
    pvalues = tc[1]
    tc = temperature_compensation(inputs, pvalues, None)
    print("tc", tc)
