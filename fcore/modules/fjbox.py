#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

def temperatureguide(inputs, pvalues, dbcur):
    """
    inputs: #cooltemp, #heattemp
    outputs : cooltemp, heattemp, stdtemp
    """
    c = inputs["#cooltemp"].getvalue()
    h = inputs["#heattemp"].getvalue()
    return (RetCode.OK, [c, h, old_div((c + h), 2.0)])

def _resetload(value):
    if value > 100:
       return 100
    elif value < 0:
       return 0
    return value

def heatload(inputs, pvalues, dbcur):
    """
    inputs : tsidx, intemp0, intemp1, heattemp, KpH, KdH, KpO, inoutdiff
    outputs : heatload
    """

    tsidx = inputs["tsidx"].getvalue()
    htemp = inputs["#intemp1"].getlastvalue() - inputs["#cooltemp"].getlastvalue()
    if pvalues is None or len(pvalues) == 0:
        thd = Variable(htemp)
    else:
        thd = pvalues[1]
        thd.setvalue(htemp)
        
    dt = old_div(inputs["period"].getvalue(), 60)
    print("calculated htemp", thd.getvalue(), thd.getdifferential(dt), thd.getintegration(dt))
    hl = inputs["#KpH"].getvalue(tsidx) * htemp + inputs["#KdH"].getvalue(tsidx) * thd.getdifferential(dt) + inputs["#KiH"].getvalue(tsidx) * thd.getintegration(dt) + inputs["#KpO"].getvalue(tsidx) * inputs["#inoutdiff"].getvalue()

    print("calculated hl", hl)

    values = [_resetload(hl), thd]
    return (RetCode.OK, values)

def coolload(inputs, pvalues, dbcur):
    """
    inputs : tsidx, intemp0, intemp1, cooltemp, KpV, KdV, KiV, KpO, inoutdiff
    outputs : ventload
    """
    tsidx = inputs["tsidx"].getvalue()
    vtemp = inputs["#intemp1"].getlastvalue() - inputs["#heattemp"].getlastvalue()
    if pvalues is None or len(pvalues) == 0:
        thd = Variable(vtemp)
    else:
        thd = pvalues[1]
        thd.setvalue(vtemp)

    dt = old_div(inputs["period"].getvalue(), 60)
    vl = inputs["#KpV"].getvalue(tsidx) * vtemp + inputs["#KdV"].getvalue(tsidx) * thd.getdifferential(dt) + inputs["#KiV"].getvalue(tsidx) * thd.getintegration(dt)  + inputs["#KpO"].getvalue(tsidx) * inputs["#inoutdiff"].getvalue()

    print("calculated vl", vl)

    values = [_resetload(vl), thd]
    return (RetCode.OK, values)

def periodicspray(inputs, pvalues, dbcur):
    """
    inputs : tsidx, workperiod, worktime, inpump90, inpump91
    outputs : cmd, flow, accflow, count, day
    """
    tsidx = inputs["tsidx"].getvalue()
    day = time.localtime(time.time()).tm_yday
    obstime = inputs["#inpump90"].getobserved().timetuple()

    if obstime.tm_yday != day: # 날이 바뀐거임
        flow = 0
        accflow = 0
        cur = Variable(0)
    else:
        flow = inputs["#inpump90"].getvalue()
        accflow = inputs["#inpump91"].getvalue()

        if pvalues is None or len(pvalues) == 0:
            cur = Variable(0)
        else:
            cur = Variable(pvalues[3])
            cur.inc()

    if cur.getvalue() % inputs["#workperiod"].getvalue(tsidx) == 0:
        flow = 6.43 * inputs["#worktime"].getvalue() - 2.6
        accflow = accflow + flow
        values = [CmdCode.TIMED_ON.value, flow, accflow, cur.getvalue(), day] 
    else:
        values = [CmdCode.OFF.value, flow, accflow, cur.getvalue(), day] 
    return (RetCode.OK, values)

def schedulespray(inputs, pvalues, dbcur):
    """
    inputs : table.cmd, table.time, table.worktime
    outputs : cmd, flow, accflow, count, day
    """

    day = time.localtime(time.time()).tm_yday
    obstime = inputs["#inpump90"].getobserved().timetuple()
    if obstime.tm_yday != day: # 날이 바뀐거임
        flow = 0
        accflow = 0
    else:
        flow = inputs["#inpump90"].getvalue()
        accflow = inputs["#inpump91"].getvalue()

    nsec = inputs["#nsec"].getvalue()
    times = inputs["#table.time"].getvalue()
    for idx in range(len(times)):
        if nsec <= times[idx] < nsec + 60:
            cmds = inputs["#table.cmd"].getvalue()
            worktime = inputs["#table.worktime"].getvalue()
            flow = 6.43 * worktime[idx] - 2.6
            accflow = accflow + flow
            return (RetCode.OK, [cmds[idx], worktime[idx], flow, accflow])

    if obstime.tm_yday != day: # 날이 바뀐거임
        return (RetCode.OK, [None, None, flow, accflow])
    else:
        return (RetCode.NOT_PROPER_TIME, None)

if __name__ == '__main__':
    pass
