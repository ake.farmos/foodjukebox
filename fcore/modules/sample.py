#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from code import VarCode
from variable import Variable

import cv2
from tensorflow.keras.models import load_model

MODEL_PATH = 'modules/sampledata/mnist_mlp_model.h5'

def loadimage(path):
    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    img = cv2.resize(img, (28, 28))
    img = 255 - img
    return img.reshape(1, 784).astype('float32') / 255.0

def mnisttest(inputs, pvalues, dbcur):
    """
    inputs : cam.img
    outputs : digit
    """
    model = load_model(MODEL_PATH)
    img = inputs["#cam.img"].getvalue()
    if img is None or 'path' not in img:
        return (RetCode.PARAM_ERROR, [])

    data = loadimage(img['path'])
    ans = model.predict_classes(data)

    print("predict", ans)

    values = [ans[0].item()]
    return (RetCode.OK, values)

if __name__ == '__main__':
    inputs= { "#cam.img" : Variable({'path':'sampledata/n2.jpg', 'meta':{}}, 110, 1, VarCode.IMG) }
    MODEL_PATH = 'sampledata/mnist_mlp_model.h5'
    pred = mnisttest(inputs, [], None)
    print("prediction should be 2", pred)
