#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

import cv2
import numpy as np


def thresholding(green_img):
    img = cv2.cvtColor(green_img, cv2.COLOR_BGR2GRAY)
    ret, thr2 = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    blur = cv2.GaussianBlur(img, (5, 5), 0)
    ret, thr3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return thr3

def rgb_ave(image_dir):
    #img= cv2.imread(image_dir)
    a, green_img=green_extract(image_dir)
    img_RGB = cv2.cvtColor(green_img, cv2.COLOR_BGR2RGB)
    len=img_RGB.shape
    img_r=0
    count=0
    img_g=0
    img_b=0
    if np.unique(img_RGB).shape[0]>1:
        for i in range(0,len[0]):
            for j in range(0,len[1]):
                if sum(img_RGB[i,j])>0:
                    img_r=img_r+img_RGB[i,j][0]
                    img_g=img_g+img_RGB[i,j][1]
                    img_b=img_b+img_RGB[i,j][2]
                    count=count+1

        img_r_ave=img_r/count
        img_g_ave=img_g/count
        img_b_ave=img_b/count
        rgb_ave=[img_r_ave,img_g_ave,img_b_ave]
    
    else : 
        print('no plant detected')
        rgb_ave=0
    return rgb_ave


def hsv_ave(image_dir):
    #img= cv2.imread(image_dir)
    a, green_img=green_extract(image_dir)
    img_HSV = cv2.cvtColor(green_img, cv2.COLOR_BGR2HSV)
    len=img_HSV.shape
    count=0
    img_H=0
    img_S=0
    img_V=0
    if np.unique(img_HSV).shape[0]>1:
        for i in range(0,len[0]):
            for j in range(0,len[1]):
                if sum(img_HSV[i,j])>0:
                    img_H=img_H+img_HSV[i,j][0]
                    img_S=img_S+img_HSV[i,j][1]
                    img_V=img_V+img_HSV[i,j][2]
                    count=count+1

        img_H_ave=img_H/count
        img_S_ave=img_S/count
        img_V_ave=img_V/count
        HSV_ave=[img_H_ave,img_S_ave,img_V_ave]
    else:
        print('no plant detected')
        HSV_ave=0
    return HSV_ave


def pixel2weight(a):    #cm^2 to weight model
    ratio=0.0373018091599477
    pixel_calibration=0.826612903
    c=b[4]*100*(1/ratio)*(1/pixel_calibration)
    weight= 1.9752e-11*c**2*1.5821e-5*c
    
    return weight


def greenex(inputs, pvalues, dbcur):
    """
    inputs: #cam.img, #cam.stat
    outputs : #leftup, #rightup, #center, #leftdown, #rightdown
    """
    # 시간 체크는 트리거에서 수행함.
    #nsec = inputs["#nsec"].getvalue()
    #times = inputs["#table.time"].getvalue()
    #for idx in range(len(times)):
    #    if nsec <= times[idx] < nsec + 60:
    #        # 가동시간이 된 경우
    #        break
    #else:
    #    return (RetCode.NOT_PROPER_TIME, None)

    camimg = inputs["#cam.img"].getvalue()
    if camimg is None or 'path' not in camimg:
        return (RetCode.PARAM_ERROR, [])

    return (RetCode.OK, _greenex(camimg['path']))

def _greenex(path):
    img = cv2.imread(path)

    norm_img = np.zeros((2464,3280))
    final_img = cv2.normalize(img,  norm_img, 0, 255, cv2.NORM_MINMAX)
    hsv = cv2.cvtColor(final_img, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, (3, 27, 58), (118, 136, 140))
    
    imask = mask>0
    green_img = np.zeros_like(final_img, np.uint8)
    green_img[imask] = img[imask]


    predicted= thresholding(green_img)

    roi1=predicted[0:1030, 0:1640]
    roi2=predicted[0:1030, 2000:3280]
    roi3=predicted[900:1700, 1200:2200]
    roi4=predicted[1300:2464, 0:1400]
    roi5=predicted[1500:2464,1500:3280]
    ratio=0.0373018091599477
    n_white_pix1 = float(np.sum(roi1 == 255)*ratio*0.01)
    n_white_pix2 = float(np.sum(roi2 == 255)*ratio*0.01)
    n_white_pix3 = float(np.sum(roi3 == 255)*ratio*0.01)
    n_white_pix4 = float(np.sum(roi4 == 255)*ratio*0.01)
    n_white_pix5 = float(np.sum(roi5 == 255)*ratio*0.01)

    return [n_white_pix1, n_white_pix2, n_white_pix3, n_white_pix4, n_white_pix5]

if __name__ == '__main__':
    import sys

    if len(sys.argv) != 2:
        print("usage: python3 fjbox2.py image_file_path")
        sys.exit(2)

    print (_greenex(sys.argv[1]))
