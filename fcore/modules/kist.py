#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

def schedulenutri(inputs, pvalues, dbcur):
    """
    inputs : table.time, table.worktime, table.sfield, table.efield, table.ec, table.ph
    outputs : cmd, worktime, sfield, efield, ec, ph
    """

    day = time.localtime(time.time()).tm_yday
    nsec = inputs["#nsec"].getvalue()
    times = inputs["#table.time"].getvalue()
    for idx in range(len(times)):
        if nsec <= times[idx] < nsec + 60:
            cmd = CmdCode.PARAMED_WATERING.value
            worktime = inputs["#table.worktime"].getvalue()
            sfield = inputs["#table.sfield"].getvalue()
            efield = inputs["#table.efield"].getvalue()
            ec = inputs["#table.ec"].getvalue()
            ph = inputs["#table.ph"].getvalue()
            
            return (RetCode.OK, [cmd, worktime[idx], sfield[idx], efield[idx], ec[idx], ph[idx]])

    return (RetCode.NOT_PROPER_TIME, None)

def _generatecmd(inputs, cmd, tsaccrad, lastsupply = None):
    tsidx = inputs["tsidx"].getvalue()
    if cmd == CmdCode.PARAMED_WATERING.value:
        lastsupply = time.time()

    return (RetCode.OK, [cmd,
        inputs["#worktime"].getvalue(tsidx),
        inputs["#sfield"].getvalue(tsidx),
        inputs["#efield"].getvalue(tsidx),
        inputs["#ec"].getvalue(tsidx),
        inputs["#ph"].getvalue(tsidx),
        lastsupply, tsaccrad, tsidx])

def accradnutri(inputs, pvalues, dbcur):
    """
    inputs : accrad, stdrad, worktime, sfield, efield, ec, ph, startsupply, limit, forcesupply
    outputs : cmd, worktime, sfield, efield, ec, ph, lastsupply, tsaccrad, lastts
    """
    tsidx = inputs["tsidx"].getvalue()
    if pvalues is None or len(pvalues) == 0:
        tsaccrad = 0
        lastsupply = None
    else: 
        if pvalues[-1] != tsidx:
            # 새로운 시간대 진입
            tsaccrad = 0
            if inputs["#startsupply"].getvalue(tsidx) is True:
                return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)
        else:
            tsaccrad = pvalues[-2]
        lastsupply = pvalues[-3]

    print (tsaccrad)
    tsaccrad = tsaccrad + inputs["#accrad"].getdelta()
    print (tsaccrad)

    if lastsupply is not None:
        force = inputs["#forcesupply"].getvalue(tsidx)
        if force > 0 and force * 60 < time.time() - lastsupply:
            # 강제 관수시간에 진입
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

    if tsaccrad > inputs["#stdrad"].getvalue():
        # 누적일사 충족
        if lastsupply is None:
            # 마지막 관수시간을 알수 없어서 일단 관수. 누적일사는 0으로 리셋
            tsaccrad = 0
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

        limit = inputs["#limit"].getvalue(tsidx)
        if limit > 0 and limit * 60 < time.time() - lastsupply:
            # 관수제한시간 지나서 관수 가능. 누적일사는 0으로 리셋
            tsaccrad = 0
            return _generatecmd(inputs, CmdCode.PARAMED_WATERING.value, tsaccrad)

    return _generatecmd(inputs, None, tsaccrad, lastsupply)

if __name__ == '__main__':
    pass
