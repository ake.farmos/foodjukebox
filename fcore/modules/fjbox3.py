#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from past.utils import old_div
import time
from datetime import datetime

import sys
sys.path.insert(0, "..")
from mblock import CmdCode
from result import RetCode
from variable import Variable

import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.misc
import scipy.ndimage
import sklearn.metrics

import scipy.misc
from matplotlib.pyplot import imread


def thresholding(green_img):
    img = cv2.cvtColor(green_img, cv2.COLOR_BGR2GRAY)
    ret, thr2 = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    blur = cv2.GaussianBlur(img, (5, 5), 0)
    ret, thr3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    
    return thr3
    

def green_extract(image_dir, NO_plant, planttype):
    img = cv2.imread(image_dir)
    
    norm_img = np.zeros((2464,3280))
    final_img = cv2.normalize(img,  norm_img, 0, 255, cv2.NORM_MINMAX)
    hsv = cv2.cvtColor(final_img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    cv2.imwrite('pic_normalized.jpg', final_img)

    if 7001 <= planttype <= 7004:  #청경채 
        mask = cv2.inRange(hsv, (30, 40, 40), (80, 200, 200))
    elif 6001 <= planttype <= 6007:  #상추
        mask = cv2.inRange(hsv, (0, 80, 40), (180, 166, 250))
    
    #erosion - dilation
    kernel = np.ones((20,20),np.uint8)
    erosion = cv2.erode(mask,kernel,iterations = 1)
    kernel = np.ones((25,25),np.uint8)
    dilation = cv2.dilate(erosion,kernel,iterations = 1)
    mask=dilation

    imask = mask>0
    green_img = np.zeros_like(final_img, np.uint8)
    green_img[imask] = img[imask]
    
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()

    plt.imshow(mask)
    predicted= thresholding(green_img)
    roi_all=predicted
    n_white_pix_all = np.sum(roi_all == 255)
    #a=[n_white_pix1,n_white_pix2,n_white_pix3,n_white_pix4,n_white_pix5,n_white_pix_all]
    a_all=round(n_white_pix_all,2)
    a=a_all/NO_plant
    
    #cm^2 to weight model
    one_pixel_area=0.0004656964656964657
    area=a*one_pixel_area
    
    if 7001 <= planttype <= 7004:  #청경채 생중량 예측 모델 
        weight= 2.05975e-11*a*a+2.03009e-5*a
    elif 6001 <= planttype <= 6007:  #상추 생중량 예측 모델
        weight= 4.03007e-9*a*a - + 7.16014e-5*a
    
    #중량이 음수이면 0으로 치환
    if weight <=0:
        weight=0
    
    print('Canopy Area of average one plant:cm2')
    print(round(area,2))
    print('Predicted weight of average one plant:gram')
    
    print(round(weight,2))
    print('Pixel count of average one plant')
    print(round(a,0))
    
    return a, area, weight, green_img
          
def hsv_ave(weight, green_img):
    img_HSV = cv2.cvtColor(green_img, cv2.COLOR_RGB2HSV)

    count=0
    img_H=0
    img_S=0
    img_V=0
    #resize 10분의 1
    img_HSV=cv2.resize(img_HSV, dsize=(0, 0), fx=0.1, fy=0.1, interpolation=cv2.INTER_LINEAR) 
    len=img_HSV.shape
    
    if np.unique(img_HSV).shape[0]>1:
        for i in range(0,len[0]):
            for j in range(0,len[1]):
                if sum(img_HSV[i,j])>0:
                    img_H=img_H+img_HSV[i,j][0]
                    img_S=img_S+img_HSV[i,j][1]
                    img_V=img_V+img_HSV[i,j][2]
                    count=count+1

        img_H_ave=img_H/count
        img_S_ave=img_S/count
        img_V_ave=img_V/count
        HSV_ave=[img_H_ave,img_S_ave,img_V_ave]
    else:
        print('no plant detected')
        HSV_ave=[None, None, None]

    return HSV_ave

def greenex(inputs, pvalues, dbcur):
    """
    inputs: #cam.img, #cam.stat #planttype #numofplant
    outputs : #leftup, #rightup, #center, #leftdown, #rightdown
    """
    camimg = inputs["#cam.img"].getvalue()
    if camimg is None or 'path' not in camimg:
        return (RetCode.PARAM_ERROR, [])

    num = inputs["#numofplant"].getvalue()
    ptype = inputs["#planttype"].getvalue()

    a, area, weight, gimg = green_extract(camimg["path"], num, ptype)
    hsv = hsv_ave(weight, gimg)
    ret = list(map(float, [area, weight] + hsv))

    return (RetCode.OK, ret)

if __name__ == '__main__':
    import sys

    if len(sys.argv) != 4:
        print("usage: python3 fjbox3.py image_file_path crop_number")
        sys.exit(2)

    a, area, weight, gimg = green_extract(sys.argv[1], int(sys.argv[2]), int(sys.argv[3]))
    hsv = hsv_ave(weight,gimg)
    #h, s, v = rgb_ave(green_img)

    print (area, weight, hsv)
