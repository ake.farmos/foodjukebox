#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#

from __future__ import division
from __future__ import print_function
from builtins import str
from builtins import range
from past.utils import old_div
from builtins import object

import json
import pymysql
import psutil
import time
import socket
import traceback
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from datetime import datetime, timedelta

import util
import processors
from code import VarCode, RetCode
from result import ProcResult
from mblock import Request, CmdCode, Notice, NotiCode
from variable import Variable

class Manager(object):
    def __init__(self, option, logger):
        self._conn = None
        self._cur = None
        self._logger = logger
        self._option = option

    def updatedb(self, conn, cur):
        self._conn = conn
        self._cur = cur

class DataManager(Manager):
    INTDATA = 0
    DEVDATA = 1
    USRDATA = 2
    RULDATA = 3
    EXTDATA = 4

    DEVDATABASE = 10000000

    PATH = "../backup/image/"

    def __init__(self, option, logger):
        self._data = {}
        super(DataManager, self).__init__(option, logger)

    def loaduuid(self):
        query = "select uuid from gate_info"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            return row['uuid']

    def loaddata(self):
        query = "select * from current_observations"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            if self.isimgdata(row['data_id']):
                continue
            if row['data_id'] not in self._data:
                v = Variable()
            else:
                v = self._data[row['data_id']]

            v.setfromdb(row)
            self._data[row['data_id']] = v

    def updatedata(self, dataid, value):
        if dataid in self._data:
            self._data[dataid].setvalue(value)
        else:
            self._logger.info("Set a new data : " + str(dataid) + " " + str(value))
            self._data[dataid] = Variable(value, dataid)

    def writedata(self):
        uquery = "update current_observations set nvalue = %s, obs_time = %s, modified_time = %s where data_id = %s"
        iquery = "insert into observations(data_id, obs_time, nvalue) values (%s, %s, %s)"
        for dataid, variable in self._data.items():
            if variable.isupdated():
                print("writedata", dataid, variable)
                try:
                    self._cur.execute(uquery, [variable.getlastestvalue(), variable.getobserved(), variable.getmodified(), dataid])
                    if self.isprocresult(dataid) == False:
                        self._cur.execute(iquery, [dataid, variable.getobserved(), variable.getlastestvalue()])
                    variable.applied()
                except Exception as ex:
                    self._logger.warn("Fail to write data : " + str([dataid, variable]) + str(ex))
        self._conn.commit()

    def isimgdata(self, dataid):
        if self.getdatagroup(dataid) == DataManager.DEVDATA and dataid % 100 == 10:
            return True
        return False

    def reloadimgdata(self, dataid, count):
        query = "select data_id, obs_time, path, meta from gallery where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])

        rows = self._cur.fetchall()
        for row in rows:
            meta = '{}' if row['meta'] is None else row['meta']
            row['nvalue'] = {'path' : DataManager.PATH + row['path'], 'meta' : json.loads(meta)}

        if self._data[dataid].setimagesfromdb(rows) is False:
            self._logger.warn("Fail to relod previous images : " + str(dataid) + "," + str(count))

    def reloaddata(self, dataid, count):
        query = "select * from observations where data_id = %s order by obs_time desc limit %s"
        self._cur.execute(query, [dataid, count])
        if self._data[dataid].setpreviousfromdb(self._cur.fetchall()) is False:
            self._logger.warn("Fail to relod previous data : " + str(dataid) + "," + str(count))

    def getdata(self, dataid, count=1):
        if dataid in self._data:
            var = self._data[dataid]
            if var.getsize() >= count:
                return var

            # count is bigger than 1
            if self.isimgdata(dataid):
                self.reloadimgdata(dataid, count)
            else:
                print ("reload", dataid, count)
                self.reloaddata(dataid, count)
            return self._data[dataid]

        if self.isimgdata(dataid):
            self._data[dataid] = Variable(vid=dataid, vcode=VarCode.IMG)
            self.reloadimgdata(dataid, count)

        self._logger.warn("Unknown data : " + str(dataid))
        return None

    def getdatagroup(self, dataid):
        return int(dataid / DataManager.DEVDATABASE)

    def getdataidfordev(self, devid, outcode):
        return DataManager.DEVDATABASE + devid * 100 + outcode

    def isprocresult(self, dataid):
        if DataManager.RULDATA == self.getdatagroup(dataid) and dataid % 100 == 0:
            return True
        else:
            return False

    def isnewdevstatus(self, devid, tm):
        dataid = self.getdataidfordev(devid, 0)
        datum = self._data[dataid]
        if tm > datum.getobserved():
            self._logger.warn("data_id(" + str(dataid) + ") is old.")
            return False
        return True

class TimeSpanManager(Manager):
    _LONGITUDE = 127.0541365
    _LATITUDE = 37.27593497

    def __init__(self, option, logger):
        self._timespans = {}
        self._day = time.localtime(time.time()).tm_yday
        super(TimeSpanManager, self).__init__(option, logger)

    def loadcoordinates(self):
        query = "select data_id, nvalue from current_observations where data_id in (1, 2)"
        self._cur.execute(query, [])
        ret = [TimeSpanManager._LATITUDE, TimeSpanManager._LONGITUDE]
        for row in self._cur.fetchall():
            if row['data_id'] == 1:
                ret[0] = row['nvalue']
            if row['data_id'] == 2:
                ret[1] = row['nvalue']
        return ret

    def getsuntime(self):
        coord = self.loadcoordinates()
        return util.SunTime(coord[1], coord[0])

    def updatesuntimespan(self, ts):
        if ts["configuration"]["timing"] == "sun":
            suntime = self.getsuntime()
            sunrise = suntime.getsunrise()
            sunset = suntime.getsunset()
            ts["times"] = {"#sunrise" : Variable(sunrise), "#sunset" : Variable(sunset)}
            for part in ts["parts"]:
                if part["type"] == "rise+":
                    part["to"] = sunrise + int(part["value"])
                elif part["type"] == "rise-":
                    part["to"] = sunrise - int(part["value"])
                elif part["type"] == "rise":
                    part["to"] = sunrise
                elif part["type"] == "set+":
                    part["to"] = sunset + int(part["value"])
                elif part["type"] == "set-":
                    part["to"] = sunset - int(part["value"])
                elif part["type"] == "set":
                    part["to"] = sunset
                else:
                    part["to"] = int(part["to"])
                print("update suntimespan", part)
        return ts

    def updatetimespans(self):
        if self._day == time.localtime(time.time()).tm_yday:
            return 

        # It needs to recalculate once a day.
        self._day = time.localtime(time.time()).tm_yday
        for tmp, ts in self._timespans.items():
            self.updatesuntimespan(ts)

    def addtimespan(self, tsid, fldid, timespan):
        print("addtimespan", tsid, fldid, timespan)
        self._timespans[(tsid, fldid)] = timespan
        if timespan["configuration"]["timing"] == "sun":
            self.updatesuntimespan(timespan)
        elif timespan["configuration"]["timing"] == "fixed":
            for p in timespan["parts"]:
                p["to"] = int(p["to"])

        else: 
            self._logger.warn("Unknown timing : " + str([tsid, fldid, timespan["configuration"]["timing"]]))

        timespan["tvar"] = {"tsidx": Variable(0)}
        for th in timespan["threshold"]:
            timespan["tvar"]["#" + th["id"]] = Variable() 

    def loadtimespan(self, updated):
        query = "select * from core_timespan where unix_timestamp(updated) >= %s and field_id in (select id from fields where deleted = 0)"
        self._cur.execute(query, [updated])
        for row in self._cur.fetchall():
            if row['field_id'] < 0:
                continue

            self.addtimespan(row['id'], row['field_id'], json.loads(row['timespan']))

    def getcurrentnsec(self):
        now = datetime.now()
        return (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()

    def getcurrentindex(self, tsid, fldid):
        """
            return : current timespan index, current epoch, times
        """
        nsec = self.getcurrentnsec()

        if tsid == 0:
            return (0, nsec, None)

        timespan = self._timespans[(tsid, fldid)]

        idx = -1
        for i in range(len(timespan["parts"])):
            if timespan["parts"][i]["to"] > nsec:
                print("found timespan index : ", i, timespan["parts"][i]["to"], nsec)
                idx = i
                break

        if idx < 0:
            self._logger.warn("Fail to get current timespan for tsid : " + str(tsid) + " " + str(nsec))
            return (None, None, None)

        if "times" in timespan:
            return (idx, nsec, timespan["times"])
        else:
            return (idx, nsec, None)

    def getthresholdvalue(self, part, thd, idx, current):
        opt = thd["timeoption"]
        if thd["linetype"] == "fixed":
            return opt[idx]["to"]

        # monotone, straight 는 같다고 취급한다. (작은 차이는 무시한다. :p)
        if idx == 0:
            return opt[idx]["to"]
        else:
            ratio  = (old_div((current * 1.0 - part[idx-1]["to"]), (part[idx]["to"] - part[idx-1]["to"])))
            return opt[idx-1]["to"] + (opt[idx]["to"] - opt[idx-1]["to"]) * ratio


    def getthresholds(self, tsid, fldid, idx, current):
        if tsid == 0:
            return {}

        timespan = self._timespans[(tsid, fldid)]
        ret = {}
        timespan["tvar"]["tsidx"].setvalue(idx)
        for thd in timespan["threshold"]:
            timespan["tvar"]["#" + thd["id"]].setvalue(self.getthresholdvalue(timespan["parts"], thd, idx, current))
        print("thresholds", timespan["tvar"])
        return timespan["tvar"]

class DeviceManager(Manager):
    def __init__(self, option, logger):
        self._devices = {}
        super(DeviceManager, self).__init__(option, logger)

    def loaddevices(self, updated):
        query = "select a.id, a.coupleid, a.gateid, a.deleted, b.id nodeid, a.control from devices a, (select * from devices where devindex is null) b where unix_timestamp(a.updated) > %s and a.nodeid = b.nodeid and a.deleted = 0"
        self._cur.execute(query, [updated])
        for row in self._cur.fetchall():
            if row["deleted"] == 1:
                if row["id"] in self._devices:
                    del self._devices[row["id"]]
            else:
                self._devices[row["id"]] = row
        print("devices ", self._devices)

    def getdevice(self, devid):
        print("getdevice", devid)
        return self._devices[devid]

    def getdevices(self):
        return self._devices.keys()

class InternalProcessor(processors.Processor):
    STATSMARGIN = 3 
    def __init__(self, logger):
        """
        Processor 의 Constructor
        :param logger: 로깅을 위한 로거.
        """
        self._logger = logger
        self._data = None
        self._dev = None
        self._timespan = None

    def setup(self, datamng, devmng, timespan):
        self._data = datamng
        self._dev = devmng
        self._timespan = timespan

    def evaluate(self, rule, proc, dbcur):
        """
        개별 processsor 를 평가하기위한 메소드
        :param rule: 룰
        :param proc: 실행할 프로세서
        :param dbcur: 디비 커서
        """
        if "devstatus" == proc["mod"]:
            return self.devstatus(rule, proc)
        elif "diskusage" == proc["mod"]:
            return self.diskusage(rule, proc)
        elif "datastats" == proc["mod"]:
            return self.datastatistics(rule, proc, dbcur)
        elif "isday" == proc["mod"]:
            return self.isday(rule, proc, dbcur)
        elif "devavail" == proc["mod"]:
            return self.devutil(rule, proc)
        return ProcResult(RetCode.OK, proc, [None])

    def isday(self, rule, proc, dbcur):
        suntime = self._timespan.getsuntime()
        sunrise = suntime.getsunrise()
        sunset = suntime.getsunset()
        nsec = self._timespan.getcurrentnsec()

        print ("isday", sunrise, sunset, nsec)

        if sunrise <= nsec <= sunset:
            return ProcResult(RetCode.OK, proc, [1])
        else:
            return ProcResult(RetCode.OK, proc, [0])

    def _makefromto(self, now, delta):
        _delta = timedelta(minutes = now.minute, seconds = now.second, microseconds = now.microsecond)
        totm = now - _delta
        fromtm = totm - delta
        return (fromtm, totm)

    def gettermfordatastats(self, term):
        now = datetime.now() 
        if term == 'hour':               #매 3분에 이전 시간대 통계를 계산
            if now.minute == InternalProcessor.STATSMARGIN:
                return self._makefromto(now, timedelta(hours = 1))

        elif term == 'daynnight':        #일출일몰 후 3분에 이전 시간대 통계를 계산
            suntime = self._timespan.getsuntime()
            sunrise = datetime.fromtimestamp(suntime.getsunrise())
            sunset = datetime.fromtimestamp(suntime.getsunset())
            nsec = self._timespan.getcurrentnsec()

            if 180 <= (now - sunrise).total_seconds() < 240:
                return (sunset - timedelta(days = 1), sunrise)
            elif 180 <= (now - sunset).total_seconds() < 240:
                return (sunrise, sunset)

        elif term == 'day':              #매일 0시 3분에 이전 시간대 통계를 계산
            if now.hour == 0 and now.minute == InternalProcessor.STATSMARGIN:
                return self._makefromto(now, timedelta(days = 1))

        elif term == 'week':             #매월요일 0시 3분에 이전 시간대 통계를 계산
            if now.hour == 0 and now.minute == InternalProcessor.STATSMARGIN and now.weekday() == 0:
                return self._makefromto(now, timedelta(days = 7))

        return (None, None)

    def datastatistics(self, rule, proc, dbcur):
        term = rule["inputs"]["#term"].getvalue()
        fromtm, totm = self.gettermfordatastats(term)

        if fromtm is None or totm is None:
            self._logger.info("It's not a proper time to make data statistics.")
            return ProcResult(RetCode.OK, None, None)

        funcs = rule["inputs"]["#funcs"].getvalue()
        dataid = rule["inputs"]["#data"].getid()

        fns = []
        for fn in funcs.split(","):
            fns.append (fn + '(nvalue) as ' + fn)

        query = "select " + ",".join(fns) + " from observations where data_id = %s and obs_time between %s and %s"
        print ("query", query, [dataid, str(fromtm), str(totm)])

        try:
            dbcur.execute(query, [dataid, fromtm, totm])
            row = dbcur.fetchone()
            ret = []
            for out in proc["outputs"]:
                ret.append (row[out[1:]])
            print ("ret", ret)
            return ProcResult(RetCode.OK, proc, ret)

        except Exception as ex:
            self._logger.warn ("Fail to get data statistics : " + str(ex))
            self._logger.warn(traceback.format_exc())
            return ProcResult(RetCode.PROCESSOR_EXCEPTION, None, None)

    def devstatus(self, rule, proc):
        if self._data and self._dev:
            cnt = 0
            tm = datetime.now() - timedelta(seconds=rule["inputs"]["#sec"].getvalue())
            for devid in self._dev.getdevices():
                if self._data.isnewdevstatus(devid, tm) == False:
                    cnt = cnt + 1

            if rule["inputs"]["#min"].getvalue() <= cnt:
                alertid = rule["inputs"]["#id"].getvalue()
                msg = rule["inputs"]["#msg"].getvalue() + " (호스트명: " + socket.gethostname() + ", 이상장비수 : " + str(cnt) + ")"
                return ProcResult(RetCode.OK, proc, [CmdCode.ALERT.value, alertid, msg, rule["inputs"]["#receiver"].getvalue()])
        return ProcResult(RetCode.OK, None, None)

    def devutil(self, rule, proc):
        if self._data and self._dev:
            tm = datetime.now()
            for devid in self._dev.getdevices():
                dataid = self._data.getdataidfordev(devid, 0) # 0은 장비 상태 - 데이터아이디관리체계 참조
                status = self._data.getdata(dataid)

                dataid = self._data.getdataidfordev(devid, 11) # 11은 장비 가동률 - 데이터아이디관리체계 참조
                utilization = self._data.getdata(dataid)

                if utilization is None or status is None:
                    self._logger.info("Device[" + str(devid) + "] does not have utilization or status.")
                    continue

                value = utilization.getvalue()
                if tm - status.getobserved() > timedelta(minutes=2):
                    value = 0 if value < 1 else value - 1
                    self._logger.info("Device[" + str(devid) + "_" + str(dataid) + "] is not updated yet. " + str(utilization.getobserved()))
                else:
                    value = 100 if value >= 99.9 else value + 0.3
                    self._logger.info("Device[" + str(devid) + "_" + str(dataid) + "] is updated. " + str(utilization.getobserved()))
                self._data.updatedata(dataid, value)
        return ProcResult(RetCode.OK, None, None)

    def diskusage(self, rule, proc):
        disk = psutil.disk_usage(rule["inputs"]["#path"].getvalue())
        free = disk.free * 100.0 / disk.total
        print ("free disk", free)
        if rule["inputs"]["#min"].getvalue() > free:
            alertid = rule["inputs"]["#id"].getvalue()
            msg = rule["inputs"]["#msg"].getvalue() + " (호스트명: " + socket.gethostname() + ", 남은공간 : " + format(free, ".2f") + "%)"
            return ProcResult(RetCode.OK, proc, [CmdCode.ALERT.value, alertid, msg, rule["inputs"]["#receiver"].getvalue()])
        return ProcResult(RetCode.OK, None, None)

class RuleManager(Manager):
    _MAX_PRIORITY = 7
    _DEF_PRIORITY = 6
    _DEF_PERIOD = 60

    def __init__(self, option, logger, sched=False, rid=-1):
        self._data = DataManager(option, logger)
        self._timespan = TimeSpanManager(option, logger)
        self._devices = DeviceManager(option, logger)
        self._lastupdated = 0
        self._sched = sched
        self._rid = rid

        self._rules = []
        for _ in range(RuleManager._MAX_PRIORITY):
            self._rules.append ({})

        self._procs = {
            "int" : InternalProcessor(logger),
            "mod" : processors.ModuleProcessor(logger),
            "ext" : processors.ExternalProcessor(logger),
            "eq" : processors.EquationProcessor(logger),
            "sql" : processors.SqlProcessor(logger),
        }
        self._procs["int"].setup(self._data, self._devices, self._timespan)

        super(RuleManager, self).__init__(option, logger)

    def updatedb(self, conn, cur):
        self._conn = conn
        self._cur = cur
        self._timespan.updatedb(conn, cur)
        self._data.updatedb(conn, cur)
        self._devices.updatedb(conn, cur)
        self._uuid = self._data.loaduuid()

    def process(self):
        updated = self._lastupdated
        self._devices.loaddevices(updated)
        self._data.loaddata()
        self._timespan.updatetimespans()
        self.loadappliedrules(updated)
        self.processrules()
        self._data.writedata()
        self._lastupdated = int(time.time())

    def getpriorityfromruleset(self, ruleid):
        # find
        for idx in range(RuleManager._MAX_PRIORITY):
            if ruleid in self._rules[idx]:
                return idx
        return -1

    def updateruleset(self, rule):
        ruleid = rule["id"]

        found = self.getpriorityfromruleset(ruleid)
        if found >= 0: # should be deleted from ruleset
            del self._rules[found][ruleid]

        if rule["used"] > 0 and rule["deleted"] == 0:
            priority = rule["inputs"]["priority"].getvalue()
            print ("rule priority", rule["name"], priority)
            self._rules[priority][rule['id']] = rule

    def loadappliedrules(self, updated):
        self._timespan.loadtimespan(updated)

        print("loadappliedrules", updated)
        if self._rid >= 0:
            query = "select * from core_rule_applied where id = %s"
            self._cur.execute(query, [self._rid])
        else:
            if self._sched:
                query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and sched <> 0"
            else:
                query = "select * from core_rule_applied where unix_timestamp(updated) >= %s and sched = 0"

            try:
                self._cur.execute(query, [updated])
            except:    # 임시 코드 : sched 가 없는 경우를 위한 하위호환성
                query = "select * from core_rule_applied where unix_timestamp(updated) >= %s"
                self._cur.execute(query, [updated])

        for row in self._cur.fetchall():
            try:
                # temporarily changes for test
                if self._rid == row["id"]:
                    row["used"] = 1
                    row["deleted"] = 0
      
                rule = self.loadrule(row)
                self.updateruleset(rule)
            except Exception as ex:
                self._logger.warn ("Fail to load a rule : " + str(ex) + " " + str(row))
                self._logger.warn(traceback.format_exc())

    def loadrule(self, row):
        if row["used"] == 0 or row["deleted"] > 0:
            return {
                "id" : row["id"],
                "name" : row["name"],
                "used" : row["used"],
                "deleted" : row["deleted"]
            }

        print ("loadrule", row["id"], row["name"])

        if self._sched and "sched" in row:
            sched = row["sched"]
        else:
            sched = False

        conf = json.loads(row["configurations"])
        print ("configurations", row["configurations"])

        dataids = {}
        kv = {}
        for datum in conf["basic"]:
            if "type" in datum:
                if datum["type"] == "table":
                    for k, v in datum["values"].items():
                        kv[datum["key"] + "." + k] = Variable(v, vcode=VarCode.LIST)
                elif datum["type"] in ("ts_float", "ts_time", "ts_check"):
                    kv[datum["key"]] = Variable(datum["value"], vcode=VarCode.TSPAN)
                elif datum["type"] == "string":
                    kv[datum["key"]] = Variable(datum["value"]) #, vcode=VarCode.STRING)  # STRING is same with NORM
                else:
                    kv[datum["key"]] = Variable(datum["value"])
            else:
                kv[datum["key"]] = Variable(datum["value"])
                
        for datum in conf["advanced"]:
            kv[datum["key"]] = Variable(datum["value"])

        if "priority" not in kv:
            kv["priority"] = Variable(RuleManager._DEF_PRIORITY)
        else:
            priority = kv["priority"].getvalue()
            if priority < 0: priority = 0
            if priority > RuleManager._MAX_PRIORITY: priority = RuleManager._DEF_PRIORITY

        if "period" not in kv:
            kv["period"] = Variable(RuleManager._DEF_PERIOD)

        if "multicond" in conf: # 기본 작동조건 - 운영모드
            multicond = conf["multicond"]
            multicond["data"] = self._data.getdata(conf["multicond"]["dataid"])
        else:
            multicond = None
            
            
        if row["inputs"] is not None:
            print ("inputs", row["inputs"])
            inputs = json.loads(row["inputs"])
            for datum in inputs:
                if "count" in datum:
                    kv[datum["key"]] = self._data.getdata(datum["dataid"], datum["count"])
                else:
                    kv[datum["key"]] = self._data.getdata(datum["dataid"])
                dataids[datum["key"]] = datum["dataid"]

        outputs = json.loads(row["outputs"])
        print ("outputs", row["outputs"])
        for (did, key) in self.getoutputdata(row["id"], outputs):
            #self._data.updatedata(did, 0) # temporary
            kv[key] = self._data.getdata(did)
            dataids[key] = did

        print("rule inputs", kv)

        ctrls = json.loads(row["controllers"])
        for proc in ctrls['processors']:
            proc["pvalues"] = None

        return {
            "id" : row["id"],
            "name" : row["name"],
            "field" : row["field_id"],
            "used" : row["used"],
            "sched" : sched,
            "deleted" : row["deleted"],
            "timespan" : conf["timespan"],
            "constraints" : json.loads(row["constraints"]),
            "controllers" : ctrls,
            "multicond" : multicond,
            "inputs" : kv,
            "dataids" : dataids,
            "outputs" : outputs,
            "executed" : 0
        }

    def setinputdata(self, rule):
        for key, did in rule["dataids"].items():
            rule["inputs"][key] = self._data.getdata(did)
        
    def processrules(self):
        ret = []
        now = int(time.time())
        reqs = {}                    # {devid : (dev, req)}
        datadic = {}
        print("now", now, self._rules)
        for rules in self._rules:
            for rid, rule in rules.items():
                try:
                    print("rule", rule["name"], rule["executed"])
                    if rule["executed"] + rule["inputs"]["period"].getvalue() < now:
                        self._logger.info(rule["name"] + " is executing.")
                        self.setinputdata(rule)
                        # rule result is ( "results of processes", "requests per device")
                        (ruleret, newreqs, newdata) = self.executerule(rule)

                        if newreqs:
                            self._logger.info("Requests for devices " + str(newreqs.keys()) + " are made.")
                            newreqs.update(reqs) # 우선순위상 먼저 만들어진 Request 를 살리고, 새로 만들어진 Request는 무시함.
                            reqs = newreqs
                            self._logger.info("Requests for devices " + str(reqs.keys()) + " are ready.")

                        if newdata:
                            datadic.update(newdata) # 데이터는 겹쳐서 만들지 않아야 함.
                            self._logger.info("Data for rules " + str(datadic.keys()) + " are ready.")

                        ret.append({"ruleid": rid, "result": ruleret})
                        rule["executed"] = now
                        self._logger.info(rule["name"] + " is executed. The result is " + str(ruleret))
                except Exception as ex:
                    self._logger.warn ("Fail to execute a rule : " + str(rid) + " " + str(ex))
                    self._logger.warn(traceback.format_exc())
                    self._logger.info ("Rule info : "  + str(rule))
                    ret.append({"ruleid": rid, "result": {"code" : RetCode.UNKNOWN_ERROR}})

        # sending request
        for devid, req in reqs.items():
            dev = self._devices.getdevice(devid)
            self.sendrequest(dev, req)
        self.sendnotice(datadic)
        return ret

    def finddevid(self, rule, target):
        for dev in rule["constraints"]["devices"]:
            print("dev", dev)
            if "outputs" in dev and dev["outputs"] == target:
                return dev["deviceid"]

    def rearrangeresult(self, ret):
        newret = {}
        for tmp in ret[1:]:
            if tmp.getretcode() == RetCode.OK:
                for key, value in tmp.getoutputs().items():
                    print("rearrange result ", key, value)
                    newret[key] = value
        return newret

    def getdataid(self, ruleid, idx, outcode):
        return 30000000 + ruleid * 10000 + idx * 100 + outcode

    def getoutputdata(self, ruleid, outputs):
        ret = []
        if "data" not in outputs:
            return ret
        for out in outputs["data"]: 
            dataid = self.getdataid(ruleid, 0, out["outcode"])
            ret.append([dataid, out["outputs"]])
        return ret

    def processdata(self, rule, ret, newret):
        for idx in range(len(ret)):
            dataid = self.getdataid(rule["id"], idx, 0)
            self._data.updatedata(dataid, ret[idx].getretcode().value)

        if "data" not in rule["outputs"]:
            return {}

        datadic = {}
        for out in rule["outputs"]["data"]: 
            dataid = self.getdataid(rule["id"], 0, out["outcode"])
            if out["outputs"] in newret:
                var = newret[out["outputs"]]
                print("data", dataid, out["outputs"], var.getvalue())
                datadic[dataid] = var.getvalue()
                self._data.updatedata(dataid, var.getvalue())
            else:
                self._logger.warn("Fail to find output : " + str(out["outputs"]))
        return datadic

    def getoutputdevdata(self, ruleid, outputs):
        ret = []
        if "dev" not in outputs:
            return ret

        for out in outputs["dev"]: 
            devid = self.finddevid(rule, out["targets"])
            dataid = self._data.getdataidfordev(devid, out["outcode"])
            ret.append([dataid, out["outputs"]])
        return ret

    def processdevdata(self, rule, ret, newret):
        if "dev" not in rule["outputs"]:
            return {}

        datadic = {}
        for out in rule["outputs"]["dev"]: 
            devid = self.finddevid(rule, out["targets"])
            dataid = self._data.getdataidfordev(devid, out["outcode"])
            if out["outputs"] in newret:
                var = newret[out["outputs"]]
                print("data", dataid, out["outputs"], var.getvalue())
                datadic[dataid] = var.getvalue()
                self._data.updatedata(dataid, var.getvalue())
            else:
                self._logger.info("Fail to find output : " + str(out["outputs"]))
        return datadic

    def makerequest(self, dev, cmd, params):
        if dev and "nodeid" in dev and dev["nodeid"]:
            req = Request(dev["nodeid"]) 
            req.setcommand(dev["id"], cmd, params)
            return req
        else:
            self._logger.warn("Not enough infomation of a device for making a request .", dev)
            return None

    def sendrequest(self, dev, req):
        topic = "/".join(["cvtgate", dev["coupleid"], str(dev["gateid"]), "req", str(dev["nodeid"])])
        if dev["control"] in [0, 2]: # normal or rule
            ret = publish.single(topic, payload=req.stringify(), qos=2, hostname=self._option["mqtt"]["host"])
            self._logger.info("Sent a request to " + topic + " : " + req.stringify())
        else:
            self._logger.info("A request is not sent to " + topic + " because of control [" + str(dev["control"]) +"] : " + req.stringify())

    def sendnotice(self, data):
        if len(data) == 0:
            return
        topic = "/".join(["cvtgate", self._uuid, '', "noti", ''])
        noti = Notice(None, NotiCode.FCORE_DATA)
        for key, value in data.items():
            noti.setkeyvalue(key, value)
        ret = publish.single(topic, payload=noti.stringify(), qos=2, hostname=self._option["mqtt"]["host"])
        self._logger.info("Sent a notice to " + topic + " : " + noti.stringify())

    def processrequest(self, rule, ret, newret):
        """
            return : dict(key is device id, value is request)
        """
        if "req" not in rule["outputs"]:
            return {} 

        reqs = {}
        for out in rule["outputs"]["req"]:
            print("req1", out)
            if out["cmd"] not in newret:
                self._logger.info("A command is not found. Therefore, it would not make a request. " + str(out))
                continue

            cmd = newret[out["cmd"]].getvalue()
            if cmd is None:
                self._logger.info("A command is None. Therefore, it would not make a request. " + str(out))
                continue

            params = {}
            if "params" in out and len(out["params"]) > 0:
                for idx in range(len(out["params"])):
                    param = out["params"][idx]
                    if "pnames" in out:
                        pname = out["pnames"][idx]
                    else:
                        pname = param[1:]

                    if isinstance(param, (int, float)):
                        params[pname] = param

                    elif param in newret:
                        params[pname] = newret[param].getvalue()   # remove '#'

                    elif param in rule["inputs"]:
                        params[pname] = rule["inputs"][param].getvalue()   # remove '#'

                    else:
                        print("param (", param, ") is not found. Do not make a request.")
                        break
                        
                if idx < len(out["params"]) -1:
                    continue
            print("req2")

            for target in out["targets"]:
                devid = self.finddevid(rule, target)
                dev = self._devices.getdevice(devid)
                req = self.makerequest(dev, cmd, params)
                if req:
                    reqs[devid] = req

        return reqs

    def checktrigger(self, rule):
        ctrl = rule["controllers"]
        if 'trigger' not in ctrl:
            ret = [ProcResult(RetCode.OK)]
        else:
            try:
                procret = self._procs[ctrl["trigger"]["type"]].evaluate(rule, ctrl["trigger"], self._cur)
                if procret.getretcode() != RetCode.OK:
                    self._logger.info(rule["name"] + " trigger is failed : " + str(procret.getretcode()))
                    return [procret]

                if procret.getvalues()[0] == 0:
                    procret.setretcode(RetCode.TRIGGER_NOT_ACTIVATED)
                    self._logger.info(rule["name"] + " trigger is not activated.")
                    return [procret]             # trigger is not activated

            except Exception as ex:
                self._logger.warn(rule["name"] + " trigger is failed to execute. " + str(ex))
                self._logger.warn(traceback.format_exc())
                return [ProcResult(RetCode.PROCESSOR_EXCEPTION)] # trigger is failed to execute

            ret = [procret]
        return ret

    def executeprocess(self, rule, ret):
        """
        return
           { "results of processes", "requests per device", "data dictionary"}
        """
        ctrl = rule["controllers"]

        for idx in range(len(ctrl["processors"])):
            try:
                proc = ctrl["processors"][idx]
                procret = self._procs[proc["type"]].evaluate(rule, proc, self._cur)
                if procret.getretcode() == RetCode.OK:
                    rule["inputs"].update(procret.getoutputs())

            except Exception as ex:
                self._logger.warn("Processors (" + str(idx) + ") is failed to execute. " + str(ex))
                self._logger.warn(traceback.format_exc())
                procret = ProcResult(RetCode.PROCESSOR_EXCEPTION) 

            ret.append(procret)

        # make output
        newret = self.rearrangeresult(ret)
        print(ret, newret)

        datadic = {}
        datadic.update(self.processdata(rule, ret, newret))
        datadic.update(self.processdevdata(rule, ret, newret))
        reqs = self.processrequest(rule, ret, newret)

        return (newret, reqs, datadic)

    def ismultiruleactiviated(self, multicond):
        if multicond is None:
            return True

        data = multicond["data"].getvalue()
        cond = multicond["cond"]

        if cond["type"] == "range":
            if "from" in cond and cond["from"] > data:
                return False
            if "to" in cond and cond["to"] <= data:
                return False
            return True
        elif cond["type"] == "equal":
            if "value" in cond and cond["value"] == data:
                return True
            return False

        self._logger.warn("Unknown multirule condition. It was evaluated as not activated : " + str(cond))
        return False

    def executerule(self, rule):
        """
        return
           { "results of processes", "requests per device"}
        """
        # 기본작동조건 - 다중작동규칙
        if self.ismultiruleactiviated(rule["multicond"]) is False:
            return (ProcResult(RetCode.MULTIRULE_NOT_ACTIVATED), None, {})
                
        (tsidx, nsec, times) = self._timespan.getcurrentindex(rule["timespan"]["id"], rule["field"])
        if tsidx is None:
            self._logger.warn(rule["name"] + " is failed to get timespan.")
            return (ProcResult(RetCode.WRONG_TIMESPAN), None, {})

        if "used" in rule["timespan"]:
            used = rule["timespan"]["used"]
        else:
            used = [True]

        if used[tsidx] is False:
            self._logger.info(rule["name"] + " is not used now. (" + str(tsidx) + "," + str(used) + ")")
            return (ProcResult(RetCode.NOT_USED), None, {})

        rule["tsidx"] = tsidx
        rule["inputs"]["#nsec"] = Variable(nsec)
        if times:
            rule["inputs"].update(times)

        thresholds = self._timespan.getthresholds(rule["timespan"]["id"], rule["field"], tsidx, nsec)
        rule["inputs"].update(thresholds)

        self._logger.debug("rule : " + str(rule))

        procrets = self.checktrigger(rule)

        if procrets[0].getretcode() == RetCode.OK:
            ruleret = self.executeprocess(rule, procrets)
        else:
            ruleret = (self.rearrangeresult(procrets), None, None)
            self.processdata(rule, procrets, ruleret[0])
        # {devid : (dev, req)}
        return ruleret

if __name__ == '__main__':
    ts = TimeSpanManager({}, util.getdefaultlogger())
    ts.addtimespan(1, 1, [{"to" : 10000}])
    assert ts.getcurrentts((0, 0)) == 0, "timespan is not matched #1"
    now = datetime.now()
    nsec = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
    ans = 0 if nsec < 10000 else -1
    assert ts.getcurrentts((1, 1)) == ans, "timespan is not matched #2" 

    rulemng = RuleManager({}, util.getdefaultlogger())
