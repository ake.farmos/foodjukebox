#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

from __future__ import print_function
from builtins import map
from builtins import object
import time
import math
import json
import subprocess32
import importlib
from datetime import datetime
from simpleeval import simple_eval

import util 
from result import RetCode, ProcResult
from variable import Variable

class Processor(object):
    """
    컨트롤러가 가지고있는 작업단위인 프로세서를 처리하기 위한 클래스

    프로세서간 데이터 활용을 위해서는 outputs 로 전달이 되어야 한다.
    values 로 전달이 되는 임시값은 단일 프로세서에서만 의미를 갖는다.
    """

    def __init__(self, logger):
        """
        Processor 의 Constructor
        :param logger: 로깅을 위한 로거.
        """
        self._logger = logger

    def evaluate(self, rule, proc):
        """
        개별 processsor 를 평가하기위한 메소드
        :param rule: 룰 
        :param proc: 실행할 프로세서
        """
        pass

class ModuleProcessor(Processor):
    """
    기 구현된 모듈을 사용하여 평가하는 프로세서
    외부 모듈은 (RetCode, [result1, result2, ...]) 의 형식으로 리턴해야한다.
    result1, result2... 는 모듈을 실행한 결과물이고, 이는 순서대로 outputs에 매칭된다. 
    outputs에 매칭되지 않는 값들은 룰에서 사용되지는 않지만 모듈의 다음 계산을 위해 사용될 임시값으로 간주한다.
    """
    def __init__(self, logger):
        super(ModuleProcessor, self).__init__(logger)

    def evaluate(self, rule, proc, dbcur):
        (modname, funcname) = proc["mod"].split(".")

        mod = importlib.import_module("modules." + modname)
        func = getattr(mod, funcname)

        print (rule["inputs"])
        (retcode, values) = func(rule["inputs"], proc["pvalues"], dbcur)
        return ProcResult(retcode, proc, values)

class EquationProcessor(Processor):
    """
    단순한 수식을 평가하는 프로세서
    해킹 방지를 위해서 simple_eval을 사용함
    """
    def __init__(self, logger):
        super(EquationProcessor, self).__init__(logger)
        self._tmpdata = None

    def getschedindex(self, times):
        now = datetime.now()
        nsec = (now - now.replace(hour=0, minute=0, second=0, microsecond=0)).total_seconds()
        print ("current nsec", nsec)
        for idx in range(len(times)):
            if 0 <= nsec - times[idx] < 60:
                print ("sched index", idx)
                return idx
        return None 

    def generatevariables(self, inputs, tsidx):
        names = {}
        for key, val in inputs.items():
            if key[0] == '#':
                tmp = key[1:].split(".")
            else:
                tmp = key.split(".")

            n = names
            l = len(tmp)
            for i in range(l):
                term = tmp[i]
                if i == l - 1:
                    n[term] = val.getvalue(tsidx)
                elif term not in n:
                    n.update({term : {}})
                n = n[term]
            print ("genvar", key, names)
        return names

    def _evaluate(self, eq, names, func):
        try:
            return simple_eval(eq, names=names, functions=func)
        except Exception as ex:
            self._logger.warn ("Fail to evaluate : " + str(eq) + " : " + str(ex))
            return None

    def evaluate(self, rule, proc, dbcur):
        if rule["sched"]:
            # 스케쥴드 룰이라면 table.time 이 꼭 있어야 함.
            sidx = self.getschedindex(rule["inputs"]["#table.time"].getvalue())
            if sidx is not None:
                rule["inputs"]["sidx"] = Variable(sidx)
            else:
                return ProcResult(RetCode.NOT_PROPER_TIME, proc, [])

        names = self.generatevariables(rule["inputs"], rule["tsidx"])
        func = {"exp":math.exp, "log":math.log, "sin": math.sin, "cos": math.cos, "sqrt": math.sqrt, "tan":math.tan, "atan":math.atan}
        values = []
        if isinstance(proc["eq"], list):
            for eq in proc["eq"]:
                values.append (self._evaluate(eq, names, func))
        else:
            values.append (self._evaluate(proc["eq"], names, func))
        
        print(proc["eq"], "evaluated.", values)
        return ProcResult(RetCode.OK, proc, values)

#    def namehandler(self, node):
#        print("#" + node.id, self._tmpdata["#" + node.id].getvalue())
#        return self._tmpdata["#" + node.id].getvalue()

class ExternalProcessor(Processor): 
    """
    외부 프로세스를 실행하여 평가하는 프로세서
    외부 프로세스에 대한 최대 실행시간은 5초를 초과할 수 없다.
    외부 프로세스를 위해 이전 데이터를 전달하거나 하지는 않는다.
    """
    _TIMEOUT = 5
    def __init__(self, logger):
        super(ExternalProcessor, self).__init__(logger)

    def _makeargs(self, proc, data):
        args = [proc["cmd"]]
        for key in proc["args"]:
            if key in data:
                args.append(data[key].getvalue())
            else:
                args.append(key)
        return args

    def evaluate(self, rule, proc, dbcur):
        lines = subprocess32.check_output(self._makeargs(proc, rule["inputs"]), timeout=self._TIMEOUT, universal_newlines=True)
        values = list(map(float, lines.split("\n")))

        return ProcResult(RetCode.OK, proc, values)

class SqlProcessor(Processor):
    def __init__(self, logger):
        super(SqlProcessor, self).__init__(logger)

    def evaluate(self, rule, proc, dbcur):
        return ProcResult(RetCode.OK, proc, [])

if __name__ == '__main__':
    eqproc = EquationProcessor(util.getdefaultlogger())
    a = Variable(10)
    s = Variable(1)
    b = Variable(20)
    d = Variable([1, 2, 3, 4])
    print(eqproc.evaluate({"field_id" : 1, "inputs": {"#a.obs" : a, "#a.stat" : s, "#b.obs": b, "#d.d" : d}}, {"type" : "eq", "eq": "a.obs + b.obs + d.d[2]"}, None)) 

