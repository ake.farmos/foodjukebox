#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#

from enum import IntEnum

class RetCode(IntEnum):
    OK = 0
    NOT_USED = 1
    TRIGGER_NOT_ACTIVATED = 2
    PROCESSOR_EXCEPTION = 3
    UNKNOWN_ERROR = 4
    WRONG_TIMESPAN = 5
    ABNORMAL_DEVICE = 6
    PARAM_ERROR = 7
    NOT_PROPER_TIME = 8
    MULTIRULE_NOT_ACTIVATED = 9

class VarCode(IntEnum):
    NORM = 0    # Normal Variable size = 1
    LIST = 1    # List Variable size = 1
    DICT = 2    # Dict Variable size = 1
    HIST = 3    # Historical Variable size = n
    TSPAN = 4   # TimeSpan Variable size = 1
    IMG = 5     # Image Variable size = n
    STRING = 6  # String Variable size = 1. same with NORM
