# 변동사항 기록
변동사항을 역순으로 기록하여, 가장 최신의 결과물이 항상 위쪽에 보이도록 합니다.

## 1.0.2
### bug
 * 새로운 이미지분석 모듈의 결과물 오류 수정. 'numpy.float64' object 문제.

## 1.0.1

### feat
 * KIST 양액기 - 일사비례제어를 위한 작동규칙 추가 [링크](https://gitlab.com/farmos_private/inftech/-/issues/46)
 * 농진청 병해 - 주기지정 촬영을 위한 작동규칙 추가 [링크](https://gitlab.com/farmos_private/inftech/-/issues/96)

### bug
 * Variable 의 getdifferential 을 계산시 이전값과의 차이가 나오지 않는 버그 수정 [링크](https://gitlab.com/farmos_private/inftech/-/issues/97)
