# fcore

## Introduction

본 프로젝트는 주식회사 지농과 서울대에서 개발하는 오픈소스기반 스마트팜 제어기의 룰을 처리하는 룰엔진을 목표로 한다. 

## 개발 환경
* python 3을 사용한다.
* 다음의 패키지들이 필요하다.
  * paho-mqtt
  * simpleeval
  * subprocess32
  * future
  * pstuil

* 설치방법
```
sudo pip3 install paho-mqtt simpleeval subprocess32 future pstuil
```

## 작동 방법

fcore 는 faroms의 데이터베이스에서 룰을 읽어 해당 룰을 해석하고, 룰에 맞는 작업을 처리하는 방식으로 작동한다. 
룰을 처리한 결과물은 다음의 2가지 이다.

* 새로운 데이터
* 제어 명령

## 동작 설정
fcore는 conf/fcore.conf 파일을 기본 설정 파일로 한다. 설정파일의 형식은 json 이며, 다음과 같은 내용을 담고 있다. 

```
{
  "db": {
    "host": "localhost",
    "user": "user",
    "password": "password",
    "db": "farmosdb"
  },
  "mqtt": {
    "host": "mqtt.jinong.co.kr",
    "port": 1883,
    "keepalive": 60,
    "svc" : "cvtgate"
  },
  "sleep" : 1
}
```

