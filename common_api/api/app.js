var express = require('express')
var compression = require('compression')
var swaggerNodeRunner = require('swagger-node-runner')
var swaggerUi = require('swagger-ui-express')
var history = require('connect-history-api-fallback')
var cors = require('cors')
var path = require('path')
var fs = require('fs')
var bodyParser = require('body-parser')
var configFile = require('../conf/config.json')
const promMid = require('express-prometheus-middleware')

// // promethes
// const Prometheus = require('prom-client')
// const httpRequestDurationMicroseconds = new Prometheus.Histogram({
//   name: 'http_request_duration_ms',
//   help: 'Duration of HTTP requests in ms',
//   labelNames: ['route'],
//   buckets: [0.10, 5, 15, 50, 100, 200, 300, 400, 500]
// })

require('database')(configFile)

process.chdir(__dirname)
global.rootPath = __dirname

const docs = '/docs'
const pool = require('database')(configFile)
let isiIitialized = false;

(async () => {
  const [result] = await pool.query('select configuration from configuration where type = "initialize"')
  if (result[0].configuration.toString() === 'true') {
    isiIitialized = true
  }
})()

const rootPath = path.join(global.rootPath, configFile.backupPath)
const obsPath = path.join(global.rootPath, configFile.backupPath, configFile.backupObs)
const imagePath = path.join(global.rootPath, configFile.backupPath, configFile.backupImage)

!fs.existsSync(rootPath) && fs.mkdirSync(rootPath)
!fs.existsSync(obsPath) && fs.mkdirSync(obsPath)
!fs.existsSync(imagePath) && fs.mkdirSync(imagePath)

var app = express()
app.use(compression({ threshold: -1 }))
app.use(bodyParser.json({ limit: 10 * 1024 * 1024 }))
app.use(bodyParser.raw({ limit: 10 * 1024 * 1024 }))

app.use(promMid({
  metricsPath: '/metrics',
  collectDefaultMetrics: true,
  requestDurationBuckets: [0.1, 0.5, 1, 1.5]
}))

// Runs before each requests
// promethes
// app.use((req, res, next) => {
//   res.locals.startEpoch = Date.now()
//   next()
// })

// promethes
// app.get('/metrics', (req, res) => {
//   res.set('Content-Type', Prometheus.register.contentType)
//   res.end(Prometheus.register.metrics())
// })

// Runs after each requests
// promethes
// app.use((req, res, next) => {
//   const responseTimeInMs = Date.now() - res.locals.startEpoch
//   httpRequestDurationMicroseconds.labels(req.url).observe(responseTimeInMs)
//   console.log(Prometheus.register.metrics())
//   next()
// })

app.use(cors({
  credentials: true,
  exposedHeaders: ['Content-Disposition']
}))

app.use(history({
  verbose: false,
  rewrites: [
    {
      from: /\/docs/,
      to: function (context) {
        return context.parsedUrl.pathname
      }
    }
  ]
}))

var config = {
  appRoot: __dirname,
  swagger: 'api/swagger/swagger.yaml'
}

swaggerNodeRunner.create(config, function (err, swaggerRunner) {
  if (err) {
    throw err
  }

  app.use(docs, swaggerUi.serve, swaggerUi.setup(require('yamljs').load(config.swagger)))

  app.use(async (req, res, next) => {
    if (req.originalUrl.startsWith('/common/v1')) {
      if (req.originalUrl.startsWith('/common/v1/init')) {
        if (req.method === 'GET') {
          const query = 'select configuration from configuration where type = "initialize"'
          const [result] = await pool.query(query)
          if (result[0].configuration.toString() === 'true') {
            isiIitialized = true
          }
          res.json(isiIitialized)
        } else {
          if (!isiIitialized) {
            next()
          } else {
            res.status(500).send('farmos is initialized')
          }
        }
      } else {
        try {
          if (!isiIitialized) {
            const query = 'select configuration from configuration where type = "initialize"'
            const [result] = await pool.query(query)
            if (result[0].configuration.toString() === 'true') {
              isiIitialized = true
            }
            if (isiIitialized) {
              next()
            } else {
              res.status(500).send('farmos is not initialized')
            }
          } else {
            next()
          }
        } catch (err) {
          res.status(500).send('farmos is not initialized')
        }
      }
    } else {
      next()
    }
  })

  const swaggerExpress = swaggerRunner.expressMiddleware()
  swaggerExpress.register(app)

  app.use(express.static('public'))
  app.use('/image', express.static(path.join(global.rootPath, configFile.backupPath, configFile.backupImage)))
  app.use(docs, swaggerUi.serve, swaggerUi.setup(require('yamljs').load(config.swagger)))

  let port = 8081
  if (swaggerExpress.runner.swagger.host.split(':')[1] !== undefined) {
    port = swaggerExpress.runner.swagger.host.split(':')[1]
  }

  app.listen(port, function () {
    console.log(`api listening on http://${swaggerExpress.runner.swagger.host + docs}`)
  })
})
