/**
 * @fileoverview farmos api for farmos
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2017.07.25
 */

/* jshint esversion: 6 */

/**
 * notice
 * swagger-node 에서 controller 에 대한 initalize 와 finalize를
 * 별도로 지원하지 않는것으로 보인다.
 * farmos.js 의 경우 initialize 는 여러번 호출해도 상관없기 때문에
 * 모든 작업 시작전에 initialize 를 호출한다.
 * finalize 의 경우 디비와의 연결을 종료하는 것인데, 프로세스 종료시
 * 자동으로 될 것으로 간주한다.
 **/

var farmos = require('farmos')
var _modulename = 'farmos api for farmos'
var auth = require('../helpers/securityHandlers')
var pcorr = require('compute-pcorr')

var farmosApi = function() {
    /**
     * @method getfields
     * @description 농장 구역정보를 가져온다.
     */
    var getfields = async(req, res) => {
        try {
            const result = await farmos.getfields()
            const obj = JSON.parse(JSON.stringify(result))
            obj.forEach(element => {
                if (element.uiinfo) {
                    element.uiinfo = JSON.parse(element.uiinfo)
                }
                if (element.cropinfo) {
                    element.cropinfo = JSON.parse(element.cropinfo)
                }
            })
            res.json(obj)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deletefield
     * @description 농장내 구역정보를 삭제한다.
     */
    var deletefield = async(req, res) => {
        try {
            const fieldId = req.swagger.params.fieldId.value
            const result = await farmos.deleteField(fieldId)
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method setfields
     * @description 농장 구역정보를 수정한다.
     */
    var setfield = async(req, res) => {
        var params = req.swagger.params
        var fieldid = params.fieldId.value
        var field = params.body.value

        try {
            const result = await farmos.setfield(fieldid, field)
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method setfieldsfromdp
     * @description 농장 구역정보를 수정한다. DP요청용
     */
     var setfieldfromdp = async(req, res) => {
      var params = req.swagger.params
      var fieldid = params.fieldId.value
      var field = params.body.value

      try {
          const result = await farmos.setfieldfromdp(fieldid, field)
          res.json(result)
      } catch (error) {
          res.status(500).send(error)
      }
  }

    /**
     * @method addfield
     * @description 농장 구역을 추가한다.
     */
    var addfield = async(req, res) => {
        try {
            var item = req.swagger.params.body.value
            await farmos.addfield(item)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addfieldfromdp
     * @description 농장 구역을 추가한다. DP요청용
     */
     var addfieldfromdp = async(req, res) => {
      try {
          var item = req.swagger.params.body.value
          await farmos.addfieldfromdp(item)
          res.json()
      } catch (error) {
          console.log(error)
          res.status(500).send(error)
      }
  }
    /**
     * @method setplantdate
     * @description 재배기간 시작,종료를 설정한다.
     */
    var setplantdate = async(req, res) => {
        try {

            var item = req.swagger.params.body.value
            if (item.cropinfo === 0) {
                item.cropinfo = null
            }
            await farmos.setplantdate(item)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getplantdate
     * @description 재배기간을 조회한다.
     */
    var getplantdate = async(req, res) => {
        try {
            var item = req.swagger.params
            const result = await farmos.getplantdate(item.fieldId.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method modifyhistory
     * @description 작기히스토리를 수정한다.
     */
    var modifyhistory = async(req, res) => {
        try {
            var item = req.swagger.params.body.value
            const result=await farmos.modifyhistory(item)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deletehistory
     * @description 작기히스토리를 삭제한다.
     */
    var deletehistory = async(req, res) => {
        try {
            var item = req.swagger.params.body.value
            await farmos.deletehistory(item)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method backupdata
     * @description farmos 데이터를 삭제 및 백업한다.
     */
    var backupdata = async(req, res) => {
        try {
            var item = req.swagger.params.body.value
            console.log("set backup reservation..")
                const result=await farmos.backupdata(item)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getbackupstate
     * @description 백업상태를 확인한다
     */
     var getbackupstate = async(req, res) => {
        try {
            let result = await farmos.getbackupstate()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }
    
    /**
     * @method setfielduidevice
     * @description 필드 ui 장비 수정한다.
     */
    var setfielduidevice = async(req, res) => {
        try {
            var params = req.swagger.params
            var data = params.body.value
            const result = await farmos.setfieldUiDevice(params.fieldId.value, data.path, data.devices, data.datas, data.typeSelect, data.timespan)
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method setFieldLocalUi
     * @description local 필드 ui 정보 수정
     */
    var setFieldLocalUi = async(req, res) => {
        try {
            var items = req.swagger.params.body.value
            const result = await farmos.setFieldLocalUi(items)
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    var makeobject = function(result) {
        const obj = {}
        result.map(element => {
            obj[element.data_id] = element
            delete obj[element.data_id].data_id
        })
        return obj
    }

    /**
     * @method getfieldobservation
     * @description 특정 농장 구역에 속한 장비들의 최근 관측치를 확인한다.
     */
    var getfieldobservation = async(req, res) => {
        var params = req.swagger.params
        var fieldid = params.fieldId.value

        try {
            const result = await farmos.getlastobservationsforfield(fieldid)
            res.json(makeobject(result))
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method getfarm
     * @description 농장 정보를 가져온다
     */
    var getfarm = async(req, res) => {
        try {
            const result = await farmos.getfarm()
            if (result.length > 0) {
                result[0].info = JSON.parse(result[0].info)
                res.json(result[0])
            } else {
                res.json({
                    name: '',
                    info: {
                        telephone: '',
                        address: '',
                        postcode: '',
                        gps: ''
                    }
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setfarm
     * @description 농장 정보를 수정한다
     */
    var setfarm = async(req, res) => {
        try {
            await farmos.setfarm(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getfarmobservation
     * @description 전체 농장에 속한 장비들의 최근 관측치를 확인한다.
     */
    var getfarmobservation = async(req, res) => {
        try {
            const result = await farmos.getlastobservations()
            res.json(makeobject(result))
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method getdevices
     * @description 특정구역에 설치된 장비들의 리스트를 확인한다.
     */
    /* var getdevices = async (req, res) => {
        console.log(_modulename, 'getdevices');
        var params = req.swagger.params;
        if (('fieldId' in params) && (params.fieldId.raw)) {
            var fieldid = params.fieldId.value;

            try {
                const result = await farmos.getfielddevices(fieldid);
                res.json(makeobject(result));
            } catch (error) {
                res.status(500).send(error);
            }
        } else {
            res.status(400).send("no fieldId");
        }
    }; */

    /**
     * @method getactuators
     * @description 특정구역에 설치된 구동기들의 상태를 확인한다.
     */
    var getactuators = async(req, res) => {
        console.log(_modulename, 'getactuators')
        var params = req.swagger.params
        if (('fieldId' in params) && (params.fieldId.value)) {
            var fieldid = params.fieldId.value

            try {
                const result = await farmos.getfieldactuators(fieldid)
                console.log(result)
                res.json(result)
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            res.status(400).send('no fieldId')
        }
    }

    /**
     * @method getactuatorstatus
     * @description 구동기의 상태를 확인한다.
     */
    var getactuatorstatus = async(req, res) => {
        console.log(_modulename, 'getactuatorstatus')
        var params = req.swagger.params
        if (('actuatorId' in params) && (params.actuatorId.value)) {
            var actid = params.actuatorId.value

            try {
                const result = await farmos.getcontrolstatus(actid)
                console.log(result)
                res.json(result)
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            res.status(400).send('no fieldId')
        }
    }

    /**
     * @method getevents
     * @description 확인되지 않은 농장 이벤트정보를 가져온다.
     */
    var getevents = async(req, res) => {
        console.log(_modulename, 'getevents')
        var option = { method: ['UI'], checked: false }
        var cnt = 0

        try {
            const ret = await farmos.geteventcnt(option)
            cnt = ret[0].cnt
            const result = await farmos.getevents(option)
            console.log(result)
            res.json({ cnt: cnt, events: result })
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getallevents
     * @description 농장 이벤트정보를 가져온다.
     */
    var getallevents = async(req, res) => {
        console.log(_modulename, 'getallevents')
        var cnt = 0
        var params = req.swagger.params
        var option = null

        if (('body' in params) && (params.body.value)) {
            option = params.body.value
        }

        try {
            const ret = await farmos.geteventcnt(option)
            cnt = ret[0].cnt
            if (!(option.limit)) {
                option.limit = cnt
            }
            const result = await farmos.getevents(option)
            console.log(result)
            res.json({ cnt: cnt, events: result })
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method checkevent
     * @description 농장 이벤트정보를 확인했다고 표시한다.
     */
    var checkevent = async(req, res) => {
        console.log(_modulename, 'checkevent')

        var params = req.swagger.params
        var eventid = null
        if (('eventid' in params) && (params.eventid.value)) {
            eventid = params.eventid.value

            try {
                const result = await farmos.checkevent(eventid)
                res.json(result)
            } catch (error) {
                res.status(500).send(error)
            }
        } else {
            res.status(404).send('Event not found.')
        }
    }

    var adddevices = async(req, res) => {
        var params = req.swagger.params
        try {
            const result = await farmos.addDevices(params.body.value)
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    var setdevice = async(req, res) => {
        var params = req.swagger.params
        try {
            await farmos.setDevice(params.body.value)
            res.json({})
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    var deletedevice = async(req, res) => {
        var params = req.swagger.params
        try {
            const result = await farmos.deleteDevice(params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    var getfielddevices = async(req, res) => {
        try {
            var params = req.swagger.params
            var fieldid = params.fieldId.value

            const result = await farmos.getfielddevices(fieldid)
            result.forEach(element => {
                element.spec = JSON.parse(element.spec)
            })
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    var getdevices = async(req, res) => {
        try {
            const result = await farmos.getDevices()
            result.forEach(element => {
                element.spec = JSON.parse(element.spec)
            })
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    var setdevicemanual = async(req, res) => {
        try {
            const result = await farmos.setDeviceManual()
            res.json(result)
        } catch (error) {
            res.status(500).send(error)
        }
    }

    /**
     * @method getdataindexes
     * @description 전제 데이터인덱스를 얻어온다
     */
    var getdataindexes = async(req, res) => {
        try {
            const result = await farmos.getDataIndexes()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setdataindexes
     * @description 데이터인덱스를 수정한다
     */
    var setdataindexes = async(req, res) => {
        try {
            await farmos.setDataIndexes(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setruleapplied
     * @description 룰을 수정한다
     */
    var setruleapplied = async(req, res) => {
        try {
            const rule = req.swagger.params.rule.value
            const ruleId = req.swagger.params.ruleId.value
            await farmos.setRuleApplied(rule, ruleId)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setruleappliedreset
     * @description 룰 초기화
     */
    var setruleappliedreset = async(req, res) => {
        try {
            const ruleId = req.swagger.params.ruleId.value
            await farmos.setRuleAppliedReset(ruleId)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deleteappliedRule
     * @description 룰을 삭제한다
     */
    var deleteappliedRule = async(req, res) => {
        try {
            const ruleId = req.swagger.params.ruleId.value
            await farmos.deleteAppliedRule(ruleId)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setRuleAppliedUse
     * @description 임시 저장된 룰 사용할 수 있게 변경
     */
    var setRuleAppliedUse = async(req, res) => {
        try {
            await farmos.setRuleAppliedUse(req.swagger.params.ruleId.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getTestRuleResult
     * @description 적용된 룰을 테스트 한다
     */
    var getTestRuleResult = async(req, res) => {
        try {
            const result = await farmos.getTestRuleResult(req.swagger.params.ruleId.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addruleapplied
     * @description 룰을 추가한다
     */
    var addruleapplied = async(req, res) => {
        try {
            const fieldId = req.swagger.params.body.value.fieldId
            const ruleId = req.swagger.params.body.value.ruleId
            const rule = req.swagger.params.body.value.rule
            const Ismod = req.swagger.params.body.value.Mod
            const item = req.swagger.params.body.value
            console.log(Ismod.IsMod)
            if (Ismod.IsMod == true) {
                const fs_result = await farmos.addRuleModule(Ismod.title, Ismod.contents)
                const result = await farmos.addRuleApplied(rule, fieldId, undefined, ruleId,item)
                res.json({ id: result })
            } else {
                const result = await farmos.addRuleApplied(rule, fieldId, undefined, ruleId,item)
                res.json({ id: result })
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method ruleJsonTrans()
     * @description 룰 아이템을 JSON으로 변환한다
     */
    var ruleJsonTrans = (jsonString) => {
        const obj = JSON.parse(JSON.stringify(jsonString))

        if (obj instanceof Array) {
            obj.forEach(element => {
                if (element.constraints) {
                    element.constraints = JSON.parse(element.constraints)
                }
                if (element.configurations) {
                    element.configurations = JSON.parse(element.configurations)
                }
                if (element.inputs) {
                    element.inputs = JSON.parse(element.inputs)
                }
                if (element.outputs) {
                    element.outputs = JSON.parse(element.outputs)
                }
                if (element.controllers) {
                    element.controllers = JSON.parse(element.controllers)
                }
            })
        } else {
            if (obj.constraints) {
                obj.constraints = JSON.parse(obj.constraints)
            }
            if (obj.configurations) {
                obj.configurations = JSON.parse(obj.configurations)
            }
            if (obj.inputs) {
                obj.inputs = JSON.parse(obj.inputs)
            }
            if (obj.outputs) {
                obj.outputs = JSON.parse(obj.outputs)
            }
            if (obj.controllers) {
                obj.controllers = JSON.parse(obj.controllers)
            }
        }
        return obj
    }

    /**
     * @method getruleappliedField
     * @description 추가된 룰을 가져온다
     */
    var getruleappliedField = async(req, res) => {
        try {
            const params = req.swagger.params

            let fieldId
            if (('fieldId' in params)) {
                fieldId = params.fieldId.value
            }

            const result = await farmos.getRuleApplied('field', fieldId)
            const obj = ruleJsonTrans(result)
            res.json(obj)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getruleapplied
     * @description 추가된 룰을 가져온다
     */
    var getruleapplied = async(req, res) => {
        try {
            const result = await farmos.getRuleApplied('rule')
            const obj = ruleJsonTrans(result)
            res.json(obj)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getruleappliedId
     * @description 추가된 룰을 가져온다
     */
    var getruleappliedId = async(req, res) => {
        try {
            const params = req.swagger.params

            let ruleId
            if (('ruleId' in params)) {
                ruleId = params.ruleId.value
            }

            const result = await farmos.getRuleApplied('rule', ruleId)
            const obj = ruleJsonTrans(result)
            res.json(obj)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method gettimespan
     * @description 해당 구역의 timespan 데이터를 가져온다
     */
    var gettimespan = async(req, res) => {
        try {
            const result = await farmos.getTimespan()

            for (const item of result) {
                if (item.timespan) {
                    item.timespan = JSON.parse(item.timespan)
                }
            }

            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addtimespan
     * @description 타임스팬 추가
     */
    var addtimespan = async(req, res) => {
        try {
            const item = req.swagger.params.body.value
            const id = await farmos.addTimespan(item)
            res.json({ id })
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deltimespan
     * @description 타임스팬을 삭제한다
     */
    var deltimespan = async(req, res) => {
        try {
            const id = req.swagger.params.timespanId.value
            await farmos.delTimespan(id)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method settimespan
     * @description 타임스팬을 수정한다
     */
    var settimespan = async(req, res) => {
        try {
            const id = req.swagger.params.timespanId.value
            const item = req.swagger.params.body.value
            const result = await farmos.setTimespan(id, item)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method settimespanfielditem
     * @description 적용된 타임스팬 수정
     */
    var settimespanfielditem = async(req, res) => {
        try {
            const params = req.swagger.params
            await farmos.setTimespanFieldItem(params.timespanId.value, params.fieldId.value, params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method gettimespanfielditem
     * @description 해당 구역의 timespan 데이터를 가져온다
     */
    var gettimespanfielditem = async(req, res) => {
        try {
            const params = req.swagger.params
            const result = await farmos.getTimespanFieldItem(params.timespanId.value, params.fieldId.value)
            const obj = JSON.parse(JSON.stringify(result))
            if (obj.timespan) {
                obj.timespan = JSON.parse(obj.timespan)
            }
            res.json(obj)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getgateinfo
     * @description cvtgate의 id 정보를 가져온다
     */
    var getgateinfo = async(req, res) => {
        try {
            const result = await farmos.getGateInfo()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getdevicegatelist
     * @description 등록된 장비의 coupleid를 가져온다
     */
    var getdevicegatelist = async(req, res) => {
        try {
            const result = await farmos.getDeviceGateList()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setgateinfodetect
     * @description cvtgate의 detect 정보를 저장한다
     */
    var setgateinfodetect = async(req, res) => {
        try {
            await farmos.setGateInfoDetect(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getdevicehistory
     * @description 구동기 상태이력을 가져온다
     */
    var getdevicehistory = async(req, res) => {
        try {
            const params = req.swagger.params
            const result = await farmos.getDeviceHistory(params.deviceId.value, params.date.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getdevicehistorycontrole
     * @description 구동기 제어이력 날짜별로 조회
     */
    var getdevicehistorycontrole = async(req, res) => {
        try {
            const params = req.swagger.params
            const result = await farmos.getDeviceHistoryControle(params.deviceId.value, params.sdate.value, params.edate.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    function makeToken(item) {
        const token = auth.createToken(item)
        const refreshToken = auth.refreshToken(item)
        return { token, refreshToken }
    }

    /**
     * @method loginUser
     * @description 로그인을 시도한다
     */
    var loginUser = async(req, res) => {
        try {
            const result = await farmos.login(req.body.userid, req.body.passwd)
            console.log(result)

            const item = {
                id: result.id,
                userid: result.userid,
                privilege: result.privilege,
                basicinfo: result.basicinfo ? JSON.parse(result.basicinfo) : '',
                name: result.name
            }
            const { token, refreshToken } = makeToken(item)

            return res.json({
                token: token,
                refreshToken: refreshToken
            }).end()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method loginRefresh
     * @description 로그인을 시도한다
     */
    var loginRefresh = async(req, res) => {
        try {
            if (req.auth) {
                const item = {
                    userid: req.auth.userid,
                    privilege: req.auth.privilege,
                    basicinfo: req.auth.basicinfo ? JSON.parse(req.auth.basicinfo) : '',
                    name: req.auth.name
                }
                const { token, refreshToken } = makeToken(item)
                return res.json({
                    token: token,
                    refreshToken: refreshToken
                }).end()
            } else {
                const response = { message: 'Error: Credentials incorrect' }
                return res.status(401).json(response)
            }
        } catch (error) {
            console.log(error)
            res.sendStatus(500)
        }
    }

    /**
     * @method getruletemplate
     * @description 룰 템플릿 리스트를 가져온다
     */
    var getruletemplate = async(req, res) => {
        try {
            const result = await farmos.getRuleTemplate()
            result.forEach(element => {
                element.constraints = JSON.parse(element.constraints)
                element.configurations = JSON.parse(element.configurations)
                element.controllers = JSON.parse(element.controllers)
                element.outputs = JSON.parse(element.outputs)
            })
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addruletemplate
     * @description 룰 템플릿 리스트를 가져온다
     */
    var addruletemplate = async(req, res) => {
        try {
            const rule = req.swagger.params.body.value
            const idx = await farmos.addRuleTemplate(rule)
            res.json({ idx })
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deleteruletemplatedetail
     * @description 룰 템플릿을 가져온다
     */
    var deleteruletemplatedetail = async(req, res) => {
        try {
            const idx = req.swagger.params.ruleId.value
            await farmos.deleteRuleTemplateDetail(idx)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getruletemplatedetail
     * @description 룰 템플릿을 가져온다
     */
    var getruletemplatedetail = async(req, res) => {
        try {
            const idx = req.swagger.params.ruleId.value
            const result = await farmos.getRuleTemplateDetail(idx)
            result.constraints = JSON.parse(result.constraints)
            result.configurations = JSON.parse(result.configurations)
            result.controllers = JSON.parse(result.controllers)
            result.outputs = JSON.parse(result.outputs)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setruletemplatedetail
     * @description 룰 템플릿을 수정한다
     */
    var setruletemplatedetail = async(req, res) => {
        try {
            const idx = req.swagger.params.ruleId.value
            const rule = req.swagger.params.body.value
            const result = await farmos.setRuleTemplateDetail(idx, rule)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getobs
     * @description observations 데이터를 가져옴
     */
    var getobs = async(req, res) => {
        try {
            const deviceId = req.swagger.params.deviceId.value
            const date = req.swagger.params.date.value
            const result = await farmos.getObs(deviceId, date)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getcalibration
     * @description calibration 데이터를 가져옴
     */
    var getcalibration = async(req, res) => {
        try {
            const deviceId = req.swagger.params.deviceId.value
            const result = await farmos.getCalibration(deviceId)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setcalibration
     * @description calibration 데이터를 가져옴
     */
    var setcalibration = async(req, res) => {
        try {
            const deviceId = req.swagger.params.deviceId.value
            const calibration = req.swagger.params.body.value
            const result = await farmos.setCalibration(deviceId, calibration)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getreferencelist
     * @description getreferencedata 레퍼런스 데이터 가져오기
     */
    var getreferencelist = async(req, res) => {
        try {
            const result = await farmos.getReferenceList()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addreferencedata
     * @description addreferencedata 데이터 추가
     */
    var addreferencedata = async(req, res) => {
        try {
            const reference = req.swagger.params.body.value
            const result = await farmos.addReferenceData(reference)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deletereferencedata
     * @description deletereferencedata 데이터 삭제
     */
    var deletereferencedata = async(req, res) => {
        try {
            const dataId = req.swagger.params.dataId.value
            const result = await farmos.deleteReferenceData(dataId)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setreferencedata
     * @description setreferencedata 데이터 수정
     */
    var setreferencedata = async(req, res) => {
        try {
            const dataId = req.swagger.params.dataId.value
            const reference = req.swagger.params.body.value
            await farmos.setReferenceData(dataId, reference)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method execProcess
     * @description execProcess 프로세스 동작 리스트
     */
    var execProcess = async(req, res) => {
        try {
            const idx = req.swagger.params.idx.value
            const type = req.swagger.params.type.value
            const result = await farmos.execProcess(idx, type)
            if (result.code === 202) {
                res.status(202).json(result)
            } else {
                res.json(result)
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getProcessLog
     * @description getProcessLog 프로세스 로그 리스트
     */
    var getProcessLog = async(req, res) => {
        try {
            const idx = req.swagger.params.idx.value
            const logData = await farmos.getProcessLog(idx)
            res.json(logData)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getrulestate
     * @description getrulestate 룰 최신 처리 이력
     */
    var getrulestate = async(req, res) => {
        try {
            const data = await farmos.getRuleState()
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getconfiguration
     * @description getconfiguration 파머스 환경설정 읽어오기
     */
    var getconfiguration = async(req, res) => {
        try {
            const type = req.swagger.params.type.value
            const data = await farmos.getConfiguration(type)
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setconfiguration
     * @description getconfiguration 파머스 환경설정 변경
     */
    var setconfiguration = async(req, res) => {
        try {
            const type = req.swagger.params.type.value
            const configuration = req.swagger.params.body.value
            await farmos.setConfiguration(type, configuration)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method dataupload
     * @description dataupload 데이터 업로드
     */
    var dataupload = async(req, res) => {
        try {
            const uploadData = req.swagger.params.body.value
            let result = false
            if (uploadData.type === 'obs') {
                result = await farmos.addObsBackup(uploadData)
            } else if (uploadData.type === 'image') {
                result = await farmos.addImage(uploadData)
            } else if (uploadData.type === 'image_experiment') {
                result = await farmos.addimageExperiment(uploadData, req.auth.id)
            }
            if (result) {
                res.json()
            } else {
                res.status(400).send()
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method delImageId
     * @description delImageId 해당 아이디로 사진 삭제
     */
    var delImageId = async(req, res) => {
        try {
            const id = req.swagger.params.id.value
            await farmos.delImageId(id)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getimagelist
     * @description getimagelist 사진 가져오기
     */
    var getimagelist = async(req, res) => {
        try {
            const id = req.swagger.params.id.value
            const sDate = req.swagger.params.sDate.value
            const eDate = req.swagger.params.eDate.value
            const data = await farmos.getImageList(id, sDate, eDate)
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getRecentImageList
     * @description getRecentImageList 최근 사진 가져오기
     */
    var getRecentImageList = async(req, res) => {
        try {
            const idList = req.swagger.params.body.value
            const data = await farmos.getRecentImageList(idList)
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    var _getdateparamforgraph = function(params) {
        var detail = false
        var today = new Date()
        var datestr = new Date(today.getTime() - today.getTimezoneOffset() * 60000)
            .toISOString()
            .slice(0, 10)
        var hourstr =
            ' ' +
            new Date(today.getTime() - (-31 + today.getTimezoneOffset()) * 60000)
            .toISOString()
            .slice(11, 19)
        var startdate = params.body.value.startdate
        if (startdate) {
            var start = new Date(startdate)
            if (startdate.substring(0, 10) === datestr) { detail = true }
            startdate =
                new Date(start.getTime() - start.getTimezoneOffset() * 60000)
                .toISOString()
                .slice(0, 10) + ' 00:00:00'
        } else {
            startdate = datestr + ' 00:00:00'
            detail = true
        }
        var enddate = params.body.value.enddate

        if (enddate) {
            var edate = new Date(params.body.value.enddate)
            if (edate > today) {
                enddate = datestr + hourstr
            }
        } else {
            enddate = datestr + hourstr
        }

        return {
            startdate: startdate,
            enddate: enddate,
            detail: detail
        }
    }

    var getfarmgraph = async function(req, res) {
        var params = req.swagger.params
        if (
            'body' in params &&
            params.body.value &&
            'device_id' in params.body.value
        ) {
            var devids = params.body.value.device_id

            var graphdata = {}
            var dateparam = _getdateparamforgraph(params)


            const result = await farmos.getgraph(
                devids,
                dateparam.startdate,
                dateparam.enddate,
                dateparam.detail,
                params.body.value.reference
            )

            graphdata.data = result
            res.json(graphdata)
        } else {
            res.status(405).send('no parameter')
        }
    }

    var getfarmgraphdownload = async function(req, res) {
        var params = req.swagger.params
        if (
            'body' in params &&
            params.body.value &&
            'device_id' in params.body.value
        ) {
            var devids = params.body.value.device_id
                // var extra = params.body.value.extra || {}
            var dateparam = _getdateparamforgraph(params)

            const result = await farmos.getgraph(
                devids,
                dateparam.startdate,
                dateparam.enddate
            )
            var data = ''
            let dateSet = new Set()
            const dataIndex = []

            var tmp = []
            var j

            for (j = 0; j < result.length; j++) {
                const name = result[j].dname ? result[j].dname : result[j].name
                    // name += result[j].unit ? ' (' + result[j].unit + ')' : ''
                tmp.push(name)
            }
            data = '시간,' + tmp.join(',') + '\n'

            for (const sensor of result) {
                dataIndex.push(0)
                for (const data of sensor.data) {
                    var date = new Date(dateparam.startdate)
                    date.setSeconds(data.time)
                    date.setSeconds(0)
                    dateSet.add(date.getTime())
                }
            }

            dateSet = Array.from(dateSet).sort()
            dateSet.forEach(function(value) {
                tmp = []
                tmp.push(getTimeStamp(new Date(value)))

                for (let index = 0; index < dataIndex.length; index++) {
                    let condition = true
                    const dataSize = result[index].data.length

                    while (condition) {
                        if (dataIndex[index] >= dataSize) {
                            condition = false
                            continue
                        }
                        var date = new Date(dateparam.startdate)
                        date.setSeconds(result[index].data[dataIndex[index]].time)
                        date.setSeconds(0)

                        if (value === date.getTime()) {
                            tmp.push(result[index].data[dataIndex[index]].value)
                            condition = false
                            dataIndex[index] += 1
                        } else if (value < date.getTime()) {
                            condition = false
                            tmp.push('')
                        } else {
                            dataIndex[index] += 1
                        }
                    }
                }
                data = data + tmp.join(',') + '\n'
            })

            res.setHeader(
                'Content-disposition',
                'attachment;filename=graphdata.csv'
            )
            res.set('Content-Type', 'text/csv')
            res.send(data)
        } else {
            res.status(405).send('no parameter')
        }
    }

    function getTimeStamp(date) {
        var s =
            leadingZeros(date.getFullYear(), 4) + '-' +
            leadingZeros(date.getMonth() + 1, 2) + '-' +
            leadingZeros(date.getDate(), 2) + ' ' +

            leadingZeros(date.getHours(), 2) + ':' +
            leadingZeros(date.getMinutes(), 2)

        return s
    }

    function leadingZeros(n, digits) {
        var zero = ''
        n = n.toString()

        if (n.length < digits) {
            for (let i = 0; i < digits - n.length; i++) { zero += '0' }
        }
        return zero + n
    }

    /**
     * @method getDeviceTime
     * @description getDeviceTime 장비 가동율, 복구시간 가져오기
     */
    var getDeviceTime = async(req, res) => {
        try {
            const deviceId = req.swagger.params.deviceId.value
            const result = await farmos.getDataErrorCheckCount(deviceId)
            if (result.length !== 0 && result[0].count !== 0) {
                const temp = await Promise.all([farmos.getDeviceName(deviceId), farmos.getDeviceErrorTime(deviceId), farmos.getDeviceRestoreTime(deviceId)])
                const obj = {}
                const workTime = temp[1].length !== 0 ? temp[1][0].work_per : 100
                const restorTime = temp[2].length !== 0 ? temp[2][0].avg_error_time : 0
                obj.deviceId = temp[0]
                obj.workTime = workTime
                obj.restoreTime = restorTime
                res.json(obj)
            } else {
                const obj2 = {}
                const temp = await Promise.all([farmos.getDeviceName(deviceId), farmos.getDeviceErrorTime(deviceId), farmos.getDeviceRestoreTime(deviceId)])
                obj2.deviceId = temp[0]
                obj2.workTime = 0
                obj2.restoreTime = 0
                res.json(obj2)
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getProcessList
     * @description getProcessList 프로세스 리스트 조회
     */
    var getProcessList = async(req, res) => {
        try {
            const data = await farmos.getProcessList()
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addProcess
     * @description addProcess 프로세스 추가
     */
    var addProcess = async(req, res) => {
        try {
            console.log(req.body)
            await farmos.addProcess(req.body)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setProcess
     * @description setProcess 프로세스 수정
     */
    var setProcess = async(req, res) => {
        try {
            await farmos.setProcess(req.body)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method delProcess
     * @description delProcess 프로세스 수정
     */
    var delProcess = async(req, res) => {
        try {
            await farmos.delProcess(req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getUnitList
     * @description getUnitList 코드리스트 조회
     */
    var getUnitList = async(req, res) => {
        try {
            const result = await farmos.getUnitList(req.body)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method putDeviceRestore
     * @description putDeviceRestore 장비복구
     */
    var putDeviceRestore = async(req, res) => {
        try {
            const result = await farmos.putDeviceRestore(req.swagger.params.id.value)
            if (result) {
                res.json()
            } else {
                res.status(202).send()
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getCorrelation
     * @description getCorrelation 상관분석 데이터 조회
     */
    var getCorrelation = async function(req, res) {
        var params = req.swagger.params

        try {
            if ('body' in params && params.body.value) {
                var maxArray = []
                var maxIdArray = []
                var countArray = []
                var pcorrList = [] // 상관관계 분석을 위한 리스트
                var nameList = [] // 상관관계 데이터 네임 리스트
                var orderList = [] // 상관관계 데이터 네임 리스트
                for (var i = 0; i < params.body.value.item.length; i++) {
                    orderList.push(params.body.value.item[i].data_id)
                    const observationResult = await farmos.getObservationCount(params.body.value.item[i].data_id, params.body.value.item[i].date[0], params.body.value.item[i].date[1], params.body.value.item[i].func, params.body.value.item[i].dateValue)
                    const currentResult = await farmos.getCurrentCount(params.body.value.item[i].data_id, params.body.value.item[i].dateValue)

                    if (observationResult.length !== 0) {
                        maxIdArray.push(params.body.value.item[i].data_id)
                        countArray.push(observationResult.length)
                        maxArray.push({
                            deviceId: params.body.value.item[i].data_id,
                            name: observationResult[0].name,
                            sdate: params.body.value.item[i].date[0],
                            edate: params.body.value.item[i].date[1],
                            dateValue: params.body.value.item[i].dateValue,
                            func: params.body.value.item[i].func,
                            count: observationResult.length,
                            data: observationResult
                        })
                    } else if (observationResult.length === 0 && currentResult.length !== 0) {
                        const currentResult = await farmos.getCurrentCount(params.body.value.item[i].data_id, params.body.value.item[i].dateValue)
                        maxIdArray.push(params.body.value.item[i].data_id)
                        countArray.push(currentResult.length)
                        maxArray.push({
                            deviceId: params.body.value.item[i].data_id,
                            name: currentResult[0].name,
                            sdate: params.body.value.item[i].date[0],
                            edate: params.body.value.item[i].date[1],
                            dateValue: params.body.value.item[i].dateValue,
                            func: params.body.value.item[i].func,
                            count: currentResult.length,
                            data: currentResult
                        })
                    } else if (observationResult.length === 0 && currentResult.length === 0) {
                        console.log('error')
                    }
                }

                var maxNum = Math.max.apply(null, countArray)
                var maxIndex = countArray.indexOf(maxNum)

                maxNum = maxIdArray[maxIndex] // 데이터 제일 많이 가진 dta_id 찾기

                let max = {}
                for (var value of maxArray) {
                    if (value.deviceId === maxNum) {
                        max = value
                    }
                }

                const maxId = max.deviceId
                nameList.push({
                    id: max.deviceId,
                    name: max.name
                })

                // console.log(maxArray)

                let maxObjList = [] // 최대카운트 가지고 있는 리스트
                const ObjList = [] // 그외의 리스트
                maxArray.forEach(function(el, index) {
                    if (el.deviceId === maxId) {
                        maxObjList = el.data
                    } else {
                        ObjList.push(el)
                    }
                })

                for (var j = 0; j < ObjList.length; j++) {
                    nameList.push({
                        id: ObjList[j].deviceId,
                        name: ObjList[j].name
                    })
                    if (ObjList[j].data.length === 1 && ObjList[j].dateValue !== 'week') { // current_observation일 경우
                        for (var value2 of ObjList[j].data) {
                            ObjList[j].data = [] // ObjList[j] 삭제
                            for (var k = 0; k < maxObjList.length; k++) {
                                ObjList[j].data.push({ // ObjList[j]에 obj 넣기
                                    obs_time: maxObjList[k].obs_time, // maxObjList 시간
                                    nvalue: value2.nvalue // 원래 값
                                })
                            }
                        }
                    } else { // observation일 경우
                        ObjList[j].data = []
                        const dataResult = await farmos.getCorrelationData(maxId, ObjList[j].sdate, ObjList[j].edate, ObjList[j].deviceId, ObjList[j].sdate, ObjList[j].edate, ObjList[j].func, ObjList[j].dateValue)
                        ObjList[j].data = dataResult
                    }
                }

                const tmepArray = []
                const count = max.count

                const tCorrList = [] // 상관관계 분석을 위한 array
                const tNameList = [] // 상관관계 분석 데이터 아이디,이름 array

                if (maxObjList.length === 1 && ObjList.length === 1) {
                    var tempList = []
                    tempList.push(maxObjList[0].nvalue)
                    tempList.push(ObjList[0].data[0].nvalue)

                    var tempList2 = []
                    tempList2.push(ObjList[0].data[0].nvalue)
                    tempList2.push(maxObjList[0].nvalue)

                    tCorrList.push(tempList)
                    tCorrList.push(tempList2)

                    for (const v2 of orderList) {
                        for (var jj = 0; jj < nameList.length; jj++) {
                            if (v2 === nameList[jj].id) {
                                tNameList.push(nameList[jj])
                            }
                        }
                    }
                } else {
                    for (const v of maxObjList) {
                        tmepArray.push(v.nvalue)
                    }
                    pcorrList.push(tmepArray)

                    for (const v of ObjList) {
                        const tmepArray = []
                        for (let i = 0; i < count; i++) {
                            tmepArray.push(v.data[i].nvalue)
                        }
                        pcorrList.push(tmepArray)
                    }

                    for (const v2 of orderList) {
                        for (var jjj = 0; jjj < nameList.length; jjj++) {
                            if (v2 === nameList[jjj].id) {
                                tNameList.push(nameList[jjj])
                                tCorrList.push(pcorrList[jjj])
                            }
                        }
                    }
                }

                const mat = pcorr(tCorrList)
                const tObj = {} // 상관관계 분석 데이터 obj
                const corNameList = []

                console.log(tCorrList)
                console.log(mat)
                for (const nameItem of tNameList) {
                    corNameList.push(nameItem.name)
                }
                tObj.name = corNameList // 상관관계 데이터 이름
                tObj.data = mat // 상관관계 분석값

                res.json(tObj)
            } else {
                res.status(405).send('no parameter')
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getCorrelationDownlaod
     * @description getCorrelation 상관분석 데이터 다운로드
     */
    var getCorrelationDownload = async function(req, res) {
        var params = req.swagger.params
        if (
            'body' in params &&
            params.body.value
        ) {
            var maxArray = []
            var maxIdArray = []
            var countArray = []
            var pcorrList = [] // 상관관계 분석을 위한 리스트
            var nameList = [] // 상관관계 데이터 네임 리스트
            var orderList = [] // 상관관계 데이터 네임 리스트
            var timeGubun = ''
            for (var i = 0; i < params.body.value.dataList.length; i++) {
                orderList.push(params.body.value.dataList[i].data_id)
                timeGubun = params.body.value.dataList[i].dateValue
                const observationResult = await farmos.getObservationCount(params.body.value.dataList[i].data_id, params.body.value.dataList[i].date[0], params.body.value.dataList[i].date[1], params.body.value.dataList[i].func, params.body.value.dataList[i].dateValue)
                const currentResult = await farmos.getCurrentCount(params.body.value.dataList[i].data_id, params.body.value.dataList[i].dateValue)

                if (observationResult.length !== 0) {
                    maxIdArray.push(params.body.value.dataList[i].data_id)
                    countArray.push(observationResult.length)
                    maxArray.push({
                        deviceId: params.body.value.dataList[i].data_id,
                        name: observationResult[0].name,
                        sdate: params.body.value.dataList[i].date[0],
                        edate: params.body.value.dataList[i].date[1],
                        dateValue: params.body.value.dataList[i].dateValue,
                        func: params.body.value.dataList[i].func,
                        count: observationResult.length,
                        data: observationResult
                    })
                } else if (observationResult.length === 0 && currentResult.length !== 0) {
                    const currentResult = await farmos.getCurrentCount(params.body.value.dataList[i].data_id, params.body.value.dataList[i].dateValue)
                    maxIdArray.push(params.body.value.dataList[i].data_id)
                    countArray.push(currentResult.length)
                    maxArray.push({
                        deviceId: params.body.value.dataList[i].data_id,
                        name: currentResult[0].name,
                        sdate: params.body.value.dataList[i].date[0],
                        edate: params.body.value.dataList[i].date[1],
                        dateValue: params.body.value.dataList[i].dateValue,
                        func: params.body.value.dataList[i].func,
                        count: currentResult.length,
                        data: currentResult
                    })
                } else if (observationResult.length === 0 && currentResult.length === 0) {
                    console.log('error')
                }
            }

            var maxNum = Math.max.apply(null, countArray)
            var maxIndex = countArray.indexOf(maxNum)

            maxNum = maxIdArray[maxIndex] // 데이터 제일 많이 가진 dta_id 찾기

            let max = {}
            for (var value of maxArray) {
                if (value.deviceId === maxNum) {
                    max = value
                }
            }

            const maxId = max.deviceId
            nameList.push({
                id: max.deviceId,
                name: max.name
            })

            let maxObjList = [] // 최대카운트 가지고 있는 리스트
            const ObjList = [] // 그외의 리스트
            maxArray.forEach(function(el, index) {
                if (el.deviceId === maxId) {
                    maxObjList = el.data
                } else {
                    ObjList.push(el)
                }
            })

            for (var j = 0; j < ObjList.length; j++) {
                nameList.push({
                    id: ObjList[j].deviceId,
                    name: ObjList[j].name
                })
                if (ObjList[j].data.length === 1) { // current_observation일 경우
                    for (var value2 of ObjList[j].data) {
                        ObjList[j].data = [] // ObjList[j] 삭제
                        for (var k = 0; k < maxObjList.length; k++) {
                            ObjList[j].data.push({ // ObjList[j]에 obj 넣기
                                obs_time: maxObjList[k].obs_time, // maxObjList 시간
                                nvalue: value2.nvalue, // 원래 값
                                name: value2.name // 원래 이름
                            })
                        }
                    }
                } else if (ObjList[j].data.length !== 1) { // observation일 경우
                    ObjList[j].data = []
                    const dataResult = await farmos.getCorrelationData(maxId, ObjList[j].sdate, ObjList[j].edate, ObjList[j].deviceId, ObjList[j].sdate, ObjList[j].edate, ObjList[j].func, ObjList[j].dateValue)
                    ObjList[j].data = dataResult
                }
            }

            const tmepArray = []
            const count = max.count
            for (const v of maxObjList) {
                tmepArray.push(v.nvalue)
            }
            pcorrList.push(tmepArray)

            for (const v of ObjList) {
                const tmepArray = []
                for (let i = 0; i < count; i++) {
                    tmepArray.push(v.data[i].nvalue)
                }
                pcorrList.push(tmepArray)
            }

            const tCorrList = [] // 상관관계 분석을 위한 array
            const tNameList = [] // 상관관계 분석 데이터 아이디,이름 array

            for (const v2 of orderList) {
                for (var jj = 0; jj < nameList.length; jj++) {
                    if (v2 === nameList[jj].id) {
                        tNameList.push(nameList[jj])
                        tCorrList.push(pcorrList[jj])
                    }
                }
            }

            // 다운로드 데이터
            const downList = []
            const downRealList = []
            downList.push(maxObjList)
            for (const v of ObjList) {
                const tmepArray = []
                for (let i = 0; i < count; i++) {
                    tmepArray.push(v.data[i])
                }
                downList.push(tmepArray)
            }

            for (const v2 of orderList) {
                for (var kk = 0; kk < nameList.length; kk++) {
                    if (v2 === nameList[kk].id) {
                        downRealList.push(downList[kk])
                    }
                }
            }

            // 시간축 만들기
            var TimeList = []
            maxObjList.forEach(function(item, index) {
                TimeList.push(item.obs_time)
            })
            var downData = ''
            for (let i = 0; i < TimeList.length; i++) {
                var temp = []
                downRealList.forEach(function(item, index) {
                    if (TimeList[i] === item[i].obs_time) {
                        if (index === 0) {
                            temp.push(item[i].obs_time)
                        }
                        temp.push(item[i].nvalue)
                        if (index === downRealList.length - 1) {
                            downData += temp.join(',') + '\n'
                        }
                    }
                })
            }

            // const mat = pcorr(tCorrList)
            const tNameList2 = []
                // console.log(tCorrList)
                // console.log(mat)

            for (const item of tNameList) {
                tNameList2.push(item.name)
            }
            let timeHead = ''
            if (timeGubun === 'min') {
                timeHead = '분,'
            } else if (timeGubun === 'hour') {
                timeHead = '시간,'
            } else if (timeGubun === 'day') {
                timeHead = '일,'
            } else if (timeGubun === 'week') {
                timeHead = '주,'
            }

            const excelName = timeHead + tNameList2.join(',') + '\n'
            const excelData = excelName + downData
            res.setHeader(
                'Content-disposition',
                'attachment;filename=graphdata.csv'
            )
            res.set('Content-Type', 'text/csv')
            res.send(excelData)
        } else {
            res.status(405).send('no parameter')
        }
    }

    /**
     * @method getRuleStatic
     * @description getRuleStatic  장비통계룰 가져오기
     */
    var getRuleStatic = async(req, res) => {
        try {
            const result = await farmos.getRuleStatic()
            if (result) {
                res.json(result)
            } else {
                res.status(202).send()
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getRuleStaticObj
     * @description getRuleStaticObj  장비통계 개별룰  가져오기
     */
    var getRuleStaticObj = async(req, res) => {
        try {
            const result = await farmos.getRuleStaticObj(req.swagger.params.ruleId.value)
            if (result) {
                res.json(result)
            } else {
                res.status(202).send()
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method initUser
     * @description initUser 사용자 초기 설정
     */
    var initUser = async(req, res) => {
        try {
            console.log(req.swagger.params.body.value)
            const result = await farmos.initUser(req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method initFarmos
     * @description initFarmos 사용자 초기 설정
     */
    var initFarmos = async(req, res) => {
        try {
            console.log(req.swagger.params.body.value)
            const result = await farmos.initFarmos(req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method initFarm
     * @description initFarm 사용자 초기 설정
     */
    var initFarm = async(req, res) => {
        try {
            console.log(req.swagger.params.body.value)
            const result = await farmos.initFarm(req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperimentData
     * @description getExperimentData 연구 조사 데이터 조회
     */
    var getExperimentData = async(req, res) => {
        try {
            const result = await farmos.getExperimentData(req.swagger.params.id.value, req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setExperimentData
     * @description setExperimentData 연구 조사 데이터 수정
     */
    var setExperimentData = async(req, res) => {
        try {
            const result = await farmos.setExperimentData(req.swagger.params.id.value, req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method delExperimentData
     * @description delExperimentData 연구 조사 데이터 삭제
     */
    var delExperimentData = async(req, res) => {
        try {
            const result = await farmos.delExperimentData(req.swagger.params.id.value, req.swagger.params.body.value)
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperimentType
     * @description getExperimentType 샘플 조사 타입 조회
     */
    var getExperimentType = async(req, res) => {
        try {
            const result = await farmos.getExperimentType()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addExperimentType
     * @description addExperimentType 샘플 조사 타입 추가
     */
    var addExperimentType = async(req, res) => {
        try {
            await farmos.addExperimentType(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setExperimentType
     * @description setExperimentType 샘플 조사 타입 수정
     */
    var setExperimentType = async(req, res) => {
        try {
            await farmos.setExperimentType(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error.message)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method delExperimentType
     * @description delExperimentType 샘플 조사 타입 삭제
     */
    var delExperimentType = async(req, res) => {
        try {
            await farmos.delExperimentType(req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperimentSample
     * @description getExperimentSample 샘플 리스트 조회
     */
    var getExperimentSample = async(req, res) => {
        try {
            const result = await farmos.getExperimentSample()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addExperimentSample
     * @description addExperimentSample 샘플 추가
     */
    var addExperimentSample = async(req, res) => {
        try {
            await farmos.addExperimentSample(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setExperimentSample
     * @description setExperimentSample 샘플 수정
     */
    var setExperimentSample = async(req, res) => {
        try {
            await farmos.setExperimentSample(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error.message)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method delExperimentSample
     * @description delExperimentSample 샘플 삭제
     */
    var delExperimentSample = async(req, res) => {
        try {
            await farmos.delExperimentSample(req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getInvestigator
     * @description getInvestigator 조사자 리스트 조회
     */
    var getInvestigator = async(req, res) => {
        try {
            const result = await farmos.getInvestigator()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addInvestigator
     * @description addInvestigator 조사자 추가
     */
    var addInvestigator = async(req, res) => {
        try {
            await farmos.addInvestigator(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method setInvestigator
     * @description setInvestigator 조사자 수정
     */
    var setInvestigator = async(req, res) => {
        try {
            await farmos.setInvestigator(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method delInvestigator
     * @description delInvestigator 조사자 삭제
     */
    var delInvestigator = async(req, res) => {
        try {
            await farmos.delInvestigator(req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperimentByInvestigator
     * @description getExperimentByInvestigator 연구 조사자 맵핑 리스트 조회
     */
    var getExperimentByInvestigator = async(req, res) => {
        try {
            const result = await farmos.getExperimentByInvestigator()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperimentBySample
     * @description getExperiment 연구 샘플 맵핑 리스트 조회
     */
    var getExperimentBySample = async(req, res) => {
        try {
            const result = await farmos.getExperimentBySample()
            res.json(result)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getExperiment
     * @description getExperiment 연구 리스트 조회
     */
    var getExperiment = async(req, res) => {
        try {
            if (req.swagger.params.id) {
                const result = await farmos.getExperiment(req.swagger.params.id.value)
                res.json(result)
            } else {
                const result = await farmos.getExperiment()
                res.json(result)
            }
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addExperiment
     * @description addExperiment 연구 추가
     */
    var addExperiment = async(req, res) => {
        try {
            await farmos.addExperiment(req.swagger.params.body.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method setExperiment
     * @description setExperiment 연구 수정
     */
    var setExperiment = async(req, res) => {
        try {
            await farmos.setExperiment(req.swagger.params.body.value, req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error.message)
        }
    }

    /**
     * @method delExperiment
     * @description delInvestigator 연구 삭제
     */
    var delExperiment = async(req, res) => {
        try {
            await farmos.delExperiment(req.swagger.params.id.value)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addExperimentDataUpload
     * @description addExperimentDataUpload 연구 데이터 업로드
     */
    var addExperimentDataUpload = async(req, res) => {
        try {
            await farmos.addExperimentDataUpload(req.swagger.params.body.value, req.auth.id)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getStaticData
     * @description getStaticData 통계 작동규칙 데이터 조회
     */
    var getStaticData = async(req, res) => {
        try {
            const ruleId = req.swagger.params.ruleId.value
            const dataResult = await farmos.getStaticData(ruleId)
            let basic = ''
            dataResult.forEach(element => {
                const configurations = JSON.parse(element.configurations)
                basic = configurations.basic
            })
            const period = basic[0].value
            const indexList = basic[1].value.split(',')
            let dataId = ''
            const dataIdList = []
            indexList.forEach(element => {
                switch (element) {
                    case 'max':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 1
                        dataIdList.push(dataId)
                        break
                    case 'min':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 2
                        dataIdList.push(dataId)
                        break
                    case 'count':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 3
                        dataIdList.push(dataId)
                        break
                    case 'sum':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 4
                        dataIdList.push(dataId)
                        break
                    case 'avg':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 5
                        dataIdList.push(dataId)
                        break
                    case 'variance':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 6
                        dataIdList.push(dataId)
                        break
                    case 'stddev':
                        dataId = 3 * 10000000 + ruleId * 10000 + 0 * 100 + 7
                        dataIdList.push(dataId)
                        break
                    default:
                        break
                }
            })
            const staticsResult = await farmos.getStaticDataPeriod(dataIdList, period)
            res.json(staticsResult)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getTemplateAPI
     * @description getTemplateAPI 템플릿 API 가져오기
     */
    var getTemplateAPI = async(req, res) => {
        try {
            const data = await farmos.getTemplateAPI()
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getNutrientInfo
     * @description getNutrientInfo 양액공급 정보 조회
     */
    var getNutrientInfo = async(req, res) => {
        try {
            const data = await farmos.getNutrientInfo()
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method addNutrientInfo
     * @description addNutrientInfo 양액공급 정보 입력
     */
    var addNutrientInfo = async(req, res) => {
        var item = req.swagger.params.body.value
        try {
            await farmos.addNutrientInfo(item)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method setNutrientInfo
     * @description setNutrientInfo 양액공급 정보 수정
     */
    var setNutrientInfo = async(req, res) => {
        var id = req.swagger.params.id.value
        var item = req.swagger.params.body.value
        try {
            await farmos.setNutrientInfo(id, item)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method deleteNutrientInfo
     * @description deleteNutrientInfo 양액공급 정보 삭제
     */
    var deleteNutrientInfo = async(req, res) => {
        var id = req.swagger.params.id.value
        try {
            await farmos.deleteNutrientInfo(id)
            res.json()
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method getUserInfo
     * @description getUserInfo 로그인 사용자 정보 가여조기
     */
    var getUserInfo = async(req, res) => {
        try {
            const data = await farmos.getUserInfo()
            console.log(data)
            res.json(data)
        } catch (error) {
            console.log(error)
            res.status(500).send(error)
        }
    }

    /**
     * @method SetOutputfromDP
     * @description 생산량데이터 관리 > DP와의 연동
     */
     var SetOutputfromDP = async(req, res) => {
      try {
          const field_id = req.swagger.params.fieldId.value
          const item = req.swagger.params.body.value
          await farmos.setoutputfromdp(field_id,item)
          res.json()
      } catch (error) {
          console.log(error)
          res.status(500).send(error)
      }
  }

    return {
        setRuleAppliedUse: setRuleAppliedUse,
        getTestRuleResult: getTestRuleResult,
        getExperimentData: getExperimentData,
        setExperimentData: setExperimentData,
        delExperimentData: delExperimentData,
        addExperimentDataUpload: addExperimentDataUpload,
        getExperimentByInvestigator: getExperimentByInvestigator,
        getExperimentBySample: getExperimentBySample,
        getExperiment: getExperiment,
        setExperiment: setExperiment,
        addExperiment: addExperiment,
        delExperiment: delExperiment,
        getInvestigator: getInvestigator,
        setInvestigator: setInvestigator,
        addInvestigator: addInvestigator,
        delInvestigator: delInvestigator,
        delExperimentSample: delExperimentSample,
        setExperimentSample: setExperimentSample,
        addExperimentSample: addExperimentSample,
        getExperimentSample: getExperimentSample,
        delExperimentType: delExperimentType,
        setExperimentType: setExperimentType,
        addExperimentType: addExperimentType,
        getExperimentType: getExperimentType,
        initFarm: initFarm,
        initFarmos: initFarmos,
        initUser: initUser,
        putDeviceRestore: putDeviceRestore,
        getUnitList: getUnitList,
        getImageList: getimagelist,
        delImageId: delImageId,
        dataUpload: dataupload,
        setConfiguration: setconfiguration,
        getConfiguration: getconfiguration,
        getRuleState: getrulestate,
        deleteReferenceData: deletereferencedata,
        setReferenceData: setreferencedata,
        getReferenceList: getreferencelist,
        addReferenceData: addreferencedata,
        setCalibration: setcalibration,
        getCalibration: getcalibration,
        getObs: getobs,
        addTimespan: addtimespan,
        delTimespan: deltimespan,
        setTimespan: settimespan,
        getRuleTemplate: getruletemplate,
        addRuleTemplate: addruletemplate,
        getRuleTemplateDetail: getruletemplatedetail,
        setRuleTemplateDetail: setruletemplatedetail,
        deleteRuleTemplateDetail: deleteruletemplatedetail,
        loginUser: loginUser,
        loginRefresh: loginRefresh,
        getDeviceHistoryControle: getdevicehistorycontrole,
        getDeviceHistory: getdevicehistory,
        getTimespan: gettimespan,
        getTimespanFieldItem: gettimespanfielditem,
        setTimespanFieldItem: settimespanfielditem,
        addRuleApplied: addruleapplied,
        getRuleApplied_Field: getruleappliedField,
        getRuleApplied: getruleapplied,
        getRuleAppliedId: getruleappliedId,
        setRuleApplied: setruleapplied,
        setRuleAppliedReset: setruleappliedreset,
        deleteAppliedRule: deleteappliedRule,
        getDataIndexes: getdataindexes,
        setDataIndexes: setdataindexes,
        addDevices: adddevices,
        setDevice: setdevice,
        setDeviceManual: setdevicemanual,
        deleteDevice: deletedevice,
        getDevices: getdevices,
        getFieldDevices: getfielddevices,
        getFarmObservation: getfarmobservation,
        getFields: getfields,
        setField: setfield,
        setFieldfromDP: setfieldfromdp,
        deleteField: deletefield,
        setFieldUiDevice: setfielduidevice,
        addField: addfield,
        addFieldfromDP: addfieldfromdp,
        SetPlantDate: setplantdate,
        GetPlantDate: getplantdate,
        ModifyHistory: modifyhistory,
        DeleteHistory: deletehistory,
        BackUpData: backupdata,
        GetBackupState: getbackupstate,
        getActuators: getactuators,
        getActuatorStatus: getactuatorstatus,
        getFieldObservation: getfieldobservation,
        getEvents: getevents,
        getAllEvents: getallevents,
        checkEvent: checkevent,
        getFarm: getfarm,
        setFarm: setfarm,
        getGateInfo: getgateinfo,
        getDeviceGateList: getdevicegatelist,
        setGateInfoDetect: setgateinfodetect,
        getFarmGraph: getfarmgraph,
        getFarmGraphDownload: getfarmgraphdownload,
        getDeviceTime: getDeviceTime,
        getProcessList: getProcessList,
        addProcess: addProcess,
        setProcess: setProcess,
        delProcess: delProcess,
        execProcess: execProcess,
        getProcessLog: getProcessLog,
        getCorrelation: getCorrelation,
        getCorrelationDownload: getCorrelationDownload,
        getRuleStatic: getRuleStatic,
        getRuleStaticObj: getRuleStaticObj,
        getStaticData: getStaticData,
        setFieldLocalUi: setFieldLocalUi,
        getRecentImageList: getRecentImageList,
        getTemplateAPI: getTemplateAPI,
        getNutrientInfo: getNutrientInfo,
        addNutrientInfo: addNutrientInfo,
        setNutrientInfo: setNutrientInfo,
        deleteNutrientInfo: deleteNutrientInfo,
        getUserInfo: getUserInfo,
        SetOutputfromDP,SetOutputfromDP
    }
}

module.exports = farmosApi()