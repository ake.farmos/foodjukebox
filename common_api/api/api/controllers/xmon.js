var auth = require('../helpers/securityHandlers')

var _pool = require('database')()

function makeToken (item) {
  const token = auth.createToken(item)
  const refreshToken = auth.refreshToken(item)
  return { token, refreshToken }
}

module.exports = {
  /**
   * @method login
   * @description 조사자 로그인
   */
  login: async (req, res) => {
    try {
      const query = 'select id, userid, privilege, name, phone, basicinfo from farmos_user where phone = ? and privilege = "investigator" and deleted = 0'
      const [rows] = await _pool.query(query, [req.swagger.params.body.value.phone])

      if (rows.length !== 1) {
        throw (new Error('fail to login user'))
      } else {
        const result = rows[0]

        const item = {
          url: req.swagger.params.body.value.url,
          port: req.swagger.params.body.value.port,
          id: result.id,
          userid: result.userid,
          privilege: result.privilege,
          basicinfo: result.basicinfo ? JSON.parse(result.basicinfo) : '',
          name: result.name
        }
        const { token, refreshToken } = makeToken(item)

        return res.json({
          token: token,
          refreshToken: refreshToken
        }).end()
      }
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  },

  /**
   * @method getMyResearch
   * @description 내 연구 리스트 조회
   */
  getMyResearch: async (req, res) => {
    try {
      console.log(req.auth.id)

      const query = 'SELECT * FROM experiment JOIN experiment_user_map USER ON USER.experiment_id=experiment.id WHERE USER.farmos_user_id= ? and USER.deleted = 0 and experiment.deleted = 0 AND DATE(experiment.start_date)<=DATE(NOW()) AND DATE(experiment.end_date)>=DATE(NOW())'
      const [researchList] = await _pool.query(query, [req.auth.id])

      for (const research of researchList) {
        const query = 'SELECT experiment_sample.id sample_id,experiment_sample.field_id,fields.name field_name,experiment_sample.NAME sample_name,experiment_sample_map.sample_item FROM experiment_sample_map JOIN experiment_sample ON experiment_sample_map.sample_id=experiment_sample.id JOIN fields ON fields.id=experiment_sample.field_id WHERE experiment_id = ? AND experiment_sample_map.deleted=0 AND experiment_sample.deleted=0 AND fields.deleted=0'
        const [sampleList] = await _pool.query(query, [research.id])
        console.log(_pool.format(query, [research.sample_id]))
        research.sampleList = sampleList
      }
      res.json(researchList)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  },

  /**
   * @method getExperimentItems
   * @description 실험 항목 조회
   */
  getExperimentItems: async (req, res) => {
    try {
      const query = 'SELECT * from experiment_items where deleted = 0'
      const [result] = await _pool.query(query)
      res.json(result)
    } catch (error) {
      console.log(error)
      res.status(500).send(error)
    }
  }
}
