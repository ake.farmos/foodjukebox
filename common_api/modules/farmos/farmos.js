/**
 * @fileoverview FARMOS_BETA Javascript API
 * @author joonyong.jinong@gmail.com
 * @version 1.0.0
 * @since 2017.07.04
 */
const { promisify } = require('util')
const psList = require('ps-list')
const readLastLines = require('read-last-lines')
const shell = require('shelljs')
const terminate = require('terminate')
const fs = require('fs')
const _path = require('path')
const writeFileAsync = promisify(fs.writeFile)
const readFileAsync = promisify(fs.readFile)
const terminateAsync = promisify(terminate)
var config = require('../../conf/config.json')
var fieldUi = require('../../conf/field_ui.json')

var codeJs = require('./code.js')
const axios = require('axios')
const { exec } = require('child_process')
var farmosApi = function() {
    var _pool = require('database')()
    var _query = ''

    /**
     * @method getfields
     * @description 농장내 구역정보를 모두 읽는다.
     */
    var getfields = async() => {
        _query = 'select id,farm_id, name, fieldtype,uiinfo from fields where deleted = 0'
        const [rows] = await _pool.query(_query)

        const fields = rows
        for (const field of fields) {
            field.data = {}
            _query = 'select * from dataindexes join current_observations on dataindexes.id = current_observations.data_id ' +
                'where dataindexes.field_id = ? '
            const [rows] = await _pool.query(_query, [field.id])
            for (const row of rows) {
                const item = {
                    id: row.id,
                    name: row.name,
                    value: Number(row.nvalue)
                }

                if (field.id * 100000 + 1 === item.id) {
                    field.data.lat = item
                } else if (field.id * 100000 + 2 === item.id) {
                    field.data.lng = item
                } else if (field.id * 100000 + 3 === item.id) {
                    field.data.crop = item
                } else if (field.id * 100000 + 4 === item.id) {
                    if (item.value != 0) {
                        item.value = dateToStr(new Date(item.value * 1000))
                    }
                    field.data.plantdate = item
                }
            }
        }
        return fields
    }

    /**
     * @method setfield
     * @param {Integer} fldid - field id
     * @param {Object} field - field information
     * @description 농장내 구역별정보를 수정한다.
     */
    var setfield = async(fldid, item) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            var result=true
            _query = 'update fields set name = ? where id = ?'
            await connection.query(_query, [item.name, fldid])
            const codeLat = (fldid * 100000 + 1).toString().padStart(8, '0')
            const codeLng = (fldid * 100000 + 2).toString().padStart(8, '0')
            const codeCrop = (fldid * 100000 + 3).toString().padStart(8, '0')
            const codePlantDate = (fldid * 100000 + 4).toString().padStart(8, '0')
            const codeEndDate = (fldid * 100000 + 5).toString().padStart(8, '0')
            var temp= new Date(item.data.plantdate.value)
            _query = 'select * from observations where data_id=? order by nvalue desc limit 1'
            const [tempdata]= await connection.query(_query,codeEndDate)
            if(tempdata.length>0){
                var last_history=tempdata[0].nvalue*1000
                var dateDiff = Math.ceil((temp.getTime()-last_history)/(1000*3600*24));
                if(dateDiff<0){
                    result= false
                }
            }
            if(result!=false){
                _query = 'update current_observations set obs_time = now(), modified_time = now(), nvalue = ? where data_id = ? '
                await connection.query(_query, [item.data.lat.value, codeLat])
                await connection.query(_query, [item.data.lng.value, codeLng])
                if (item.data.crop) {
                    await connection.query(_query, [item.data.crop.value, codeCrop])
                } 
                if (item.data.plantdate && new Date(item.data.plantdate.value) instanceof Date && !isNaN(new Date(item.data.plantdate.value))) {
                    await connection.query(_query, [Math.floor(new Date(item.data.plantdate.value).getTime() / 1000.0), codePlantDate])
                }
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
            return result
        }
    }

    /**
     * @method setfieldfromdp
     * @param {Integer} fldid - field id
     * @param {Object} field - field information
     * @description 농장내 구역별정보를 수정한다.
     */
     var setfieldfromdp = async(fldid, item) => {
      const connection = await _pool.getConnection(async conn => conn)
      try {
          await connection.beginTransaction()
          var result=true
          _query = 'update fields set name = ? where id = ?'
          await connection.query(_query, [item.name, item.id])
          const codeLat = (fldid * 100000 + 1).toString().padStart(8, '0')
          const codeLng = (fldid * 100000 + 2).toString().padStart(8, '0')
          _query = 'update current_observations set obs_time = now(), modified_time = now(), nvalue = ? where data_id = ? '
          if(item.data.lng &&item.data.lat ){
            await connection.query(_query, [item.data.lat.value, codeLat])
            await connection.query(_query, [item.data.lng.value, codeLng])
          }
          await connection.commit()
      } catch (error) {
          await connection.rollback()
          console.log(error)
          throw (error)
      } finally {
          connection.release()
          return result
      }
  }
    /**
     * @method deletefield
     * @param {Object} field - field information
     * @description 농장내 구역정보를 삭제한다.
     */
    var deletefield = async(id) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            let _query = 'select count(*)count from device_field where field_id = ?'
            const [result] = await _pool.query(_query, [id])

            if (result[0].count === 0) {
                _query = 'update fields set deleted = 1 where id = ?'
                await _pool.query(_query, [id])

                _query = 'update dataindexes set deleted = 1 where field_id = ?'
                await _pool.query(_query, [id])

                _query = 'update core_rule_applied set deleted = 1 where field_id = ?'
                await _pool.query(_query, [id])

                _query = 'update core_timespan set deleted = 1 where field_id = ?'
                await _pool.query(_query, [id])
            } else {
                return false
            }
            await connection.commit()
            return true
        } catch (error) {
            await connection.rollback()
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method addfield
     * @param {Object} field - field information
     * @description 농장내 구역별정보를 수정한다.
     */
    var addfield = async(item) => {
      const connection = await _pool.getConnection(async conn => conn)
        try {
          let _query = "";
    
          await connection.beginTransaction();
          let id = 1;
          _query = "select max(id) id from fields";
          const [rows] = await connection.query(_query);
          if (rows[0].id) {
            id = rows[0].id + 1;
          }
    
          const uiInfo = JSON.stringify(fieldUi);
    
          _query =
            "insert into fields " +
            "(id,name,fieldtype,uiinfo,farm_id) " +
            "values (?, ?, ?, ?, ?)";
          await connection.query(_query, [
            id,
            item.name,
            item.fieldtype,
            uiInfo,
            1,
          ]);
    
          _query =
            "INSERT INTO dataindexes (id,name,unit,field_id) " +
            "values (?, ?, ?, ?) ";

            const codeLat = (id * 100000 + 1).toString().padStart(8, '0')
            const codeLng = (id * 100000 + 2).toString().padStart(8, '0')
            const codeCrop = (id * 100000 + 3).toString().padStart(8, '0')
            const codePlantDate = (id * 100000 + 4).toString().padStart(8, '0')
            const codeEndDate = (id * 100000 + 5).toString().padStart(8, '0')
            const codeTotalYield = (id * 100000 + 8).toString().padStart(8, '0')
            await connection.query(_query, [codeLat, '위도', '°', id])
            await connection.query(_query, [codeLng, '경도', '°', id])
            await connection.query(_query, [codeCrop, '작물', 'crop', id])
            await connection.query(_query, [codePlantDate, '정식일', 'date', id])
            await connection.query(_query, [codeEndDate, '종료일', 'date', id])
            await connection.query(_query, [codeTotalYield, '재배주수', null, id]);

            _query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
                'values (?, now(), ?, now()) '

            await connection.query(_query, [codeLat, item.latitude])
            await connection.query(_query, [codeLng, item.longitude])
            await connection.query(_query, [codeCrop, null])
            await connection.query(_query, [codePlantDate, null])
            await connection.query(_query, [codeEndDate, null])
            await connection.query(_query, [codeTotalYield, null])


            const { data: ruleTemplate } = await axios.get(`${config.templateApi}/rule`)
            let autoRuleTemplateList = ruleTemplate.filter((item) => item.autoapplying === 1 && item.used === 1)

            _query = 'select * from core_rule_template where autoapplying = 1 and deleted = 0'
            const [localRule] = await connection.query(_query)

            if (localRule instanceof Array) {
                localRule.forEach(element => {
                    if (element.constraints) {
                        element.constraints = JSON.parse(element.constraints)
                    }
                    if (element.configurations) {
                        element.configurations = JSON.parse(element.configurations)
                    }
                    if (element.inputs) {
                        element.inputs = JSON.parse(element.inputs)
                    }
                    if (element.outputs) {
                        element.outputs = JSON.parse(element.outputs)
                    }
                    if (element.controllers) {
                        element.controllers = JSON.parse(element.controllers)
                    }
                })
            }
            
            connection.release()
            autoRuleTemplateList = autoRuleTemplateList.concat(localRule)
            for (const obj of autoRuleTemplateList) {
                await addruleapplied(obj, id, connection)
            }
           
      await connection.commit();
    } catch (error) {
      await connection.rollback();
      throw error;
    } finally {
      connection.release();
    }
  };

    /**
     * @method addfieldfromdp
     * @param {Object} field - field information
     * @description 농장내 구역별정보를 수정한다.
     */
     var addfieldfromdp = async(item) => {
      const connection = await _pool.getConnection(async conn => conn)
      try {
          let _query = ''
          const id = item.id
          await connection.beginTransaction()
          const uiInfo = JSON.stringify(fieldUi)

          _query = 'insert into fields ' +
              '(id,name,fieldtype,uiinfo,farm_id) ' +
              'values (?, ?, ?, ?, ?)'
          await connection.query(_query, [id, item.name, item.fieldtype, uiInfo, 1])

          _query = 'INSERT INTO dataindexes (id,name,unit,field_id) ' +
              'values (?, ?, ?, ?) '

          const codeLat = (id * 100000 + 1).toString().padStart(8, '0')
          const codeLng = (id * 100000 + 2).toString().padStart(8, '0')
          const codeCrop = (id * 100000 + 3).toString().padStart(8, '0')
          const codePlantDate = (id * 100000 + 4).toString().padStart(8, '0')
          const codeEndDate = (id * 100000 + 5).toString().padStart(8, '0')
          const codeharvestdate = (id * 100000 + 6).toString().padStart(8, '0')
          const codeYieldCount = (id * 100000 + 8).toString().padStart(8, '0')
          await connection.query(_query, [codeLat, '위도', '°', id])
          await connection.query(_query, [codeLng, '경도', '°', id])
          await connection.query(_query, [codeCrop, '작물', 'crop', id])
          await connection.query(_query, [codePlantDate, '정식일', 'date', id])
          await connection.query(_query, [codeEndDate, '종료일', 'date', id])
          await connection.query(_query, [codeharvestdate, '수확일', 'date', id])
          await connection.query(_query, [codeYieldCount, '재배주수', null, id])

          _query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
              'values (?, now(), ?, now()) '

          await connection.query(_query, [codeLat, item.latitude])
          await connection.query(_query, [codeLng, item.longitude])
          await connection.query(_query, [codeCrop, null])
          await connection.query(_query, [codePlantDate, null])
          await connection.query(_query, [codeharvestdate, null])
          if(item.yieldcount){
          await connection.query(_query, [codeYieldCount, item.yieldcount])
          }else {
            await connection.query(_query, [codeYieldCount, null])
            
          }
          await connection.commit()
      } catch (error) {
          await connection.rollback()
          throw (error)
      } finally {
          connection.release()
      }
  }
    /**
     * @method setfieldUiDevice
     * @param {Array} fldid - field id
     * @param {Array} path - ui path
     * @param {Array} device - ui device
     * @param timespan - ui timespan
     * @description 필드 ui 장비 수정한다.
     */
    var setfieldUiDevice = async(fldid, path, devices, datas, typeSelect, timespan) => {
        _query = 'select uiinfo from fields where id = ? '
        const [rows] = await _pool.query(_query, [fldid])
        const uiInfo = JSON.parse(rows[0].uiinfo)
        let newInfo = uiInfo
        for (let index = 0; index < path.length; index++) {
            newInfo = newInfo[path[index]]
        }
        if (devices) {
            newInfo.device = devices
            newInfo.isFull = false
        }
        if (datas) {
            newInfo.data = datas
            newInfo.isFull = false
        }

        if (typeSelect) {
            newInfo.select = typeSelect
        }

        if (timespan) {
            newInfo.timespan = timespan
        }

        _query = 'update fields set uiinfo = ? where id = ?'
        await _pool.query(_query, [JSON.stringify(uiInfo), fldid])
        return uiInfo
    }

    /**
     * @method getlastobservationsforfield
     * @param Integer fieldid - fieldid
     * @description 특정 농장 구역에 설치된 장비들의 최근 관측치를 확인한다.
     */
    var getlastobservationsforfield = async(fieldid) => {
        _query = 'select c.obstime, c.device_id, c.nvalue, m.name, m.unit, ' +
            " IFNULL(a.name, '') parent " +
            'from gos_environment_current c, gos_devicemap m ' +
            'LEFT JOIN ( select p.device_id, m.name ' +
            'from gos_devicemap m, gos_device_portmap p ' +
            'where p.channel = m.device_id ' +
            "and p.name in ('motor', 'actuator')) a " +
            'on a.device_id = m.device_id ' +
            'where m.field_id in (0, ?) and ' +
            'c.device_id = m.device_id '

        const [rows] = await _pool.query(_query, [fieldid])
        return rows
    }

    /**
     * @method getlastobservations
     * @description 전체 농장 구역에 설치된 장비들의 최근 관측치를 확인한다.
     */
    var getlastobservations = async() => {
        _query = 'select * from current_observations join dataindexes on current_observations.data_id = dataindexes.id where dataindexes.deleted = 0'
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method getfieldactuators
     * @param {Integer} fieldid - field id
     * @description 특정 구역의 구동기 정보를 얻는다.
     */
    var getfieldactuators = async(fieldid) => {
        _query = 'select dev.id device_id, dev.devtype, dev.subtype, ' +
            'map.name, map.unit, e.worktime, e.position, e.lasttime ' +
            'from gos_devices dev, gos_devicemap map, ' +
            '(select FORMAT(FLOOR(e1.device_id / 10),0) pid, ' +
            'e1.nvalue worktime, e2.nvalue position, e3.nvalue lasttime ' +
            'from gos_environment_current e1 ' +
            'LEFT JOIN gos_environment_current e2 ' +
            'on e1.device_id = e2.device_id - 1 ' +
            'LEFT JOIN gos_environment_current e3 ' +
            'on e1.device_id = e3.device_id - 2 ' +
            'WHERE e1.device_id % 10 = 1 and e1.device_id in ' +
            '(select device_id from gos_devicemap where field_id = ?)) e ' +
            'where dev.id = map.device_id ' +
            'and e.pid = dev.id and map.field_id = ? ' +
            "and dev.devtype in ('actuator', 'iactuator')"

        const [rows] = await _pool.query(_query, [fieldid, fieldid])
        return rows
    }

    /**
     * @method setplantdate
     * @param {Object} item - plantdate information
     * @description 재배기간의 시작/종료일을 등록한다 
     */
    var setplantdate = async(item) => {
        const field_id = item.field_id
        const date = item.date
        const crop = item.cropinfo
        const end_id = (field_id * 100000 + 5).toString().padStart(8, '0')
        const plant_id = (field_id * 100000 + 4).toString().padStart(8, '0')
        const crop_id = (field_id * 100000 + 3).toString().padStart(8, '0')
        const yield_id = (field_id * 100000 + 8).toString().padStart(8, '0')
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            if (item.started === true) {
                _query = 'select nvalue from current_observations where data_id=?'
                const [crop] = await connection.query(_query, crop_id)
                const [plant] = await connection.query(_query, plant_id)
                _query = 'update current_observations set nvalue=null,obs_time=now() where data_id=?'
                await connection.query(_query, plant_id)
                await connection.query(_query, crop_id)
                await connection.query(_query, yield_id)
                _query = 'INSERT INTO observations (data_id,obs_time,nvalue) ' +
                    'values (?, now(), ?) '
                await connection.query(_query, [plant_id, plant[0].nvalue])
                await connection.query(_query, [crop_id, crop[0].nvalue])
                await connection.query(_query, [end_id, date])
            } else {
                _query = 'update current_observations set nvalue=?,obs_time=now() where data_id=?'
                await connection.query(_query, [date, plant_id])
                _query = 'update current_observations set nvalue=?,obs_time=now() where data_id=?'
                await connection.query(_query, [crop, crop_id])
                _query = 'update current_observations set nvalue=?,obs_time=now() where data_id=?'
                if(item.yieldcount){
                  await connection.query(_query, [item.yieldcount, yield_id])
                  _query = 'INSERT INTO observations (data_id,obs_time,nvalue) ' +
                      'values (?, now(), ?) '
                await connection.query(_query, [yield_id, item.yieldcount])

                }else {
                await connection.query(_query, [0, yield_id])
                }
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method getplantdate
     * @param integer item - 구역조회를 위한  field_id
     * @description 재배기간을 조회한다. 종료일, 시작일, 재배작물로 파싱하는 작업을 수행한다.
     */
    var getplantdate = async(item) => {
        const field_id = item
        const crop_id = (field_id * 100000 + 3).toString().padStart(8, '0')
        const plant_date_id = (field_id * 100000 + 4).toString().padStart(8, '0')
        const end_date_id = (field_id * 100000 + 5).toString().padStart(8, '0')
        var historylist = {
            fieldid: item,
            croplist: [],
            plantlist: [],
            endlist: []
        }
        var temp_croplist = []
        const connection = await _pool.getConnection(async conn => conn)
        try {
            _query = `select DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')obs_time, nvalue from observations where data_id=?  order by obs_time desc`
            const [end] = await connection.query(_query, end_date_id)
            for (i = 0; i < end.length; i++) {
                var enddata = {
                    idx: i,
                    value: end[i].nvalue,
                    obstime: end[i].obs_time
                }
                historylist.endlist.push(enddata)
            }
            _query = `select DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')obs_time,nvalue from observations where data_id=?  order by obs_time desc`
            const [plant] = await connection.query(_query, plant_date_id)

            for (i = 0; i < end.length; i++) {
                var plantdata = {
                    idx: i,
                    value: plant[i].nvalue,
                    obstime: plant[i].obs_time
                }
                historylist.plantlist.push(plantdata)
            }
            _query = `select DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')obs_time,nvalue from observations where data_id=? order by obs_time desc`
            const [crop] = await connection.query(_query, crop_id)
            for (i = 0; i < end.length; i++) {
                var cropdata = {
                    idx: i,
                    value: crop[i].nvalue,
                    obstime: crop[i].obs_time
                }
                
                temp_croplist.push(cropdata)
            }
            await connection.commit()
            historylist.plantlist.sort(function(a, b) {
                return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
            })
            historylist.endlist.sort(function(a, b) {
                return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
            })
            for (i = 0; i < temp_croplist.length; i++) {
                for (var t = 0; t < temp_croplist.length; t++) {
                    if (historylist.plantlist[i].idx == temp_croplist[t].idx) {
                        historylist.croplist.push(temp_croplist[t])
                    }
                }
            }
            return historylist
        } catch (error) {
            await connection.rollback()
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method modifyhistory
     * @param {Object} item - history information
     * @description 재배기간 히스토리를 수정한다. 
     */
    var modifyhistory = async(item) => {
        var result=true
        const field_id = item.fieldid
        const crop_id = (field_id * 100000 + 3).toString().padStart(8, '0')
        const plantdate_id = (field_id * 100000 + 4).toString().padStart(8, '0')
        const enddate_id = (field_id * 100000 + 5).toString().padStart(8, '0')
        var i = 0
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            _query='select nvalue from current_observations where data_id=?'
            const [currentplant]=await connection.query(_query, plantdate_id)
            if(currentplant[0].nvalue!=null){
                const temp=Math.floor((currentplant[0].nvalue+9*3600)/(3600*24))
                const lasthis=Math.floor((item.endlist[0].value+(9*3600))/(3600*24))
                const dateDiff=temp-lasthis
                if(dateDiff<0){
                    result=false
                }
            }if(result!=false){
            for (i = 0; i < item.croplist.length; i++) {
                //item.croplist[i].obstime = new Date(item.croplist[i].obstime)
                _query = `update observations set nvalue=? where data_id=? and DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')=DATE_FORMAT(?, '%Y-%m-%d %H:%i:%s')`
                await connection.query(_query, [item.croplist[i].value, crop_id, item.croplist[i].obstime])
            }
            for (i = 0; i < item.endlist.length; i++) {
                //item.endlist[i].obstime = new Date(item.endlist[i].obstime)
                _query = `update observations set nvalue=? where data_id=? and DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')=DATE_FORMAT(?, '%Y-%m-%d %H:%i:%s')`
                await connection.query(_query, [item.endlist[i].value, enddate_id, item.endlist[i].obstime])
            }
            for (i = 0; i < item.plantlist.length; i++) {
                //item.plantlist[i].obstime = new Date(item.plantlist[i].obstime)
                _query = `update observations set nvalue=? where data_id=? and DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')=DATE_FORMAT(?, '%Y-%m-%d %H:%i:%s')`
                await connection.query(_query, [item.plantlist[i].value, plantdate_id, item.plantlist[i].obstime])

            }
        }
        await connection.commit()
        } catch (error) {
            await connection.rollback()
            throw(error)
        } finally {
            connection.release()
            return result
        }
    }

    /**
     * @method deletehistory
     * @param {Object} item - history information for delete
     * @description 재배기간 히스토리를 삭제한다. 
     */
    var deletehistory = async(item) => {
        const field_id = item.fieldid
        const crop_id = (field_id * 100000 + 3).toString().padStart(8, '0')
        const plantdate_id = (field_id * 100000 + 4).toString().padStart(8, '0')
        const enddate_id = (field_id * 100000 + 5).toString().padStart(8, '0')
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            _query = `delete from observations where nvalue=? and data_id=? and DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i:%s')=DATE_FORMAT(?, '%Y-%m-%d %H:%i:%s') `
            await connection.query(_query, [item.dellist[0].value, crop_id, item.dellist[0].obstime])
            await connection.query(_query, [item.dellist[1].value, plantdate_id, item.dellist[1].obstime])
            await connection.query(_query, [item.dellist[2].value, enddate_id, item.dellist[2].obstime])
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method getbackupstate
     * @description 백업상태를 확인한다 
     */
    var getbackupstate = async() => {
        try {
            var result
            if(fs.existsSync('/var/run/fosbackup.pid')){
                result=true
            }else{
                result=false
            }
            return result
        } catch (error) {
            throw (error)
        }
    }

    /**
     * @method backupdata
     * @param {Object} item - history information for FarmOSdataBackup
     * @description 작기종료시 farmos의 관련 데이터를  export
     */
    var backupdata = async(item) => {
        const field_id = item.fieldid
        const d = new Date(item.recentdate*1000)
        const date = String(d.getFullYear()) + ("0" + String((d.getMonth() + 1))).slice(-2) + String(d.getDate()) + String(d.getHours()) + String(d.getMinutes())
        var cmd = 'mysqldump --no-tablespaces -u' + config.database.user + ' -p' + config.database.pass + ' ' + config.database.db + '>fos_' + field_id + "_" + date + '.sql'          
        try {
            await setTimeout(async function(){
                StopProcess('fcore')
                StopProcess('cvtgate')
                await writeFileAsync('/var/run/fosbackup.pid','farmos doing backup' )
                console.log("write pid..")
                const exec = require('child_process').exec
                let dump = new Promise((resolve, reject) => {
                    console.log('-----------------------------------')
                    console.log("dump DB start")
                    console.log('-----------------------------------')
                    exec(cmd, { cwd: '../db/' }, (error, stderr,stdout) => {
                        if (error) {
                            console.log(error)
                            resolve({ result: 500, error, stderr })
                        } else {
                            console.log(stdout)
                            resolve({ result: 200, stderr })
                        }
                    })
                })
                dump.then(async function() {
                    const connection = await _pool.getConnection(async conn => conn)
                    try{
                        console.log('-----------------------------------')
                        console.log("dump success")
                        console.log("delete DB start")
                        console.log('-----------------------------------')
                        var i = 0
                        await connection.beginTransaction()
                        console.log("DB connect")
                        _query = 'select id from core_rule_applied where field_id=?'
                        const [ruleid] = await connection.query(_query, field_id)
                        _query = 'select device_id from device_field where field_id=?'
                        const [devid] = await connection.query(_query, field_id)
                        _query = `select id from devices where gateid='CAMERA'`
                        const [camid] = await connection.query(_query)
                        for (i = 0; i < devid.length; i++) {
                            const devidx_start = 10000000 + (devid[i].device_id * 100)
                            const devidx_end = 10000100 + (devid[i].device_id * 100)
                            _query = 'delete from observations where data_id>=? and data_id<?'
                            await connection.query(_query, [devidx_start, devidx_end])
                        }
                        console.log("delete device obs data success")
                        for (i = 0; i < ruleid.length; i++) {
                            const ruleidx_start = 30000000 + (ruleid[i].id * 10000)
                            const ruleidx_end = 30010000 + (ruleid[i].id * 10000)
                            _query = 'delete from observations where data_id>=? and data_id<?'
                            await connection.query(_query, [ruleidx_start, ruleidx_end])
                        }
                        console.log("delete rule obs data success")
                        for (i = 0; i < camid.length; i++) {
                            const camidx_start = 10000000 + (camid[i].id * 100)
                            const camidx_end = 1000100 + (camid[i].id * 100)
                            _query = 'delete from observations where data_id>=? and data_id<?'
                            await connection.query(_query, [camidx_start, camidx_end])
                        }
                        console.log("delete gallery data success")
                        const fieldidx_start = 20000000 + (field_id * 100000)
                        const fieldidx_end = 20100000 + (field_id * 100000)
                        _query = 'delete from observations where data_id>=? and data_id<?'
                        await connection.query(_query, [fieldidx_start, fieldidx_end])
                        _query = 'delete from observations where data_id>=? and data_id<?'
                        await connection.query(_query, [40000000, 50000000])
                        console.log("delete outside obs data success")
                        _query = 'delete from uploadhistory'
                        await connection.query(_query)
                        console.log("delete uploadhistory data success")
                        _query = 'delete from requests'
                        await connection.query(_query)
                        console.log("delete request data success")
                        _query = 'delete from nutrient_supply'
                        await connection.query(_query)
                        console.log("delete nutrient data success")
                        const test = new Date()
                        const testdate = String(test.getFullYear()) + ("0" + String((test.getMonth() + 1))).slice(-2) + String(test.getDate()) + String(test.getHours()) + String(test.getMinutes())
                        console.log("Backup End time : "+testdate)
                        await connection.commit()
                        console.log("DB commit")
                        StartProcess('fcore')
                        StartProcess('cvtgate')
                        if(fs.existsSync('/var/run/fosbackup.pid')){
                            fs.unlink('/var/run/fosbackup.pid',function(){
                                console.log("delete pid")
                            })
                        }
                        console.log('-----------------------------------')
                        console.log('Data backup and delete success')
                        console.log('-----------------------------------')
                    } catch (error) {
                        StartProcess('fcore')
                        StartProcess('cvtgate')
                        if(fs.existsSync('/var/run/fosbackup.pid')){
                            fs.unlink('/var/run/fosbackup.pid',function(){
                                console.log("delete pid")
                            })
                        }
                        await connection.rollback()
                        throw (error)
                    }finally {
                        connection.release()
                        console.log("DB release")
                        const success=true
                        return success
                    }
                })
            },item.backupdate)
        } catch (error) {
            throw (error)
        }
    }

    /**
     * @method setdeviceproperties
     * @param {Integer} devid - device id
     * @param {Array[Property]} properties - [{"propkey" : "key", "propvalue" : "value"}]
     * @description 장비의 속성 정보를 업데이트한다.
     */
    var setdeviceproperties = async(devid, properties) => {
        var tmpquery = null
        var arg = null
        if (properties.length > 0) {
            var i
            tmpquery = 'insert into gos_device_properties(propkey, propvalue) ' +
                'values (?, ?) '
            arg = [properties[0].propkey, properties[0].propvalue]

            for (i = 1; i < properties.length; i++) {
                tmpquery += tmpquery + ', (?, ?)'
                arg.push(properties[i].propkey)
                arg.push(properties[i].propvalue)
            }
        }
        const connection = await _pool.getConnection(async conn => conn)
        try {
            _query = 'delete from gos_device_properties where device_id = ?'
            await connection.beginTransaction()
            await _pool.query(_query, [devid])
            if (tmpquery !== null) {
                _query = tmpquery
                await _pool.query(_query, arg)
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            throw (new Error('fail to set device properties.'))
        } finally {
            connection.release()
        }
    }

    /**
     * @method getsensor
     * @param {Integer} devid - device id
     * @description 센서의 속성을 가져온다.
     */
    var getsensor = async(devid) => {
        _query = 'select ctype, configuration, offset from gos_device_convertmap ' +
            'where device_id = ?'
        const [rows] = await _pool.query(_query, [devid])
        return rows
    }

    /**
     * @method getvirtualsensor
     * @param {Integer} devid - device id
     * @description 가상 센서의 속성을 가져온다.
     */
    var getvirtualsensor = async(devid) => {
        _query = 'select convert.configuration as name, port.channel as sensor, ' +
            'port.name as argument, port.opt as option ' +
            'from gos_device_convertmap convert, gos_device_portmap port ' +
            'where convert.device_id = ? and convert.device_id = port.device_id'

        const [rows] = await _pool.query(_query, [devid])
        return rows
    }

    /**
     * @method getgraph
     * @param {Array[Integer]} devids - array of device ids
     * @param {string} startdate - start datetime for query
     * @param {string} enddate - end datetime for query
     * @param {boolean} detail - if it needs detail data then true
     * @description 그래프를 그리기 위한 데이터를 획득한다.
     */
    var getgraph = async(devids, startdate, enddate, detail, reference) => {
        var args = []

        let query = ''

        if (reference) {
            query = 'select a.name as name, a.id as id, a.unit as unit, a.device_id as device_id, ' +
                ' TIME_TO_SEC(e.obs_time) obs_time, ' +
                ' e.nvalue as nvalue, a.NAME AS dname from (dataindexes a left join devices on devices.id = a.device_id), (' +
                ' select data_id, obs_time, nvalue from observations ' +
                ' where data_id in (?) ) e ' +
                ' where a.id = e.data_id ' +
                ' order by data_id, obs_time asc'
            args = [devids]
        } else {
            query = 'select a.name as name, a.id as id, a.unit as unit, a.device_id as device_id, ' +
                ' TIME_TO_SEC(e.obs_time) + DATEDIFF(e.obs_time, ?) * 86400 obs_time, ' +
                ' e.nvalue as nvalue, devices.name as dname from (dataindexes a left join devices on devices.id = a.device_id), (' +
                ' select data_id, obs_time, nvalue from observations ' +
                /* " where data_id in (?) and MINUTE(obs_time) % 10 = 0 and obs_time >= ?" + */
                ' where data_id in (?)  and nvalue is not null and obs_time >= ?' +
                ' and obs_time < ? - INTERVAL 60 MINUTE union all ' +
                ' select data_id, obs_time, nvalue from observations ' +
                ' where data_id in (?) and nvalue is not null and obs_time >= ? - INTERVAL 60 MINUTE ' +
                ' and obs_time < ? ) e ' +
                ' where a.id = e.data_id ' +
                ' order by data_id, obs_time asc'
            args = [startdate, devids, startdate, enddate, devids, enddate, enddate]
        }

        const [rows] = await _pool.query(query, args)
        var graphdata = []
        var devid = -1
        var data = null

        for (var i in rows) {
            var row = rows[i]
            if (devid !== row.id) {
                if (data !== null) {
                    graphdata.push(data)
                }

                data = {
                    id: row.id,
                    name: row.name,
                    dname: row.dname,
                    unit: row.unit,
                    data: [{
                        time: row.obs_time.toString(),
                        value: row.nvalue
                    }]
                }
                devid = row.id
            } else {
                data.data.push({
                    time: row.obs_time.toString(),
                    value: row.nvalue
                })
            }
        }
        if (data !== null) {
            // console.log(data)
            graphdata.push(data)
        }
        return graphdata
    }

    /**
     * @method getgraphall
     * @param {Array[Integer]} devids - array of device ids
     * @param {string} startdate - start datetime for query
     * @param {string} enddate - end datetime for query
     * @description 상세 그래프 데이터를 획득한다.
     */
    var getgraphall = async(devids, startdate, enddate) => {
        _query = 'select a.name as name, a.device_id as device_id, a.unit as unit, ' +
            ' TIME_TO_SEC(e.obstime) + DATEDIFF(e.obstime, ?) * 86400 obstime, ' +
            ' e.nvalue as nvalue from gos_devicemap a, ' +
            ' (select device_id, obstime, nvalue from gos_environment ' +
            ' where device_id in (?) and obstime >= ?' +
            ' and obstime < ? ' +
            ' union all ' +
            ' select device_id, obstime, nvalue from gos_environment_' +
            startdate.substring(0, 4) + startdate.substring(5, 7) +
            ' where device_id in (?) and obstime >= ?' +
            ' and obstime < ? ' +
            ' ) e where a.device_id = e.device_id ' +
            ' order by device_id, obstime asc'

        const [rows] = await _pool.query(_query, [startdate, devids, startdate, enddate, devids, startdate, enddate])
        var graphdata = []
        var devid = -1
        var data = null

        for (var i in rows) {
            var row = rows[i]
            if (devid !== row.device_id) {
                if (data !== null) {
                    console.log(data)
                    graphdata.push(data)
                }

                data = {
                    device_id: row.device_id,
                    name: row.name,
                    unit: row.unit,
                    data: [{
                        time: row.obstime,
                        value: row.nvalue
                    }]
                }
                devid = row.device_id
            } else {
                data.data.push({ time: row.obstime, value: row.nvalue })
            }
        }
        if (data !== null) {
            graphdata.push(data)
        }
        return graphdata
    }

    /**
     * @method getcondensation
     * @param Integer devid - device id
     * @param Float threshold - 결로 상황에 대한 임계치
     * @description 그래프를 그리기 위해 당일의 결로 상황을 확인한다. 이 메소드는 정확히 안맞을 수 있으니 확인이 필요하다. ^^;;;
     */
    var getcondensation = async(devid, threshold) => {
        _query = 'select min(TIME_TO_SEC(obstime)) as x1, ' +
            ' max(TIME_TO_SEC(obstime)) as x2 from gos_environment ' +
            ' where device_id = ? and nvalue < ? and ' +
            " DATE_FORMAT(obstime, '%Y-%m-%d') = DATE_FORMAT(now(), '%Y-%m-%d') " +
            ' order by obstime asc'

        const [rows] = await _pool.query(_query, [devid, threshold])
        if (rows[0].x1 === null || rows[0].x2 === null) {
            return []
        } else {
            return [
                { time: rows[0].x1, value: 0 },
                { time: rows[0].x2, value: 0 }
            ]
        }
    }

    /**
     * @method _geteventcond
     * @description 이벤트 정보 조회를 위한 조건식을 만든다.
     */
    var _geteventcond = function(option) {
        var param = []
        var where = ''
        var limit = 'limit 5'

        if (option !== null && Object.keys(option).length !== 0) {
            console.log('keys', Object.keys(option).length)
            if (option.field_id) {
                where = where + 'and e.field_id in (?) '
                param.push(option.field_id)
            }
            if (option.startdate) {
                where = where + 'and e.occured >= ? '
                param.push(option.startdate)
            }
            if (option.enddate) {
                where = where + 'and e.occured < ? '
                param.push(option.enddate)
            }

            if (option.checked === false) { where = where + 'and d.checked is null ' } else if (option.checked === true) { where = where + 'and d.checked is not null ' }

            if (option.method) {
                where = where + 'and d.method in (?) '
                param.push(option.method)
            }
            if (option.limit) {
                limit = 'limit ?'
                param.push(option.limit)
            }
        }

        where = where + "and d.method = 'UI'"

        return [where, param, limit]
    }

    /**
     * @method getevents
     * @description 농장내 이벤트정보를 읽는다.
     */
    var getevents = async(option) => {
        var cond = _geteventcond(option)

        _query = 'select d.id, e.occured, e.field_id, e.severity, e.name, ' +
            " d.senttime, IFNULL(d.checked, '') checked, d.contact, d.message, d.method " +
            ' from gos_event e, gos_event_delivery d ' +
            ' where e.id = d.event_id ' + cond[0] +
            ' order by e.occured desc, e.id desc ' + cond[2]

        const [rows] = await _pool.query(_query, cond[1])
        return rows
    }

    /**
     * @method geteventcnt
     * @description 농장내 이벤트정보 개수를 읽는다.
     */
    var geteventcnt = async(option) => {
        var cond = _geteventcond(option)

        _query = 'select count(*) cnt ' +
            ' from gos_event e, gos_event_delivery d ' +
            ' where e.id = d.event_id ' + cond[0]

        const [rows] = await _pool.query(_query, cond[1])
        return rows
    }

    /**
     * @method checkevent
     * @description 농장내 이벤트정보를 확인했다고 표시한다.
     */
    var checkevent = async(eventid) => {
        var param = [eventid]

        _query = 'update gos_event_delivery set checked=now() where id = ?'
        await _pool.query(_query, param)
    }

    /**
     * @method getfarm
     * @description 농장 정보를 얻는다.
     */
    var getfarm = async() => {
        _query = 'select id, name, info from farm'
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method setfarm
     * @description 농장 정보를 추가 or 수정한다.
     */
    var setfarm = async(item) => {
        /* _query = "insert into farm(id,name,address,postcode,telephone,owner,maincrop)" +
            "values (?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE name = ?, address = ?," +
            "postcode = ?, telephone = ?, owner = ?, maincrop = ?";
        await _pool.query(_query, [1, item.name, item.address, item.postcode, item.telephone, item.owner, item.maincrop, item.name, item.address, item.postcode, item.telephone, item.owner, item.maincrop]); */

        _query = 'insert into farm(id,name,info)' +
            'values (?, ?, ?) ON DUPLICATE KEY UPDATE name = ?, info = ?'
        await _pool.query(_query, [1, item.name, JSON.stringify(item.info), item.name, JSON.stringify(item.info)])
    }

    /**
     * @method getuser
     * @param {Object} user
     * @description 사용자 정보를 가져온다.
     */
    var _getuser = async(userid, passwd) => {
        _query = 'select id, userid,'
        _query += 'privilege,basicinfo from farmos_user '
        _query += "where userid = '" + userid
        _query += "' and passwd = password('" + passwd + "')"
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method login
     * @param {Object} user
     * @description 사용자 로그인을 수행한다.
     */
    var login = async(userid, passwd) => {
        const users = await _getuser(userid, passwd)
        if (users.length !== 1) {
            throw (new Error('fail to login user'))
        } else {
            return users[0]
        }
    }

    /**
     * @method setdevice
     * @description farmos_api 장비를 수정 한다
     */
    var setdevice = async(devices) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            for (const device of devices) {
                let _query = 'update devices set name = ? where id = ?'
                await connection.query(_query, [device.name, device.id])
                _query = 'delete from device_field where device_id = ?'
                await connection.query(_query, [device.id])

                _query = 'INSERT INTO device_field ( device_id, field_id, sort_no) values (?, ?, ?)'

                for (const [index, place] of device.place.entries()) {
                    await connection.query(_query, [device.id, place, index + 1])
                }

                for (const data of device.datas) {
                    if (data.id.toString().endsWith('01')) {
                        _query = 'update dataindexes set unit = ? where id = ? '
                        await connection.query(_query, [data.unit, data.id])
                        break
                    }
                }
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            throw error
        } finally {
            connection.release()
        }
    }

    /**
     * @method isNumber
     * @description 숫자인지 체크
     */
    function isNumber(s) {
        s += '' // 문자열로 변환
        s = s.replace(/^\s*|\s*$/g, '') // 좌우 공백 제거
        if (s === '' || isNaN(s)) return false
        return true
    }

    /**
     * @method deletedevice
     * @description farmos_api 장비를 삭제 한다
     */
    var deletedevice = async(devices) => {
        let isSuccess = true
        const queryResult = []
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            for (const device of devices) {
                if (!isNumber(device.id)) {
                    continue
                }
                // _query = "select count(*)count from device_field where device_id = ?";
                _query = 'delete from device_field where device_id = ? '
                await _pool.query(_query, [device.id])
                _query = 'update devices set deleted = 1, updated = now() where id = ?'
                await connection.query(_query, [device.id])
            }

            const cvtgate = await getgateinfo()
            const { data } = await axios.get(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple`)

            // const await axios.get(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${cvtgate.couple}/children`)
            for (const device of devices) {
                if (!isNumber(device.id)) {
                    continue
                }

                for (const couple of data) {
                    if (couple.id === device.coupleid) {
                        for (const gateway of couple.children) {
                            if (gateway.id === device.gateid) {
                                for (const node of gateway.children) {
                                    if (device.devindex === null) {
                                        if (device.id.toString() === node.id.toString()) {
                                            if (device.coupleid !== cvtgate.couple) {
                                                node.id = ''
                                            } else {
                                                gateway.children.splice(gateway.children.indexOf(node), 1)
                                            }
                                            break
                                        }
                                    } else {
                                        for (const sensor of node.children) {
                                            if (sensor.id.toString() === device.id.toString()) {
                                                if (sensor.coupleid !== cvtgate.couple) {
                                                    sensor.id = ''
                                                } else {
                                                    node.children.splice(node.children.indexOf(sensor), 1)
                                                }
                                                break
                                            }
                                        }
                                    }
                                }
                                break
                            }
                        }
                        break
                    }
                }
            }

            for (const couple of data) {
                if (couple.id === cvtgate.couple) {
                    for (let i = couple.children.length - 1; i >= 0; --i) {
                        if (couple.children[i].children.length === 0) {
                            couple.children.splice(i, 1)
                        }
                    }
                    break
                }
            }

            for (const couple of data) {
                await axios.put(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${couple.id}`, couple)
            }

            await connection.commit()
        } catch (error) {
            console.log(error)
            await connection.rollback()
            isSuccess = false
        } finally {
            connection.release()
        }
        return { result: isSuccess, list: queryResult }
    }

    /**
     * @method addDataIndex
     * @description 장비 추가시 CommSpec 에 따라 dataindex를 추가한다
     */
    var addDataIndex = async(connection, id, spec) => {
        const queryDataIndex = 'INSERT INTO dataindexes (id,name,unit,sigdigit,device_id) values (?, ?, ?, ?, ?) '
        const queryCurrentObs = 'INSERT INTO current_observations (data_id,obs_time,nvalue) values (?, now(),?) '

        console.log(spec)
        const dataId = 1 * 10000000 + id * 100 + 11
        await connection.query(queryDataIndex, [dataId, '장비가동율', '', 2, id])
        await connection.query(queryCurrentObs, [dataId, 0])

        if (spec.Class === 'sensor') {
            const dataId = 1 * 10000000 + id * 100 + 9
            const estimatedId = 1 * 10000000 + id * 100 + 12
            const vcntId = 1 * 10000000 + id * 100 + 21
            const vavgId = 1 * 10000000 + id * 100 + 22
            const vmaxId = 1 * 10000000 + id * 100 + 23
            const vminId = 1 * 10000000 + id * 100 + 24
            const vstdId = 1 * 10000000 + id * 100 + 25
            const diffstdId = 1 * 10000000 + id * 100 + 26
            await connection.query(queryDataIndex, [dataId, 'Raw값', '', 2, id])
            await connection.query(queryCurrentObs, [dataId, 0])
            await connection.query(queryDataIndex,[estimatedId, '추정관측치','',0,id])
            await connection.query(queryCurrentObs, [estimatedId,null])
            await connection.query(queryDataIndex,[vcntId, '일간 개수','',0,id])
            await connection.query(queryCurrentObs, [vcntId,null])
            await connection.query(queryDataIndex,[vavgId, '일간 평균','',0,id])
            await connection.query(queryCurrentObs, [vavgId,null])
            await connection.query(queryDataIndex,[vmaxId, '일간 최고','',0,id])
            await connection.query(queryCurrentObs, [vmaxId,null])
            await connection.query(queryDataIndex,[vminId, '일간 최저','',0,id])
            await connection.query(queryCurrentObs, [vminId,null])
            await connection.query(queryDataIndex,[vstdId, '일간 표준편차','',0,id])
            await connection.query(queryCurrentObs, [vstdId,null])
            await connection.query(queryDataIndex,[diffstdId, '일간 변동편차','',0,id])
            await connection.query(queryCurrentObs, [diffstdId,null])
        } else if (spec.Type === 'camera/level0') {
            const dataId = 1 * 10000000 + id * 100 + 10
            await connection.query(queryDataIndex, [dataId, '사진', '사진', 2, id])
            await connection.query(queryCurrentObs, [dataId, 0])
        }

        if (!spec.CommSpec['KS-X-3267:2018']) {
            return
        }

        for (const item of spec.CommSpec['KS-X-3267:2018'].read.items) {
            let dataId = 1 * 10000000 + id * 100
            switch (item) {
                case 'status':
                    await connection.query(queryDataIndex, [dataId, '상태', '상태', 2, id])
                    break
                case 'value':
                    dataId += 1
                    await connection.query(queryDataIndex, [dataId, '관측치', codeJs.getValueCode(spec.ValueUnit), Object.prototype.hasOwnProperty.call(spec, 'SignificantDigit') ? spec.SignificantDigit : 2, id])
                    break
                case 'position':
                    dataId += 2
                    await connection.query(queryDataIndex, [dataId, '개폐율', '%', 2, id])
                    break
                case 'state-hold-time':
                    dataId += 3
                    await connection.query(queryDataIndex, [dataId, '지속시간', 's', 2, id])
                    break
                case 'remain-time':
                    dataId += 4
                    await connection.query(queryDataIndex, [dataId, '작동남은시간', 's', 2, id])
                    break
                case 'ratio':
                    dataId += 5
                    await connection.query(queryDataIndex, [dataId, '동작강도', '%', 2, id])
                    break
                case 'control':
                    dataId += 6
                    await connection.query(queryDataIndex, [dataId, '제어권', '제어권', 2, id])
                    break
                case 'area':
                    dataId += 7
                    await connection.query(queryDataIndex, [dataId, '관수구역', '관수구역', 2, id])
                    break
                case 'alert':
                    dataId += 8
                    await connection.query(queryDataIndex, [dataId, '경보', '경보', 2, id])
                    break
            }
            try {
                await connection.query(queryCurrentObs, [dataId, 0])
            } catch (error) {}
        }
    }

    /**
     * @method adddevices
     * @description farmos_api 장비 추가 한다.
     */
    var adddevices = async(devices) => {
        let connection
        try {
            const nodeList = devices.reduce((nodes, currentObject) => {
                if (currentObject.devindex === undefined) {
                    currentObject.children = []
                    nodes.push(currentObject)
                } else {
                    for (const node of nodes) {
                        if (node.coupleid === currentObject.coupleid && node.gateid === currentObject.gateid && node.nodeid === currentObject.nodeid) {
                            node.children.push(currentObject)
                            break
                        }
                    }
                }
                return nodes
            }, [])

            connection = await _pool.getConnection(async conn => conn)
            await connection.beginTransaction()

            for (const node of nodeList) {
                _query = 'select * from devices where nodeid = ? and gateid = ? and coupleid = ? and devindex is null and deleted = 0'
                const [rows] = await connection.query(_query, [node.nodeid, node.gateid, node.coupleid])
                if (rows.length > 0) {
                    if (rows[0].compcode !== node.compcode || rows[0].devcode !== node.devcode || (node.nodetype ? rows[0].nodetype !== node.nodetype : false)) {
                        _query = 'update devices set deleted = 1 where nodeid = ?'
                        await connection.query(_query, [node.nodeid])
                    }
                }
                _query = 'INSERT INTO devices ( name, nodeid, compcode, devcode, devindex, nodetype, spec, gateid, coupleid ) select  ?, ?, ?, ?, ?, ?, ?, ?, ?' +
                    'FROM (SELECT count(id) as count FROM devices ' +
                    'WHERE nodeid = ? AND compcode = ? AND devcode = ? AND (nodetype is null or nodetype = ?) AND gateid = ? AND coupleid = ?  AND devindex is null  AND deleted = 0) a where a.count = 0 '
                const [rows2] = await connection.query(_query, [node.name, node.nodeid, node.compcode, node.devcode, node.devindex, node.nodetype, JSON.stringify(node.spec), node.gateid, node.coupleid,
                    node.nodeid, node.compcode, node.devcode, node.nodetype, node.gateid, node.coupleid
                ])

                if (rows2.affectedRows === 1) {
                    await addDataIndex(connection, rows2.insertId, node.spec)
                }

                for (const device of node.children) {
                    _query = 'select * from devices where nodeid = ? and gateid = ? and devindex = ? and (nodetype is null or nodetype = ?) and coupleid = ? and deleted = 0'
                    const [rows] = await connection.query(_query, [device.nodeid, device.gate, device.devindex, device.nodetype, device.coupleid])
                    if (rows.length > 0) {
                        if (rows[0].compcode !== device.compcode || rows[0].devcode !== device.devcode || (node.nodetype ? rows[0].nodetype !== node.nodetype : false)) {
                            _query = 'update devices set deleted = 1 where id = ?'
                            await connection.query(_query, [rows[0].id])
                        }
                    }
                    _query = 'INSERT INTO devices ( name, nodeid, compcode, devcode, devindex, nodetype, spec, gateid , coupleid) select ?, ?, ?, ?, ?, ?, ?, ?, ?' +
                        'FROM (SELECT count(id) as count FROM devices ' +
                        'WHERE nodeid = ? AND compcode = ? AND devcode = ? AND devindex = ? AND (nodetype is null or nodetype = ?) AND gateid = ? AND coupleid = ? AND deleted = 0) a where a.count = 0 '
                    const [rows2] = await connection.query(_query, [device.name, device.nodeid, device.compcode, device.devcode, device.devindex, node.nodetype, JSON.stringify(device.spec), device.gateid, device.coupleid,
                        device.nodeid, device.compcode, device.devcode, device.devindex, device.nodetype, device.gateid, device.coupleid
                    ])

                    if (rows2.affectedRows === 1) {
                        await addDataIndex(connection, rows2.insertId, device.spec)
                        for (const [index, place] of device.place.entries()) {
                            _query = 'INSERT INTO device_field ( device_id,field_id,sort_no ) values (?, ?, ?) '
                            await connection.query(_query, [rows2.insertId, place, index + 1])
                        }
                    }
                }
            };

            const cvtgate = await getgateinfo()

            _query = 'select * from devices where deleted = 0 and coupleid = ?'
            const [rows] = await connection.query(_query, [cvtgate.couple])

            const cvtListTemp = rows.reduce((gates, currentItems) => {
                if (gates.filter(gate => gate.id === currentItems.gateid).length === 0) {
                    gates.push({ id: currentItems.gateid, dt: 'gw', dk: currentItems.gateid, opt: {}, children: [], devTempList: [] })
                }

                for (const gate of gates) {
                    if (gate.id === currentItems.gateid) {
                        const dkTemp = []
                        const spec = JSON.parse(currentItems.spec)
                        if (spec.CommSpec['KS-X-3267:2018'].read) {
                            dkTemp.push(currentItems.nodeid)
                            dkTemp.push(spec.CommSpec['KS-X-3267:2018'].read['starting-register'])
                            dkTemp.push(`[${spec.CommSpec['KS-X-3267:2018'].read.items.map(x => '"' + x + '"')}]`)
                        }
                        if (spec.CommSpec['KS-X-3267:2018'].write) {
                            dkTemp.push(spec.CommSpec['KS-X-3267:2018'].write['starting-register'])
                            dkTemp.push(`[${spec.CommSpec['KS-X-3267:2018'].write.items.map(x => '"' + x + '"')}]`)
                        }

                        let dt = ''
                        if (spec.Class === 'node') {
                            dt = 'nd'
                        } else if (spec.Class === 'sensor') {
                            dt = 'sen'
                        } else if (spec.Class === 'actuator' || spec.Class === 'nutrient-supply') {
                            dt = `act/${spec.Type}`
                        }

                        const deviceTemp = {
                            nid: currentItems.nodeid,
                            id: currentItems.id.toString(),
                            dk: `[${dkTemp.toString()}]`,
                            dt: dt,
                            children: []
                        }
                        if (dt === 'nd') {
                            gate.children.push(deviceTemp)
                        } else {
                            gate.devTempList.push(deviceTemp)
                        }
                        break
                    }
                }
                return gates
            }, [])

            cvtListTemp.map(gate => {
                gate.devTempList.map(dev => {
                    for (const node of gate.children) {
                        if (node.nid === dev.nid) {
                            delete dev.nid
                            node.children.push(dev)
                            break
                        }
                    }
                })
                delete gate.devTempList
            })

            cvtListTemp.map(gate => {
                gate.children.map(node => {
                    delete node.nid
                })
            })

            await axios.put(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${cvtgate.couple}/children`, cvtListTemp)
            await connection.commit()
        } catch (error) {
            console.log(error)
            await connection.rollback()
            throw error
        } finally {
            connection.release()
        }
    }

    /**
     * @method getdevices
     * @description 전체 장비 리스트를 가져온다
     */
    var getdevices = async() => {
        let _query = 'SELECT *,(' +
            'SELECT id FROM devices WHERE a.coupleid=devices.coupleid AND a.gateid=devices.gateid AND a.nodeid=devices.nodeid AND devindex IS NULL AND deleted=0) nid FROM (' +
            'SELECT id,name,spec,gateid,coupleid,nodeid,compcode,devcode,devindex,nodetype,devices.deleted,updated FROM devices WHERE devices.deleted=0 order by id) a '
        const [rows] = await _pool.query(_query)
        for (const row of rows) {
            /* _query = 'select * from dataindexes where id like ? and deleted = 0'
            const [rows2] = await _pool.query(_query, [100000 + row.id + '__'])
            row.datas = rows2 */
            row.datas = []

            _query = 'select field_id from device_field where device_id = ? order by sort_no asc'
            const [rows3] = await _pool.query(_query, [row.id])
            row.place = rows3.map(field => field.field_id)
        }
        return rows
    }

    /**
     * @method setdevicemanual
     * @description 수동장비 동기화
     */
    var setdevicemanual = async() => {
        const manualResult = []
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            const cvtgate = await getgateinfo()
            console.log(cvtgate.uuid)
            const { data } = await axios.get(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple`)

            const coupleIdList = []
            for (const couple of data) {
                coupleIdList.push(couple.id)
                if (couple.id !== cvtgate.couple) {
                    const coupleId = couple.id

                    const gateIdList = []
                    for (const gate of couple.children) {
                        const gateId = gate.id
                        gateIdList.push(gateId)

                        const nodeIdList = []
                        for (const node of gate.children) {
                            let isNode = true
                            let nid = 100000

                            const tempNode = {}
                            tempNode.id = node.id
                            tempNode.manualRes = 'none'
                            tempNode.manualResDesc = '노드에 속한 장비중 오류가 있습니다.'
                            tempNode.device = []

                            if (node.id === '') {
                                const nodeSpec = {
                                    Class: 'node',
                                    Type: 'node',
                                    Model: '',
                                    Name: '',
                                    CommSpec: {
                                        'KS-X-3267:2018': {
                                            read: {
                                                items: []
                                            }
                                        }
                                    }
                                }

                                if (node.dk && node.dk.length > 0) {
                                    nodeSpec.Type = node.dk
                                }
                                nodeSpec.CommSpec['KS-X-3267:2018'].read.items.push('status')

                                const _query = 'INSERT INTO devices ( coupleid, gateid, nodeid, compcode, devcode, name, spec) value(?, ?, ?, ?, ?, ?, ?)'
                                const [nodeInsertRes] = await connection.query(_query, [coupleId, gateId, 1, 1, 1, '노드', JSON.stringify(nodeSpec)])

                                if (nodeInsertRes.affectedRows === 1) {
                                    nid = Number(nid) + Number(nodeInsertRes.insertId)
                                    const _query = 'update devices set nodeid = ? where id = ?'
                                    await connection.query(_query, [nid, nodeInsertRes.insertId])
                                    await addDataIndex(connection, nodeInsertRes.insertId, nodeSpec)

                                    node.id = String(nodeInsertRes.insertId)
                                    nodeIdList.push(node.id)
                                } else {
                                    isNode = false
                                }
                            } else {
                                const _query = 'select * from devices where id = ?'
                                const [result] = await connection.query(_query, [node.id])

                                console.log(' ')
                                console.log('node')
                                console.log(node.id)

                                if (result.length > 0) {
                                    nid = Number(nid) + Number(node.id)
                                    if (result[0].deleted === 0) {
                                        nodeIdList.push(node.id)
                                            // cvtgate gw 아이디가 변경 되었으면 farmos도 변경
                                            // eslint-disable-next-line no-labels
                                        outer:
                                            for (const gate of couple.children) {
                                                for (const node of gate.children) {
                                                    if (Number(node.id) === Number(result[0].id)) {
                                                        if (String(gate.id) !== String(result[0].gateId)) {
                                                            const _query = 'update devices set gateid = ? where id = ?'
                                                            await connection.query(_query, [gate.id, node.id])
                                                        }
                                                        // eslint-disable-next-line no-labels
                                                        break outer
                                                    }
                                                }
                                            }
                                    }

                                    const spec = JSON.parse(result[0].spec)
                                    const checkClass = 'node'

                                    if (spec.Class.startsWith(checkClass)) {
                                        if (spec.Type === 'node' && result[0].deleted === 1) {
                                            console.log('node_ 미확인 노드가 있습니다. 복구 가능합니다.')
                                            tempNode.manualResDesc = '미확인 노드가 있습니다. 복구 가능합니다.'
                                            tempNode.manualRes = 'restore'
                                            isNode = false
                                            for (const device of node.children) {
                                                if (device.id === '') {
                                                    const _query = 'update devices set deleted = 0 where deleted = 1 and id = ? '
                                                    await connection.query(_query, [node.id])
                                                    nodeIdList.push(node.id)
                                                    isNode = true
                                                    tempNode.manualResDesc = '노드에 속한 장비중 오류가 있습니다.'
                                                    break
                                                }
                                            }

                                            // isNode = false
                                        } else if (spec.Type !== 'node' && result[0].deleted === 0) {
                                            console.log('node_ 서버 데이터와 노드 타입이 다릅니다. 관리자 문의 바랍니다.')
                                            tempNode.manualResDesc = '서버 데이터와 노드 타입이 다릅니다. 관리자 문의 바랍니다.'
                                            tempNode.manualRes = 'question'
                                            isNode = false
                                        }
                                    } else if (result[0].deleted === 0) {
                                        console.log('node_ 서버 데이터와 노드 클래스가 다릅니다. 관리자 문의 바랍니다.')
                                        tempNode.manualResDesc = '서버 데이터와 노드 클래스가 다릅니다. 관리자 문의 바랍니다.'
                                        tempNode.manualRes = 'question'
                                        isNode = false
                                    }
                                } else {
                                    console.log('node_ 서버 데이터가 Farmos 프로그램에 없습니다. 관리자 문의 바랍니다.')
                                    tempNode.manualResDesc = '서버 데이터가 Farmos 프로그램에 없습니다. 관리자 문의 바랍니다.'
                                    tempNode.manualRes = 'question'
                                    isNode = false
                                }

                                manualResult.push(tempNode)
                            }

                            const deviceIdList = []
                            for (const [devindex, device] of node.children.entries()) {
                                if (isNode & device.id === '') {
                                    const _query = 'INSERT INTO devices ( coupleid, gateid, nodeid, compcode, devcode, devindex, name, spec) value(?, ?, ?, ?, ?, ?, ?, ?)'

                                    const spec = {
                                        Class: '',
                                        Type: '',
                                        Model: '',
                                        Name: '',
                                        CommSpec: {
                                            'KS-X-3267:2018': {
                                                read: {
                                                    items: []
                                                }
                                            }
                                        }
                                    }

                                    if (device.dt === 'sen') {
                                        spec.Class = 'sensor'
                                        if (Object.prototype.hasOwnProperty.call(device.opt, 'type')) {
                                            spec.Type = device.opt.type
                                        } else {
                                            spec.Type = device.dk
                                        }
                                    } else if (device.dt.indexOf('act/retractable') > -1 || device.dt.indexOf('act/switch') > -1 || device.dt.indexOf('act/alarm') > -1 || device.dt.indexOf('act/camera') > -1) {
                                        spec.Class = 'actuator'
                                        spec.Type = device.dt.split('act/')[1]
                                    } else if (device.dt.indexOf('act/nutrient-supplier') > -1 || device.dt.indexOf('act/nutrient-supply') > -1) {
                                        spec.Class = 'nutrient-supply'
                                        spec.Type = device.dt.split('act/')[1].replace('nutrient-supplier', 'nutrient-supply')
                                    } else {
                                        continue
                                    }
                                    spec.Name = device.dk
                                    spec.CommSpec['KS-X-3267:2018'].read.items.push('status')

                                    const [deviceInsertRes] = await connection.query(_query, [coupleId, gateId, nid, 1, 1, devindex, spec.Name, JSON.stringify(spec)])

                                    if (deviceInsertRes.affectedRows === 1) {
                                        device.id = String(deviceInsertRes.insertId)
                                        deviceIdList.push(deviceInsertRes.insertId)

                                        if (device.dt === 'sen') {
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('value')
                                        } else if (device.dt.indexOf('act/retractable') >= 0) {
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('state-hold-time')
                                            if (device.dt === 'act/retractable/level1' || device.dt === 'act/retractable/level2') {
                                                spec.CommSpec['KS-X-3267:2018'].read.items.push('position')
                                                spec.CommSpec['KS-X-3267:2018'].read.items.push('remain-time')
                                            }
                                        } else if (device.dt.indexOf('act/switch') >= 0) {
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('state-hold-time')
                                            if (device.dt === 'act/switch/level1' || device.dt === 'act/switch/level2') {
                                                spec.CommSpec['KS-X-3267:2018'].read.items.push('remain-time')
                                            }
                                            if (device.dt === 'act/switch/level2') {
                                                spec.CommSpec['KS-X-3267:2018'].read.items.push('ratio')
                                            }
                                        } else if (device.dt.indexOf('act/nutrient-supplier') >= 0) {
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('control')
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('area')
                                            spec.CommSpec['KS-X-3267:2018'].read.items.push('alert')
                                        }
                                        await addDataIndex(connection, deviceInsertRes.insertId, spec)
                                    }
                                } else if (device.id !== '') {
                                    const _query = 'select * from devices where id = ?'
                                    const [result] = await connection.query(_query, [device.id])

                                    const tempDevice = {}
                                    tempDevice.id = device.id
                                    tempDevice.manualRes = 'none'
                                    tempDevice.dk = device.dk
                                    tempDevice.dt = device.dt

                                    if (device.dt === 'sen' && Object.prototype.hasOwnProperty.call(device.opt, 'type')) {
                                        tempDevice.dk = device.opt.type
                                    } else {
                                        tempDevice.dk = device.dk
                                    }

                                    if (result.length > 0) {
                                        if (result[0].deleted === 0) {
                                            deviceIdList.push(result[0].id)
                                                // cvtgate gw 아이디가 변경 되었으면 farmos도 변경
                                            if (String(gateId) !== String(result[0].gateid)) {
                                                const _query = 'update devices set gateid = ? where id = ?'
                                                await connection.query(_query, [gateId, device.id])
                                            }
                                        }

                                        const spec = JSON.parse(result[0].spec)
                                        const checkClass = device.dt.startsWith('act/nutrient-supply') || device.dt.startsWith('act/nutrient-supplier') ? 'nutrient-supply' : device.dt.startsWith('act') ? 'actuator' : 'sensor'

                                        if (spec.Class.startsWith(checkClass)) {
                                            const tempDt = device.dt.split('/')
                                            let deviceType = ''
                                            if (tempDt.length >= 2) {
                                                tempDt.splice(0, 1)
                                                deviceType = tempDt.join('/')
                                            } else {
                                                deviceType = tempDt[0]
                                            }

                                            if ((checkClass === 'sensor' && spec.Type === tempDevice.dk) || ((checkClass === 'actuator' || checkClass === 'nutrient-supply') && spec.Type === deviceType)) {
                                                if (result[0].deleted === 1) {
                                                    console.log('device_ 미확인 디바이스가 있습니다. 복구 가능합니다.')
                                                    tempDevice.manualResDesc = '미확인 디바이스가 있습니다. 복구 가능합니다.'
                                                    tempDevice.manualRes = 'restore'
                                                }
                                            } else if (spec.Type !== device.dk) {
                                                console.log('device_ 서버 데이터와 디바이스 타입이 다릅니다. 관리자 문의 바랍니다.')
                                                tempDevice.manualResDesc = '서버 데이터와 디바이스 타입이 다릅니다. 관리자 문의 바랍니다.'
                                                tempDevice.manualRes = 'question'
                                            }
                                        } else if (result[0].deleted === 0) {
                                            console.log('device_ 서버 데이터와 디바이스 클래스가 다릅니다. 관리자 문의 바랍니다.')
                                            tempDevice.manualResDesc = '서버 데이터와 디바이스 클래스가 다릅니다. 관리자 문의 바랍니다.'
                                            tempDevice.manualRes = 'question'
                                        }
                                    } else {
                                        console.log('device_ 서버 데이터가 Farmos 프로그램에 없습니다. 관리자 문의 바랍니다.')
                                        tempDevice.manualResDesc = '서버 데이터가 Farmos 프로그램에 없습니다. 관리자 문의 바랍니다.'
                                        tempDevice.manualRes = 'question'
                                    }
                                    if (tempDevice.manualRes !== 'none') {
                                        tempNode.device.push(tempDevice)
                                    }
                                }
                            }

                            // cvtgate 에서 장비 삭제시 장비 삭제
                            const _query = 'select * from devices where coupleid = ? and gateid = ? and nodeid = ? and devindex is not null and deleted = 0'
                            const [result] = await connection.query(_query, [coupleId, gateId, nid])

                            for (const res of result) {
                                if (deviceIdList.indexOf(res.id) === -1) {
                                    const _query = 'update devices set deleted = 1 where id = ?'
                                    await connection.query(_query, [res.id])
                                }
                            }
                        }

                        // cvtgate 에서 노드 삭제시 노드와 하위 장비 삭제
                        const _query = 'select * from devices where coupleid = ? and gateId = ? and devindex is null and deleted = 0'
                        const [result] = await connection.query(_query, [coupleId, gateId])

                        for (const res of result) {
                            if (nodeIdList.indexOf(String(res.id)) === -1) {
                                const _query = 'update devices set deleted = 1 where coupleid = ? and gateid = ? and nodeid = ?'
                                await connection.query(_query, [res.coupleid, res.gateid, 100000 + res.id])
                            }
                        }
                    }

                    // cvtgate 에서 게이트 삭제시 게이트와 하위 장비 삭제
                    const _query = 'select * from devices where coupleid = ? and deleted = 0'
                    const [result] = await connection.query(_query, [coupleId])

                    for (const res of result) {
                        if (gateIdList.indexOf(String(res.gateid)) === -1) {
                            const _query = 'update devices set deleted = 1 where coupleid = ? and gateid = ?'
                            await connection.query(_query, [res.coupleid, res.gateid])
                        }
                    }
                }
            }

            // cvtgate 에서 게이트 삭제시 게이트와 하위 장비 삭제
            const _query = 'select * from devices where deleted = 0'
            const [result] = await connection.query(_query)

            for (const res of result) {
                if (coupleIdList.indexOf(String(res.coupleid)) === -1) {
                    const _query = 'update devices set deleted = 1 where coupleid = ?'
                    await connection.query(_query, [res.coupleid])
                }
            }

            for (const couple of data) {
                if (couple.id !== cvtgate.couple) {
                    await axios.put(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${couple.id}/children`, couple.children)
                }
            }
            await connection.commit()
        } catch (error) {
            console.log(error)
            await connection.rollback()
        } finally {
            connection.release()
        }

        const result = []
        for (const node of manualResult) {
            if ((node.manualRes === 'none' && node.device.length > 0) || (node.manualRes !== 'none')) {
                result.push(node)
            }
        }
        return result
    }

    /**
     * @method getFieldDevices
     * @description 구역별 장비 리스트를 가져온다
     */
    var getfielddevices = async(fieldId) => {
        _query = 'SELECT*,(' +
            'SELECT id FROM devices WHERE a.coupleid=devices.coupleid AND a.gateid=devices.gateid AND a.nodeid=devices.nodeid AND devindex IS NULL AND deleted=0) nid FROM (' +
            'SELECT id,name,spec,gateid,coupleid,nodeid,compcode,devcode,devindex,devices.deleted,updated,device_id,field_id FROM devices JOIN device_field ON devices.id=device_field.device_id WHERE device_field.field_id= ? AND devices.deleted=0) a '
        const [rows] = await _pool.query(_query, [fieldId])
        for (const row of rows) {
            _query = 'select * from dataindexes where id like ?'
            const [rows2] = await _pool.query(_query, [100000 + row.id + '__'])
            row.datas = rows2

            _query = 'select field_id from device_field where device_id = ?'
            const [rows3] = await _pool.query(_query, [row.id])
            row.place = rows3.map(field => field.field_id)
        }
        return rows
    }

    /**
     * @method getdataindexes
     * @description 전제 데이터인덱스를 얻어온다
     */
    var getdataindexes = async() => {
        _query = 'select b.* , (select a.name from devices a where a.id= b.device_id) as device_name from dataindexes b where b.deleted = 0  '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method setdataindexes
     * @description 전제 데이터인덱스를 얻어온다
     */
    var setdataindexes = async(items) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            for (const item of items) {
                _query = 'update dataindexes set name = ?, unit = ? where id = ? '
                await connection.query(_query, [item.name, item.unit, item.id])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method deleteappliedRule
     * @description 룰을 삭제한다
     */
    var deleteappliedRule = async(ruleId) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()
            let _query = 'update dataindexes set deleted = 1 where rule_id = ? '
            await connection.query(_query, [ruleId])

            // 삭제시 사용여부도 0으로 변경
            _query = 'update core_rule_applied set deleted = 1, used = 0, updated = now() where id = ?'
            await connection.query(_query, [ruleId])

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method setruleappliedreset
     * @description 룰 초기화
     */
    var setruleappliedreset = async(ruleId) => {
        const connection = await _pool.getConnection(async conn => conn)
        let _query = ''
        try {
            await connection.beginTransaction()

            _query = 'select id from dataindexes where rule_id = ?'
            const [dataindexes] = await connection.query(_query, [ruleId])

            const dataindexList = []
            for (const dataindex of dataindexes) {
                dataindexList.push(dataindex.id)
            }
            if (dataindexList.length > 0) {
                _query = 'delete from observations where data_id in(?)'
                await connection.query(_query, [dataindexList])

                _query = 'delete from current_observations where data_id in (?)'
                await connection.query(_query, [dataindexList])

                _query = 'delete from dataindexes where rule_id = ?'
                await connection.query(_query, [ruleId])
            }

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method setruleapplied
     * @description 룰을 수정한다
     */
    var setruleapplied = async(rule, ruleId) => {
        const connection = await _pool.getConnection(async conn => conn)
        let _query = ''
        try {
            await connection.beginTransaction()

            if (Object.prototype.hasOwnProperty.call(rule, 'constraints')) {
                _query = 'update core_rule_applied set constraints = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.constraints), ruleId])
            }
            if (Object.prototype.hasOwnProperty.call(rule, 'inputs')) {
                _query = 'update core_rule_applied set inputs = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.inputs), ruleId])
            }

            if (Object.prototype.hasOwnProperty.call(rule, 'controllers')) {
                _query = 'update core_rule_applied set controllers = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.controllers), ruleId])
            }

            if (Object.prototype.hasOwnProperty.call(rule, 'name')) {
                _query = 'update core_rule_applied set name = ?, updated = now() where id = ?'
                await connection.query(_query, [rule.name, ruleId])
            }
            if (Object.prototype.hasOwnProperty.call(rule, 'outputs')) {
                _query = 'update core_rule_applied set outputs = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.outputs), ruleId])
                _query = 'update dataindexes set name = ? where id = ?'

                if (rule.template_id == 26) {
                    if (rule.outputs.data) {
                        for (const item of rule.outputs.data) {
                            const dataId = 30000000 + (ruleId * 10000) + item.outcode
                            await connection.query(_query, [item.name, dataId])
                        }
                    }
                } else {
                    if (rule.outputs.data) {
                        for (const item of rule.outputs.data) {
                            if (item.name && item.name.startsWith('#')) {
                                const splite = item.name.split(' ')
                                if (splite.length > 1) {
                                    const name = splite[0]
                                    const deviceIndex = name.replace('#', '')
                                    const deviceName = rule.constraints.devices[deviceIndex - 1].name

                                    const dataId = 30000000 + (ruleId * 10000) + item.outcode
                                    await connection.query(_query, [deviceName + ' ' + splite[1], dataId])
                                }
                            }
                        }
                    }
                }

                if (rule.outputs.dev) {
                    const currentRule = await getruleapplied('rule', ruleId)
                    const currentConstraints = JSON.parse(currentRule.constraints)

                    const queryDataIndex = 'INSERT INTO dataindexes (id,name,unit,sigdigit,device_id,rule_id) values (?, ?, ?, ?, ?, ?)  ON DUPLICATE KEY UPDATE deleted = 0 , rule_id = ?'
                    const queryCurrentObs = 'INSERT INTO current_observations (data_id,obs_time,nvalue) values (?, now(),?)  ON DUPLICATE KEY UPDATE nvalue = 0 '

                    for (const dev of rule.outputs.dev) {
                        let currentDevDeviceId = null
                        for (const device of currentConstraints.devices) {
                            if (device.outputs && device.outputs === dev.targets) {
                                currentDevDeviceId = device.deviceid
                                break
                            }
                        }

                        for (const device of rule.constraints.devices) {
                            if (device.outputs && device.outputs === dev.targets) {
                                if (currentDevDeviceId !== null && currentDevDeviceId !== device.deviceid) {
                                    _query = 'update dataindexes set deleted = 1 where id = ? '
                                    await connection.query(_query, [currentDevDeviceId * 100 + 10000000 + dev.outcode])
                                }

                                const dataId = device.deviceid * 100 + 10000000 + dev.outcode
                                const [result] = await connection.query(queryDataIndex, [dataId, dev.name, dev.unit && dev.unit !== null && dev.unit.trim().length > 0 ? dev.unit : '', 2, device.deviceid, ruleId, ruleId])

                                if (result.affectedRows > 0) {
                                    await connection.query(queryCurrentObs, [dataId, 0])
                                }
                                break
                            }
                        }
                    }
                }
            }
            if (Object.prototype.hasOwnProperty.call(rule, 'controllers')) {
                _query = 'update core_rule_applied set controllers = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.controllers), ruleId])
            }
            if (Object.prototype.hasOwnProperty.call(rule, 'configurations')) {
                _query = 'update core_rule_applied set configurations = ?, updated = now() where id = ?'
                await connection.query(_query, [JSON.stringify(rule.configurations), ruleId])
            }
            if (Object.prototype.hasOwnProperty.call(rule, 'used')) {
                _query = 'update core_rule_applied set used = ?, updated = now() where id = ?'
                await connection.query(_query, [rule.used, ruleId])
            }
            if (rule.timespan) {
                _query = 'update core_timespan set timespan = ?, updated = now() where id = ? and field_id = ?'
                await connection.query(_query, [JSON.stringify(rule.timespan.timespan), rule.timespan.id, rule.timespan.field_id])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method setRuleAppliedUse
     * @description 임시 저장된 룰 사용할 수 있게 변경
     */
    var setRuleAppliedUse = async(ruleId) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'update dataindexes set deleted = 0 where rule_id = ? '
            await connection.query(query, [ruleId])

            query = 'update core_rule_applied set deleted = 0, used = 1, updated = now() where id = ? '
            await connection.query(query, [ruleId])

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method getTestRuleResult
     * @description 적용된 룰을 테스트 한다
     */
    var getTestRuleResult = async(ruleId) => {
        const result = await execShellCommand('python fcore.py ' + ruleId)

        if (result.result === 200) {
            const query = 'select id,name,unit,current_observations.nvalue from dataindexes join current_observations on dataindexes.id = current_observations.data_id where dataindexes.rule_id = ? '
            const [rows] = await _pool.query(query, [ruleId])
            return {...result, rows }
        } else {
            return result
        }
    }

    /**
     * Executes a shell command and return it as a Promise.
     * @param cmd {string}
     * @return {Promise<string>}
     */
    function execShellCommand(cmd) {
        const exec = require('child_process').exec
        return new Promise((resolve, reject) => {
            exec(cmd, { cwd: '../../fcore' }, (error, stdout, stderr) => {
                if (error) {
                    console.warn(error)
                    resolve({ result: 500, error, stdout, stderr })
                } else {
                    resolve({ result: 200, stdout, stderr })
                }
            })
        })
    }

    /**
     * @method addruleapplied
     * @description 룰을 추가한다
     */
    var addruleapplied = async(rule, fieldId, prevConnection, ruleId,item) => {
        const connection = prevConnection || await _pool.getConnection(async conn => conn)
        try {
            let _query = ''

            if (!prevConnection) {
                await connection.beginTransaction()
            }
            let appliedId = null
            if (ruleId === undefined || ruleId === null) {
              if(item){ 
                if(item.platform_id!=undefined){
                  _query = 'insert into core_rule_applied ' +
                      '(id,name,updated,field_id,used,constraints,configurations,inputs,controllers,outputs,groupname,autoapplying,sched,template_id,deleted, advanced) ' +
                      'values (?,?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) '
                  let [appliedResult] = ''
                  if (rule.idx === 26) { // 데이터 통계함수일 경우 used 처리
                      [appliedResult] = await connection.query(_query, [item.platform_id,rule.name, item.fieldId, rule.used, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, 0, rule.advanced ? rule.advanced : 0])
                  } else {
                      [appliedResult] = await connection.query(_query, [item.platform_id,rule.name, item.fieldId, 0, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, rule.deleted ? rule.deleted : 0, rule.advanced ? rule.advanced : 0])
                  }
                
                  appliedId = appliedResult.insertId

                }else {
                  _query = 'insert into core_rule_applied ' +
                      '(name,updated,field_id,used,constraints,configurations,inputs,controllers,outputs,groupname,autoapplying,sched,template_id,deleted, advanced) ' +
                      'values (?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) '
                  let [appliedResult] = ''
                  if (rule.idx === 26) { // 데이터 통계함수일 경우 used 처리
                      [appliedResult] = await connection.query(_query, [rule.name, item.fieldId, rule.used, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, 0, rule.advanced ? rule.advanced : 0])
                  } else {
                      [appliedResult] = await connection.query(_query, [rule.name, item.fieldId, 0, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, rule.deleted ? rule.deleted : 0, rule.advanced ? rule.advanced : 0])
                  }
                
                  appliedId = appliedResult.insertId

                }
                }else{
                _query = 'insert into core_rule_applied ' +
                    '(name,updated,field_id,used,constraints,configurations,inputs,controllers,outputs,groupname,autoapplying,sched,template_id,deleted, advanced) ' +
                    'values (?, now(), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) '
                let [appliedResult] = ''
                if (rule.idx === 26) { // 데이터 통계함수일 경우 used 처리
                    [appliedResult] = await connection.query(_query, [rule.name, fieldId, rule.used, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, 0, rule.advanced ? rule.advanced : 0])
                } else {
                    [appliedResult] = await connection.query(_query, [rule.name, fieldId, 0, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, rule.deleted ? rule.deleted : 0, rule.advanced ? rule.advanced : 0])
                }
              
                appliedId = appliedResult.insertId
              }
            } else {
                _query = 'update core_rule_applied set name = ?, updated = now(), field_id = ?, used = ?, constraints = ?, configurations = ?, inputs = ?, controllers = ?, outputs = ?, groupname = ?, autoapplying = ?, sched = ?, template_id = ?, deleted = ?, advanced = ? where id = ?'
                await connection.query(_query, [rule.name, fieldId, 0, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.inputs), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), rule.groupname, rule.autoapplying, rule.sched, rule.idx, rule.deleted ? rule.deleted : 0, rule.advanced ? rule.advanced : 0, ruleId])
                appliedId = ruleId
            }

            let id = 30000000 + (appliedId * 10000)

            if (rule.controllers.trigger) {
                _query = 'insert into dataindexes ' +
                    '(id,rule_id,name,unit,field_id,deleted) ' +
                    'values (?, ?, ?, ?, ?, ?)'
                await connection.query(_query, [id, appliedId, `${appliedId}번 룰 트리거 처리현황`, '', fieldId, rule.deleted ? rule.deleted : 0])

                _query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
                    'values (?, now(), ?, now()) '
                await connection.query(_query, [id, 0])
            }

            if (rule.controllers.processors) {
                for (let i = 0; i < rule.controllers.processors.length; i++) {
                    id = 30000000 + (appliedId * 10000) + (100 * (i + 1))
                    _query = 'insert into dataindexes ' +
                        '(id,rule_id,name,field_id,unit,deleted) ' +
                        'values (?, ?, ?, ?, ?, ?)'
                    await connection.query(_query, [id, appliedId, `${appliedId}번 룰 ${i + 1}번 컨트롤러처리현황`, fieldId, '', rule.deleted ? rule.deleted : 0])

                    _query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
                        'values (?, now(), ?, now()) '
                    await connection.query(_query, [id, 0])
                }
            }

            if (rule.outputs.data) {
                for (const item of rule.outputs.data) {
                    id = 30000000 + (appliedId * 10000) + Number(item.outcode)
                    _query = 'insert into dataindexes ' +
                        '(id,rule_id,name,field_id,unit,deleted) ' +
                        'values (?, ?, ?, ?, ?, ?)'

                    let name = item.outputs
                    let unit = ''
                    if (item.name) {
                        name = item.name
                    }
                    if (item.unit) {
                        unit = item.unit
                    }

                    await connection.query(_query, [id, appliedId, name, fieldId, unit, rule.deleted ? rule.deleted : 0])

                    _query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
                        'values (?, now(), ?, now()) '
                    await connection.query(_query, [id, 0])
                }
            }

            if (rule.outputs.dev) {
                const queryDataIndex = 'INSERT INTO dataindexes (id,name,unit,sigdigit,device_id,rule_id,deleted) values (?, ?, ?, ?, ?, ?, ?)  ON DUPLICATE KEY UPDATE deleted = ? , rule_id = ?'
                const queryCurrentObs = 'INSERT INTO current_observations (data_id,obs_time,nvalue) values (?, now(),?)  ON DUPLICATE KEY UPDATE nvalue = 0 '

                for (const dev of rule.outputs.dev) {
                    let currentDevDeviceId = null
                    for (const device of rule.constraints.devices) {
                        if (device.outputs && device.outputs === dev.targets) {
                            currentDevDeviceId = device.deviceid
                            break
                        }
                    }

                    if (currentDevDeviceId !== null && String(currentDevDeviceId).trim().length > 0) {
                        const dataId = currentDevDeviceId * 100 + 10000000 + dev.outcode

                        /* const [result] = await connection.query(queryDataIndex, [dataId, dev.name, dev.unit && dev.unit !== null && dev.unit.trim().length > 0 ? dev.unit : '', 2, currentDevDeviceId, appliedId, rule.deleted ? rule.deleted : 0, rule.deleted ? rule.deleted : 0, appliedId])
                        if (result.affectedRows > 0) {
                            await connection.query(queryCurrentObs, [dataId, 0])
                        } */

                        const query = 'select count(*) count from dataindexes where id = ? and deleted = 0'
                        const [result] = await connection.query(query, [dataId])

                        if (result[0].count === 0) {
                            const [result] = await connection.query(queryDataIndex, [dataId, dev.name, dev.unit && dev.unit !== null && dev.unit.trim().length > 0 ? dev.unit : '', 2, currentDevDeviceId, appliedId, rule.deleted ? rule.deleted : 0, rule.deleted ? rule.deleted : 0, appliedId])
                            if (result.affectedRows > 0) {
                                await connection.query(queryCurrentObs, [dataId, 0])
                            }
                        } else {
                            throw (new Error('isnt device dataid'))
                        }
                    }
                }
            }

            if (rule.autoapplying > 0) {
                const inputs = []
                if (rule.constraints.data) {
                    for (const data of rule.constraints.data) {
                        const spliteData = data.idfmt.split(']')
                        const id = (fieldId * 100000) + Number(spliteData[spliteData.length - 1])
                        inputs.push({
                            key: data.key,
                            dataid: id
                        })
                    }
                }
                const updateQuery = 'update core_rule_applied set inputs = ?, used = 1 where id = ? '
                await connection.query(updateQuery, [JSON.stringify(inputs), appliedId])
            }

            if (rule.configurations && rule.configurations.timespan && rule.configurations.timespan.id && rule.configurations.timespan.id > 0) {
                _query = 'select count(id) count from core_timespan where id = ? and field_id = ? '
                const [rows] = await connection.query(_query, [rule.configurations.timespan.id, fieldId])

                if (rows[0].count === 0) {
                    if (rule.configurations.timespan.id > 10000) {
                        _query = 'select * from core_timespan where id = ? and field_id = -1 '
                        const [rows] = await connection.query(_query, [rule.configurations.timespan.id])

                        if (rows) {
                            _query = 'INSERT INTO core_timespan (id,field_id,timespan,name) values (?, ?, ?, ?) '
                            await connection.query(_query, [rule.configurations.timespan.id, fieldId, rows[0].timespan, rows[0].name])
                        }
                    } else {
                        const { data: timeSpanList } = await axios.get(`${config.templateApi}/timespan`)
                        const usedTimeSpanList = timeSpanList.filter((item) => item.used === 1)
                        _query = 'INSERT INTO core_timespan (id,field_id,timespan,name) values (?, ?, ?, ?) '
                        for (const timespan of usedTimeSpanList) {
                            if (timespan.idx === rule.configurations.timespan.id) {
                                await connection.query(_query, [timespan.idx, fieldId, JSON.stringify(timespan.timespan), timespan.name])
                                break
                            }
                        }
                    }
                }
            }

            if (!prevConnection) {
                await connection.commit()
                connection.release()
            }
            return appliedId
        } catch (error) {
            if (!prevConnection) {
                await connection.rollback()
                connection.release()
            }
            console.log('error')
            console.log(error)
            throw (error)
        }
    }

    /**
     * @method getruleapplied
     * @description 설정된 룰을 가져온다
     */
    var getruleapplied = async(type, id) => {
        if (type === 'rule') {
            if (id) {
                _query = 'select * from core_rule_applied where id = ? '
                const [rows] = await _pool.query(_query, [id])
                return rows[0]
            } else {
                _query = 'select * from core_rule_applied where deleted = 0'
                const [rows] = await _pool.query(_query)
                return rows
            }
        } else if (type === 'field') {
            _query = 'select * from core_rule_applied where field_id = ? and deleted = 0'
            const [rows] = await _pool.query(_query, [id])
            return rows
        }
    }

    /**
     * @method gettimespan
     * @description 해당 구역의 timespan 데이터를 가져온다
     */
    var gettimespan = async(timespanId, fieldId) => {
        _query = 'select a.* , (select b.name from fields b where b.id = a.field_id) as filed_name from core_timespan a where a.deleted = 0 '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method settimespanfielditem
     * @description 적용된 타임스팬 수정
     */
    var settimespanfielditem = async(timespanId, fieldId, timeSapn) => {
        const _query = 'update core_timespan set timespan = ? , updated = now() where id = ? and field_id = ?'
        await _pool.query(_query, [JSON.stringify(timeSapn), timespanId, fieldId])
    }

    /**
     * @method gettimespanfielditem
     * @description 해당 구역의 timespan 데이터를 가져온다
     */
    var gettimespanfielditem = async(timespanId, fieldId) => {
        _query = 'select * from core_timespan where id = ? and field_id = ?'
        const [rows] = await _pool.query(_query, [timespanId, fieldId])
        return rows[0]
    }

    /**
     * @method getgateinfo
     * @description cvtgate의 id 정보를 가져온다
     */
    var getgateinfo = async() => {
        _query = 'select * from gate_info'
        const [rows] = await _pool.query(_query)
        return rows[0]
    }

    /**
     * @method getdevicegatelist
     * @description 등록된 장비의 coupleid를 가져온다
     */
    var getdevicegatelist = async() => {
        _query = 'select DISTINCT coupleid from devices where deleted = 0'
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method setgateinfodetect
     * @description cvtgate의 detect 정보를 저장한다
     */
    var setgateinfodetect = async(detect) => {
        _query = 'update gate_info set detect = ? '
        await _pool.query(_query, [JSON.stringify(detect)])
    }

    /**
     * @method getdevicehistorycontrole
     * @description 구동기 제어이력 날짜별로 조회
     */
    var getdevicehistorycontrole = async(deviceId, sdate, edate) => {
        const query = 'select * from requests where device_id = ? and senttime BETWEEN DATE(?) AND DATE_ADD( ?, INTERVAL 1 DAY)'
        console.log(_pool.format(query, [deviceId, sdate, edate]))
        const [rows] = await _pool.query(query, [deviceId, sdate, edate])
        return rows
    }

    /**
     * @method getdevicehistory
     * @description 구동기 타임라인 상태이력을 가져온다
     */
    var getdevicehistory = async(deviceId, date) => {
        const stateId = deviceId * 100 + 10000000
            // _query = "SELECT data_id,obs_time,nvalue FROM (SELECT*,(SELECT a.nvalue !=nvalue FROM observations WHERE data_id= ? AND obs_time BETWEEN DATE(?) AND DATE(?)+1 AND obs_time< a.obs_time ORDER BY obs_time DESC LIMIT 1) chg FROM observations a WHERE data_id= ? AND obs_time BETWEEN DATE(?) AND DATE(?)+1 ORDER BY obs_time DESC) result WHERE chg=1 or chg is null ORDER BY obs_time asc ";
        let query = 'SELECT aaa.data_id,aaa.obs_time,aaa.nvalue,aaa.check_value FROM (' +
            "SELECT aa.data_id,aa.obs_time,aa.ROW,aa.nvalue,bb.obs_time AS obs_time2,bb.nvalue AS nvalue2,bb.row2,CASE WHEN aa.nvalue=bb.nvalue THEN 'Y' ELSE 'N' END AS check_value FROM (" +
            'SELECT a.*,@rownum :=@rownum+1 AS ROW FROM observations a WHERE a.data_id=? AND (@rownum :=0)=0 AND a.obs_time BETWEEN DATE(?) AND DATE_ADD( ?, INTERVAL 1 DAY) ORDER BY a.obs_time DESC) aa,(' +
            'SELECT a.*,@RNUM :=@RNUM+1 AS row2 FROM observations a,(' +
            "SELECT @RNUM :=0) a WHERE a.data_id=? AND a.obs_time BETWEEN DATE(?) AND DATE_ADD( ?, INTERVAL 1 DAY) ORDER BY a.obs_time DESC) bb WHERE aa.ROW+1=bb.row2) aaa WHERE aaa.check_value='N' ORDER BY aaa.obs_time ASC "
        let [states] = await _pool.query(query, [stateId, date, date, stateId, date, date])

        console.log(_pool.format(query, [stateId, date, date, stateId, date, date]))

        if (states.length === 0) {
            query = 'SELECT data_id,DATE(?) obs_time,nvalue from observations where data_id = ? and obs_time >= DATE(?) order by obs_time asc LIMIT 1 '
            const [statesNext] = await _pool.query(query, [date, stateId, date])
            console.log(_pool.format(query, [date, stateId, date]))
            states = statesNext
        }

        query = 'SELECT * FROM requests where device_id = ? and senttime BETWEEN DATE(?) AND DATE_ADD( ?, INTERVAL 1 DAY) ORDER BY senttime asc '
        console.log(_pool.format(query, [deviceId, date, date]))
        const [requests] = await _pool.query(query, [deviceId, date, date])

        for (const request of requests) {
            if (request.params) {
                request.params = JSON.parse(request.params)
            }
        }

        const obj = {
            states,
            requests
        }
        return obj
    }

    /**
     * @method getlastquery
     * @description farmos_api 모듈에서 마지막으로 사용한 쿼리를 보여준다. 이 함수는 테스트용으로 사용된다.
     */
    var getlastquery = function() {
        return _query
    }

    function dateToStr(format) {
        var year = format.getFullYear()
        var month = format.getMonth() + 1
        if (month < 10) month = '0' + month
        var date = format.getDate()
        if (date < 10) date = '0' + date
        return year + '-' + month + '-' + date
    }

    /**
     * @method getruletemplate
     * @description 룰 템플릿 리스트를 가져온다
     */
    var getruletemplate = async() => {
        _query = 'select * from core_rule_template where deleted = 0'
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method addruletemplate
     * @description 룰 템플릿 을 추가한다
     */
    var addruletemplate = async(rule) => {
        let _query = 'select max(idx)max from core_rule_template'
        const [result] = await _pool.query(_query)

        let idx = 100001
        if (result[0].max >= idx) {
            idx = result[0].max + 1
        }
        if(rule.platform_id){
          idx = rule.platform_id
        }
        _query = 'INSERT INTO core_rule_template (idx,name,`desc`,groupname,sched,autoapplying,constraints,configurations,controllers,outputs) ' +
            'values (?,?,?,?,?,?,?,?,?,?) '
        const [rows] = await _pool.query(_query, [idx, rule.name, rule.desc, rule.groupname, rule.sched, rule.autoapplying, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs)])
        return rows.insertId
    }

    /**
     * @method deleteruletemplatedetail
     * @description 룰 템플릿을 삭제한다
     */
    var deleteruletemplatedetail = async(idx) => {
        _query = 'update core_rule_template set deleted = 1 where idx = ?'
        await _pool.query(_query, [idx])
    }

    /**
     * @method getruletemplatedetail
     * @description 상세 템플릿을 가져온다
     */
    var getruletemplatedetail = async(idx) => {
        _query = 'select * from core_rule_template where idx = ?'
        const [rows] = await _pool.query(_query, [idx])
        return rows[0]
    }

    /**
     * @method setruletemplatedetail
     * @description 룰 템플릿을 수정한다
     */
    var setruletemplatedetail = async(idx, rule) => {
        _query = 'update core_rule_template set name = ?, `desc` =?, groupname = ?, sched = ?, autoapplying = ?, constraints = ?, configurations = ? , controllers = ?, outputs = ? where idx = ? '
        await _pool.query(_query, [rule.name, rule.desc, rule.groupname, rule.sched, rule.autoapplying, JSON.stringify(rule.constraints), JSON.stringify(rule.configurations), JSON.stringify(rule.controllers), JSON.stringify(rule.outputs), idx])
    }

    /**
     * @method addtimespan
     * @description 타임스팬을 추가한다
     */
    var addtimespan = async(item) => {
        let _query = 'select max(id) max from core_timespan where field_id = -1'
        const [rows] = await _pool.query(_query)

        let id = 100001
        if (rows[0].max >= id) {
            id = rows[0].max + 1
        }
        if(item.platform_id){
          id = item.platform_id
        }
        _query = 'INSERT INTO core_timespan (timespan,name,updated,id,field_id) ' +
            'values (?,?,now(),?,?) '
        await _pool.query(_query, [JSON.stringify(item.timespan), item.name, id, -1])
        return id
    }

    /**
     * @method deltimespan
     * @description 타임스팬을 삭제한다
     */
    var deltimespan = async(id) => {
        const _query = 'update core_timespan set deleted = 1 where id = ? and field_id = -1'
        await _pool.query(_query, [id])
    }

    /**
     * @method settimespan
     * @description 타임스팬을 수정한다
     */
    var settimespan = async(id, item) => {
        _query = 'update core_timespan set name = ?, timespan = ?  where id = ? and field_id = -1'
        await _pool.query(_query, [item.name, JSON.stringify(item.timespan), id])
    }

    /**
     * @method addrulemodule 
     * @description 사용자규칙에서 작성한 모듈을 저장한다
     */
    var addrulemodule = async(title, contents) => {
        const module_path = "../../fcore/modules/"
        var test = `${ module_path }` + `${ title }`
        console.log(test) //await writeFileAsync(${module_path}+${title}, contents)
        console.log("user rule module file saved")
    }

    /**
     * @method getobs
     * @description observations 데이터를 가져옴
     */
    var getobs = async(id, date) => {
        const _query = "SELECT distinct DATE_FORMAT(obs_time, '%Y-%m-%d %H:%i')obs_time , nvalue from observations where data_id = ? and DATE_FORMAT(obs_time, '%Y-%m-%d') = DATE_FORMAT(?, '%Y-%m-%d') order by obs_time asc"
        const [rows] = await _pool.query(_query, [1 * 10000000 + id * 100, date])
        return rows
    }

    /**
     * @method getcalibration
     * @description calibration 데이터를 가져옴
     */
    var getcalibration = async(deviceId) => {
        const query = 'select * from devices where id = ?'
        const [rows] = await _pool.query(query, [deviceId])
        if (rows.length > 0) {
            const cvtgate = await getgateinfo()
            const { data } = await axios.get(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${rows[0].coupleid}`)
            if (Object.prototype.hasOwnProperty.call(data.dsmate.opt, 'calibration') && data.dsmate.opt.calibration[deviceId] !== undefined) {
                return data.dsmate.opt.calibration[deviceId]
            } else {
                return null
            }
        } else {
            return null
        }
    }

    /**
     * @method setcalibration
     * @description calibration 데이터를 수정한다
     */
    var setcalibration = async(deviceId, calibration) => {
        try {
            const query = 'select * from devices where id = ?'
            const [rows] = await _pool.query(query, [deviceId])
            if (rows.length > 0) {
                const cvtgate = await getgateinfo()
                const { data } = await axios.get(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${rows[0].coupleid}`)
                if (Object.prototype.hasOwnProperty.call(data.dsmate.opt, 'calibration')) {
                    if (Object.prototype.hasOwnProperty.call(calibration, 'type')) {
                        data.dsmate.opt.calibration[deviceId] = calibration
                    } else {
                        delete data.dsmate.opt.calibration[deviceId]
                    }
                } else {
                    data.dsmate.opt.calibration = {}
                    data.dsmate.opt.calibration[deviceId] = calibration
                }
                await axios.put(`${config.cvgateIp}/gate/${cvtgate.uuid}/couple/${rows[0].coupleid}`, data)
            } else {
                throw (new Error('no device.'))
            }
        } catch (error) {
            console.log(error)
            throw (error)
        }
    }

    /**
     * @method getreferencelist
     * @description getreferencelist 사용자 레퍼런스 리스트 가져오기
     */
    var getreferencelist = async(reference) => {
        let query = "select id, name, unit, sigdigit, field_id from dataindexes where id like '20001%' and deleted = 0 "
        const [rows] = await _pool.query(query)

        query = "select DATE_FORMAT(obs_time,'%Y-%m-%d') date from observations where data_id = ? GROUP BY DATE_FORMAT(obs_time,'%Y-%m-%d') "
        for (const row of rows) {
            const [subRows] = await _pool.query(query, [row.id])
            row.dateList = subRows
        }
        return rows
    }


    /**
     * @method addreferencedata
     * @description addreferencedata 데이터 추가
     */
    var addreferencedata = async(reference) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = "select max(id)id from dataindexes where id like '20001%'"
            const [rows] = await connection.query(query)

            let id = 1
            if (rows.length > 0) {
                if (rows[0].id !== null) {
                    const tempId = rows[0].id + 1
                    id = tempId - 20001000
                }
            }
            let newId = 20001000 + id
            if(reference.platform_id){
              newId = reference.platform_id
            }
            query = 'insert into dataindexes (id,name,unit,sigdigit,field_id) values (?, ?, ?, ?, ?)'
            await connection.query(query, [newId, reference.name, reference.unit, reference.sigdigit, reference.field_id])

            let lastData = null
            for (const data of reference.data) {
                query = 'insert into observations (data_id,obs_time,nvalue) values (?, ?, ?)'
                const currentTime = new Date(data.date).getTime()
                let lastTime = null
                if (lastData !== null) {
                    lastTime = new Date(lastData.date).getTime()
                }
                if (lastData === null || lastTime < currentTime) {
                    lastData = data
                }
                await connection.query(query, [newId, data.date, data.value])
            }
            if (lastData !== null) {
                query = 'INSERT INTO current_observations (data_id,nvalue,obs_time,modified_time) values (?, ?, ?, now()) '
                await connection.query(query, [newId, lastData.value, lastData.date])
            }

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method deletereferencedata
     * @description deletereferencedata 데이터 삭제
     */
    var deletereferencedata = async(dataId) => {
        const query = 'update dataindexes set deleted = 1 where id = ?'
        await _pool.query(query, [dataId])
    }

    /**
     * @method setreferencedata
     * @description setreferencedata 데이터 변경
     */
    var setreferencedata = async(dataId, reference) => {
        if (reference.data) {
            const query = 'INSERT INTO observations (data_id,obs_time,nvalue) select ?, ?, ? from dual WHERE NOT EXISTS ( SELECT * FROM observations WHERE data_id = ? AND obs_time = ? ) '
            for (const data of reference.data) {
                await _pool.query(query, [dataId, data.date, data.value, dataId, data.date])
            }
        } else {
            const query = 'update dataindexes set name = ? , unit = ? , sigdigit = ?, field_id = ? where id = ?'
            await _pool.query(query, [reference.name, reference.unit, reference.sigdigit, reference.field_id, dataId])
        }
    }

    var isLiveProcess = async(processPid) => {
        try {
            console.log(processPid)
            const pidFile = processPid.endsWith('.pid') ? processPid : processPid + '.pid'
            let liveCheck = false
            let pid = null
            if (shell.cat(`/var/run/${pidFile}`).code === 0) {
                pid = shell.cat(`/var/run/${pidFile}`).stdout.replace(/[^0-9]/g, '')
                console.log(`pid : ${pid}`)

                // 프로세스 실행 체크
                const tasks = await psList('pid')
                for (const task of tasks) {
                    if (String(task.pid) === String(pid)) {
                        liveCheck = true
                        break
                    }
                }
                /* if (!liveCheck) {
                    shell.exec(`rm -f /var/run/${pidFile}`)
                } */
            } else {
                console.log('pid 파일 없음..')
            }
            return { live: liveCheck, pidFile, pidNumber: pid }
        } catch (error) {
            console.log(error)
            return null
        }
    }

    /**
     * @method execProcess
     * @description execProcess 프로세스 동작 관리
     */
    var execProcess = async(idx, type) => {
        const process = await getProcess(idx)
        if (!process) {
            throw (new Error('프로세스 정보가 없습니다.'))
        } else {
            const result = await isLiveProcess(process.pid)
            if (!result) {
                throw (new Error(`fail to ${process.name} execProcess`))
            }
            process.live = result.live
            process.pidFile = result.pidFile
            process.pidNumber = result.pidNumber
        }

        if (type === 'start') {
            try {
                if (!process.live && process.cmd_start) {
                    if (shell.exec(process.cmd_start).code === 0) {
                        console.log(`${process.name} start`)
                    } else {
                        throw (new Error(`fail to ${process.name} start`))
                    }
                    return { code: 200, msg: `${process.name} 실행 하였습니다.` }
                } else {
                    console.log('이미 실행중')
                    return { code: 202, msg: `${process.name} 이미 실행 중입니다.` }
                }
            } catch (error) {
                throw (new Error(`fail to ${process.name} start`))
            }
        }
        if (type === 'stop') {
            try {
                if (process.live && process.cmd_stop) {
                    if (shell.exec(process.cmd_stop).code === 0) {
                        console.log(`${process.name} stop`)
                    } else {
                        throw (new Error(`fail to ${process.name} stop`))
                    }
                    return { code: 200, msg: `${process.name} 중지 하였습니다.` }
                } else {
                    console.log('이미 중지중')
                    return { code: 202, msg: `${process.name} 이미 중지중 입니다.` }
                }
            } catch (error) {
                throw (new Error(`fail to ${process.name} stop`))
            }
        } else if (type === 'stopForce') {
            try {
                if (process.live && process.pidNumber) {
                    const tasks = await psList('pid')
                    for (const task of tasks) {
                        if (String(task.pid) === String(process.pidNumber)) {
                            await terminateAsync(process.pidNumber)
                            console.log('프로그램 종료')
                            break
                        }
                    }
                    /* if (shell.exec(`rm -f /var/run/${process.pidFile}`).code === 0) {
                        console.log(`${process.pidFile} 파일 삭제`)
                    } else {
                        console.log(`fail to ${process.pidFile} delete`)
                    } */
                    return { code: 200, msg: `${process.name} 종료 하였습니다.` }
                } else {
                    console.log(`${process.name} 미실행 중`)
                    return { code: 202, msg: `${process.name} 실행 상태가 아닙니다.` }
                }
            } catch (error) {
                console.log(error)
                throw (new Error(`fail to ${process.name} stop`))
            }
        }
    }

    /**
     * @method addProcess
     * @description addProcess 프로세스 추가
     */
    var addProcess = async(process) => {
        const query = 'insert into process (name,pid,cmd_start,cmd_stop,log,`desc`,deleted) values (?, ?, ?, ?, ?, ?, ?)'
        await _pool.query(query, [process.name, process.pid, process.cmd_start, process.cmd_stop, process.log, process.desc, 0])
    }

    /**
     * @method setProcess
     * @description setProcess 프로세스 수정
     */
    var setProcess = async(process) => {
        const prevProcess = await getProcess(process.idx)
        if (prevProcess) {
            const result = await isLiveProcess(prevProcess.pid)
            if (result && result.live) {
                throw (new Error('실행중인 프로세스 입니다.'))
            } else if (!result || (result && !result.live)) {
                const query = 'update process set name = ?, pid = ?, cmd_start = ?, cmd_stop = ?, log = ?, `desc` = ?, updated = now() where idx = ?'
                await _pool.query(query, [process.name, process.pid, process.cmd_start, process.cmd_stop, process.log, process.desc, process.idx])
            }
        } else {
            throw (new Error('프로세스 정보가 없습니다.'))
        }
    }

    /**
     * @method delProcess
     * @description delProcess 프로세스 삭제
     */
    var delProcess = async(idx) => {
        const prevProcess = await getProcess(idx)
        if (prevProcess) {
            const result = await isLiveProcess(prevProcess.pid)
            if (result && result.live) {
                throw (new Error('실행중인 프로세스 입니다.'))
            } else if (!result || (result && !result.live)) {
                const query = 'update process set deleted = 1, updated = now() where idx = ?'
                await _pool.query(query, [idx])
            }
        } else {
            const query = 'update process set deleted = 1, updated = now() where idx = ?'
            await _pool.query(query, [idx])
        }
    }

    /**
     * @method StopProcess
     * @description stopProcess 프로세스 중단
     */
    var StopProcess = async(processid) => {
        var directory=''
        var cmd=''
        if(processid=='fcore'){
            directory='../../fcore/'
            cmd='python fcore.py stop'
        }else if(processid=='cvtgate'){
            directory='../../cvtgate/gate'
            cmd='python3 couplemng.py stop'
        }
        try{
            const exec = require('child_process').exec
            let stop = new Promise((resolve, reject) => {
                    console.log(processid+" stop..")     
                    exec(cmd, { cwd: directory }, (error, stderr) => {
                        if (error) {
                            console.log(error)
                            resolve({ result: 500, error, stderr })
                        } else {
                            resolve({ result: 200, stderr })
                        }
                    })
                })
                stop.then(function(){
                    console.log("stop process successful")
                })
            } catch (error) {
                throw (error)
            }
        }

    /**
     * @method StartProcess
     * @description startProcess 프로세스 시작
     */
    var StartProcess = async(processid) => {
        var directory=''
        var cmd=''
        if(processid=='fcore'){
            directory='../../fcore/'
            cmd='sudo python fcore.py start'
        }else if(processid=='cvtgate'){
            directory='../../cvtgate/gate'
            cmd='sudo python3 couplemng.py start'
        }
        try{
            const exec = require('child_process').exec
            let stop = new Promise((resolve, reject) => {
                    console.log(processid+" start..")     
                    exec(cmd, { cwd: directory }, (error, stderr) => {
                        if (error) {
                            console.log(error)
                            resolve({ result: 500, error, stderr })
                        } else {
                            resolve({ result: 200, stderr })
                        }
                    })
                })
                stop.then(function(){
                    console.log("start process successful")
                })
            } catch (error) {
                throw (error)
            }
        }

    /**
     * @method getprocesslog
     * @description getprocessLog 프로세스 로그 정보
     */
    var getprocesslog = async(idx) => {
        const process = await getProcess(idx)

        let data = ''
        if (process) {
            data = await readLastLines.read(process.log, 100)
        } else {
            throw (new Error('프로세스 정보가 없습니다.'))
        }

        let d = new Date()
        d = new Date(d.getTime() - 3000000)
        const time = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()

        return { data, time }
    }

    /**
     * @method getProcessList
     * @description getProcessList 프로세서 리스트 조회
     */
    var getProcessList = async _ => {
        const query = 'select * from process where deleted = 0'
        const [rows] = await _pool.query(query)

        for (const process of rows) {
            const result = await isLiveProcess(process.pid)
            if (result) {
                process.live = result.live
            } else {
                process.live = false
            }
        }
        return rows
    }

    /**
     * @method getProcess
     * @description getProcess 프로세서 조회
     */
    var getProcess = async(idx) => {
        const query = 'select * from process where deleted = 0 and idx = ?'
        const [rows] = await _pool.query(query, [idx])
        return rows[0]
    }

    /**
     * @method getrulestat
     * @description getrulestate 룰 최신 처리 이력
     */
    var getrulestate = async _ => {
        const query = "SELECT dataindexes.id,dataindexes.rule_id,dataindexes.name,dataindexes.unit,dataindexes.sigdigit,current_observations.obs_time,current_observations.nvalue,core_rule_applied.field_id,core_rule_applied.used,core_rule_applied.NAME AS rule_name FROM dataindexes JOIN current_observations ON dataindexes.id=current_observations.data_id JOIN core_rule_applied ON dataindexes.rule_id=core_rule_applied.id WHERE dataindexes.id LIKE '3%%%%%%00' AND core_rule_applied.deleted=0 ORDER BY core_rule_applied.id DESC"
        const [rows] = await _pool.query(query)
        return rows
    }

    /**
     * @method getconfiguration
     * @description getconfiguration 파머스 환경설정 읽어오기
     */
    var getconfiguration = async(type) => {
        const query = 'SELECT * from configuration where type = ?'
        const [rows] = await _pool.query(query, [type])
        return rows[0]
    }

    /**
     * @method setconfiguration
     * @description setconfiguration 파머스 환경설정 변경
     */
    var setconfiguration = async(type, configuration) => {
        const query = 'update configuration set configuration = ?, lastupdated = now() where type = ?'
        const [rows] = await _pool.query(query, [JSON.stringify(configuration), type])
        return rows
    }

    /**
     * @method addobsbackup
     * @description addobsbackup Observation 백업 추가
     */
    var addobsbackup = async(uploadData) => {
        let d = new Date()
        d = new Date(d.getTime() - 3000000)
        const uploadTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        const newfileName = `${uploadTime}.log`
        let isProcessed = true
        try {
            const fileData = Buffer.from(uploadData.data, 'base64')
            const subPath = _path.join(global.rootPath, config.backupPath, config.backupObs) 
	        !fs.existsSync(subPath) && fs.mkdirSync(subPath)
            await writeFileAsync(`${subPath}/${newfileName}`, fileData)
            console.log('obs file save')

            const file = await readFileAsync(`${subPath}/${newfileName}`, 'utf8')
            const obsList = file.split('\n')
            const query = 'INSERT INTO observations (data_id,obs_time,nvalue) select ?, ?, ? from dual WHERE NOT EXISTS ( SELECT * FROM observations WHERE data_id = ? AND obs_time = ? ) '
            for (const obsString of obsList) {
                try {
                    if (obsString.trim().length <= 0) {
                        continue
                    }
                    const obsJson = JSON.parse(obsString)
                    for (const key in obsJson.content) {
                        if (key === 'time') { continue }
                        if (Object.prototype.hasOwnProperty.call(obsJson.content, key)) {
                            const element = obsJson.content[key]
                            const dataIdObs = 10000000 + Number(key) * 100 + 1
                            const dataIdState = 10000000 + Number(key) * 100

                            await _pool.query(query, [dataIdObs, obsJson.content.time, element[0], dataIdObs, obsJson.content.time])
                            await _pool.query(query, [dataIdState, obsJson.content.time, element[1], dataIdState, obsJson.content.time])
                        }
                    }
                } catch (error) {
                    isProcessed = false
                    console.log(error)
                }
            }
        } catch (error) {
            isProcessed = false
            console.log(error)
        }
        const pcTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        const query = 'INSERT INTO uploadhistory (uptype,originalfile,filepath,uptime,pctime,processed,meta) values(?,?,?,?,?,?,?) '
        await _pool.query(query, [uploadData.type, uploadData.filename, newfileName, uploadTime, pcTime, isProcessed, JSON.stringify(uploadData.meta)])

        return isProcessed
    }

    /**
     * @method addimageExperiment
     * @description addimageExperiment 실험 사진 저장
     */
    var addimageExperiment = async(uploadData, userIdx) => {
        let d = new Date()
        const newfileName = `${d.getTime()}.png`
        let filePath = ''
        d = new Date(d.getTime() - 3000000)
        const uploadTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        let isProcessed = true

        try {
            const fileData = Buffer.from(uploadData.data.split(',')[1], 'base64')
            const subPath = _path.join(global.rootPath, config.backupPath, config.backupImage, String(uploadData.meta.dataId)) 
	        !fs.existsSync(subPath) && fs.mkdirSync(subPath)
            await writeFileAsync(`${subPath}/${newfileName}`, fileData)
            console.log('image save')

            const dataId = uploadData.meta.dataId
            filePath = `${uploadData.meta.dataId}/${newfileName}`
            const query = 'insert into gallery (data_id,obs_time,path,meta,source_id) values (?, ?, ?, ?, ?) '
                // console.log(_pool.format(query, [dataId, uploadData.meta.date, filePath, JSON.stringify(uploadData.meta.meta), userIdx]))
            await _pool.query(query, [dataId, uploadData.meta.date, filePath, JSON.stringify(uploadData.meta.meta), userIdx])
        } catch (error) {
            console.log(error)
            isProcessed = false
        }
        const pcTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        const query = 'INSERT INTO uploadhistory (uptype,originalfile,filepath,uptime,pctime,processed,meta) values(?,?,?,?,?,?,?) '
        await _pool.query(query, [uploadData.type, uploadData.filename, filePath, uploadTime, pcTime, isProcessed, JSON.stringify(uploadData.meta)])

        return isProcessed
    }

    /**
     * @method addimage
     * @description addimage 사진 저장
     */
    var addimage = async(uploadData) => {
        let d = new Date()
        console.log(uploadData)
        const newfileName = `${d.getTime()}.png`
        let filePath = ''
        d = new Date(d.getTime() - 3000000)
        const uploadTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        let isProcessed = true

        try {
            const fileData = Buffer.from(uploadData.data, 'base64')
            const subPath = _path.join(global.rootPath, config.backupPath, config.backupImage, String(uploadData.meta.deviceId)) 
            !fs.existsSync(subPath) && fs.mkdirSync(subPath)

            await writeFileAsync(`${subPath}/${newfileName}`, fileData)
            console.log('image save')

            const dataId = 1 * 10000000 + uploadData.meta.deviceId * 100 + 10
            filePath = `${uploadData.meta.deviceId}/${newfileName}`
            const query = 'insert into gallery (data_id,obs_time,path,meta) values (?, ?, ?, ?) '
            await _pool.query(query, [dataId, uploadData.meta.date, filePath, JSON.stringify(uploadData.meta.meta)])
        } catch (error) {
            console.log(error)
            isProcessed = false
        }
        const pcTime = d.getFullYear().toString() + '-' + ((d.getMonth() + 1).toString().length === 2 ? (d.getMonth() + 1).toString() : '0' + (d.getMonth() + 1).toString()) + '-' + (d.getDate().toString().length === 2 ? d.getDate().toString() : '0' + d.getDate().toString()) + ' ' + (d.getHours().toString().length === 2 ? d.getHours().toString() : '0' + d.getHours().toString()) + ':' + ((parseInt(d.getMinutes() / 5) * 5).toString().length === 2 ? (parseInt(d.getMinutes() / 5) * 5).toString() : '0' + (parseInt(d.getMinutes() / 5) * 5).toString()) + ':' + d.getSeconds()
        const query = 'INSERT INTO uploadhistory (uptype,originalfile,filepath,uptime,pctime,processed,meta) values(?,?,?,?,?,?,?) '
        await _pool.query(query, [uploadData.type, uploadData.filename, filePath, uploadTime, pcTime, isProcessed, JSON.stringify(uploadData.meta)])

        return isProcessed
    }

    /**
     * @method delImageId
     * @description delImageId 해당 아이디로 사진 삭제
     */
    var delImageId = async(id) => {
        const query = 'update gallery set deleted = 1 where id = ?'
        await _pool.query(query, [id])
    }

    /**
     * @method getimagelist
     * @description getimagelist 사진 가져오기
     */
    var getimagelist = async(id, sDate, eDate) => {
        let query = ''
        if (sDate && eDate) {
            query = "select * from gallery where data_id = ? and DATE_FORMAT(obs_time, '%Y-%m-%d') between DATE_FORMAT(?, '%Y-%m-%d') and DATE_FORMAT(?, '%Y-%m-%d') and deleted = 0 order by obs_time desc "
            const [result] = await _pool.query(query, [id, sDate, eDate])
            return result
        } else {
            query = 'select * from gallery where data_id = ? and deleted = 0 order by obs_time desc'
            const [result] = await _pool.query(query, [id])
            return result
        }
    }

    /**
     * @method getRecentImageList
     * @description getRecentImageList 최근 사진 가져오기
     */
    var getRecentImageList = async(idList) => {
        console.log(idList)
        query = "select a.* from gallery a, dataindexes b  where a.data_id = b.id  and a.deleted = 0  and b.deleted = 0  and data_id and a.data_id in (?) order by a.obs_time  desc limit 2  "
            // query = "select a.* from gallery a, dataindexes b  where a.data_id = b.id  and a.deleted = 0  and b.deleted = 0  and data_id order by a.obs_time  desc limit 2  "
        const [result] = await _pool.query(query, [idList.id])
        console.log(result)
        return result
    }

    /**
     * @method getDeviceName
     * @description getDeviceName 장비 이름 가져오기
     */
    var getDeviceName = async(id) => {
        const _query = 'select name , devindex from devices where id = ?'
        const [rows] = await _pool.query(_query, [id])
        return rows
    }

    /**
     * @method getDeviceErrorTime
     * @description getDeviceErrorTime 장비 가동율  데이터를 가져옴
     */
    var getDeviceErrorTime = async(id) => {
        let _query = 'select round(((obs_cal_data.real_total_time - obs_cal_data.total_sub_time) / obs_cal_data.real_total_time) * 100 , 2) as work_per  '
        _query += 'from '
        _query += '(select sum(obs_data.sub_time) + obs_data.sub_now_time as total_sub_time, TIMESTAMPDIFF(minute, min(obs_data.min_dt), max(obs_data.max_dt)) as real_total_time  '
        _query += 'from '
        _query += '(select pre_rank_obs.obs_time as pre_obs_time, next_rank_obs.obs_time as next_obs_time, '
        _query += 'TIMESTAMPDIFF(minute, pre_rank_obs.obs_time, next_rank_obs.obs_time) as sub_time, '
        _query += 'max_min_dt.min_dt, max_min_dt.max_dt, '
        _query += 'TIMESTAMPDIFF(minute, DATE_FORMAT(max_min_dt.max_dt, "%Y-%m-%d %H:%i"), DATE_FORMAT(now(), "%Y-%m-%d %H:%i")) as sub_now_time '
        _query += 'from '
        _query += '(select @RNUM := @RNUM +1 AS ROWNUM, data.* '
        _query += 'from '
        _query += '(select * from observations where data_id = ? and DATE_FORMAT(obs_time, "%Y-%m-%d") >= date_add(now(), interval-1 month ) order by obs_time asc) as data, '
        _query += '(select @RNUM :=0) R) as pre_rank_obs, '
        _query += '(select @rank:= @rank+1 AS rank, data.*  '
        _query += 'from '
        _query += '(select * from observations where data_id = ? and DATE_FORMAT(obs_time, "%Y-%m-%d") >= date_add(now(), interval-1 month ) order by obs_time asc) as data,  '
        _query += '(select @rank :=0) R) as next_rank_obs,  '
        _query += '(select min(obs_time) as min_dt, max(obs_time) as max_dt, data_id  from observations  where data_id = ? and obs_time >= date_add(now(), interval-1 month )) max_min_dt  '
        _query += 'where pre_rank_obs.rownum + 1 = next_rank_obs.rank '
        _query += 'and pre_rank_obs.data_id = next_rank_obs.data_id '
        _query += 'and pre_rank_obs.data_id = max_min_dt.data_id) as obs_data '
        _query += 'where obs_data.sub_time >= 5 or obs_data.sub_now_time >= 5  '
        _query += 'group by obs_data.sub_now_time) as obs_cal_data '

        const [rows] = await _pool.query(_query, [1 * 10000000 + id * 100, 1 * 10000000 + id * 100, 1 * 10000000 + id * 100])
        return rows
    }

    /**
     * @method getDeviceRestoreTime
     * @description getDeviceRestoreTime 장비 복구시간  데이터를 가져옴
     */
    var getDeviceRestoreTime = async(id) => {
        let _query = 'select ifnull((round(avg(obs_data.sub_time),0) + round(avg(obs_data.sub_now_time),0)),0) as avg_error_time '
        _query += 'from '
        _query += '(select TIMESTAMPDIFF(minute, pre_rank_obs.obs_time, next_rank_obs.obs_time) as sub_time, '
        _query += 'TIMESTAMPDIFF(minute, DATE_FORMAT(max_min_dt.max_dt, "%Y-%m-%d %H:%i"), DATE_FORMAT(now(), "%Y-%m-%d %H:%i")) as sub_now_time '
        _query += 'from   '
        _query += '(select @RNUM := @RNUM +1 AS ROWNUM, data.* '
        _query += 'from '
        _query += '(select * from observations where data_id = ? and obs_time >= date_add(now(), interval-1 month ) order by obs_time asc) data, '
        _query += '(select @RNUM :=0) R) as pre_rank_obs, '
        _query += '(select @rank:= @rank+1 AS rank, data.* '
        _query += 'from '
        _query += '(select * from observations where data_id = ? and obs_time >= date_add(now(), interval-1 month ) order by obs_time asc) data, '
        _query += '(select @rank :=0) R) as next_rank_obs, '
        _query += '(select min(obs_time) as min_dt, max(obs_time) as max_dt, data_id from observations where data_id = ? and obs_time >= date_add(now(), interval-1 month )) as max_min_dt '
        _query += 'where pre_rank_obs.rownum + 1 = next_rank_obs.rank '
        _query += 'and pre_rank_obs.data_id = next_rank_obs.data_id '
        _query += 'and pre_rank_obs.data_id = max_min_dt.data_id) as obs_data  '
        _query += 'where obs_data.sub_time >= 5 or obs_data.sub_now_time >= 5  '
        _query += 'group by obs_data.sub_now_time '

        const [rows] = await _pool.query(_query, [1 * 10000000 + id * 100, 1 * 10000000 + id * 100, 1 * 10000000 + id * 100])
        return rows
    }

    /**
     * @method getDataErrorCheckCount
     * @description getDataErrorCheckCount 장비상태값 체크
     */

    var getDataErrorCheckCount = async(id) => {
        const _query = 'select count(data_id) as count from observations where data_id = ? and DATE_FORMAT(obs_time, "%Y-%m-%d") >= date_add(now(), interval-1 month ) group by data_id'
        const [rows] = await _pool.query(_query, [1 * 10000000 + id * 100])
        return rows
    }

    /**
     * @method getUnitList
     * @description getUnitList Unit 리스트 조회
     */
    var getUnitList = async() => {
        const _query = 'select * from units'
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method putDeviceRestore
     * @description putDeviceRestore 장비 복구
     */
    var putDeviceRestore = async(id) => {
        const connection = await _pool.getConnection(async conn => conn)
        let isSuccess = false
        try {
            await connection.beginTransaction()
            _query = 'update devices set deleted = 0 where deleted = 1 and id = ? '
            const [result] = await connection.query(_query, [id])
            if (result.affectedRows === 1) {
                _query = 'select nodeid from devices where id = ? '
                const [rows] = await connection.query(_query, [id])
                _query = 'update devices set deleted = 0 where deleted = 1 and devindex is null and nodeid = ? '
                await connection.query(_query, [rows[0].nodeid])
                isSuccess = true
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
        return isSuccess
    }

    /**
     * @method getObservationCount
     * @description getObservationCount 상관관계 분석데이터 observation 갯수 가져오기
     */
    var getObservationCount = async(deviceId, sdate, edate, func, dateValue) => {
        let _query = ''

        if (dateValue === 'min') {
            if (func === 'min') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i")  as obs_time, round(min(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) aa '
            } else if (func === 'max') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i")  as obs_time, round(max(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) aa '
            } else if (func === 'avg') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i")  as obs_time, round(avg(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) aa '
            }
        } else if (dateValue === 'hour') {
            if (func === 'min') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H")  as obs_time, round(min(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) aa '
            } else if (func === 'max') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H")  as obs_time, round(max(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) aa '
            } else if (func === 'avg') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H")  as obs_time, round(avg(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) aa '
            }
        } else if (dateValue === 'day') {
            if (func === 'min') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d")  as obs_time, round(min(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) aa '
            } else if (func === 'max') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d")  as obs_time, round(max(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) aa '
            } else if (func === 'avg') {
                _query = 'select aa.obs_time, aa.nvalue, concat((select name from devices where id = aa.device_id), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d")  as obs_time, round(avg(nvalue),2) as nvalue, (select name from dataindexes where id =?) as name, (select device_id from dataindexes where id =?) as device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) aa '
            }
        } else if (dateValue === 'week') {
            if (func === 'min') {
                _query = 'SELECT aa.obs_time, aa.nvalue, Concat((SELECT name FROM   devices WHERE  id = aa.device_id), aa.name) AS name FROM   (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(Min(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(Min(nvalue), 2) AS nvalue, (SELECT a.name FROM   dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM dataindexes a WHERE  id = data_id) AS device_id FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) aa '
            } else if (func === 'max') {
                _query = 'SELECT aa.obs_time, aa.nvalue, Concat((SELECT name FROM   devices WHERE  id = aa.device_id), aa.name) AS name FROM   (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(max(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(max(nvalue), 2) AS nvalue, (SELECT a.name FROM   dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM dataindexes a WHERE  id = data_id) AS device_id FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) aa  '
            } else if (func === 'avg') {
                _query = 'SELECT aa.obs_time, aa.nvalue, Concat((SELECT name FROM   devices WHERE  id = aa.device_id), aa.name) AS name FROM   (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(avg(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(avg(nvalue), 2) AS nvalue, (SELECT a.name FROM   dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM dataindexes a WHERE  id = data_id) AS device_id FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) aa   '
            }
        }
        let [rows] = ''
        if (dateValue === 'week') {
            [rows] = await _pool.query(_query, [sdate, edate, deviceId, sdate, edate])
        } else {
            [rows] = await _pool.query(_query, [deviceId, deviceId, deviceId, sdate, edate])
        }

        return rows
    }

    /**
     * @method getCurrentCount
     * @description getCurrentCount 상관관계 분석데이터 current_observation 갯수 가져오기
     */
    var getCurrentCount = async(deviceId) => {
        const _query = 'select aa.obs_time, aa.nvalue, concat(ifnull((select name from   devices where  id = aa.device_id),""), aa.name) as name from (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time, round(nvalue,2) as nvalue, (select name from dataindexes where id =?) as name,  (select device_id from dataindexes where id =?) as device_id FROM current_observations WHERE data_id = ?) aa '
        const [rows] = await _pool.query(_query, [deviceId, deviceId, deviceId])
        return rows
    }

    /**
     * @method getCorrelationData
     * @description getCorrelationData 상관관계 분석 데이터를 가져온다
     */
    var getCorrelationData = async(maxId, sdate1, edate1, deviceId, sdate2, edate2, func, dateValue) => {
        let _query = ''

        if (dateValue === 'min') {
            if (func === 'min') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time, Round(min(nvalue), 2) AS nvalue, (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC  '
            } else if (func === 'max') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time, Round(max(nvalue), 2) AS nvalue,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id  FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC  '
            } else if (func === 'avg') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT Date_format(obs_time, "%Y-%m-%d %H:%i") AS obs_time, Round(avg(nvalue), 2) AS nvalue,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id  FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H:%i") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC  '
            }
        } else if (dateValue === 'hour') {
            if (func === 'min') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time, round(min(nvalue), 2) AS nvalue,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'max') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time, round(max(nvalue), 2) AS nvalue ,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'avg') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d %H") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d %H") AS obs_time, round(avg(nvalue), 2) AS nvalue ,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d %H") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            }
        } else if (dateValue === 'day') {
            if (func === 'min') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM   (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time, round(min(nvalue), 2) AS nvalue ,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'max') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM   (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time, round(max(nvalue), 2) AS nvalue ,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'avg') {
                _query = 'SELECT a.obs_time, b.nvalue, concat(ifnull((select name from   devices where  id = b.device_id),""), b.name) as name FROM   (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time FROM   observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP BY date_format(obs_time, "%Y-%m-%d") ORDER BY obs_time ASC) a LEFT OUTER JOIN (SELECT date_format(obs_time, "%Y-%m-%d") AS obs_time, round(avg(nvalue), 2) AS nvalue ,  (select a.name from dataindexes a where a.id = data_id) as name, (select a.device_id from dataindexes a where id = data_id) as device_id FROM observations WHERE  data_id = ? AND date_format(obs_time, "%Y-%m-%d") BETWEEN date_format(?, "%Y-%m-%d") AND date_format(?, "%Y-%m-%d") GROUP  BY date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) b ON a.obs_time =  b.obs_time ORDER  BY a.obs_time ASC '
            }
        } else if (dateValue === 'week') {
            if (func === 'min') {
                _query = 'SELECT a.obs_time, b.nvalue, Concat(Ifnull((SELECT name FROM   devices WHERE  id = b.device_id), ""), b.name) AS name FROM   (SELECT concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC limit 1) a LEFT OUTER JOIN (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(Min(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(Min(nvalue), 2) AS nvalue, (SELECT a.name FROM dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM   dataindexes a WHERE  id = data_id) AS device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) b ON a.obs_time = b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'max') {
                _query = 'SELECT a.obs_time, b.nvalue, Concat(Ifnull((SELECT name FROM   devices WHERE  id = b.device_id), ""), b.name) AS name FROM   (SELECT concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC limit 1) a LEFT OUTER JOIN (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(max(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(max(nvalue), 2) AS nvalue, (SELECT a.name FROM dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM   dataindexes a WHERE  id = data_id) AS device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) b ON a.obs_time = b.obs_time ORDER  BY a.obs_time ASC '
            } else if (func === 'avg') {
                _query = 'SELECT a.obs_time, b.nvalue, Concat(Ifnull((SELECT name FROM   devices WHERE  id = b.device_id), ""), b.name) AS name FROM   (SELECT concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time FROM   observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC limit 1) a LEFT OUTER JOIN (select cc.*, concat(date_format(?, "%Y-%m-%d") , "~", date_format(?, "%Y-%m-%d")) as obs_time from (select Round(avg(aa.nvalue), 2) AS nvalue, aa.name, aa.device_id from (SELECT Date_format(obs_time, "%Y-%m-%d") AS obs_time, Round(avg(nvalue), 2) AS nvalue, (SELECT a.name FROM dataindexes a WHERE  a.id = data_id) AS name, (SELECT a.device_id FROM   dataindexes a WHERE  id = data_id) AS device_id FROM observations WHERE  data_id = ? AND Date_format(obs_time, "%Y-%m-%d") BETWEEN Date_format(?, "%Y-%m-%d") AND Date_format(?, "%Y-%m-%d") GROUP  BY Date_format(obs_time, "%Y-%m-%d") ORDER  BY obs_time ASC) aa GROUP  BY aa.name, aa.device_id) cc) b ON a.obs_time = b.obs_time ORDER  BY a.obs_time ASC '
            }
        }
        let [rows] = ''
        if (dateValue === 'week') {
            [rows] = await _pool.query(_query, [sdate1, edate1, maxId, sdate1, edate1, sdate1, edate1, deviceId, sdate2, edate2])
        } else {
            [rows] = await _pool.query(_query, [maxId, sdate1, edate1, deviceId, sdate2, edate2])
        }

        return rows
    }

    /**
     * @method   getRuleStatic
     * @description getRuleStatic  데이터 통계룰 리스트 가져오기
     */
    var getRuleStatic = async() => {
        const _query = 'select id ,name, updated ,field_id ,used ,deleted ,constraints ,configurations ,inputs ,controllers ,outputs ,autoapplying ,sched ,groupname ,template_id from core_rule_applied where template_id = "26" and deleted ="0" '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method   getInvestigator
     * @description getInvestigator  조사자 리스트 조회
     */
    var getInvestigator = async() => {
        const _query = 'select id, userid, name, phone from farmos_user where privilege = "investigator" and deleted = 0 '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method addInvestigator
     * @description addInvestigator 조사자 추가
     */
    var addInvestigator = async(data) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from farmos_user where phone = ? and deleted = 0 '
            const [result] = await connection.query(query, [data.phone])

            if (result.length > 0) {
                throw (new Error('중복되는 전화번호가 있습니다.'))
            }

            query = 'select count(id) count from farmos_user where privilege = "investigator" '
            const [result2] = await connection.query(query)
            if(data.platform_id){
              query = 'insert into farmos_user (id,userid,passwd,privilege,name,phone,lastupdated) values (?,?, password(?),?,?,?, now())'
              await connection.query(query, [data.platform_id,'investigator_' + (result2[0].count + 1), 'investigatorinvestigator', 'investigator', data.name, data.phone])

            }else {
              query = 'insert into farmos_user (userid,passwd,privilege,name,phone,lastupdated) values (?, password(?),?,?,?, now())'
              await connection.query(query, ['investigator_' + (result2[0].count + 1), 'investigatorinvestigator', 'investigator', data.name, data.phone])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method setInvestigator
     * @description addInvestigator 조사자 수정
     */
    var setInvestigator = async(data) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from farmos_user where phone = ? and deleted = 0 and id != ?'
            const [result] = await connection.query(query, [data.phone, data.id])

            if (result.length > 0) {
                throw (new Error('중복되는 전화번호가 있습니다.'))
            }

            query = 'update farmos_user set name = ?, phone = ? , lastupdated = now() where id = ?'
            await connection.query(query, [data.name, data.phone, data.id])
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method delInvestigator
     * @description delInvestigator 조사자 삭제
     */
    var delInvestigator = async(data) => {
        const query = 'update farmos_user set deleted = 1 , lastupdated = now() where id = ?'
        await _pool.query(query, [data])
    }

    /**
     * @method   getExperimentByInvestigator
     * @description getExperimentBySample  연구 조사자 맵핑 리스트 조회
     */
    var getExperimentByInvestigator = async(id) => {
        const _query = 'select * from experiment_user_map where deleted = 0 '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method   getExperimentBySample
     * @description getExperimentBySample  연구 샘플 맵핑 리스트 조회
     */
    var getExperimentBySample = async(id) => {
        const _query = 'select * from experiment_sample_map '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method   getExperiment
     * @description getExperiment  연구 리스트 조회
     */
    var getExperiment = async(id) => {
        if (id) {
            const _query = 'select id, name, date_format(start_date, "%Y-%m-%d") as startDate, date_format(end_date, "%Y-%m-%d") as endDate, memo from experiment where deleted = 0 and id = ? '
            const [rows] = await _pool.query(_query, [id])
            return rows
        } else {
            const _query = 'select id, name, date_format(start_date, "%Y-%m-%d") as startDate, date_format(end_date, "%Y-%m-%d") as endDate, memo from experiment where deleted = 0 '
            const [rows] = await _pool.query(_query)
            return rows
        }
    }

    /**
     * @method addExperiment
     * @description addExperiment 연구 추가
     */
    var addExperiment = async(data, userIdx) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
          let resultid
          let query
            await connection.beginTransaction()
            if(data.platform_id){
              query = 'insert into experiment (id,name,start_date,end_date,memo,lastupdated) values (?, ?, ?, ?, ?, now())'
              const [res] = await connection.query(query, [data.platform_id,data.name, data.startDate, data.endDate, data.memo])
              resultid = res.insertId
            } else{
              query = 'insert into experiment (name,start_date,end_date,memo,lastupdated) values (?, ?, ?, ?, now())'
              const [res] = await connection.query(query, [data.name, data.startDate, data.endDate, data.memo])
              resultid = res.insertId
            }
            for (const sampleId of data.sampleList) {
                query = 'insert into experiment_sample_map (experiment_id,sample_id,sample_item) values (?, ?, ?)'
                await connection.query(query, [resultid, sampleId, data.sampleTypeItem[sampleId].join(',')])
            }

            for (const investigatorId of data.investigatorList) {
                query = 'insert into experiment_user_map (experiment_id,farmos_user_id) values (?, ?)'
                await connection.query(query, [resultid, investigatorId])
            }

            await addExperimentDataIndex(connection, data, userIdx)

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method setExperiment
     * @description setExperiment 연구 수정
     */
    var setExperiment = async(data, userIdx) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'update experiment set name = ?, start_date = ?, end_date = ?, memo = ?, lastupdated = now() where id = ?'
            await connection.query(query, [data.name, data.startDate, data.endDate, data.memo, data.id])

            query = 'update experiment_sample_map set deleted = 1 where experiment_id = ?'
            await connection.query(query, [data.id])

            query = 'update experiment_user_map set deleted = 1 where experiment_id = ?'
            await connection.query(query, [data.id])

            for (const sampleId of data.sampleList) {
                query = 'INSERT INTO experiment_sample_map (experiment_id,sample_id,sample_item) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE deleted = 0, sample_item = ?'
                await connection.query(query, [data.id, sampleId, data.sampleTypeItem[sampleId].join(','), data.sampleTypeItem[sampleId].join(',')])
            }

            for (const investigatorId of data.investigatorList) {
                query = 'INSERT INTO experiment_user_map (experiment_id,farmos_user_id) VALUES (?, ?) ON DUPLICATE KEY UPDATE deleted = 0 '
                await connection.query(query, [data.id, investigatorId])
            }

            await addExperimentDataIndex(connection, data, userIdx)

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method addExperimentDataIndex
     * @description addExperimentDataIndex 연구 데이터 인덱스 추가
     */
    var addExperimentDataIndex = async(connection, data, userIdx) => {
        let query = 'select * from experiment_sample '
        const [resultSample] = await connection.query(query)

        query = 'select * from experiment_items '
        const [resultType] = await connection.query(query)

        const sampleObject = {}
        const typeObject = {}

        for (const sample of resultSample) {
            sampleObject[sample.id] = sample
        }

        for (const type of resultType) {
            typeObject[type.id] = type
        }

        if (data.sampleList.length > 0) {
            for (const sampleId of data.sampleList) {
                /* query = ' select name from experiment_sample where id = ? '
                const [sampleName] = await connection.query(query, [sampleId]) */

                for (const type of data.sampleTypeItem[sampleId]) {
                    query = 'INSERT INTO dataindexes (id,name,unit) select ?,?,? from dual where not exists(select id from dataindexes where id = ?) '
                    const dataId = 5 * 10000000 + sampleId * 10000 + type
                        // 5 * 10000000 + 샘플 * 10000 + 데이터종류

                    // await connection.query(query, [dataId, `${sampleName[0].name}-${typeObject[type].name}`, typeObject[type].unit, dataId])
                    await connection.query(query, [dataId, `${sampleObject[sampleId].name}_${typeObject[type].name}`, typeObject[type].unit, dataId])

                    query = 'INSERT INTO current_observations (data_id,source_id,nvalue,obs_time,modified_time) select ?, ?, 0, now(), now() from dual where not exists(select data_id from current_observations where data_id = ?) '
                    await connection.query(query, [dataId, userIdx, dataId])
                }
            }
        }
        /*  else {
              for (const type of data.sampleTypeList) {
                query = 'INSERT INTO dataindexes (id,name,unit) select ?,?,? from dual where not exists(select id from dataindexes where id = ?) '
                const dataId = 5 * 10000000 + type
                await connection.query(query, [dataId, typeObject[type].name, typeObject[type].unit, dataId])

                query = 'INSERT INTO current_observations (data_id,source_id,nvalue,obs_time,modified_time) select ?, ?, 0, now(), now() from dual where not exists(select data_id from current_observations where data_id = ?) '
                await connection.query(query, [dataId, userIdx, dataId])
              }
            } */
    }

    /**
     * @method delExperiment
     * @description delExperiment 연구 삭제
     */
    var delExperiment = async(data) => {
        const query = 'update experiment set deleted = 1 , lastupdated = now() where id = ?'
        await _pool.query(query, [data])
    }

    /**
     * @method   initUser
     * @description initUser 사용자 초기 설정
     */
    var initUser = async(data) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select configuration from configuration where type = "initialize"'
            const [result] = await connection.query(query)
            if (result[0].configuration.toString() === 'false') {
                query = 'delete from farmos_user'
                await connection.query(query)

                query = 'insert into farmos_user (userid,passwd,privilege,lastupdated,name, phone) values (?, password(?), ?, now(), "admin", "010-1234-1234")'
                await connection.query(query, [data.userid, data.passwd, 'admin'])

                await connection.commit()
            }
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   initFarmos
     * @description initFarmos 사용자 초기 설정
     */
    var initFarmos = async(data) => {
        console.log('data')
        console.log(data)
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select configuration from configuration where type = "initialize"'
            const [result] = await connection.query(query)
            if (result[0].configuration.toString() === 'false') {
                query = 'delete from gate_info'
                await connection.query(query)

                query = 'insert into gate_info (uuid,couple) values (?, ?)'
                await connection.query(query, [data.uuid, data.couple])

                query = 'delete from configuration where type = "mapkey"'
                await connection.query(query)

                query = 'delete from configuration where type = "mqtt"'
                await connection.query(query)

                query = 'insert into configuration (type,configuration,lastupdated) values (?, ?, now())'
                await connection.query(query, ['mapkey', data.mapKey])

                query = 'insert into configuration (type,configuration,lastupdated) values (?, ?, now())'
                await connection.query(query, ['mqtt', data.mqtt])

                await connection.commit()
            }
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   initFarm
     * @description initFarm 사용자 초기 설정
     */
    var initFarm = async(data) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select configuration from configuration where type = "initialize"'
            const [result] = await connection.query(query)
            if (result[0].configuration.toString() === 'false') {
                query = 'delete from observations'
                await connection.query(query)

                query = 'delete from current_observations'
                await connection.query(query)

                query = 'delete from dataindexes'
                await connection.query(query)

                query = 'delete from device_field'
                await connection.query(query)

                query = 'delete from core_rule_applied'
                await connection.query(query)

                query = 'delete from core_timespan'
                await connection.query(query)

                query = 'delete from fields'
                await connection.query(query)

                query = 'delete from farm'
                await connection.query(query)

                query = 'insert into farm (id,name,info) values (?, ?, ?)'
                await connection.query(query, [1, data.name, JSON.stringify(data.info)])

                const uiInfo = JSON.stringify(fieldUi)

                query = 'insert into fields (id,name,fieldtype,uiinfo,farm_id) values (?, ?, ?, ?, ?)'
                await connection.query(query, [0, '온실외부', 'local', uiInfo, 1])

                query = 'INSERT INTO dataindexes (id,name,unit,field_id) values (?, ?, ?, ?) '

                await connection.query(query, [1, '위도', '°', 0])
                await connection.query(query, [2, '경도', '°', 0])

                query = 'INSERT INTO current_observations (data_id,obs_time,nvalue,modified_time) ' +
                    'values (?, now(), ?, now()) '

                const latlng = data.info.gps.split(',')
                if (latlng[0] && latlng[1]) {
                    await connection.query(query, [1, latlng[0]])
                    await connection.query(query, [2, latlng[1]])
                } else {
                    await connection.query(query, [1, 0])
                    await connection.query(query, [2, 0])
                }

                // 농장 룰 자동 적용
                const { data: ruleTemplate } = await axios.get(`${config.templateApi}/rule`)
                const autoRuleTemplateList = ruleTemplate.filter((item) => item.autoapplying === 2 && item.used === 1)

                for (const obj of autoRuleTemplateList) {
                    await addruleapplied(obj, 0, connection)
                }

                query = 'update configuration set configuration = ? where type = ?'
                await connection.query(query, ['true', 'initialize'])

                await connection.commit()
            }
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   getExperimentData
     * @description getExperimentData 연구 조사 데이터 조회
     */
    var getExperimentData = async(experimentDataId, searchData) => {
        let result = []
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from experiment_items where id and deleted = 0 '
            const [resultType] = await connection.query(query)

            const typeObject = {}
            for (const type of resultType) {
                typeObject[type.id] = type
            }

            let querySelect = ' select DATE_FORMAT(r.obs_time, "%Y-%m-%d %H:%i:%s")obs_time , '
            let queryJoin = ''
            for (const [index, id] of searchData.dataIdList.entries()) {
                if (index > 0) {
                    querySelect += ' , '
                }
                querySelect += ` r${index}.nvalue as '${id}' `

                const typeId = Number(String(id).substring(5, 8))
                console.log(typeObject)
                console.log(typeId)
                console.log(typeObject[typeId])
                if (typeObject[typeId].type === 'number') {
                    queryJoin += ` left join observations r${index} on r${index}.obs_time = r.obs_time and r${index}.data_id = ${id} `
                } else {
                    queryJoin += ` left join experiment_comments r${index} on r${index}.obs_time = r.obs_time and r${index}.data_id = ${id} `
                }
            }

            if (searchData.startDate && searchData.endDate) {
                query = querySelect + ' from (SELECT DISTINCT obs_time FROM observations  WHERE data_id in ( ? ) and obs_time between ? and ? UNION SELECT DISTINCT obs_time FROM experiment_comments  WHERE data_id in ( ? ) and obs_time between ? and ? ) r ' + queryJoin
                query += ' ORDER BY obs_time desc'
                console.log(connection.format(query, [searchData.dataIdList, searchData.startDate, searchData.endDate, searchData.dataIdList, searchData.startDate, searchData.endDate]))
                const [resultData] = await connection.query(query, [searchData.dataIdList, searchData.startDate, searchData.endDate, searchData.dataIdList, searchData.startDate, searchData.endDate])
                result = resultData
            } else {
                query = querySelect + ' from (SELECT DISTINCT obs_time FROM observations  WHERE data_id in ( ? ) UNION SELECT DISTINCT obs_time FROM experiment_comments  WHERE data_id in ( ? ) ) r ' + queryJoin
                query += ' ORDER BY obs_time desc'
                console.log(connection.format(query, [searchData.dataIdList, searchData.dataIdList]))
                const [resultData] = await connection.query(query, [searchData.dataIdList, searchData.dataIdList])
                result = resultData
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
        return result
    }

    /**
     * @method   setExperimentData
     * @description setExperimentData 연구 조사 데이터 수정
     */
    var setExperimentData = async(dataId, data) => {
        const typeId = String(dataId).substring(6, 8)
        let query = 'select type from experiment_items where id = ?'
        const [result] = await _pool.query(query, [typeId])

        query = ''

        if (result[0].type === 'string') {
            query = 'update experiment_comments set nvalue = ? where data_id = ? and obs_time = ?'
        } else if (result[0].type === 'number') {
            query = 'update observations set nvalue = ? where data_id = ? and obs_time = ?'
        }
        console.log(_pool.format(query, [data.value, dataId, data.obs_time]))
        await _pool.query(query, [data.value, dataId, data.obs_time])
    }

    /**
     * @method   delExperimentData
     * @description delExperimentData 연구 조사 데이터 삭제
     */
    var delExperimentData = async(dataId, data) => {
        const typeId = String(dataId).substring(6, 8)
        let query = 'select type from experiment_items where id = ?'
        const [result] = await _pool.query(query, [typeId])

        query = ''

        if (result[0].type === 'string') {
            query = 'delete from experiment_comments where data_id = ? and obs_time = ?'
        } else if (result[0].type === 'number') {
            query = 'delete from observations where data_id = ? and obs_time = ?'
        }
        const [res] = await _pool.query(query, [dataId, data.obs_time])
        console.log(res)
    }

    /**
     * @method   getExperimentType
     * @description getExperimentType 샘플 조사 타입 조회
     */
    var getExperimentType = async() => {
        const query = 'select * from experiment_items where deleted = 0 '
        const [rows] = await _pool.query(query)
        return rows
    }

    /**
     * @method   addExperimentType
     * @description addExperimentType 샘플 조사 타입 추가
     */
    var addExperimentType = async(item) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from experiment_items where name = ? and deleted = 0 '
            const [result] = await connection.query(query, [item.name])

            if (result.length > 0) {
                throw (new Error('중복되는 조사항목명이 있습니다.'))
            }
            if(item.platform_id){
              query = 'insert into experiment_items (id,name,type,unit) values (?, ?, ?, ?)'
              await connection.query(query, [item.platform_id,item.name, item.type, item.unit])
            }else {
              query = 'insert into experiment_items (name,type,unit) values (?, ?, ?)'
              await connection.query(query, [item.name, item.type, item.unit])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   setExperimentType
     * @description setExperimentType 샘플 조사 타입 수정
     */
    var setExperimentType = async(item) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            // let query = 'select * from experiment_items where name = ? and deleted = 0 '
            // const [result] = await connection.query(query, [item.name])

            // if (result.length > 0) {
            //   throw (new Error('중복되는 조사항목명이 있습니다.'))
            // }

            query = 'update experiment_items set name = ?, type = ?, unit = ? where id = ? '
            await connection.query(query, [item.name, item.type, item.unit, item.id])
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   delExperimentType
     * @description delExperimentType 샘플 조사 타입 삭제
     */
    var delExperimentType = async(id) => {
        const query = 'update experiment_items set deleted = 1 where id = ? '
        await _pool.query(query, [id])
    }

    /**
     * @method   getExperimentSample
     * @description getExperimentSample 샘플 리스트 조회
     */
    var getExperimentSample = async() => {
        const query = 'select * from experiment_sample where deleted = 0 '
        const [rows] = await _pool.query(query)
        return rows
    }

    /**
     * @method   addExperimentSample
     * @description addExperimentSample 샘플 추가
     */
    var addExperimentSample = async(item) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from experiment_sample where name = ? and deleted = 0 '
            const [result] = await connection.query(query, [item.name])

            if (result.length > 0) {
                throw (new Error('중복되는 조사항목명이 있습니다.'))
            }
            if(item.platform_id){
              query = 'insert into experiment_sample (id,name,field_id) values (?, ?, ?)'
              await connection.query(query, [item.platform_id, item.name, item.field_id])
            }else {
              query = 'insert into experiment_sample (name,field_id) values (?, ?)'
              await connection.query(query, [item.name, item.field_id])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   setExperimentSample
     * @description setExperimentSample 샘플 수정
     */
    var setExperimentSample = async(item) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from experiment_sample where name = ? and deleted = 0 '
            const [result] = await connection.query(query, [item.name])

            if (result.length > 0) {
                throw (new Error('중복되는 샘플이름이 있습니다.'))
            }

            query = 'update experiment_sample set name = ?, field_id = ? where id = ? '
            await connection.query(query, [item.name, item.field_id, item.id])

            /* query = 'select * from dataindexes where id like ?'

            query = 'update dataindexes set name = ? where id like ?'
            await connection.query(query, [item.name, String(500000 + item.id) + '%']) */

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   delExperimentSample
     * @description delExperimentSample 샘플 삭제
     */
    var delExperimentSample = async(id) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'update experiment_sample set deleted = 1 where id = ? '
            await connection.query(query, [id])

            query = 'update dataindexes set deleted = 1 where id like ? '
            await connection.query(query, [5 + ('000' + id).slice(-3) + '%'])

            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   addExperimentDataUpload
     * @description addExperimentDataUpload 연구 데이터 업로드
     */
    var addExperimentDataUpload = async(data, userIdx) => {
        const connection = await _pool.getConnection(async conn => conn)
        try {
            await connection.beginTransaction()

            let query = 'select * from experiment_sample where deleted = 0 '
            const [resultSample] = await connection.query(query)
            const sampleObject = {}

            for (const sample of resultSample) {
                sampleObject[sample.name] = sample.id
            }

            query = 'select * from experiment_items where deleted = 0 '
            const [resultType] = await connection.query(query)
            const typeObject = {}

            for (const type of resultType) {
                typeObject[type.name] = type
            }

            const dataIdSet = new Set()

            for (const sample of data) {
                const sampleId = sampleObject[sample.sample]

                for (const dateData of sample.data) {
                    const date = dateData.date

                    for (const type of dateData.data) {
                        const typeId = typeObject[type.type].id

                        const dataId = 5 * 10000000 + sampleId * 10000 + typeId
                        console.log(sampleId)
                        console.log(typeId)
                        console.log(dataId)

                        if (typeObject[type.type].type === 'number') {
                            dataIdSet.add(dataId)
                            query = 'INSERT INTO observations (data_id,obs_time,nvalue,source_id) select ?,?,?,? from dual where not exists(select data_id from observations where data_id = ? and obs_time = ?) '
                        } else if (typeObject[type.type].type === 'string') {
                            query = 'INSERT INTO experiment_comments (data_id,obs_time,nvalue,source_id) select ?,?,?,? from dual where not exists(select data_id from experiment_comments where data_id = ? and obs_time = ?) '
                        }
                        console.log(connection.format(query, [dataId, date, type.value, userIdx, dataId, date]))
                        await connection.query(query, [dataId, date, type.value, userIdx, dataId, date])
                    }
                }
            }

            for (const dataId of dataIdSet) {
                query = 'select nvalue,obs_time from observations where data_id = ? ORDER BY obs_time desc limit 1'
                const [resultObs] = await connection.query(query, [dataId])
                query = 'update current_observations set nvalue = ? where data_id = ? '
                await connection.query(query, [resultObs[0].nvalue, dataId])
            }
            await connection.commit()
        } catch (error) {
            await connection.rollback()
            console.log(error)
            throw (error)
        } finally {
            connection.release()
        }
    }

    /**
     * @method   getStaticData
     * @description getStaticData 통계 작동규칙 데이터 조회
     */
    var getStaticData = async(id) => {
        const query = 'SELECT configurations FROM core_rule_applied  WHERE  template_id = "26"  AND deleted = "0"  and id = ? '
        const [rows] = await _pool.query(query, [id])
        return rows
    }

    /**
     * @method   getStaticDataPeriod
     * @description getStaticData 조회기간에 따른 통계 작동규칙 데이터 조회
     */
    var getStaticDataPeriod = async(dataIdList, period) => {
        let query = ''
        if (period === 'hour') {
            const resultList = []
            for (const el of dataIdList) {
                query = 'select (select name from dataindexes where id = ?) as name, data_id, date_format(obs_time, "%Y-%m-%d %H:%i") as obs_time, round(nvalue,2) as nvalue from observations where data_id = ? and DATE_FORMAT(now(), "%Y-%m-%d") = DATE_FORMAT(obs_time, "%Y-%m-%d") '
                const [rows] = await _pool.query(query, [el, el])
                resultList.push(rows)
            }
            return resultList
        } else if (period === 'daynight' || period === 'day') {
            const resultList = []
            for (const el of dataIdList) {
                query = 'select (select name from dataindexes where id = ?) as name, date_format(obs_time, "%Y-%m-%d %H:%i") as obs_time, round(nvalue,2) as nvalue from observations where data_id = ? and DATE_FORMAT(obs_time, "%Y-%m-%d") between DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -5 DAY) , "%Y-%m-%d") and DATE_FORMAT(NOW(), "%Y-%m-%d") '
                const [rows] = await _pool.query(query, [el, el])
                resultList.push(rows)
            }
            return resultList
        } else if (period === 'week') {
            const resultList = []
            for (const el of dataIdList) {
                query = 'select (select name from dataindexes where id = ?) as name, data_id, date_format(obs_time, "%Y-%m-%d %H:%i") as obs_time, round(nvalue,2) as nvalue from observations where data_id = ? and DATE_FORMAT(obs_time, "%Y-%m-%d") between DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 MONTH) , "%Y-%m-%d") and DATE_FORMAT(NOW(), "%Y-%m-%d") '
                const [rows] = await _pool.query(query, [el, el])
                resultList.push(rows)
            }
            return resultList
        }
    }

    /**
     * @method setFieldLocalUi   
     * @description local 필드 ui 정보 수정.
     */
    var setFieldLocalUi = async(items) => {
        console.log('setFieldLocalUi')
        console.log(items.length)

        _query = 'update fields set uiinfo = ? where id = ?'

        for (let i = 0; i < items.length; i++) {
            if (items[i].fieldtype === 'local') {
                await _pool.query(_query, [JSON.stringify(items[i].uiinfo), items[i].id])
            }
        }
        return items
    }

    /**
     * @method getTemplateAPI
     * @description getTemplateAPI 템플릿 ID 정보 가져오기
     */
    var getTemplateAPI = async() => {
        console.log('templateAPI')
        const item = config.templateApi
        return item
    }

    /**
     * @method getNutrientInfo
     * @description getNutrientInfo 양액공급정보를 조회한다
     */
    var getNutrientInfo = async() => {
        console.log('getNutrientInfo')
        _query = 'select * from nutrient_supply where deleted = 0 order by obstime desc '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method addNutrientInfo
     * @description addNutrientInfo 양액공급정보를 입력한다
     */
    var addNutrientInfo = async(item) => {
        console.log('addNutrientInfo')
        if(item.platform_id){
        _query = 'insert into nutrient_supply (id,obstime,nutinfo) values (?,now(),?) '
        await _pool.query(_query, [item.platform_id, JSON.stringify(item)])
        } else {
        _query = 'insert into nutrient_supply (obstime,nutinfo) values (now(),?) '
        await _pool.query(_query, [JSON.stringify(item)])
        }
    }

    /**
     * @method setNutrientInfo
     * @description setNutrientInfo 양액공급정보를 수정한다
     */
    var setNutrientInfo = async(id, item) => {
        console.log('setNutrientInfo')
        _query = 'update nutrient_supply set nutinfo=? where id = ? '
        await _pool.query(_query, [JSON.stringify(item), id])
    }

    /**
     * @method deleteNutrientInfo
     * @description deleteNutrientInfo 양액공급정보를 삭제한다
     */
    var deleteNutrientInfo = async(id) => {
        console.log('deleteNutrientInfo')
        _query = 'update nutrient_supply set deleted = 1 where id = ? '
        await _pool.query(_query, [id])
    }

    /**
     * @method getUserInfo
     * @description getUserInfo 로그인 사용자 정보 조회
     */
    var getUserInfo = async() => {
        console.log('getUserInfo')
        _query = 'select userid, privilege, name, phone from farmos_user where privilege = "admin" '
        const [rows] = await _pool.query(_query)
        return rows
    }

    /**
     * @method setoutputfromdp
     * @description getUserInfo 로그인 사용자 정보 조회
     */
    var setoutputfromdp = async(field_id, item) => {
        console.log('setoutputfromdp')
        //데이터 관리체계 중 총생산량, 재배주수가 있는데, 쥬크박스는 생산량 측정이 어려우므로 구역별데이터로 재배주수를 사용
        //주석처리한부분은 6,7번 수확량 /수확일에 대한것이므로 farmos2.5에서 사용할 예정
        let codeYieldCount = (field_id * 100000 + 8).toString().padStart(8, "0");
        _query = 'update current_observations set nvalue = ?, obs_time = now() where data_id = ? '
        await _pool.query(_query,[item.yieldcount,codeYieldCount])

        /*let codeHarvestDate = (field_id * 100000 + 6).toString().padStart(8, "0");
        let codeYieldCount = (field_id * 100000 + 8).toString().padStart(8, "0");
        if(item.IsAdd == true){
          if(item.isbefore == false){
          _query = 'update current_observations set nvalue = ?, obs_time = now() where data_id = ? '
          await _pool.query(_query,[item.plantdate,codeHarvestDate])
          await _pool.query(_query,[item.totalvolume,codeYieldCount])
          }
          _query = 'insert into observations (data_id, obs_time, nvalue) values (?, now() , ?)'
          await _pool.query(_query,[codeHarvestDate,item.plantdate])
          await _pool.query(_query,[codeYieldCount,item.totalvolume])
        }else {
          if(item.isbefore ==false){
          _query = 'update current_observations set nvalue = ?, obs_time = now() where data_id = ? '
          await _pool.query(_query,[item.totalvolume,codeYieldCount])
          }
          _query = 'insert into observations (data_id, obs_time, nvalue) values (?, now() , ?)'
          await _pool.query(_query,[codeYieldCount,(-1)*(item.volume)])
          await _pool.query(_query,[codeHarvestDate,item.plantdate])
        }*/
    }

    return {
        setRuleAppliedUse: setRuleAppliedUse,
        getTestRuleResult: getTestRuleResult,
        addExperimentDataUpload: addExperimentDataUpload,
        getExperimentByInvestigator: getExperimentByInvestigator,
        getExperimentBySample: getExperimentBySample,
        getExperiment: getExperiment,
        addExperiment: addExperiment,
        setExperiment: setExperiment,
        delExperiment: delExperiment,
        getInvestigator: getInvestigator,
        addInvestigator: addInvestigator,
        setInvestigator: setInvestigator,
        delInvestigator: delInvestigator,
        getExperimentSample: getExperimentSample,
        addExperimentSample: addExperimentSample,
        setExperimentSample: setExperimentSample,
        delExperimentSample: delExperimentSample,
        getExperimentType: getExperimentType,
        setExperimentType: setExperimentType,
        delExperimentType: delExperimentType,
        addExperimentType: addExperimentType,
        getExperimentData: getExperimentData,
        setExperimentData: setExperimentData,
        delExperimentData: delExperimentData,
        initFarm: initFarm,
        initFarmos: initFarmos,
        initUser: initUser,
        putDeviceRestore: putDeviceRestore,
        getUnitList: getUnitList,
        getImageList: getimagelist,
        delImageId: delImageId,
        addImage: addimage,
        addimageExperiment: addimageExperiment,
        addObsBackup: addobsbackup,
        setConfiguration: setconfiguration,
        getConfiguration: getconfiguration,
        getRuleState: getrulestate,
        getProcessLog: getprocesslog,
        deleteReferenceData: deletereferencedata,
        setReferenceData: setreferencedata,
        getReferenceList: getreferencelist,
        addReferenceData: addreferencedata,
        getCalibration: getcalibration,
        setCalibration: setcalibration,
        getObs: getobs,
        addTimespan: addtimespan,
        delTimespan: deltimespan,
        setTimespan: settimespan,
        addRuleModule: addrulemodule,
        getRuleTemplate: getruletemplate,
        addRuleTemplate: addruletemplate,
        getRuleTemplateDetail: getruletemplatedetail,
        setRuleTemplateDetail: setruletemplatedetail,
        deleteRuleTemplateDetail: deleteruletemplatedetail,
        getDeviceHistoryControle: getdevicehistorycontrole,
        getDeviceHistory: getdevicehistory,
        getGateInfo: getgateinfo,
        getDeviceGateList: getdevicegatelist,
        setGateInfoDetect: setgateinfodetect,
        getTimespan: gettimespan,
        getTimespanFieldItem: gettimespanfielditem,
        setTimespanFieldItem: settimespanfielditem,
        addRuleApplied: addruleapplied,
        getRuleApplied: getruleapplied,
        setRuleApplied: setruleapplied,
        setRuleAppliedReset: setruleappliedreset,
        deleteAppliedRule: deleteappliedRule,
        getDataIndexes: getdataindexes,
        setDataIndexes: setdataindexes,
        addDevices: adddevices,
        setDevice: setdevice,
        setDeviceManual: setdevicemanual,
        getDevices: getdevices,
        deleteDevice: deletedevice,
        getfields: getfields,
        setfield: setfield,
        addfield: addfield,
        setfieldfromdp: setfieldfromdp,
        addfieldfromdp: addfieldfromdp,
        deleteField: deletefield,
        setfieldUiDevice: setfieldUiDevice,
        getfielddevices: getfielddevices,
        getfieldactuators: getfieldactuators,
        getlastobservations: getlastobservations,
        getlastobservationsforfield: getlastobservationsforfield,
        setplantdate: setplantdate,
        getplantdate: getplantdate,
        modifyhistory: modifyhistory,
        deletehistory: deletehistory,
        backupdata: backupdata,
        getbackupstate:getbackupstate,
        setdeviceproperties: setdeviceproperties,
        getsensor: getsensor,
        getvirtualsensor: getvirtualsensor,
        getgraph: getgraph,
        getgraphall: getgraphall,
        getcondensation: getcondensation,
        getlastquery: getlastquery,
        getevents: getevents,
        geteventcnt: geteventcnt,
        checkevent: checkevent,
        getfarm: getfarm,
        setfarm: setfarm,
        login: login,
        getDeviceName: getDeviceName,
        getDeviceErrorTime: getDeviceErrorTime,
        getDeviceRestoreTime: getDeviceRestoreTime,
        getProcessList: getProcessList,
        addProcess: addProcess,
        setProcess: setProcess,
        delProcess: delProcess,
        StopProcess:StopProcess,
        StartProcess:StartProcess,
        execProcess: execProcess,
        getDataErrorCheckCount: getDataErrorCheckCount,
        getObservationCount: getObservationCount,
        getCurrentCount: getCurrentCount,
        getCorrelationData: getCorrelationData,
        getRuleStatic: getRuleStatic,
        getStaticData: getStaticData,
        getStaticDataPeriod: getStaticDataPeriod,
        setFieldLocalUi: setFieldLocalUi,
        getRecentImageList: getRecentImageList,
        getTemplateAPI: getTemplateAPI,
        getNutrientInfo: getNutrientInfo,
        addNutrientInfo: addNutrientInfo,
        setNutrientInfo: setNutrientInfo,
        deleteNutrientInfo: deleteNutrientInfo,
        getUserInfo: getUserInfo,
        setoutputfromdp: setoutputfromdp
    }
}

module.exports = farmosApi()
