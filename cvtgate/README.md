# cvtgate3 

## Introduction

본 프로젝트는 주식회사 지농에서 개발하는 게이트웨이용 프로그램으로 장비간의 연동을 용이하게 하는 것을 목표로 삼고 있다.

## 개발 환경
* python 3을 사용한다.
* 다음의 패키지들이 필요하다.
  * enum34 (필수)
  * requests
  * paho.mqtt
  * prometheus_client

## 작동 방법
cvtgate는 mate라고 부르는 모듈을 연계하는 목적으로 설계되었다. 즉 두 개의 서로다른 mate가 couple manager에 의해 상호작동할 수 있는 형태로 구성되었다고 할 수 있다. 각각의 관계는 독립적이며 json 기반의 메시지를 전송한다.

이전 버전의 cvtgate 는 하나의 프로세스에서 스레드로 동작하는 방식이었는데, python의 특성상 동시 실행이 되지 않는 문제가 있었다. 새 버전에서는 하나의 메이트가 하나의 프로세스로 동작하는 방식으로 처리된다. 또한 프로메테우스와 동작할 수 있도록 메트릭을 전송하는 기능을 포함한다.

## 동작 설정
cvtgate는 디폴트로 cpmng.conf 라는 파일에서 설정을 읽는다. 해당 파일에는 커플매니저 설정을 취득하기 위해 필요한 url, sleep시간, 반복회수, couples 정보등이 있어야 한다.
```
{
    "id" : "35263a38-c5c4-4531-ae3a-6114db698cbd",
    "url" : "http://dev.jinong.co.kr:8880/v1/gate",
    "sleep" : 1,
    "iter" : 10,
    "couples" : [...]
}
```
위의 설정이 주어졌을때, cvtgate는 http://dev.jinong.co.kr:8880/v1/gate 에서 작동을 위한 설정을 받아온다.

sleep은 sleep 시간을 의미하고, iter 는 설정을 다시 읽기위한 반복회수를 의미한다. SLEEP이 1이고, ITER가 10 이라면, 10초마다 신규설정을 확인한다.

couples가 없다면 받아온 설정을 모두 적용한다. couples 가 있다면 couples에 있는 커플들만 작동시킨다.

## 커플매니저 설정
커플매니저 설정 역시 JSON으로 되어 있으며 다음과 같은 형식을 갖는다.
```
[{
    'id' : '1',
    'ssmate': {
        'mod':'mate_jnmqtt', 
        'class':'JNMQTTMate', 
        'opt':{
            'svc':'exkit3', 
            'id': '7001',
            'conn':{'host': 'sul.jinong.co.kr', 'port' : 1883, 'keepalive' : 60}
        }
    },
    'dsmate': {
        'mod':'mate_cdtp', 
        'class':'CDTPMate', 
        'opt':{
            'conn':{'port': '/dev/ttyO1', 'baudrate': 9600, 'BBB' : true},
            'gatewayid': '7001'
        }
    },
    'devinfo': [{
        "id" : "10", "dk" : "-1", "dt": "gw", "chd" : [{
            "id" : "1000", "dk" : "-1", "dt": "nd", "chd" : [
                {"id" : "1025", "dk" : "1", "dt": "sen"},   
                {"id" : "1026", "dk" : "2", "dt": "sen"}, 
                {"id" : "1027", "dk" : "3", "dt": "sen"}, 
                {"id" : "1028", "dk" : "4", "dt": "sen"}
            ]
        }]
    }]
}]
```


## (메시지)블럭 VS. 메시지

CoupleManager 는 mate간의 연결을 돕지만 서로의 통신에는 관여하지 않고, 로그와 매트릭 만을 수신한다.

여기서 메시지와 블럭이라는 용어가 중요하다. 메세지는 mate가 실제 장비와 통신하는 프로토콜에 담긴 데이터교환의 단위를 의미한다. (메시지) 블럭은 cvtgate내부에서 사용되는 데이터 교환의 단위이다. 즉, mate끼리는 서로 (메시지) 블럭 단위로 통신을 하고, mate가 외부의 장비와 통신하는 경우에는 메시지라는 용어를 사용한다.  

mate간의 블럭은 node 단위로 만들어지며, 노드ID, type, content로 구성된다. content는 메시지 종류에 따라 다른 값을 갖는다.

* 블럭의 타입
  * NONE
  * OBSERVATION
  * REQUEST
  * RESPONSE
  * STATUS
  * UNDEFINED

* OBSERVATION의 content는 센서의 데이터를 의미하며 JSON 형태의 데이터를 갖는다.
```
{ time: '2018-10-24 14:34:42', 1 : 123, 2 :456 }
```

* STATUS의 content는 장비의 상태를 의미한다.
```
{ status: 'on'}
```
```
Class StatCode(IntEnum):
    ON = 0
    OFF = 1
    ABNORMAL = 2
```

* RESPONSE의 content는 결과를 의미한다.
```
{ res : 'ok'}
```
```
class ResCode(IntEnum):
    OK = 0
    FAIL = 1
```


## 설정 방법
### 옵션 설정 (접속 설정)
mate에서 사용하는 옵션을 설정하는 것으로 mate마다 다 다를 수 있다. 아래는 예시이다.
```
{
  "conn" : {
    "port" : "/dev/ttyUSB0",
    "baudrate": 9600,
  }
}
```

### 장비 설정
* id : 장비 아이디
* dk : 장비 아이디 선택을 위한 키 값. 장비에서 온 데이터에서 dk 값을 활용해서 데이터 추출후 id에 맵핑하는 형태임.
* dt : 장비 타입 - gw: 게이트웨이, nd : 노드, sen : 센서
* chd : 자식 장비, 일반적으로 센서노드에는 다수의 센서가 있을 수 있음. 
```
[
  {"id" : "2", "dk" : "11", "dt": "gw", "chd" : [
    {"id" : "3", "dk" : "1", "dt": "nd", "chd" : [
      {"id" : "4", "dk" : "0", "dt": "sen"},
      {"id" : "5", "dk" : "1", "dt": "sen"},
      {"id" : "6", "dk" : "2", "dt": "sen"},
      {"id" : "7", "dk" : "3", "dt": "sen"}
    ]}
  ]}
]
```

## cvtgate의 구조
### 전체 구조
* (특정업체의) 노드, 게이트웨이 -> cvtgate -> 클라우드 서버

### cvtgate의 구조
* (device side) mate <-> couple manager <-> (server side) mate

## mate의 구조
하나의 mate 는 couplemanager에 의해서 다른 mate와 큐를 공유한다. 
큐는 3개로 구성되는데, 요청큐, 응답큐, 관측치 및 노티큐 이다. 
요청큐는 서버사이드 메이트에서 장비사이드 메이트로 요청을 전송하는데 사용된다.
응답큐는 반대로 장비사이드 메이트에서 응답을 전송할때 사용된다.
관측치 및 노티큐도 장비사이드에서 만들어진 관측치와 노티를 전달하는데 사용된다. 

메이트는 couplemanager 와 하나의 큐를 공유하는데, 이는 로그/매트릭 큐이다.
로그 매트릭 큐는 로깅할 내용과 퍼포먼스 체크를 위한 매트릭 정보를 전송하기 위해 사용되는데, 매트릭 정보는 기본적으로 서버사이드에서 관리하는 것으로 한다.

