#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#

import os
import sys
import time
import datetime
import json
import traceback
import shlex
import subprocess
import psutil

from collections import deque
from multiprocessing import Process, Queue

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import pyotp
import base64

from lib import *

class FarmOSManager(Runner):
    def __init__(self, configfile):
        self._isrunning = False
        self._isconnected = False
        self._client = None
        self._auth = None
        self._msgq = deque()
        self._client = None

        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()

        if 'services' not in self._option:
            self._option['services'] = {
                "cvtgate" : {
                    "path" : ".",
                    "dname" : "cpmng",
                    "start" : [{"cmd" : "python3 couplemng.py start"}],
                    "stop" : [{"cmd" : "python3 couplemng.py stop"}],
                    "restart" : [{"cmd" : "python3 couplemng.py stop"}, {"cmd" : "python3 couplemng.py start"}]
                }
            }

        self._topic = "farmos/" + self._option["mqtt"]["serial"]
        self._otpkey = base64.b32encode(bytes(self._option["mqtt"]["serial"], 'utf-8'))
        self._totp = pyotp.TOTP(self._otpkey)

    def connect(self):
        try:
            self._client = mqtt.Client()
            self._client.loop(.1)
            self._client.on_message = self.onmsg

            self._client.on_socket_close = self.onclose
            self._client.on_disconnect = self.onclose

            if self._auth:
                self._client.username_pw_set(self._auth["username"], self._auth["password"])

            self._client.connect(self._option["mqtt"]["host"], self._option["mqtt"]["port"], self._option["mqtt"]["keepalive"])
            self._logger.info (self._topic + "/req/# was subscribed.")
            self._client.subscribe(self._topic + "/req/#" , 2)

            self._client.loop_start()
            self._isconnected = True
        except Exception as ex:
            self._logger.warning("fail to connect mqttserver : " + str(ex))
            self._isconnected = False

        return self._isconnected

    def getdname(self):
        return "fosmng"

    def stop(self):
        self._logger.info("FarmOS Manager tries to stop")
        self._isrunning = False

    def run(self, debug=False):
        i = 0
        self._logger.info("FarmOS Manager is running...")
        self._logger.info("OTP KEY is " + str(self._otpkey))
        self._isrunning = True
        while self._isrunning:
            if self._isconnected is False:
                self.connect()
                continue
            
            try:
                blk = self._msgq.popleft()
            except:
                time.sleep(self._option['sleep'])
                i = i + 1
                if i % 60 == 0:
                    self.heartbeat()
                continue

            topics = blk.topic.split('/')
            try:
                msg = json.loads(blk.payload)
            except Exception as ex:
                self._logger.warning("Unknown message : " + str(blk.payload) + " from " + str(blk.topic))
                continue

            self._totp.at(datetime.datetime.now())

            if "key" not in msg or msg["key"] != str(self._totp.now()):
                self._logger.warning("OTP is not matched : "+ str(blk.payload) + " from " + str(blk.topic))
                continue

            if len(topics) == 4: #and topics[2] == 'req':
                if topics[3] in self._option['services']:
                    ret = self.doservice(topics[3], msg)
                else:
                    self._logger.info ("Unknown service : " + str(blk.topic))
                    ret = [{"message" : "Unknown service : " + str(blk.topic)}]
                self.response(topics[3], ret)

            else:
                self._logger.info ("Unknown message : " + cmd + " from " + str(blk.topic))

        self._logger.info("FarmOS Manager is stopping.")

    def execcmd(self, cmd):
        try:
            cmds = shlex.split(cmd)
            if subprocess.call(cmds) != 0:
                self._logger.warning("Fail to execute command. " + str(cmds))
                return False
            
        except Exception as ex:
            self._logger.warning("Fail to execute with exception: " + str(cmds) + " : " + str(ex))
            return False

        return True

    def doservice(self, key, msg):
        svc = self._option['services'][key]
        ret = {'action' : msg['cmd'], 'results' : []}

        cwd = os.getcwd()
        if msg['cmd'] in svc['actions']:
            for acts in svc['actions'][msg['cmd']]:
                if "path" in svc:
                    os.chdir(svc["path"])
                msg = self.execcmd(acts["cmd"])
                ret['results'].append({'result' : msg}) 
        else:
            ret['results'].append({'result' : False, 'message' : "Unknown command : " + str(msg) + " for " + str(key)})
        os.chdir(cwd)
        return ret

    def response(self, svc, msg):
        topic = self._topic + "/res/" + svc
        self.publish(topic, msg)

    def checkprocess(self, key):
        fname = "/var/run/" + key + ".pid"
        if os.path.isfile(fname):
            try:
                fp = open(fname, "r")
                pid = fp.readline()
                fp.close()
                return psutil.pid_exists(int(pid))
            except Exception as ex:
                self._logger.warning("Fail to get pid from " + fname + " : " + str(ex))
        return False

    def heartbeat(self):
        topic = self._topic + "/heartbeat"

        msg = {}
        for key, svc in self._option["services"].items():
            msg[key] = self.checkprocess(svc["dname"])

        self.publish(topic, msg)

    def publish(self, topic, msg):
        try:
            publish.single(topic, payload=json.dumps(msg), qos=2, hostname=self._option["mqtt"]["host"], port=self._option["mqtt"]["port"], auth=self._auth)
        except Exception as ex:
            self._logger.warning("publish exception : " + str(ex) + " " + topic + "  " + str(msg))

    def onmsg(self, client, obj, blk):
        self._logger.info(("FarmOS Manager Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        self._msgq.append(blk)

    def onclose(self, client, udata, sock):
        self._logger.info("Disconnected with mqtt server.")
        self._isconnected = False

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python farmosmng.py [start|stop|restart|run|single]")
        sys.exit(2)

    mode = sys.argv[1]
    runner = FarmOSManager('conf/fosmng.conf')
    dname = runner.getdname()
    adaemon = Daemon(dname, runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.dstop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    elif 'single' == mode:
        adaemon.run(True)
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)

