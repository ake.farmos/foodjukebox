#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import sys
import time
import importlib
import json
import requests
import paho.mqtt.client as mqtt
from queue import Queue

from lib import *
from lib.ssmate.farmosdb import FarmosDB
from lib.ssmate.jnmqtt import JNMQTT

class Subscriber(Mate):
    def __init__(self, option, devinfo, subscriberid, logger):
        self._client = None
        self._subscriberid = subscriberid
        super(Subscriber, self).__init__(option, devinfo, subscriberid, logger)
        if "remote" in option and "db" in option["remote"]:
            option["db"] = option["remote"]["db"]
        self._farmosdb = FarmosDB(option, self._devinfo, logger)

    def start(self):
        self._farmosdb.start()
        super(Subscriber, self).start(None)
        self.connect()

    def connect(self):
        self._farmosdb.connect()
        self.mqttconnect()
        super(Subscriber, self).connect()

    def mqttconnect(self):
        self._client = mqtt.Client()
        self._client.loop(.1)
        self._client.on_message = self.onmsg
        self._client.on_socket_close = self.onclose
        self._client.on_disconnect = self.onclose
        if "remote" in self._option and "conn" in self._option["remote"]:
            conn = self._option["remote"]["conn"]
        else:
            conn = self._option["conn"]

        while self._executing:
            try:
                self._client.connect(conn["host"], conn["port"], conn["keepalive"])
                self._client.subscribe(self._option["mqtt"]["svc"] + "/" + self._option["mqtt"]["id"] + "/#")
                self._logger.info("subscribe : " + self._option["mqtt"]["svc"] + "/" + self._option["mqtt"]["id"] + "/#")
                self._client.loop_start()
                break
            except Exception as ex:
                self._logger.warn("fail to connect mqtt server.")
                time.sleep(5)

    def close(self):
        self._farmosdb.close()
        self._client.loop_stop(force=True)
        super(Subscriber, self).close()

    def stop(self):
        self._farmosdb.stop()
        super(Subscriber, self).stop()

    def onclose(self, client, udata, sock):
        self._logger.warn("Disconnected from mqtt server. Try to reconnect.")
        self.mqttconnect()

    def getpropermsg(self, blk):
        tmp = blk.topic.split('/')
        msg = MBlock.load(blk.payload)
        if msg is None:
            self._logger.info("Fail to parse a message. " + str(blk.payload))
            return None

        if tmp[3] == JNMQTT._REQ and BlkType.isrequest(msg.gettype()):
            return msg
        if tmp[3] == JNMQTT._RES and BlkType.isresponse(msg.gettype()):
            return msg
        if tmp[3] == JNMQTT._NOTI and BlkType.isnotice(msg.gettype()):
            return msg
        if tmp[3] == JNMQTT._OBS and BlkType.isobservation(msg.gettype()):
            return msg

        self._logger.info("Topic is not matched. Check [3]. " + str(tmp))
        return None

    def writeblk(self, blk):
        pass

    def onmsg(self, client, obj, blk):
        self._logger.info("Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos))
        try:
            tmp = blk.topic.split('/')

            if tmp[2] in [JNMQTT._SVR, JNMQTT._SELF] and JNMQTT._STAT == tmp[3]:
                if blk.payload == JNMQTT._STAT_ON:
                    self._logger.info("The server is OK now.")
                else:
                    self._logger.warn("The server might have some problems.")

            msg = self.getpropermsg(blk)
            if msg:
                #self._logger.info("received a message.")
                self._farmosdb.writeblk(msg)
            else:
                self._logger.warn("It is not a proper message.")
        except Exception as ex:
            self._logger.warn("fail to call onmsg: " + str(ex) + " "  + blk.payload)

class MQTTSubscriber(Runner):
    _subscribers = []
    _isrunning = False

    def __init__(self, configfile):
        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()
        if 'sleep' not in self._option:
            self._option['sleep'] = 1
        if 'iter' not in self._option:
            self._option['iter'] = 10

    def getdname(self):
        if 'dname' in self._option:
            return self._option['dname']
        else:
            return "mqttsub"

    def loadcandidates(self):
        """ this can throw an exception. """
        #try:
        res = requests.get(self._option["url"] + "/" + self._option["id"])
        if res.status_code == 200:
            gate = res.json()
            #print gate
            if "subscribers" in self._option:
                subscribers = []
                for cp in gate["children"]:
                    if cp["id"] in self._option["subscribers"]:
                        subscribers.append(cp)
            else:
                subscribers = gate["children"]
            return subscribers
        else:
            self._logger.warn("subscriber server error : status : " + str(res.status_code))
            return None
        #except Exception as ex:
        #    self._logger.warn("fail to load : " + self._option["url"] + " // " + str(ex))
        #return None

    def popsubscriber(self, candy):
        for idx in range(len(self._subscribers)):
            if self._subscribers[idx]['id'] == candy['id']:
                return self._subscribers.pop(idx)
        return None

    def execute(self, candy):
        try:
            ssmate = candy['ssmate']
            devinfo = candy['children']
            subscriber = {}
            subscriber['id'] = candy['id']
            subscriber['candy'] = json.dumps(candy)
            subscriber['ssmate'] = self.loadsubscriber(ssmate, devinfo, candy['id'])
            if subscriber['ssmate'] is None:
                self._logger.warn("fail to load mate")
                return None

            self._logger.info("A subscriber [" + ssmate["mod"] + "/" + ssmate["class"] + "] is loading.")
            subscriber['ssmate'].start()
            return subscriber
        except Exception as ex:
            self._logger.warn("fail to execute a candy : " + str(ex))
        return None

    def loadsubscriber(self, conf, devinfo, subscriberid):
        if conf['mod'] == 'mate_jnmqtt' and conf['class'] == 'JNMQTTMate':
            self._logger.info("mate_jnmqtt is found.")
            return Subscriber(conf['opt'], devinfo, subscriberid, self._logger)

        if conf['mod'] == 'mate_farmos' and conf['class'] == 'FarmosMate':
            self._logger.info("mate_farmos is found.")
            return Subscriber(conf['opt'], devinfo, subscriberid, self._logger)

    def stopsubscriber(self, subscriber):
        subscriber['ssmate'].stop()
        self._logger.info(subscriber['id'] + " stopped")

    def stopold(self):
        for subscriber in self._subscribers:
            self.stopsubscriber(subscriber)

    def stop(self):
        self._logger.info("Couple Manager tries to stop")
        self._isrunning = False

    def run(self, debug=False):
        self._isrunning = True
        i = 0
        n = 0
        while self._isrunning:
            if i % self._option['iter'] == 0:
                newsubscribers = []
                try:
                    candidates = self.loadcandidates()
                    if candidates is None:
                        self._logger.info("retry to load candidates..." + str(i / self._option['iter']))
                        continue
                    n = 0
                except Exception as ex:
                    n = n + 1
                    if n > 4:
                        self._logger.warn("fail to load candidates.... it would be stopped.")
                        self.stop()
                        break
                    else:
                        continue

                for candy in candidates:
                    subscriber = self.popsubscriber(candy)
                    if subscriber is None:
                        self._logger.info("A new subscriber [" + candy["name"] + "] is loading.")
                        subscriber = self.execute(candy)
                    elif subscriber['candy'] != json.dumps(candy):
                        self._logger.info("Configuration of a subscriber [" + candy["name"] + "] is changed.")
                        self.stopsubscriber(subscriber)
                        subscriber = self.execute(candy)

                    if subscriber is not None:
                        newsubscribers.append(subscriber)
                    else:
                        self._logger.info("A subscriber [" + candy["name"] + "] is not loading.")

                self.stopold()
                self._subscribers = newsubscribers
                i = 1
            else:
                i = i + 1
            time.sleep(self._option['sleep'])
        self.stopold()

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python mqttsubscriber.py [start|stop|restart|run]")
        sys.exit(2)

    mode = sys.argv[1]
    runner = MQTTSubscriber('mqttsub.conf')
    dname = runner.getdname()
    adaemon = Daemon(dname, runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.dstop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)
