#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#
# devices for interaction with farmos 

import abc
# compatible with Python 2 *and* 3:
ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()})

import json
import time
import datetime
import pymysql
import logging
from collections import deque

from .. import *
from .farmos import FarmOS
from .obsmodel import *
from .dbmng import FDBManager

class FDevice(ABC):
    def __init__(self, devid, dbm, logger, farmid=None):
        self._id = devid
        self._farmid = farmid
        self._dbm = dbm
        self._status = StatCode.READY.value
        self._logger = logger

    def getstatus(self):
        return self._status

    def setstatus(self, status):
        self._status = status

class FGateway(FDevice):
    pass

class ObsModelFactory:
    _models = {
        "DefaultModel" : DefaultModel,
        "YesterdayPropModel" : YesterdayPropModel,
        "LinearRegModel" : LinearRegModel
    }

    def generate(name, sid, dbm, farmid, option=None):
        model = ObsModelFactory._models[name](FarmOS.getdataid(sid, "value"), farmid)
        model.setdatamanager(dbm)
        return model

class FSensor(FDevice):
    def __init__(self, devid, dbm, logger, farmid=None):
        super(FSensor, self).__init__(devid, dbm, logger, farmid)
        self._nvalue, self._obstime, sigma = self._dbm.getlastsensorinfo(devid, farmid)
        print ("FSensor", self._nvalue, self._obstime, sigma)
        self.setsigma(sigma)
        self._raw = None
        self._obsmodel = None
        nowts = time.time()
        self._offset = datetime.datetime.fromtimestamp(nowts) - datetime.datetime.utcfromtimestamp(nowts)

    def setsigma(self, sigma, numsigma = 6):
        if sigma is None or numsigma is None:
            self._sigma = 0
        else:
            self._sigma = sigma * numsigma

    def setobsmodel(self, name, option=None):
        self._obsmodel = ObsModelFactory.generate(name, self._id, self._dbm, self._farmid, option)

    def isabnormal(self, nvalue, obstime):
        if isinstance(self._obstime, str):
            prev = datetime.datetime.strptime(self._obstime, "%Y-%m-%d %H:%M:%S")
        else:
            prev = self._obstime
        now = datetime.datetime.strptime(obstime, "%Y-%m-%d %H:%M:%S")

        if prev is None or (now - prev).total_seconds() > 300: 
            return False

        if self._sigma == 0 or self._nvalue is None:        # sigma 값이 0이면, 판단불가. 항상 정상
            return False

        #self._logger.info ("isabnormal : " + str([nvalue - self._nvalue, self._sigma, abs(nvalue - self._nvalue) > self._sigma]))
        return abs(nvalue - self._nvalue) > self._sigma

    def setvalue(self, obstime, values):
        # values : observation, status, raw
        if self.isabnormal(values[0], obstime):
            self.setstatus(StatCode.NEEDTOCHECK.value)
        else:
            self.setstatus(values[1])
            #if values[0] == StatCode.READY and self._obsmodel:
            #    self._obsmodel.setvalue(obstime, values[1])
            self._obstime = obstime
            self._nvalue = values[0]

        if len(values) == 3:
            self._raw = values[2]
        else:
            self._raw = values[0]

    def getvalue(self):
        return self._nvalue

    def getobstime(self):
        return self._obstime

    def getestimated(self, tm=time.time()):
        if self._obsmodel:
            return self._obsmodel.getvalue(tm)
        return None

    def setcurrent(self, current):
        if self._obsmodel:
            utc = datetime.datetime.strptime(current, "%Y-%m-%d %H:%M:%S")
            epoch = (utc + self._offset - datetime.datetime(1970, 1, 1)).total_seconds()

            if self._nvalue and self._status != StatCode.NEEDTOCHECK:
                self._obsmodel.setvalue([epoch], self._nvalue)

            estimated = self.getestimated(epoch)
            #self._logger.info ("estimated: " + str([current, self._nvalue, estimated, epoch]))
            if estimated and self.isabnormal(estimated, current):
                estimated = None
        else:
            estimated = None

        if self._obstime == current:
            if self._status == StatCode.NEEDTOCHECK and self._obsmodel:
                values = [self._status, estimated, self._raw, estimated]
            else:
                values = [self._status, self._nvalue, self._raw, estimated]
        else:
            values = [StatCode.MISSING.value, estimated, None, estimated]
        print ("SetCurrent", self._id, current, values, self._farmid)
        self._dbm.writeobservation(self._id, current, values, self._farmid)
        return values

class FActuator(FDevice):
    def __init__(self, devid, dbm, logger, farmid=None):
        super(FActuator, self).__init__(devid, dbm, logger, farmid)
        self._opids = []
        self._lastreqs = {}
        self._content = {"status":0, "opid":0}
        self._updated = None
    
    def request(self, req):
        opid = req.getopid()
        if opid in self._opids:
            self._logger.info("Same opid request exists. Old opid request would be removed. " + str(opid))
            self._opids.remove(opid)
        self._opids.append(opid)
        self._lastreqs[opid] = req
        self._dbm.insertrequest(req, self._farmid)

    def response(self, res):
        opid = res.getopid()

        print("response", self._opids)
        idx = self._finishpreviousrequests(opid) 
        self._logger.info ("Actuator[" + str(self._id) + "] is processing a command " + str(opid))
        self._dbm.execrequest(res, self._farmid)
        if idx >= 0:
            self._opids = self._opids[idx:]

    def _finishpreviousrequests(self, opid):
        try:
            idx = self._opids.index(opid)
            for op in self._opids[:idx]:
                self._logger.info ("Actuator[" + str(self._id) + "] lost a command " + str(op))
                self._dbm.finishrequest(self._id, op, self._farmid)
                del self._lastreqs[op]
        except:
            self._logger.info ("Actuator[" + str(self._id) + "] has no request for " + str(opid))
            idx = -1
        return idx

    def issamestatus(self, content):
        #print("issamestatus", self._content, content)
        return self._content["status"] == content["status"]

    def issameopid(self, content):
        #assert "opid" in content, "Content does not have opid " + str([self._id, content, self._content])
        if "opid" in content:
            return self._content["opid"] == content["opid"]
        return True   #명령을 받을 수 없으니 늘 같은 걸로 함.

    def updatelaststatus(self):
        #self._dbm.updatestatus(self._id, self._content, self._farmid)
        self._dbm.writestatus(self._id, self._content, farmid=self._farmid)

    def updatestatus(self, content, tm=None):
        if self.issamestatus(content):
            if self.issameopid(content) is False:
                # 다른 명령에 의한 기존 명령의 종료
                self._logger.info ("Actuator[" + str(self._id) + "] finish a command " + str(self._content["opid"]) + " because a new command " + str(content["opid"]))
                self._dbm.finishrequest(self._id, content["opid"], self._farmid)
                self._dbm.writestatus(self._id, content, farmid=self._farmid)
            else:
                # only update current
                # self._dbm.updatestatus(self._id, content, self._farmid)
                pass
        else: 
            if self.issameopid(content) and content["opid"] != 0:
                # status 는 다른데, 상태가 달라지면 - 명령의 종료
                if content["status"] != StatCode.READY:
                    # 변경되는 상태가 정지가 아니라면 이상한거임. 
                    self._logger.warn("Status changed but it's not ready." + str((self._content, content)))
                else:
                    self._logger.info ("Actuator[" + str(self._id) + "] finish a command " + str(self._content["opid"]))
                    self._dbm.finishrequest(self._id, content["opid"], self._farmid)
            else:
                # 새로운 명령의 시작 
                # update current and insert observations
                self._logger.info ("Actuator[" + str(self._id) + "] start a command " + str(content["opid"]))
            self._dbm.writestatus(self._id, content, farmid=self._farmid)

        self._content = content
        self.setstatus(content['status'])
        self._updated = time.time()

    def getcontent(self):
        return self._content

    def getupdatedtime(self):
        return self._updated

    def getkeyvalue(self, key):
        if key in self._content:
            return self._content[key]
        return None

class FNode(FActuator):
    _CODESET = {"status":0, "control" : 6}
    pass

class FSwitchLv0(FActuator):
    pass

class FSwitchLv1(FSwitchLv0):
    pass

class FSwitchLv2(FSwitchLv1):
    pass

class FRetractableLv0(FActuator):
    pass

class FRetractableLv1(FRetractableLv0):
    pass

class FRetractableLv2(FRetractableLv1):
    pass

class FNutrientSupplyLv0(FActuator):
    pass

class FNutrientSupplyLv1(FNutrientSupplyLv0):
    pass

class FNutrientSupplyLv2(FNutrientSupplyLv1):
    pass

class FNutrientSupplyLv3(FNutrientSupplyLv2):
    pass

class FCameraLv0(FActuator):
    pass

if __name__ == "__main__":
    pass
