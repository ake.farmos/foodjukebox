#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import threading
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from calendar import timegm
from collections import deque

from .. import *

from .dbmng import FDBManager
from .fdev import *

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class FarmosMate2(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(FarmosMate2, self).__init__(option, devinfo, coupleid, quelist)
        if "conn" in option:
            self._jnmqtt = JNMQTT(option, DevInfo(devinfo), self._logger)
        else:
            self._jnmqtt = FakeJNMQTT(option, DevInfo(devinfo), self._logger)
        self._dbm = FDBManager(option, self._logger)
        self._devices = {}
        self._lastobsupdated = None
        self._reqs = deque()

    def _finddevbydt(dt):
        objdict = {
            "gw" : FGateway,
            "nd" : FNode,
            "sen" : FSensor,
            "act" : FActuator,
            "act/switch/level0" : FSwitchLv0,
            "act/switch/level1" : FSwitchLv1,
            "act/switch/level2" : FSwitchLv2,
            "act/retractable/level0" : FRetractableLv0,
            "act/retractable/level1" : FRetractableLv1,
            "act/retractable/level2" : FRetractableLv2,
            "act/nutrient-supply/level0" : FNutrientSupplyLv0,
            "act/nutrient-supply/level1" : FNutrientSupplyLv1,
            "act/nutrient-supply/level2" : FNutrientSupplyLv2,
            "act/nutrient-supply/level3" : FNutrientSupplyLv3,
            "act/camera/level0" : FCameraLv0,
            "act/misc/display" : FActuator
        }
        return objdict[dt]

    def initialize(self):
        self._devices = {}
        for gw in self._devinfo:
            if self._devinfo.hasgateway(gw):
                self._devices[gw["id"]] = FarmosMate2._finddevbydt(gw["dt"])(gw["id"], self._dbm, self._logger)
            for nd in gw["children"]:
                self._devices[int(nd["id"])] = FarmosMate2._finddevbydt(nd["dt"])(int(nd["id"]), self._dbm, self._logger)
                for dev in nd["children"]:
                    if "id" not in dev or dev["id"] is None or dev["id"] == "" or self._dbm.isgooddeviceid(dev["id"]) == False:
                        self._logger.info(str(dev) + " has no id so it would be ignored.")
                        continue
                    self._devices[int(dev["id"])] = FarmosMate2._finddevbydt(dev["dt"])(int(dev["id"]), self._dbm, self._logger)
                    if isinstance(self._devices[int(dev["id"])], FSensor):
                        self._devices[int(dev["id"])].setobsmodel("LinearRegModel")

    def connect(self):
        self._jnmqtt.connect(True)
        self._dbm.connect()
        super(FarmosMate2, self).connect()
        self.initialize()
        return True

    def close(self):
        self._jnmqtt.close()
        self._dbm.close()
        super(FarmosMate2, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        super(FarmosMate2, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        super(FarmosMate2, self).matestop()

    def processresponse(self, res):
        self._jnmqtt.writeblk(res)
        if res.getdevid() in self._devices:
            self._devices[res.getdevid()].response(res)

    def processobsnoti(self, msg):
        if BlkType.isnotice(msg.gettype()):
            self.notice(msg)
            self._jnmqtt.writeblk(msg)
        else:
            self.observation(msg)
            self._jnmqtt.writeblk(msg)

    def writeobservation(self):
        noti = Notice(None, NotiCode.OBSERVATIONS)
        noti.settime(self._lastobsupdated)
        for did, obj in self._devices.items():
            if isinstance(obj, FSensor):
                noti.setcontent(did, obj.setcurrent(self._lastobsupdated))
        self._jnmqtt.writeblk(noti)

    def observation(self, obs):
        tm = obs.gettime()[:-2] + '00'
        if tm != self._lastobsupdated:  # passed one minute then write observations
            if self._lastobsupdated is not None:
                self.writeobservation()
            self._lastobsupdated = tm

        for tid in obs.getsensorids():
            sid = int(tid)
            if sid not in self._devices:
                self._logger.info ("observation [" + tid + "] would be ignored.")
                continue
            self._devices[sid].setvalue(tm, obs.getobservation(sid))

    def notice(self, notice):
        """
        funcs = {
            NotiCode.ACTUATOR_STATUS: self.setactstatus,
        }
        """
        #self._logger.info(notice.stringify())
        if notice.getcode() == NotiCode.ACTUATOR_STATUS:
            tm = notice.gettime()
            content = notice.getcontent()
            for tid in notice.getdevids():
                did = int(tid)
                self._devices[did].updatestatus(content[tid], tm)
        else:
            self._logger.info("Other notices are ignored." + notice.stringify())

    def obsnotidone(self):
        # update last status of received message. especially notice
        for did, dev in self._devices.items():
            if issubclass(type(dev), FActuator):
                dev.updatelaststatus() 

    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        print(("FarmosMate2 Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        self._reqs.append((blk.topic, msg))

    def doextra(self):
        while True:
            try:
                topic, msg = self._reqs.popleft()
            except:
                return 
            tmp = topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                pass

            if BlkType.isrequest(msg.gettype()):
                self._logger.info(msg.stringify())
                if msg.getdevid() in self._devices:
                    self._devices[msg.getdevid()].request(msg)
                self._writereq(msg)
            else:
                self._logger.warn("Wrong message : " + msg.stringify())

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
        "db" : {"host": "localhost", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt" : {"svc" : "cvtgate", "id" : "1"},
        "area" : "local"
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "11", "dk" : "11", "dt": "sen"},
                {"id" : "12", "dk" : "12", "dt": "act"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = FarmosMate2(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/req/1", cmd.stringify(), hostname="dev.jinong.co.kr")
    print("published")

    time.sleep(5)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
