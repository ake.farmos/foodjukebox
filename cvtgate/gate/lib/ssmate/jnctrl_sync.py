#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#
# 지농 관제 연동을 위한 메이트

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from collections import deque

from .. import *

from .fdev import *
from .dbmng import MySQLManager

"""
{"host" : "220.90.133.21", "user" : "smartLink", "password" : "smartLink1!", "db" : "smartLink"}
"""
class JNCDBSync(MySQLManager):
    def _readdevfromfarmos(self, cur, svcid):
        # should be sorted. 
        query = "select coupleid, gateid, id, devindex, spec from devices where deleted = 0 order by id"

        ret = [(svcid, '', None, None, None, ' ')]
        nid = None
        cid = None
        try:
            cur.execute(query, [])
            rows = cur.fetchall()
            for row in rows:
                if row[3] is None:
                    if cid != row[0]:
                        ret.append((svcid, row[0], None, None, None, ' '))
                        ret.append((svcid, row[0], row[1], None, None, ' '))
                        cid = row[0]
                    ret.append((svcid, row[0], row[1], row[2], None, row[4]))
                    nid = row[2]
                else:
                    ret.append((svcid, row[0], row[1], nid, row[2], row[4]))
            
            return ret
        except Exception as ex:
            self._logger.warning("DB other exception : " + str([ex, query]))
            return None

    def _readdevfromjnctrl(self, svcid):
        # should be sorted
        query = "select LINK_SRVC_ID, LINK_GTWY_ID, GTWY_ID, NODE_ID, EQPMN_ID, UPDT_DT, USE_YN, EQPMN_ETC_CN, INSTL_EQPMN_ID from TB_CNTC_FARMOS_EQPMN_M where LINK_SRVC_ID = %s"
        rows = self._select(query, [svcid])
        return rows

    def _mergedevinfo(self, newdevs, olddevs, debug=False):
        devdict = {}
        for odev in olddevs:
            devdict[odev[:5]] = [odev[5:], 'd']

        for ndev in newdevs:
            key = ndev[:5]
            if key in devdict:
                # do nothing
                devdict[key][1] = '-'
            else:
                # new dev
                devdict[key] = [ndev[5:], 'i']

        inq = "INSERT INTO TB_CNTC_FARMOS_EQPMN_M(LINK_SRVC_ID, LINK_GTWY_ID, GTWY_ID, NODE_ID, EQPMN_ID, UPDT_DT, USE_YN, EQPMN_ETC_CN) values (%s, %s, %s, %s, %s, now(), 'Y', %s)"
        upq = "UPDATE TB_CNTC_FARMOS_EQPMN_M SET UPDT_DT = now(), USE_YN = 'N' where INSTL_EQPMN_ID = %s"
        for k, v in devdict.items():
            #print ("merge", v[1], k, v)
            if v[1] == 'i':
                if debug:
                    tmp = ["'" + str(x) + "'" if x else 'NULL' for x in list(k)] + ["'" + v[0][0] + "'"]
                    print (inq % tuple(tmp), ';')
                else:
                    self._execute(inq, list(k) + [v[0][0]])
              
            elif v[1] == 'd':
                if debug:
                    print (upq % v[0][0], ';')
                else:
                    self._execute(upq, [v[0][0]])
                #del self._devs[v[0][0]]

        return devdict

    def sync(self, fosoption, svcid, debug=False):
        fconn, fcur = self._connect(fosoption)
        newdevs= self._readdevfromfarmos(fcur, svcid)
        try:
            olddevs = self._readdevfromjnctrl(svcid)
        except:
            olddevs = []
        # compare & update
        self._mergedevinfo(newdevs, olddevs, debug)

if __name__ == "__main__":
    import sys
    option = {
        'conn': {'host': 'farmos003.jinong.co.kr', 'port': 1883, 'keepalive': 60}, 
        'db': {'host': '182.162.27.78', 'user': 'smartLink', 'password': 'smartLink1!', 'db': 'smartLink'}, 
        'mqtt': {'svc': 'cvtgate', 'id': '#'}
    }
    fosoption = {'host': 'localhost', 'user': 'farmos', 'password': 'farmosv2@', 'db': 'farmos'} 

    logger = util.getdefaultlogger()
    syncer = JNCDBSync(option, logger)

    if len(sys.argv) == 2:
        debug = False if sys.argv[1] == 'write' else True
    else:
        debug = True
    syncer.sync(fosoption, 'e7d0-6a1a-6042', debug)
