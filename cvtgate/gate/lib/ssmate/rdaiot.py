#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#
# rda.iot 

import requests

class RDAIoTManager:
    def __init__(self, option, logger):
        self._logger = logger
        if "rdaiot" in option and "url" in option["rdaiot"]:
            self._url = option["rdaiot"]["url"]
        else:
            self._url = None

    def updateobservation(self, eqid, tm, obs): 
        if self._url:
            url = self._url.format(eqid, tm, obs)
            try:
                res = requests.get(url)
            except Exception as ex:
                self._logger.warning("fail to update observation in RDA IoT." + str(ex))

