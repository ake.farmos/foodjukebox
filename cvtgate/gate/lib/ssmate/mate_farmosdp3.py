#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import threading
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from calendar import timegm
from collections import deque

from .. import *

from .dpmng import FDPManager
from .fdev import *

'''
    option = {
        "conn" : {"host" : "fjboxtest.jinong.co.kr", "port" : 1883, "keepalive" : 60},
        "mater" : {"host": "27.96.131.12", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt" : {"svc" : "cvtgate"},
        "datanode" : {"host" : "fjboxtest.jinong.co.kr", "farms" : [1, 2]},
        "area" : "local"
    }

# not to use devinfo!!
devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class FarmosDP3Mate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(FarmosDP3Mate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = SimpleJNMQTT(option, self._logger)
        self._dbm = None
        self._devices = {}
        self._lastobsupdated = None
        self._msgq = deque()
        self._msgfuncs = {BlkType.OBSERVATION: self.observation, BlkType.REQUEST: self.request, 
                BlkType.RESPONSE: self.response, BlkType.NOTICE: self.notice}

    def _finddevbydt(dt):
        objdict = {
            "gw" : FGateway,
            "nd" : FNode,
            "sen" : FSensor,
            "act" : FActuator,
            "act/switch/level0" : FSwitchLv0,
            "act/switch/level1" : FSwitchLv1,
            "act/switch/level2" : FSwitchLv2,
            "act/retractable/level0" : FRetractableLv0,
            "act/retractable/level1" : FRetractableLv1,
            "act/retractable/level2" : FRetractableLv2,
            "act/nutrientsupply/level0" : FNutrientSupplyLv0,
            "act/nutrientsupply/level1" : FNutrientSupplyLv1,
            "act/nutrientsupply/level2" : FNutrientSupplyLv2,
            "act/nutrientsupply/level3" : FNutrientSupplyLv3,
            "act/camera/level0" : FCameraLv0
        }
        if dt in objdict:
            return objdict[dt]

        if dt[:3] in objdict:
            return objdict[dt[:3]]

    def getnewid(self, farmid, devid):
        return str(farmid) + "-" + str(devid)

    def reload(self):
        self._dbm = FDPManager.loadDPDataManager(self._option, self._logger)
        farms = self._dbm.getfarms()

        devices = {}
        for farmid, farm in farms.items():
            for devid, dev in farm["devices"].items():
                newid = self.getnewid (farmid, devid)
                devices[newid] = FarmosDP3Mate._finddevbydt(dev["type"])(devid, self._dbm, self._logger, farmid)
        self._devices = devices;

    def connect(self):
        super(FarmosDP3Mate, self).connect()
        self._jnmqtt.connect()
        self.reload()
        return True

    def close(self):
        self._jnmqtt.close()
        if self._dbm:
            self._dbm.close()
        super(FarmosDP3Mate, self).close()

    def matestart(self):
        super(FarmosDP3Mate, self).matestart()
        self._jnmqtt.start(self.onmsg)
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        super(FarmosDP3Mate, self).matestop()

    def request(self, req, farmid):
        newid = self.getnewid (farmid, req.getdevid())
        if newid in self._devices:
            self._devices[newid].request(req)
        else:
            self._logger.info("No device for a request: " + newid + req.stringify())

    def response(self, res, farmid):
        newid = self.getnewid (farmid, res.getdevid())
        if newid in self._devices:
            self._devices[newid].response(res)
        else:
            self._logger.info("No device for a response : " + newid + res.stringify())

    def writeactuatorstatus(self):
        for did, obj in self._devices.items():
            if issubclass(type(obj), FActuator):
                obj.updatelaststatus() 

    def observation(self, obs, farmid):
        # mate_farmos2 를 사용하고, Observation 메세지는 처리하지 않는다.  
        pass

    def notice(self, notice, farmid):
        code = notice.getcode()
        tm = notice.gettime()
        content = notice.getcontent()

        if code == NotiCode.ACTUATOR_STATUS:
            for devid in notice.getdevids():
                aid = self.getnewid (farmid, devid)
                if aid not in self._devices:
                    self._logger.info ("notice [" + aid + "] would be ignored. " + notice.stringify())
                    continue
                # debug
                if "opid" not in content[devid]:
                    self._logger.info (str(aid) + ":" + str(devid) + " has weired content " + str(content[devid]))
                else:
                    #self._logger.info (str(farmid) + ":" + str(devid) + " was updated : " + str(content[devid]))
                    self._devices[aid].updatestatus(content[devid], tm)

        elif code == NotiCode.OBSERVATIONS:
            for devid, obs in content.items():
                if devid in ['time', 'code']:
                    continue
                sid = self.getnewid(farmid, devid)
                if sid not in self._devices:
                    self._logger.info ("observation [" + sid + "] would be ignored. " + str(obs))
                    continue
                #self._devices[sid].setvalue(tm, obs.getobservation(devid))
                self._dbm.writeobservation(devid, tm, obs, farmid)
            # 적당한 주기로 한번씩 업데이트 한다.
            self.writeactuatorstatus()

        elif code == NotiCode.SYSTEM_MONITOR:
            self._dbm.writesystemusage(tm, content, farmid)

        elif code == NotiCode.FCORE_DATA:
            self._dbm.writeruledata(tm, content, farmid)

        elif code == NotiCode.CVTGATE_STATUS:
            # should write the status of cvtgate
            self._logger.info("status of cvtgate [" + str(farmid) + "] changes." + notice.stringify())

        else:
            self._logger.info("Other notices are ignored." + notice.stringify())

    def doextra(self):
        if self._timetocheck(Mate.RELOADTYPE):
            self._logger.info("The system information is reloading....")
            self.reload()
            self._updatetime(Mate.RELOADTYPE)

        while True:
            try:
                topic, msg = self._msgq.popleft()
            except:
                return 

            topics = topic.split('/')

            if _JNMQTT._SVR == topics[2] and _JNMQTT._STAT == topics[3]:
                self._logger.info("Update couples: " + topic + " " + msg.stringify())
                self._dbm.updatecouple(topics[1], msg.getcontent()['status'])

            farmid = self._dbm.getfarmid(topics[1])
            if farmid is None:
                self._logger.info("Unknown topics : " + topic)
                continue

            msgtype = msg.gettype()
            if msgtype in self._msgfuncs:
                self._msgfuncs[msgtype](msg, farmid)
            else:
                self._logger.info("Unknown type of messages : " + msg.stringify())

    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        print(("FarmosDP3Mate Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        self._msgq.append((blk.topic, msg))

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "masternode" : {"host": "fjbox.jinong.co.kr", "user": "farmos", "password": "farmosv2@", "db": "master_node"},
        "mqtt" : {"host" : "fjboxtest.jinong.co.kr", "port" : 1883, "keepalive" : 60, "svc" : "cvtgate"},
        "datanode" : {"host" : "fjboxtest.jinong.co.kr"},
        "_datanode" : {"host" : "fjboxtest.jinong.co.kr", "farms" : [73]},
        "area" : "local"
    }

    devinfo = []

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = FarmosDP3Mate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    time.sleep(150)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
