#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import threading
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from calendar import timegm
from collections import deque

from .. import *

from .dbmng import FDBManager
from .fdev import *

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class FarmosDPMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(FarmosDPMate, self).__init__(option, devinfo, coupleid, quelist)
        self._dbm = FDBManager(option, self._logger)
        self._devices = {}
        self._lastobsupdated = None
        self._lock = threading.Lock()
        self._reqs = deque()

    def _finddevbydt(dt):
        objdict = {
            "gw" : FGateway,
            "nd" : FNode,
            "sen" : FSensor,
            "act" : FActuator,
            "act/switch/level0" : FSwitchLv0,
            "act/switch/level1" : FSwitchLv1,
            "act/switch/level2" : FSwitchLv2,
            "act/retractable/level0" : FRetractableLv0,
            "act/retractable/level1" : FRetractableLv1,
            "act/retractable/level2" : FRetractableLv2,
            "act/nutrientsupply/level0" : FNutrientSupplyLv0,
            "act/nutrientsupply/level1" : FNutrientSupplyLv1,
            "act/nutrientsupply/level2" : FNutrientSupplyLv2,
            "act/nutrientsupply/level3" : FNutrientSupplyLv3,
            "act/camera/level0" : FCameraLv0
        }
        return objdict[dt]

    def initialize(self):
        self._devices = {}
        for gw in self._devinfo:
            if self._devinfo.hasgateway(gw):
                self._devices[gw["id"]] = FarmosDPMate._finddevbydt(gw["dt"])(gw["id"], self._dbm, self._logger)
            for nd in gw["children"]:
                self._devices[int(nd["id"])] = FarmosDPMate._finddevbydt(nd["dt"])(int(nd["id"]), self._dbm, self._logger)
                for dev in nd["children"]:
                    if "id" not in dev or dev["id"] is None or dev["id"] == "" or self._dbm.isgooddeviceid(dev["id"]) == False:
                        self._logger.info(str(dev) + " has no id so it would be ignored.")
                        continue
                    self._devices[int(dev["id"])] = FarmosDPMate._finddevbydt(dev["dt"])(int(dev["id"]), self._dbm, self._logger)
                    if isinstance(self._devices[int(dev["id"])], FSensor):
                        self._devices[int(dev["id"])].setobsmodel("LinearRegModel")

    def connect(self):
        self._dbm.connect()
        super(FarmosDPMate, self).connect()
        self.initialize()
        return True

    def close(self):
        self._dbm.close()
        super(FarmosDPMate, self).close()

    def matestart(self):
        super(FarmosDPMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        super(FarmosDPMate, self).matestop()

    def processresponse(self, res):
        if res.getdevid() in self._devices:
            self._devices[res.getdevid()].response(res)
        else:
            self._logger.info("Wrong device response: " + res.stringify())

    def processobsnoti(self, msg):
        if BlkType.isnotice(msg.gettype()):
            self.notice(msg)
        else:
            self.observation(msg)

    def writeobsnoti(self):
        for did, obj in self._devices.items():
            if isinstance(obj, FSensor):
                obj.setcurrent(self._lastobsupdated)
            elif issubclass(type(obj), FActuator):
                obj.updatelaststatus() 

    def observation(self, obs):
        tm = obs.gettime()[:-2] + '00'
        if tm != self._lastobsupdated:  # passed one minute then write observations
            if self._lastobsupdated is not None:
                self.writeobsnoti()
            self._lastobsupdated = tm

        for tid in obs.getsensorids():
            sid = int(tid)
            if sid not in self._devices:
                self._logger.info ("observation [" + tid + "] would be ignored.")
                continue
            self._devices[sid].setvalue(tm, obs.getobservation(sid))

    def notice(self, notice):
        """
        funcs = {
            NotiCode.ACTUATOR_STATUS: self.setactstatus,
        }
        """
        self._logger.info(notice.stringify())
        if notice.getcode() == NotiCode.ACTUATOR_STATUS:
            tm = notice.gettime()
            content = notice.getcontent()
            for tid in notice.getdevids():
                did = int(tid)
                if did in self._devices:
                    self._devices[did].updatestatus(content[tid], tm)
                else:
                    self._logger.info("wrong device id:" + notice.stringify())
        else:
            self._logger.info("Other notices are ignored." + notice.stringify())

    def doextra(self):
        while True:
            try:
                #self._lock.acquire()
                topic, msg = self._reqs.popleft()
                #self._lock.release()
            except:
                return 
            tmp = topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                pass

            if BlkType.isrequest(msg.gettype()):
                self._logger.info(msg.stringify())
                self._devices[msg.getdevid()].request(msg)
                self._writereq(msg)
            else:
                self._logger.warn("Wrong message : " + msg.stringify())

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
        "db" : {"host": "localhost", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt" : {"svc" : "cvtgate", "id" : "1"},
        "area" : "local"
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "11", "dk" : "11", "dt": "sen"},
                {"id" : "12", "dk" : "12", "dt": "act"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = FarmosDPMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/req/1", cmd.stringify(), hostname="dev.jinong.co.kr")
    print("published")

    time.sleep(5)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
