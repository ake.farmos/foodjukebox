#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
from lib.mate import Logger
from lib.dsmate.mate_jnmqtt import JNMQTTMate
from lib.dsmate.mate_forwarder import MqttForwardMate
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import requests
import hashlib
import traceback
import pprint

from collections import deque, defaultdict
from .. import *

#comp_number = "C-4BD511DE"
#site_serial = "S-875B4192DB62"


class Encoder:
    @staticmethod
    def encode(comp_number, site_serial, unixtime):
        enc_number_un_hashed = comp_number + site_serial + str(unixtime)
        hash_obj = hashlib.sha256(enc_number_un_hashed.encode())
        return hash_obj.hexdigest()


class RegisterMessageBuilder:

    def __init__(self, comp_number, site_serial) -> None:
        self.comp_number = comp_number
        self.site_serial = site_serial
        self.body = {
            "comp_number": comp_number,
            "data": []
        }

    def _settime(self, ut):
        """
        외부에서 호출하지 않는 함수
        Args:
            ut ([type]): [description]
        """
        self.body["ut"] = ut

    def _encode(self, ut):
        self.body["enc_number"] = Encoder.encode(
            self.comp_number, self.site_serial, ut)

    def set_time_stamp(self):
        ut = int(time.time())
        self._settime(ut)
        self._encode(ut)

    def adddata(self, env_code_cate1, env_code, unique_number):
        self.body["data"].append(
            {"env_code_cate1": env_code_cate1,
             "env_code": env_code,
             "unique_number": unique_number})


class RequestSender:
    @staticmethod
    def send_request(url, body, logger: Logger) -> bool:
        try:
            logger.debug("send request : " + str(body)[:20] + "...")
            response = requests.post(url, json=body).json()
            if response["result"] == "ok":
                return True
            logger.error(response["result"])
            return False
        except Exception as e:
            logger.error("Request send error")
            logger.error(e)
            return False


class StatusInfoCache:
    def __init__(self, env_code_map) -> None:
        self.default_env_code_map = env_code_map
        self.value_status = defaultdict(None, {})

    def reset(self):
        self.value_status = {}

    def set_number(self, unique_number, value):
        self.value_status[unique_number] = value

    def is_full(self):
         return len(self.value_status) == len(self.default_env_code_map)


class StatusMessageBuilder:
    def __init__(self, comp_number, site_serial) -> None:
        self.comp_number = comp_number
        self.site_serial = site_serial
        self.body = {
            "comp_number": comp_number,
            "data": []
        }

    def _settime(self, ut):
        """
        외부에서 호출하지 않는 함수
        Args:
            ut ([type]): [description]
        """
        self.body["ut"] = ut

    def _encode(self, ut):
        self.body["enc_number"] = Encoder.encode(
            self.comp_number, self.site_serial, ut)

    def _set_time_stamp(self):
        ut = int(time.time())
        self._settime(ut)
        self._encode(ut)

    def _adddata(self, env_code, value, unique_number):
        self.body["data"].append(
            {"env_code": env_code,
             "value": str(value),
             "unique_number": unique_number})

    def build(self, env_code_map, values):
        for unique_number, value in values:
            env_code = None
            for env_code_info in env_code_map:
                if env_code_info["unique_number"] == unique_number:
                    env_code = env_code_info["env_code"]
            self._adddata(env_code, value, unique_number)
        self._set_time_stamp()
        return self.body


class ObservationParser:
    @staticmethod
    def parseobs(obs: Observation, deviceInfo: DevInfo, logger:Logger=None):
        VALUE_INDEX = 0
        gateway_key = obs.getextra("gateway-id")
        
        if logger:
            logger.debug("observation gateway key : " + gateway_key)
        
        node_id = obs.getnodeid()
        
        gateway = [x for x in deviceInfo._devinfo if x["dk"] == gateway_key][0]
        devices = [x["children"]
                   for x in gateway["children"] if x["id"] == node_id][0]
        content = [(x, values)
                   for x, values in obs.getcontent().items() if x.isdigit()]
        return tuple((device["opt"]["unique_number"], values[VALUE_INDEX])
                     for device_key, values in content
                     for device in devices
                     if device["dk"] == device_key and
                     VALUE_INDEX < len(values) and
                     "opt" in device and
                     "unique_number" in device["opt"])


class EnvCodesMapGenerator:
    @staticmethod
    def generate(device_info: DevInfo):
        device_options = [d["opt"] for root in device_info
                          for node in root["children"]
                          for d in node["children"] if "opt" in d]
        return tuple(defaultdict(None, {"env_code_cate1": "SE",
                                        "env_code": option["env_code"],
                                        "unique_number": option["unique_number"]})
                     for option in device_options)


class McopiaMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(McopiaMate, self).__init__(option, devinfo, coupleid, quelist)
        self.comp_number = option["conn"]["compnumber"]
        self.url = option["conn"]["url"]
        self.siteserial = option["conn"]["siteserial"]
        self.env_code_map = EnvCodesMapGenerator.generate(self._devinfo)
        self.status_cache = StatusInfoCache(self.env_code_map)

    def register_url(self):
        return self.url + "regist"

    def connect(self):
        super(McopiaMate, self).connect()
        try:
            builder = RegisterMessageBuilder(self.comp_number, self.siteserial)
            for e in self.env_code_map:
                builder.adddata(e["env_code_cate1"],
                                e["env_code"], e["unique_number"])
            builder.set_time_stamp()
            address_register_message = builder.body
            self._logger.debug(address_register_message)
            register_url = self.register_url()
            response = RequestSender.send_request(
                register_url, address_register_message, self._logger)
            self._logger.debug(response)
        except Exception as e:
            self._logger.error(traceback.format_exc())
            self._logger.error("Mate Connection error.")
            self._logger.error(e)
            return False
        return True

    def close(self):
        super(McopiaMate, self).close()

    def matestart(self):
        super(McopiaMate, self).matestart()
        self.connect()

    def matestop(self):
        super(McopiaMate, self).matestop()

    def status_url(self):
        return self.url + "status"

    def processobsnoti(self, blk):
        try:
            parsed_result = ObservationParser.parseobs(
                blk, self._devinfo, self._logger)
            
            builder = StatusMessageBuilder(
                    self.comp_number, self.siteserial)  
            message = builder.build(self.env_code_map, parsed_result)                
            response = RequestSender.send_request(
                    self.status_url(), message, self._logger)
            return response
        except Exception as e:
            self._logger.error("process observation notification.")
            self._logger.error(e)
            return False


if __name__ == "__main__":

    devinfo = [
        {"id": "1", "dk": "1", "dt": "gw", "couple_id": "ae67e222-0afc-4400-9a6e-bcc398a7052d",
         "children": [
             {"id": "2", "dk": "1", "dt": "nd", "children": [
                 {"id": "4", "dk": "2", "dt": "sen",
                  "opt": {"unique_number": "7372", "env_code": "SEEICI"}},
                 {"id": "5", "dk": "3", "dt": "sen",
                  "unique_number": "7373", "env_code": "SEEIHI"},
                 {"id": "6", "dk": "4", "dt": "sen",
                  "unique_number": "7374", "env_code": "SEEIHI"},
                 {"id": "7", "dk": "5", "dt": "sen",
                  "unique_number": "7375", "env_code": "SEEIHI"},
                 {"id": "8", "dk": "6", "dt": "sen",
                  "unique_number": "7376", "env_code": "SEEIHI"},
                 {"id": "9", "dk": "7", "dt": "sen",
                  "unique_number": "7377", "env_code": "SEEICI"},
                 {"id": "10", "dk": "8", "dt": "sen",
                  "unique_number": "7378", "env_code": "SEEIHI"},
                 {"id": "11", "dk": "9", "dt": "sen",
                  "unique_number": "7379", "env_code": "SEEIHI"},
                 {"id": "12", "dk": "10", "dt": "sen",
                  "unique_number": "7380", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "11", "dt": "sen",
                  "unique_number": "7381", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "12", "dt": "sen",
                  "unique_number": "7382", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "13", "dt": "sen",
                  "unique_number": "7383", "env_code": "SEEIHI"},
                 {"id": "12", "dk": "14", "dt": "sen",
                  "unique_number": "7384", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "15", "dt": "sen",
                  "unique_number": "7385", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "16", "dt": "sen",
                  "unique_number": "7386", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "17", "dt": "sen",
                  "unique_number": "7387", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "18", "dt": "sen",
                  "unique_number": "7388", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "19", "dt": "sen",
                  "unique_number": "7389", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "20", "dt": "sen",
                  "unique_number": "7390", "env_code": "SEEIHI"},
             ]}
         ]},
        {"id": "1", "dk": "1", "dt": "gw", "couple_id": "a2fb2f33-7cc7-45c0-bdfd-5ba4ef95ede2",
         "children": [
             {"id": "2", "dk": "1", "dt": "nd", "children": [
                 {"id": "4", "dk": "2", "dt": "sen",
                  "unique_number": "7472", "env_code": "SEEICI"},
                 {"id": "5", "dk": "3", "dt": "sen",
                  "unique_number": "7473", "env_code": "SEEIHI"},
                 {"id": "6", "dk": "4", "dt": "sen",
                  "unique_number": "7474", "env_code": "SEEIHI"},
                 {"id": "7", "dk": "5", "dt": "sen",
                  "unique_number": "7475", "env_code": "SEEIHI"},
                 {"id": "8", "dk": "6", "dt": "sen",
                  "unique_number": "7476", "env_code": "SEEIHI"},
                 {"id": "9", "dk": "7", "dt": "sen",
                  "unique_number": "7477", "env_code": "SEEICI"},
                 {"id": "10", "dk": "8", "dt": "sen",
                  "unique_number": "7478", "env_code": "SEEIHI"},
                 {"id": "11", "dk": "9", "dt": "sen",
                  "unique_number": "7479", "env_code": "SEEIHI"},
                 {"id": "12", "dk": "10", "dt": "sen",
                  "unique_number": "7480", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "11", "dt": "sen",
                  "unique_number": "7481", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "12", "dt": "sen",
                  "unique_number": "7482", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "13", "dt": "sen",
                  "unique_number": "7483", "env_code": "SEEIHI"},
                 {"id": "12", "dk": "14", "dt": "sen",
                  "unique_number": "7484", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "15", "dt": "sen",
                  "unique_number": "7485", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "16", "dt": "sen",
                  "unique_number": "7486", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "17", "dt": "sen",
                  "unique_number": "7487", "env_code": "SEEIHI"},
                 {"id": "13", "dk": "18", "dt": "sen",
                  "unique_number": "7488", "env_code": "SEEIHI"},
                 {"id": "14", "dk": "19", "dt": "sen",
                  "unique_number": "7489", "env_code": "SEEIHI"},
                 {"id": "15", "dk": "20", "dt": "sen",
                  "unique_number": "7490", "env_code": "SEEIHI"},
             ]}
         ]}
    ]
    from multiprocessing import Queue
    option = {
        "conn": {"host": "farmos003.jinong.co.kr", "port": 1883, "keepalive": 60},
        "db": {"host": "localhost", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt": {"svc": "cvtgate", "id": "2"},
        "area": "local"
    }

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = McopiaMate(option, devinfo, "23", quelist)
    dsmate = MqttForwardMate(option, devinfo, "23", quelist)
    ssmate.start()
    dsmate.start()

    time.sleep(50)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
