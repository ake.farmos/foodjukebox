#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#
# utility for farmos 

class FarmOS:
    _CODESET = {
        "status" : 0, 
        "value" : 1,
        "position" : 2, 
        "state-hold-time" : 3,
        "remain-time": 4,
        "ratio" : 5,
        "control" : 6,
        "area" : 7,
        "alert" : 8,
        "rawvalue" : 9,
        "image" : 10,
        "availability" : 11,
        "estimated" : 12,
        "vcnt" : 21,
        "vavg" : 22,
        "vmax" : 23,
        "vmin" : 24,
        "vstd" : 25,
        "diffstd" : 26
    }

    def getdataid(devid, code):
        if code in FarmOS._CODESET:
            return 10000000 + int(devid) * 100 + int(FarmOS._CODESET[code])

if __name__ == "__main__":
    print ("sensor", 2, "observation dataid", FarmOS.getdataid(2, "value"))

