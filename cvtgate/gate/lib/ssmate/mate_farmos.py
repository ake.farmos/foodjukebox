#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from collections import deque

from .. import *

from .farmosdb import FarmosDB

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class FarmosMate(SSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(FarmosMate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = JNMQTT(option, DevInfo(devinfo), self._logger)
        self._farmosdb = FarmosDB(option, DevInfo(devinfo), self._logger)
        self._reqs = deque()

    def connect(self):
        self._jnmqtt.connect(True)
        self._farmosdb.connect()
        super(FarmosMate, self).connect()
        return True

    def close(self):
        self._jnmqtt.close()
        self._farmosdb.close()
        super(FarmosMate, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        self._farmosdb.start()
        super(FarmosMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        self._farmosdb.stop()
        super(FarmosMate, self).matestop()

    def processresponse(self, blk):
        self._jnmqtt.writeblk(blk)
        self._farmosdb.writeblk(blk)

    def processobsnoti(self, blk):
        self._jnmqtt.writeblk(blk)
        self._farmosdb.writeblk(blk)

    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        print(("FarmosMate Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos)))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        self._reqs.append((blk.topic, msg))

    def doextra(self):
        while True:
            try:
                topic, msg = self._reqs.popleft()
            except:
                return 

            tmp = topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                pass

            if BlkType.isrequest(msg.gettype()):
                self._farmosdb.writeblk(msg)
                self._writereq(msg)
            else:
                self._logger.warn("Wrong message : " + msg.stringify())

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "conn" : {"host" : "localhost", "port" : 1883, "keepalive" : 60},
        "db" : {"host": "localhost", "user": "farmos", "password": "farmosv2@", "db": "farmos"},
        "mqtt" : {"svc" : "cvtgate", "id" : "1"},
        "area" : "local"
    }

    devinfo = [
        {"id" : "0", "dk" : "0", "dt": "gw", "children" : [
            {"id" : "1", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "11", "dk" : "11", "dt": "sen"},
                {"id" : "12", "dk" : "12", "dt": "act"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = FarmosMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/req/1", cmd.stringify(), hostname="dev.jinong.co.kr")
    print("published")

    time.sleep(5)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
