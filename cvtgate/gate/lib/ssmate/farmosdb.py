#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#
# mate for interaction with farmos 

import json
import time
import datetime
import pymysql
import logging

from .. import *

'''
option : {
    "conn" : {"host" : "localhost", "user" : "root",
              "password" : "pass", "db" : "db"}
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class FarmosDB:
    _CUROBS_QUERY = "update current_observations set obs_time = %s, nvalue = %s where data_id = %s"
    _OBS_QUERY = "insert observations(obs_time, nvalue, data_id) values(%s, %s, %s)"
    _DUPOBS_QUERY = "insert observations(obs_time, nvalue, data_id) values(%s, %s, %s) ON DUPLICATE KEY UPDATE nvalue = %s"
    _REQINS_QUERY = "insert requests(opid, device_id, command, params) values(%s, %s, %s, %s)"
    _REQUPS_QUERY = "update requests set status = %s, exectime = now() where opid = %s and senttime >= DATE_SUB(NOW(), INTERVAL 1 MINUTE)"
    _REQFIN_QUERY = "update requests set finishtime = now() where device_id = %s and opid = %s "
    _REQFUP_QUERY = "update requests set finishtime = null where device_id = %s and opid = %s "
    _CODESET = {
        "status" : 0, 
        "value" : 1,
        "position" : 2, 
        "state-hold-time" : 3,
        "remain-time": 4,
        "ratio" : 5,
        "control" : 6,
        "area" : 7,
        "alert" : 8,
        "rawvalue" : 9
    }

    def __init__(self, option, devinfo, logger):
        self._conn = None
        self._option = option
        self._devinfo = devinfo
        self._logger = logging.getLogger('farmos') if logger is None else logger
        self._lastopid = {}     # Request 의 OPID가 아니라 Notice에서 올라오는 OPID가 기준임.
        self._actuators = {}

    def start(self):
        pass

    def connect(self):
        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                                             password=copt["password"], db=copt["db"])
        self._cur = self._conn.cursor()
        self._logger.info("Connection initialized.")

    def close(self):
        self._cur.close()
        self._conn.close()
        self._logger.info("Connection closed.")

    def stop(self):
        pass

    def readmsg(self):
        pass

    def getopid(self, did):
        try:
            return self._lastopid[did]
        except:
            return None

    def setopid(self, did, opid):
        self._lastopid[did] = opid
        print("setopid", did, self._lastopid[did])

    def finishrequest(self, did, opid):
        print(FarmosDB._REQFIN_QUERY, [did, opid])
        try:
            self._cur.execute(FarmosDB._REQFIN_QUERY, [did, opid])
            self._conn.commit()
        except pymysql.IntegrityError as ex:
            self._logger.info("DB Integrity exception clear opid : " + str(ex))
        except Exception as ex:
            self._logger.warn("DB exception clear opid : " + str(ex))

    def notfinishrequest(self, did, opid):
        print(FarmosDB._REQFUP_QUERY, [did, opid])
        try:
            self._cur.execute(FarmosDB._REQFUP_QUERY, [did, opid])
            self._conn.commit()
        except pymysql.IntegrityError as ex:
            self._logger.info("DB Integrity exception clear opid : " + str(ex))
        except Exception as ex:
            self._logger.warn("DB exception clear opid : " + str(ex))

    def _writedata(self, time, nvalue, dataid, option=None):
        if dataid is None:
            return 0
        params = [time, nvalue, dataid]
        print("_writedata", FarmosDB._CUROBS_QUERY, params)
        try:
            self._cur.execute(FarmosDB._CUROBS_QUERY, params)
            if option == "updatable":
                params.append(nvalue)
                self._cur.execute(FarmosDB._DUPOBS_QUERY, params)
            else:
                self._cur.execute(FarmosDB._OBS_QUERY, params)
            self._conn.commit()
            return 0
        except pymysql.IntegrityError as ex:
            self._logger.info(FarmosDB._OBS_QUERY + " " + str(params))
            self._logger.info("DB Integrity exception obs: " + str(ex))
            return 0
        except Exception as ex:
            self._logger.warn("DB exception obs : " + str(ex))
            return 1

    def getdataid(self, devid, code):
        if code in FarmosDB._CODESET:
            return 10000000 + int(devid) * 100 + int(FarmosDB._CODESET[code])
        else:
            self._logger.warn("No datacode : " + code)
            return None

    def checkdata(self, dataid, value, status):
        if status == StatCode.READY:
            pass # should check
            #self._logger.warn("Abnormal data : " + str(value))
            return status # should return new status
        return status

    def writeobs(self, blk):
        content = blk.getcontent()
        tm = content.pop('time', None)
        ret = 0
        for devid, values in blk.getcontent().items():
            dev = self._devinfo.finddevbyid(devid)
            if dev is None:
                self._logger.warn("There is no device : " + str(devid))
                continue 

            if DevType.issensor(dev["dt"]):
                vdid = self.getdataid(devid, "value")
                if vdid:
                    status = self.checkdata(vdid, values[0], values[1])
                    if status == StatCode.READY:
                        ret = ret + self._writedata(tm, values[0], vdid)
                    ret = ret + self._writedata(tm, status, self.getdataid(devid, "status"))
                    try:
                        if values[0] != values[2] and value[2] is not None:
                            ret = ret + self._writedata(tm, values[2], self.getdataid(devid, "rawvalue"))
                    except:
                        pass

            # Notification is enough for actuators
            #elif DevType.isactuator(dev["dt"]):
            #    self._writedata(tm, values[1], self.getdataid(devid, "status"))
        return True if ret == 0 else False

    def writenoti(self, blk):
        content = blk.getcontent()
        tm = content.pop("time")
        code = content.pop("code")
        if code == NotiCode.ACTUATOR_STATUS: # Actuator Status
            for devid, noti in content.items():
                did = str(devid)
                if "opid" in noti and "status" in noti:
                    opid = self.getopid(did)
                    if opid != noti["opid"]: # OPID 가 변경되는 시점이 중요
                        if opid == 0 and noti["status"] != 0:   # 새로운 명령 시작
                            self.notfinishrequest(did, noti["opid"])
                        if opid == 0 and noti["status"] == 0:   # 중지 명령. 중지명령에 대해서 처리. 
                            self.finishrequest(did, noti["opid"])
                        if opid is not None and opid != 0 and noti["status"] == 0:   # 중지명령.
                            self.finishrequest(did, noti["opid"])
                        # 달라졌다면 기본적으로 이전 명령은 종료.
                        self.finishrequest(did, opid)

                        self.setopid(did, noti["opid"])
                    else:
                        if opid != 0 and noti["status"] == 0:     # 정상 종료
                            self.finishrequest(did, noti["opid"])

                # 실제로 상태가 변경되는 시점에서 혹은 저장한지 1분이 지나면 디비로 기록한다.
                cur = datetime.datetime.now()
                if did not in self._actuators or self._actuators[did][0] != noti["status"] or (cur - self._actuators[did][1]).total_seconds() >= 60:
                    self._actuators[did] = [noti["status"], cur]
                    for key, value in noti.items():
                        dataid = self.getdataid(devid, key)
                        if dataid:
                            self._writedata(tm, value, dataid)

        elif code >= NotiCode.DETECT_NODE_STARTED and code < NotiCode.ACTUATOR_STATUS: # device detection
            pass
        elif code == NotiCode.EXTERNAL_DATA:
            for row in content["data"]:
                dt = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(row["time"]))
                self._writedata(dt, row["value"], row["id"], content["option"])

        else: # Just Notification
            pass
        return True

    def writeblk(self, blk):
        print("farmosdb writeblk", blk.getcontent())
        ret = True

        if self._conn.open:
            pass
        else:
            self.connect()

        if BlkType.isobservation(blk.gettype()):
            ret = self.writeobs(blk)

        elif BlkType.isrequest(blk.gettype()):
            content = blk.getcontent()
            if content["cmd"] == CmdCode.DETECT_DEVICE or content["cmd"] == CmdCode.CANCEL_DETECT:
                self._logger.info("User tries to detect devices or cancel detecting.")
            else:
                if "param" not in content:
                    self._logger.warn("Request needs 'param' " + blk.stringify())
                    return True
                
                params = [content["opid"], content["id"], content["cmd"], json.dumps(content["param"])]
                did = str(content["id"])
                try:
                    self._cur.execute(FarmosDB._REQINS_QUERY, params)
                    self._conn.commit()
                    self._logger.info("Device [" + did + "] get a request. " + str(content))
                except pymysql.IntegrityError as ex:
                    self._logger.info("DB Integrity exception req : " + str(ex))
                    ret = False
                except Exception as ex:
                    self._logger.warn("DB exception req : " + str(ex))
                    ret = False

        elif BlkType.isresponse(blk.gettype()):
            content = blk.getcontent()
            params = [content["res"], content["opid"]]
            print(FarmosDB._REQUPS_QUERY, params)
            try:
                self._cur.execute(FarmosDB._REQUPS_QUERY, params)
                self._conn.commit()
            except pymysql.IntegrityError as ex:
                self._logger.info("DB Integrity exception res : " + str(ex))
                ret = False
            except Exception as ex:
                self._logger.warn("DB exception res : " + str(ex))
                ret = False

        elif BlkType.isnotice(blk.gettype()):
            ret = self.writenoti(blk)

        else:
            # error log
            self._logger.warn("wrong type messsage : " + blk.stringify())

        if ret == False:
            self._logger.warn("Writing a block is failed.")
            return False

        return True

if __name__ == "__main__":
    pass
