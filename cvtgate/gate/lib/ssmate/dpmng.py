#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#
# data platform manager for interaction with farmos DP

import json
import datetime

from .farmos import FarmOS
from .dbmng import MySQLManager

class DPMasterManager(MySQLManager):
    def loadfarms(self, opt):
        if "farms" in opt:
            _QUERY = "select farm_id, datainfo, data_node from farm where deleted = 0 and farm_id in %s and data_node = %s"
            rows = self._select(_QUERY, [opt["farms"], opt["host"]])
        else:
            _QUERY = "select farm_id, datainfo, data_node from farm where deleted = 0 and data_node = %s"
            rows = self._select(_QUERY, [opt["host"]])
        farms = {}

        for row in rows:
            try:
                info = json.loads(row[1])
            except Exception as ex:
                self._logger.info ("Wrong data information : " + str(row) + str(ex))
                continue

            if 'db' in info and 'host' not in info['db']:
                info['db']['host'] = row[2]

            info['farmid'] = row[0]
            info['fields'] = self.loadfields(row[0])
            info['devices'] = self.loaddevices(row[0], info['fields'])
            info['dataids'] = self.loaddataids(row[0])
            # UI에서 설정이 id, pw 로 되어 있는데, 기존 cvtgate와의 호환성을 위해서 정보 생성
            info['db']['user'] = info['db']['id']
            info['db']['password'] = info['db']['pw']

            farms[row[0]] = info
        return farms

    def loadcouples(self, farms):
        _QUERY = "select g.farm_id, c.gateid, c.id from couple c, gate g where c.deleted = 0 and g.deleted = 0 and c.gateid = g.id and g.farm_id in %s"
        if len(farms) == 0:
            return {}, {}

        keys = list(farms.keys())
        rows = self._select(_QUERY, [keys])
        couples = {}
        gates = {}
        for row in rows:
            couple = {"farmid": row[0], "gateid": row[1], "coupleid": row[2]}
            couples[row[2]] = couple
            gates[row[1]] = row[0]
        return couples, gates

    def loaddevices(self, farmid, fseasons):
        classstr = {"gateway" : "gw", "node" : "nd", "sensor" : "sen", "actuator" : "act"}

        _QUERY = "select d.id, f.id, d.spec from device_field df, fields f, devices d where f.farm_id = %s and d.farm_id = f.farm_id and df.field_id = f.id and df.farm_id = f.farm_id and d.id = df.device_id and f.deleted = 0 and d.deleted = 0"
        rows = self._select(_QUERY, [farmid])
        devices = {}
        for row in rows:
            try:
                spec = json.loads(row[2])
                devtype = classstr[spec["Class"]] + "/" + spec["Type"]
            except Exception as ex:
                self._logger.info("Fail to load device spec : " + str(row) + str(ex))
                continue

            devices[row[0]] = {"type" : devtype, "fields" : []}
            if row[1] == 0: # 외부라면 모든 작기 데이터에 추가되어야 함
                for fid, info in fseasons.items():
                    devices[row[0]]["fields"].append({'fieldid' : fid, 'postfix': info['postfix']})
            else:
                if row[1] in fseasons:
                    devices[row[0]]["fields"].append({'fieldid' : row[1], 'postfix': fseasons[row[1]]['postfix']})
                else:
                    self._logger.info(str(row[0]) + " devices was installed on farm-field " + str(farmid) + "-" + str(row[1]) + ", but no season.")
        return devices

    def loadfields(self, farmid):
        _QUERY = "select id from fields where farm_id = %s and id != 0 and deleted = 0"
        rows = self._select(_QUERY, [farmid])
        fields = {}
        for row in rows:
            fields[row[0]] = self.loadlastseason(farmid, row[0])
        return fields

    def loadlastseason(self, farmid, fldid):
        _QUERY = "select season_id, postfix from season where deleted = 0 and field_id = %s and farm_id = %s order by season_id desc limit 1"
        rows = self._select(_QUERY, [fldid, farmid])
        if len(rows) == 0:
            return {'seasonid' : 0, 'postfix' : '_' + str(farmid) + '_' + str(fldid)}
        else:
            return {'seasonid' : rows[0][0], 'postfix' : rows[0][1]}

    def loaddataids(self, farmid):
        _QUERY = "select id, field_id from dataindexes where farm_id = %s and deleted = 0"
        rows = self._select(_QUERY, [farmid])
        dataids = {}
        for row in rows:
            dataids[row[0]] = row[1]
        return dataids

class DPDataManager(MySQLManager):
    def __init__(self, option, logger):
        super(DPDataManager, self).__init__(option, logger)
        self._systemids = {"loads_1m" : 51, "loads_5m": 52, "loads_15m" : 53,
                   "vmem" : 54, "swap": 55, "disk" : 56,
                   "ioread" : 57, "iowrite" : 58, "netsent" : 59, "netrecv" : 60}
        self._farms = None
        self._couples = None
        self._gates = None

    def setfarms(self, farms):
        self._farms = farms 

    def setcouples(self, couples, gates):
        self._couples = couples
        self._gates = gates 

    def getfarms(self):
        return self._farms

    def getfarmid(self, coupleid):
        try:
            if coupleid in self._couples:
                return self._couples[coupleid]["farmid"]
            elif coupleid in self._gates:
                return self._gates[coupleid]
            self._logger.info("Fail to find couple info : " + coupleid)
        except Exception as ex:
            self._logger.info("Fail to find couple info : " + coupleid + " " + str(ex))
        return None

    def getdevicefield(self, farmid, devid):
        try:
            return self._farms[farmid]["devices"][int(devid)]["fields"]
        except Exception as ex:
            self._logger.info("Fail to find device field info : " + str([farmid, devid]) + str(self._farms[farmid]["devices"]) + " : " + str(ex))
            return None

    def getfields(self, farmid):
        try:
            return self._farms[farmid]["fields"]
        except Exception as ex:
            self._logger.info("Fail to find fields info : " + str(farmid))
            return None

    def updatecouples(self, coupleid, status):
        _QUERY = "update couple set status = %s where id = %s"
        self._execute(_QUERY, [status, coupleid])
        # gate 상태 업데이트도 해야함.

    def getlastsensorinfo(self, sid, farmid=None):
        valid = FarmOS.getdataid(sid, "value")
        sigmaid = FarmOS.getdataid(sid, "diffstd")
        _SENINFO_QUERY = "select data_id, obs_time, nvalue from current_observations where data_id in %s and farm_id = %s"
        rows = self._select(_SENINFO_QUERY, [[valid, sigmaid], farmid])
        if rows and len(rows) == 2:
            if rows[0][0] == valid:
                return (rows[0][2], rows[0][1], rows[1][2])
            else:
                return (rows[1][2], rows[1][1], rows[0][2])
        elif rows and len(rows) == 1: # no diffstd
            return (rows[0][2], rows[0][1], None)
        return (None, None, None)

    def insertrequest(self, request, farmid):
        content = request.getcontent()
        fields = self.getdevicefield(farmid, content["id"])
        for fld in fields:
            _REQINS_QUERY = "insert requests" + fld['postfix'] + "(opid, device_id, command, params, farm_id) values(%s, %s, %s, %s, %s)"
            params = [content["opid"], content["id"], content["cmd"], json.dumps(content["param"]), farmid]
            self._execute(_REQINS_QUERY, params)

    def execrequest(self, response, farmid):
        content = response.getcontent()
        fields = self.getdevicefield(farmid, content["id"])
        for fld in fields:
            _REQUPS_QUERY = "update requests" + fld['postfix'] + " set status = %s, exectime = now() where opid = %s and device_id = %s and farm_id = %s"
            params = [content["res"], content["opid"], content["id"], farmid]
            self._execute(_REQUPS_QUERY, params)

    def finishrequest(self, did, opid, farmid):
        fields = self.getdevicefield(farmid, did)
        for fld in fields:
            _REQFIN_QUERY = "update requests" + fld['postfix'] + " set finishtime = now() where opid = %s and device_id = %s and farm_id = %s"
            self._execute(_REQFIN_QUERY, [opid, did, farmid])

    def unfinishrequest(self, did, opid, farmid):
        fields = self.getdevicefield(farmid, did)
        for fld in fields:
            _REQFUP_QUERY = "update requests" + fld['postfix'] + " set finishtime = null where opid = %s and device_id = %s and farm_id = %s"
            self._execute(_REQFUP_QUERY, [opid, did, farmid])

    def writeobservation(self, devid, tm, values, farmid):
        dataid = FarmOS.getdataid(devid, "status")
        codes = [FarmOS._CODESET["status"], FarmOS._CODESET["value"],
                FarmOS._CODESET["rawvalue"], FarmOS._CODESET["estimated"]]
        fields = self.getdevicefield(farmid, devid)

        if fields:
            for idx, val in enumerate(values):
                for fld in fields:
                    #self._logger.info("Write Observation : " + str([tm, val, dataid + codes[idx], farmid]) + str(fld['postfix']))
                    self._writedata([tm, val, dataid + codes[idx], farmid], fld['postfix'])

    def updatestatus(self, devid, content, farmid):
        self.writestatus(devid, content, farmid, "onlyupdate")

    def writestatus(self, devid, content, farmid, option=None):
        fields = self.getdevicefield(farmid, devid)
        self._logger.info("Write Status Start : " + str([farmid, devid, fields]))
        tm = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        if fields:
            for key, value in content.items():
                if key == "opid":
                    continue
                dataid = FarmOS.getdataid(devid, key)
                for fld in fields:
                    self._logger.info("Write Status: " + str([tm, value, dataid, farmid]) + str(fld['postfix']))
                    self._writedata([tm, value, dataid, farmid], fld['postfix'], option)

    def writesystemusage(self, tm, content, farmid):
        fields = self.getfields(farmid)
        if fields:
            for key, value in content.items():
                if key in self._systemids:
                    for fid, fld in fields.items():
                        self._writedata([tm, value, self._systemids[key], farmid], fld['postfix'])

    def writeruledata(self, tm, content, farmid):
        fields = self.getfields(farmid)
        if len(fields) == 0:
            self._logger.warn("Farm [" + str(farmid) + "] has no field.")
            return

        for key, value in content.items():
            if key in ["time", "code"]:
                continue
            dataid = int(key)
            if dataid not in self._farms[farmid]["dataids"]:
                self._logger.warn("There is no data id. " + str([farmid, dataid]) + str(content))
                continue

            fid = self._farms[farmid]["dataids"][dataid]
            if fid is None:
                self._logger.warn("There is no field. " + str([farmid, dataid]) + str(content))
                continue

            self._logger.info("writeruledata : " + str([farmid, fid, dataid]) + " " + str(value))
            if fid == 0:
                for fid, fld in fields.items():
                    self._writedata([tm, value, dataid, farmid], fld['postfix'])
            else:
                self._writedata([tm, value, dataid, farmid], fields[fid]["postfix"])

    def _writedata(self, params, postfix, option=None):
        # params = [time, nvalue, dataid, farmid]
        if params[2] is None:
            return 0

        _UPDOBS_QUERY = "update current_observations set obs_time = %s, nvalue = %s where data_id = %s and farm_id = %s"
        _INSOBS_QUERY = "insert observations" + postfix + "(obs_time, nvalue, data_id, farm_id) values(%s, %s, %s, %s)"
        _DUPOBS_QUERY = "insert observations" + postfix + "(obs_time, nvalue, data_id, farm_id) values(%s, %s, %s, %s) ON DUPLICATE KEY UPDATE nvalue = %s"

        self._execute(_UPDOBS_QUERY, params, True)
        if option == "updatable":
            params.append(nvalue)
            self._execute(_DUPOBS_QUERY, params, True)
        elif option != "onlyupdate":
            self._execute(_INSOBS_QUERY, params, True)
        return 0

    # Special Method for DataEstimationModel
    def getcurrentdata(self, dataid, farmid=None):
        if farmid:
            _SELOBS_QUERY = "select unix_timestamp(obs_time), nvalue from current_observations where data_id = %s and farm_id = %s"
            rows = self._select(_SELOBS_QUERY, [dataid])
            return rows[0]
        return None

    # Special Method for DataEstimationModel
    def getdataduring(self, dataid, fromtime, totime, farmid=None):
        if farmid:
            _SELOBS_QUERY = "select unix_timestamp(obs_time), nvalue from observations where data_id = %s and obs_time between from_unixtime(%s) and from_unixtime(%s)"
            return self._select(_SELOBS_QUERY, [dataid, fromtime, totime])
        return []

class FDPManager:
    @staticmethod
    def loadDPDataManager(option, logger):
        option["db"] = option["masternode"]
        masterdbm = DPMasterManager(option, logger)
        masterdbm.connect()
        farms = masterdbm.loadfarms(option['datanode'])
        logger.info(str(len(farms)) + " farms are loaded.")
        couples, gates = masterdbm.loadcouples(farms)
        logger.info(str(len(couples)) + " couples are loaded.")
        logger.info(str(len(gates)) + " gates are loaded.")
        masterdbm.close()

        print (farms, couples, gates)

        # 다중 디비는 다루지 않고, 하나의 디비만 다루는 것으로 함.
        datadbm = DPDataManager(farms[next(iter(farms))], logger)
        datadbm.setcouples(couples, gates)
        print(couples)
        datadbm.setfarms(farms)
        print(farms)
        return datadbm

if __name__ == "__main__":
    import time
    import logging
    opt = {
        "db" : {"host" : "49.50.172.135", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"},
        "datanode" :  {"host" : "49.50.172.135", "farms" : [1,2]}
    }
    dm = FDPManager.loadDPDataManager(opt, logging.getLogger(''))
    dm.connect()
    print(dm.getdevicefield(1, 3))
    dm.close()

