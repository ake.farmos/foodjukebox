#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
from enum import IntEnum
from threading import Thread, Lock
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.client.sync import ModbusTcpClient

from .. import *
from .mate_ksx3267v3 import STDDevice, STDConnection, KSX3267MateV3, RegisterMap

class HSDevice(object):
    """ 경농 관수기에만 사용되는 디바이스 """
    """ dk 가 ["HS", [ ], []] 형식이어야 함 """
    def __init__(self, devid, dk, dt, register, logger):
        self._devid = devid
        self._dk = dk
        self._dt = dt
        self._logger = logger
        self._lastopid = 0
        self._ftime = 0
        self._status = StatCode.READY.value
        self._devices = [STDDevice(0, dk[1], dt, register, logger), STDDevice(1, dk[2], dt, register, logger)]

    def getdevicetype(self):
        return self._dt

    def update(self):
        if self._status == StatCode.WORKING.value and self._ftime <= time.time():
            self.turnoff()

    def _makereq(self):
        req = Request(None)
        req.setcommand(self._lastopid, CmdCode.TIMED_ON, {"hold-time" : 4})
        return req

    def turnon(self):
        req = self._makereq()
        self._status = StatCode.WORKING.value
        ret = self._devices[0].processrequest(req)
        self._devices[0].setopid(0)
        return ret

    def turnoff(self):
        req = self._makereq()
        self._status = StatCode.READY.value
        self._ftime = 0
        ret = self._devices[1].processrequest(req)
        self._devices[1].setopid(0)
        return ret

    def readinfo(self, direct=False):
        self.update()
        remain = self._ftime - time.time()
        return {"opid" : self._lastopid, "status": self._status, "remain-time": 0 if remain < 0 else remain}

    def processrequest(self, request):
        operation = request.getcommand()
        self._lastopid = request.getopid()

        if operation == CmdCode.OFF.value:
            return self.turnoff()
        elif operation == CmdCode.ON.value:
            self._ftime = 1000000 + time.time()
            return self.turnon()
        elif operation == CmdCode.TIMED_ON.value:
            params = request.getparams()
            if "hold-time" in params:
                self._ftime = time.time() + params['hold-time']
                return self.turnon()
            else:
                return ResCode.FAIL_WRONG_KEYWORD
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND

class HSConnection(STDConnection):
    def setupnode(self, node, option):
        dk = json.loads(node["dk"])
        size = 0 if "readsize" not in option else option["readsize"]
        register = RegisterMap(self._conn, dk[0], self._lock, size, self._logger)
        self._registers[node["id"]] = register

        nodedev = STDDevice(node["id"], json.loads(node["dk"]), node["dt"], register, self._logger) 
        devices = {}
        for dev in node["children"]:
            jsondk = json.loads(dev["dk"])
            if jsondk[0] == "HS":
                devices[dev["id"]] = HSDevice(dev["id"], jsondk, dev["dt"], register, self._logger)
            else:
                devices[dev["id"]] = STDDevice(dev["id"], jsondk, dev["dt"], register, self._logger)
        self._devices[node["id"]] = (nodedev, devices)
        register.optimize()
        return (register, devices)

class HSKSX3267Mate(KSX3267MateV3):
    def connect(self):
        for copt in self._option["conn"]:
            dinfo = self._devinfo.findgatewaybydk(copt["name"])
            if dinfo:
                conn = HSConnection(copt, dinfo, self._logger)
                for nid in conn.getnodeids():
                    self._nidconn[nid] = conn
                self._conn[copt["name"]] = conn
                self._logger.info(copt["name"] + " makes connection " + str(conn.getnodeids()))
            else:
                self._logger.info(copt["name"] + " didn't make connection since no device")

        self._logger.info("Mate connected : " + str(self._nidconn))

        self._detector.setupconn(self._conn)
        super(KSX3267MateV3, self).connect()

if __name__ == "__main__":
    from multiprocessing import Queue

    quelist = [Queue(), Queue(), Queue(), Queue()]
    opt = {
        'conn' : [{
            'method': 'rtu',
            'name' : 'USB0',
            'port' : '/dev/ttyUSB0',
            'baudrate' : 9600,
            'timeout': 5
        }]
    }
    
    devinfo = [{
        "id" : "1", "dk" : "USB0", "dt": "gw", "children" : [ {
            "id" : "101", "dk" : '[1,201,["status"],301,["operation","opid"]]', "dt": "nd", "children" : [
                {"id" : "102", "dk" : '[1,210,["value","status"]]', "dt": "sen"},
                {"id" : "103", "dk" : '[1,220,["value","status"]]', "dt": "sen"}
            ]
         } ]
    }]

    kdmate = HSKSX3267Mate(opt, devinfo, "1", quelist)
    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    kdmate.start ()
    print("mate started")

    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    kdmate.stop()
    ssmate.stop()
    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()


