#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
from enum import IntEnum
from threading import Thread, Lock
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.client.sync import ModbusTcpClient

from .. import *

_UNIT = 0x1

class FJDevice(object):
    def __init__(self, devid, dk, logger):
        self._addr = dk["addr"]
        self._devid = devid
        self._status = StatCode.READY
        self._logger = logger
        self._conn = None

    def setupconn(self, conn):
        self._conn = conn

    def getid(self):
        return self._devid

    def getstatus(self):
        return self._status

    def readregisters(self, addr, size, vtype):
        try:
            res = self._conn.read_holding_registers(addr, size, unit=_UNIT)
        except Exception as ex:
            self._logger.warn("fail to read registers. : " + str(ex))
            return None

        if res is None or res.isError():
            self._logger.warn("fail to read registers [" + str(addr) + "]. " + str(res))
            return None

        if vtype == 'i':
            return struct.unpack('i', struct.pack('HH', res.registers[0], res.registers[1]))[0]
        elif vtype == 'f':
            return struct.unpack('f', struct.pack('HH', res.registers[0], res.registers[1]))[0]
        elif vtype == 's':
            return res.registers[0]
        else:
            return None

class FJSensor(FJDevice):
    def __init__(self, devid, dk, logger):
        super(FJSensor, self).__init__(devid, dk, logger)
        self._value = 0
        
    def update(self):
        val = self.readregisters (self._addr, 2, 'f')
        if val is None or val <= -9000:
            self._status = StatCode.ERROR
        else:
            self._status = StatCode.READY
            self._value = val
        return self._status

    def getvalue(self):
        return self._value

class FJSwitch(FJDevice):
    def __init__(self, devid, dk, logger):
        super(FJSwitch, self).__init__(devid, dk, logger)
        self._opid = 0
        self._cmd = {
            CmdCode.OFF
        }
        self._rtime = 0

    def getopid(self):
        return self._opid

    def getrtime(self):
        return self._rtime

    def getcontent(self):
        content = {"status" : self.getstatus().value, "opid" : self.getopid(), "remain-time" : self.getrtime()}
        return content

    def update(self):
        rtime = self.readregisters(self._addr, 2, 'i')
        if rtime is None or rtime < 0:
            self._status = StatCode.ERROR
        elif rtime > 0:
            self._status = StatCode.WORKING
            self._rtime = rtime
        else:
            self._status = StatCode.READY
            self._rtime = 0
        return self._status

    def writeregisters(self, addr, registers):
        print ("write registers : ", addr, registers)
        try:
            if len(registers) > 1:
                res = self._conn.write_registers(addr, registers, unit=_UNIT)
            else:
                res = self._conn.write_register(addr, registers[0], unit=_UNIT)
        except Exception as ex:
            self._logger("Fail to write a request : " + str(ex))
            return ResCode.FAIL_TO_WRITE
        
        if res is None or res.isError():
            self._logger.warn("Fail to write a request to dev. " + str(addr) + " " + str(res))
            return ResCode.FAIL_TO_WRITE
        return ResCode.OK

    def processrequest(self, request):
        operation = request.getcommand()
        self._opid = request.getopid()

        if operation == CmdCode.TIMED_ON:
            tm = request.getparams()['hold-time']
        elif operation == CmdCode.ON:
            tm = 100000
        elif operation == CmdCode.OFF:
            if self._status == StatCode.READY:
                return ResCode.OK
            tm = 0
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND

        registers = struct.unpack('HH', struct.pack('i', tm))
        if self.writeregisters(self._addr, registers) != ResCode.OK:
            return ResCode.FAIL_TO_WRITE

        time.sleep(1)
        self.update()
        return ResCode.OK

class FJSwitch2(FJSwitch):
    def __init__(self, devid, dk, logger):
        super(FJSwitch2, self).__init__(devid, dk, logger)
        self._ratio = 0
        self._raddr = dk["addr_ratio"]

    def getratio(self):
        return self._ratio

    def getcontent(self):
        content = {"status" : self.getstatus().value, "opid" : self.getopid(), 
                   "remain-time" : self.getrtime(), "ratio" : self.getratio()}
        return content

    def update(self):
        rtime = self.readregisters(self._addr, 2, 'i')
        ratio = self.readregisters(self._raddr, 1, 's')
        if rtime is None or ratio is None or rtime < 0:
            self._status = StatCode.ERROR
        elif rtime > 0:
            self._status = StatCode.WORKING
            self._rtime = rtime
            self._ratio = ratio
        else:
            self._status = StatCode.READY
            self._ratio = ratio
            self._rtime = 0
        return self._status

    def processrequest(self, request):
        operation = request.getcommand()
        self._opid = request.getopid()
        if operation == CmdCode.DIRECTIONAL_ON:
            tm = request.getparams()['hold-time']
            ratio = request.getparams()['ratio']
        elif operation == CmdCode.TIMED_ON:
            tm = request.getparams()['hold-time']
            ratio = 100
        elif operation == CmdCode.ON:
            tm = 10000 
            ratio = 100
        elif operation == CmdCode.OFF:
            tm = 0
            ratio = 0
        else:
            return ResCode.FAIL_NOT_PROPER_COMMAND

        if self.writeregisters(self._raddr, (int(ratio),)) != ResCode.OK:
            return ResCode.FAIL_TO_WRITE

        time.sleep(1)
        registers = struct.unpack('HH', struct.pack('i', tm))
        if self.writeregisters(self._addr, registers) != ResCode.OK:
            return ResCode.FAIL_TO_WRITE

        time.sleep(1)
        self.update()
        return ResCode.OK
   

class FJBoxMate(DSMate):
    _SLEEP = 0.5
    _OBS_TIMEOUT = 50

    _ADDR_ID = 0x0
    _NODE_ID = 0xAC20
    _ADDR_SERIAL = 0x1

    def __init__(self, option, devinfo, coupleid, quelist):
        super(FJBoxMate, self).__init__(option, devinfo, coupleid, quelist)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._conn = None
        self._devices = self.setupdevices(devinfo)
        self._boxid = None
        self._serial = None
        self._devidsfornoti = []
        self._logger.info("FJBoxMate Started.")

    def setupdevices(self, devinfo):
        devs = {"nd" : None, "sen" : [], "act" : []}
        for gw in devinfo:
            for nd in gw["children"]:
                dk = json.loads(nd["dk"])
                devs["nd"] = FJDevice(nd["id"], dk, self._logger)
                for dev in nd["children"]:
                    print (dev)
                    dk = json.loads(dev["dk"])
                    if dev["dt"] == "sen":
                        devs["sen"].append (FJSensor(dev["id"], dk, self._logger))
                    elif dev["dt"] == "act/switch/level1":
                        devs["act"].append (FJSwitch(dev["id"], dk, self._logger))
                    elif dev["dt"] == "act/switch/level2":
                        devs["act"].append (FJSwitch2(dev["id"], dk, self._logger))
                    else:
                        self._logger.info("Unmatched devices information : " + str(dev))
        return devs

    def readnodeinfo(self):
        try:
            res = self._conn.read_holding_registers(0, 2, unit=_UNIT)
        except Exception as ex:
            self._logger.warn("fail to read node info. : " + str(ex))
            return 

        if res is None or res.isError():
            self._logger.warn("fail to read nodeinfo registers(0~2). " + str(res))
            return 

        self._boxid = res.registers[0]
        self._serial = res.registers[1]
        self._logger.info ("Box ID is " + str(self._boxid) + " Serial is " + str(self._serial))

    def getboxid(self):
        return self._boxid

    def getserial(self):
        return self._serial

    def connect(self):
        opt = self._option['conn']
        ret = False
        conn = None

        if opt['method'] == 'rtu':
            conn =  ModbusSerialClient(method='rtu', port=opt['port'],
                    timeout=self._timeout, baudrate=opt['baudrate'])
            ret = conn.connect()
            msg = "failed to connect with rtu"
            code = NotiCode.RTU_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
        else:
            msg = "It's a wrong connection method. " + str(opt['method'])

        if ret == False:
            self._logger.warn(msg)
            noti = Notice(None, NotiCode.RTU_FAILED_CONNECTION) # detection is canceled
        else:
            noti = Notice(None, NotiCode.RTU_CONNECTED) # detection is canceled

        self._writenoti(noti)
        self._conn = conn

        self._devices["nd"].setupconn(conn)
        for dev in self._devices["sen"]:
            dev.setupconn(conn)
        for dev in self._devices["act"]:
            dev.setupconn(conn)
    
        self.readnodeinfo()

        super(FJBoxMate, self).connect()
        return ret

    def close(self):
        self._conn.close()
        super(FJBoxMate, self).close()

    def process(self):
        self.processrequests()
        self.processobservations()
        self.processnotices()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE):
            nid = self._devices["nd"].getid()
            obsblk = Observation(nid)
            obsblk.setobservation(nid, 0, self._devices["nd"].getstatus())

            for sen in self._devices["sen"]:
                sen.update()
                obsblk.setobservation(sen.getid(), sen.getvalue(), sen.getstatus())

            self._writeobs(obsblk)
            self._updatetime(Mate.OBSTYPE)

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE):
            nid = self._devices["nd"].getid()
            blk = Notice(nid, NotiCode.ACTUATOR_STATUS)
            for act in self._devices["act"]:
                act.update()
                blk.setcontent(act.getid(), act.getcontent())
            self._writenoti(blk)
            self._updatetime(Mate.NOTITYPE)
        elif self._timetocheck("actnoti"):
            nid = self._devices["nd"].getid()
            blk = Notice(nid, NotiCode.ACTUATOR_STATUS)
            shouldsend = False
            for act in self._devices["act"]:
                if act.getstatus() == StatCode.WORKING or act.getid() in self._devidsfornoti:
                    try:
                        self._devidsfornoti.remove(act.getid())
                    except:
                        pass
                    act.update()
                    blk.setcontent(act.getid(), act.getcontent())
                    shouldsend = True
            if shouldsend:
                self._writenoti(blk)
            self._updatetime("actnoti")

    def findactuator(self, devid):
        for act in self._devices["act"]:
            if str(act.getid()) == str(devid):
                return act
        return None
            
    def processrequest(self, req):
        """ 하나의 요청을 처리한다. """
        print("received request", req.getdevid(), self._coupleid)
        if BlkType.isrequest(req.gettype()) is False:
            self._logger.warn("The message is not request. " + str(req.gettype()))
            return False

        if str(req.getnodeid()) != str(self._devices["nd"].getid()):
            self._logger.warn("Node id is not matched. " + str(req.getnodeid()))
            return False
         
        act = self.findactuator(req.getdevid())
        if act is None:
            code = ResCode.FAIL_NO_DEVICE
        else:
            code = act.processrequest(req)

        response = Response(req)
        response.setresult(code)
        self._logger.info("write response: " + str(response))
        self._writeres(response)
        self._devidsfornoti.append(str(req.getdevid()))
        return True 

if __name__ == "__main__":
    from multiprocessing import Queue

    isnutri = False
    quelist = [Queue(), Queue(), Queue(), Queue()]
    opt = {
        'conn' : {
            'port': '/dev/ttyS0',
            'method': 'rtu',
            'baudrate' : 115200,
            'timeout': 5
        }
    }
    
    devinfo = [{
        "id" : "1", "dk" : "S0", "dt": "gw", "children" : [{
            "id" : "11", "dk" : '{"addr":0}', "dt": "nd", "children" : [
                {"id" : "101", "dk" : '{"addr":16}', "dt": "sen"},
                {"id" : "102", "dk" : '{"addr":18}', "dt": "sen"},
                {"id" : "103", "dk" : '{"addr":20}', "dt": "sen"},
                {"id" : "104", "dk" : '{"addr":22}', "dt": "sen"},
                {"id" : "105", "dk" : '{"addr":24}', "dt": "sen"},
                {"id" : "106", "dk" : '{"addr":26}', "dt": "sen"},
                {"id" : "107", "dk" : '{"addr":28}', "dt": "sen"},
                {"id" : "108", "dk" : '{"addr":30}', "dt": "sen"},
                {"id" : "201", "dk" : '{"addr":64}', "dt": "act/switch/level1"},
                {"id" : "202", "dk" : '{"addr":66}', "dt": "act/switch/level1"},
                {"id" : "203", "dk" : '{"addr":68}', "dt": "act/switch/level1"},
                {"id" : "204", "dk" : '{"addr":76}', "dt": "act/switch/level1"},
                {"id" : "205", "dk" : '{"addr":78}', "dt": "act/switch/level1"},
                {"id" : "206", "dk" : '{"addr":70, "addr_ratio":96}', "dt": "act/switch/level2"},
                {"id" : "207", "dk" : '{"addr":72, "addr_ratio":97}', "dt": "act/switch/level2"},
                {"id" : "208", "dk" : '{"addr":74, "addr_ratio":98}', "dt": "act/switch/level2"}
            ]
        }]
    }]

    kdmate = FJBoxMate(opt, devinfo, "1", quelist)
    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    kdmate.start ()
    print("mate started")

    time.sleep(10)
    req = Request(None)
    req = Request(11)
    req.setcommand(205, CmdCode.TIMED_ON, {"hold-time":20})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(206, CmdCode.DIRECTIONAL_ON, {"hold-time":20, "ratio": 80})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(11)
    req.setcommand(205, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    time.sleep(5)
    req = Request(11)
    req.setcommand(206, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    kdmate.stop()
    ssmate.stop()

    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

