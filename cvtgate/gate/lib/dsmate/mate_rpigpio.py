#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
import RPi.GPIO as gpio
from enum import IntEnum
from threading import Thread, Lock

from .. import *

_ON = gpio.LOW
_OFF = gpio.HIGH

class RPiGPIODevice(object):
    def __init__(self, devid, dk, logger):
        self._channels = dk["channels"]
        self._devid = devid
        self._opid = 0
        self._status = StatCode.READY.value
        self._timetostop = None
        self._cmd = {}
        self._logger = logger
        self.setup()

    def setup(self):
        gpio.setup(self._channels, gpio.OUT, initial=_OFF)

    def getopid(self):
        return self._opid

    def getstatus(self):
        return self._status

    def getcontent(self):
        content = {"status" : self.getstatus(), "opid" : self.getopid()}
        if self._timetostop is not None:
            content["remain-time"] = self._timetostop - time.time()
        else:
            content["remain-time"] = 0
        return content

    def execute(self):
        if self._timetostop is not None and self._timetostop < time.time():
            print("timed stop", self._devid)
            self._cmd[CmdCode.OFF](None)
        return self._status

    def processrequest(self, request):
        operation = request.getcommand()
        self._opid = request.getopid()
        if operation in self._cmd:
            self._cmd[operation](request.getparams())
            return True
        else:
            return False

class RPiGPIOMotor(RPiGPIODevice):
    # channels [open, close]
    def __init__(self, devid, dk, logger):
        super(RPiGPIOMotor, self).__init__(devid, dk, logger)
        self._cmd = {
            CmdCode.OFF : self.mstop,
            CmdCode.OPEN : self.mopen,
            CmdCode.CLOSE : self.mclose,
            CmdCode.TIMED_OPEN: self.topen,
            CmdCode.TIMED_CLOSE: self.tclose,
            CmdCode.POSITION: self.setpos
        }
        self._opentime = dk["times"][0]
        self._closetime = dk["times"][1]
        self._postm = None
        self._pos = 0

    def getcontent(self):
        content = super(RPiGPIOMotor, self).getcontent()
        content["position"] = self._pos
        return content

    def execute(self):
        self.calculatepos()
        super(RPiGPIOMotor, self).execute()
        return self._status

    def calculatepos(self):
        if self._status == StatCode.OPENNING.value:
            delta = (time.time() - self._postm) / self._opentime * 100.0
            pos = self._pos + delta
            self._pos = 100 if pos > 100 else pos
            self._postm = time.time()
            print("calculate pos", delta, pos, self._pos, self._postm)
        elif self._status == StatCode.CLOSING.value:
            delta = (time.time() - self._postm) / self._closetime * 100.0
            pos = self._pos - delta
            self._pos = 0 if pos < 0 else pos
            self._postm = time.time()

    def _stop(self):
        gpio.output(self._channels, [_OFF, _OFF])
        time.sleep(0.5)

    def _open(self):
        gpio.output(self._channels, [_ON, _OFF])
        self._postm = time.time()

    def _close(self):
        gpio.output(self._channels, [_OFF, _ON])
        self._postm = time.time()

    def setpos(self, params):
        if params['position'] == 0:
            self.tclose({'time':self._closetime})
        elif params['position'] == 100:
            self.topen({'time':self._opentime})
        elif self._pos > params['position']:
            # close
            tm = self._closetime * (self._pos - params['position']) / 100
            print("setpos tm", tm)
            self.tclose({'time':tm})
        elif self._pos < params['position']:
            # open
            tm = self._opentime * (params['position'] - self._pos) / 100
            print("setpos tm", tm)
            self.topen({'time':tm})

    def mopen(self, params):
        self._logger.info("Motor open : " + str(params))
        self._stop()
        self._open()
        self._timetostop = None
        self._status = StatCode.OPENNING.value

    def mclose(self, params):
        self._logger.info("Motor close : " + str(params))
        self._stop()
        self._close()
        self._timetostop = None
        self._status = StatCode.CLOSING.value

    def mstop(self, params):
        self._logger.info("Motor stop : " + str(params))
        self._stop()
        self._timetostop = None
        self._status = StatCode.READY.value

    def topen(self, params):
        self._logger.info("Motor time open : " + str(params))
        self._stop()
        self._open()
        self._timetostop = time.time() + params['time']
        self._status = StatCode.OPENNING.value

    def tclose(self, params):
        self._logger.info("Motor time close : " + str(params))
        self._stop()
        self._close()
        self._timetostop = time.time() + params['time']
        self._status = StatCode.CLOSING.value

class RPiGPIOSwitch(RPiGPIODevice):
# channels [on]
    def __init__(self, devid, dk, logger):
        super(RPiGPIOSwitch, self).__init__(devid, dk, logger)
        self._timetostop = None
        self._cmd = {
            CmdCode.OFF : self.moff,
            CmdCode.ON : self.mon,
            CmdCode.TIMED_ON: self.ton
        }

    def moff(self, params):
        self._logger.info("Switch off : " + str(params))
        gpio.output(self._channels, [_OFF])
        self._timetostop = None
        self._status = StatCode.READY.value

    def mon(self, params):
        self._logger.info("Switch on : " + str(params))
        gpio.output(self._channels, [_ON])
        self._timetostop = None
        self._status = StatCode.WORKING.value
        
    def ton(self, params):
        self._logger.info("Switch time on : " + str(params))
        gpio.output(self._channels, [_ON])
        self._timetostop = time.time() + params['hold-time']
        self._status = StatCode.WORKING.value
        
class RPiGPIOMate(DSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        global _ON
        global _OFF

        super(RPiGPIOMate, self).__init__(option, devinfo, coupleid, quelist)
        if gpio.BOARD != gpio.getmode():
            gpio.setmode(gpio.BOARD)

        if "lowison" in option and option["lowison"] is False:
            _ON = gpio.HIGH
            _OFF = gpio.LOW

        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._conn = {}
        self._devices = {}
        self.setup(devinfo)

    def setup(self, devinfo):
        for gw in devinfo:
            if "children" not in gw:
                continue

            for nd in gw["children"]:
                if "children" not in nd:
                    continue

                for dev in nd["children"]:
                    dk = json.loads(dev["dk"])
                    if DevType.isretractable(dev["dt"]): 
                        self._devices[dev["id"]] = RPiGPIOMotor(dev["id"], dk, self._logger)
                    elif DevType.isswitch(dev["dt"]): 
                        self._devices[dev["id"]] = RPiGPIOSwitch(dev["id"], dk, self._logger)

    def connect(self):
        super(RPiGPIOMate, self).connect()
        return True

    def close(self):
        super(RPiGPIOMate, self).close()
        gpio.cleanup()

    def processnotices(self):
        isnoti = False
        for devid, dev in self._devices.items():
            if dev.execute() != StatCode.READY.value:
                isnoti = True

        if self._timetocheck(Mate.NOTITYPE) or isnoti:
            self.sendnoti()
            self._updatetime(Mate.NOTITYPE)

    def processactrequest(self, dev, request, node):
        ret = self._devices[dev['id']].processrequest(request)
        if ret == False:
            self._logger.warn("Fail to process a request to dev." + str(dev) + "," + str(res) + ":" + str(request))
            return ResCode.FAIL_TO_WRITE

        self.sendnoticeforactuator(node, dev)
        return ResCode.OK

    def processrequest(self, blk):
        print("received message", blk.getdevid(), self._coupleid)
        if BlkType.isrequest(blk.gettype()) is False:
            self._logger.warn("The message is not request. " + str(blk.gettype()))
            return False
        self._logger.info("The message is a request. " + str(blk.getcontent()))

        response = Response(blk)
        cmd = blk.getcommand()
        nd = self._devinfo.finddevbyid(blk.getnodeid())
        dev = self._devinfo.finddevbyid(blk.getdevid())

        if blk.getdevid() == self._coupleid:
            params = blk.getparams()
            self._logger.warn("Nothing to do. " + str(blk) + ", " + str(dev))
            code = ResCode.FAIL

        elif dev is None:
            self._logger.warn("There is no device. " + str(blk.getdevid()))
            code = ResCode.FAIL_NO_DEVICE

        elif DevType.ispropercommand(dev['dt'], cmd) is False:
            self._logger.warn("The request is not proper. " + str(cmd) + " " + str(dev['dt']))
            code = ResCode.FAIL_NOT_PROPER_COMMAND

        elif DevType.isactuator(dev['dt']) or DevType.isnode(dev['dt']):
            if gpio.BOARD != gpio.getmode():
                gpio.setmode(gpio.BOARD)
            code = self.processactrequest(dev, blk, nd)

        elif DevType.isgateway(dev['dt']):
            self._logger.info("Gateway does not receive a request")
            code = ResCode.FAIL

        else:
            self._logger.warn("Unknown Error. " + str(blk) + ", " + str(dev))
            code = ResCode.FAIL

        response.setresult(code)
        self._writeres(response)
        self._logger.info("A response is sent. " + str(response.getcontent()))
        return True #if code == ResCode.OK else False

    def sendnoti(self):
        for gw in self._devinfo:
            if "children" not in gw:
                continue
            for nd in gw["children"]:
                self.sendnoticeforactuatornode(nd)

    def sendnoticeforactuator(self, nd, act):
        blk = Notice(nd["id"], NotiCode.ACTUATOR_STATUS, nd["id"], {"status" : StatCode.READY.value})
        device = self._devices[act["id"]]
        blk.setcontent(act["id"], device.getcontent())
        self._logger.info("A noti is sent. " + str(blk.getcontent()))
        self._writenoti(blk)

    def sendnoticeforactuatornode(self, nd):
        blk = Notice(nd["id"], NotiCode.ACTUATOR_STATUS, nd["id"], {"status" : StatCode.READY.value})
        if "children" in nd:
            for dev in nd["children"]:
                device = self._devices[dev["id"]]
                blk.setcontent(dev["id"], device.getcontent())
        self._logger.info("A noti for a node is sent. " + str(blk.getcontent()))
        self._writenoti(blk)


if __name__ == "__main__":
    from multiprocessing import Queue
    opt = {
        'conn' : [{ }]
    }
    
    devinfo = [{
        "id" : "1", "dk" : "R2", "dt": "gw", "children" : [{
            "id" : "10", "dk" : "R2-AND", "dt": "nd", "children" : [
                {"id" : "11", "dk" : '{"channels": [7, 11], "times": [24, 23]}', "dt": "act/retractable/level2"},
                {"id" : "12", "dk" : '{"channels": [13, 15], "times": [24, 23]}', "dt": "act/retractable/level2"},
                {"id" : "13", "dk" : '{"channels": [29]}', "dt": "act/switch/level1"}
            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = SSMate({}, [], None, quelist)
    dsmate = RPiGPIOMate(opt, devinfo, "1", quelist)

    ssmate.start ()
    dsmate.start ()
    print("mate started")

    time.sleep(5)
    req = Request(10)
    req.setcommand(11, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    time.sleep(10)
    req = Request(10)
    req.setcommand(11, CmdCode.POSITION, {"position":50})
    ssmate._writereq(req)
    time.sleep(15)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    ssmate.stop()
    dsmate.stop()
    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
