#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

"""
    Dummy DS Mate를 정의함.
"""

from .. import *

class DummyMate(DSMate):
    pass
