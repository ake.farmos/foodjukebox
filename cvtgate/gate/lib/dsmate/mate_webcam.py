#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import os
import subprocess
import traceback
import shlex
import time
from datetime import datetime

from scapy.all import srp, Ether, ARP

from .. import *
from lib.dsmate.mate_extproc import _ExtProcMate

class WebCamMate(_ExtProcMate):
    def __init__(self, option, devinfo, coupleid, logger=None):
        super(WebCamMate, self).__init__(option, devinfo, coupleid, logger)
        if "prefix" in self._option["extproc"]:
            self._prefix = self._option["extproc"]["prefix"]
        else:
            self._prefix = None
        self._lastopid = None

        if "mac" in self._option["extproc"]:
            ip = self._option["extproc"]["ip"] if "ip" in self._option["extproc"] else "192.168.0.1"
            net = ".".join(ip.split(".")[:3] + ["0"]) + "/24"
            ans,unans=srp(Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(pdst=net),timeout=2)
            for src, ret in ans:
              if self._option["extproc"]["mac"] == ret.hwsrc:
                 self._option["extproc"]["ip"] = ret.psrc
                 self._logger.info("New ip address is " + self._option["extproc"]["ip"])

    def makecmdline(self, fname):
        if "user" in self._option["extproc"] and "passwd" in self._option["extproc"]:
            url = self._option["extproc"]["user"] + ":" + self._option["extproc"]["passwd"] + "@"
        else:
            url = "admin:jinong#0801!"
        url = url + self._option["extproc"]["ip"] #+ "/11"
        cmdline = self._option["extproc"]["cmdfmt"] % (url, fname)
        print (cmdline)
        return cmdline

    def processrequest(self, req):
        response = Response(req)
        cmd = req.getcommand()

        if self._lastopid == req.getopid():
            self._logger.info ("This request would be ignored. The previouse opid is same with this time. " + str(self._lastopid))
            return None
        self._lastopid = req.getopid()

        if cmd == CmdCode.TAKE_PICTURE:
            devid = str(req.getdevid())
            self._stats[devid] = StatCode.WORKING
            self.sendnotiforact(req.getnodeid(), devid)
            
            if self._prefix:
                fname = self._prefix + "-" + datetime.now().strftime("%Y%m%d-%H%M") + ".jpg"
            else:
                fname = devid + "-" + datetime.now().strftime("%Y%m%d-%H%M") + ".jpg"

            path = self._option["extproc"]["base"] + fname
            cmdline = self.makecmdline(path)
            ret = self.execute(cmdline)
            extra = {
                "deviceId": req.getdevid(), 
                "date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), 
                "meta" : {"fname" : fname}
            }
            if self.upload(Mate.IMGTYPE, path, extra):
                os.remove(path)
            # process rep 
            self._stats[devid] = StatCode.READY
            self.sendnotiforact(req.getnodeid(), devid)
            response.setkeyvalue("fname", fname)
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        self._writeres(response)
        self._logger.info ("Response : " + response.stringify())
        return response

if __name__ == "__main__":
    from multiprocessing import Queue

    opt = {
        "upload" : {
            "URL" : "http://localhost:8081/common/v1/upload",
            "SFTP" : {
                "host_" : "220.90.133.19",
                "host" : "farmos003.jinong.co.kr",
                "port" : 22,
                "user" : "root",
                "passwd" : "jinong#0801!",
                "path" : "/root/rda_outdoor/",
                "path_" : "/usr/local/tomcat/apache-tomcat-7_jinongSmartFarmCloud/webapps/ROOT/images/smartLink"
            }
        },
        "extproc" : {
            "cmdfmt" : "ffmpeg -y -loglevel debug -rtsp_transport tcp -i 'rtsp://%s' %s",
            "stdout" : False,
            "base" : "picture/",
            "user" : "admin",
            "passwd" : "jinong#0801!",
            "ip" : "192.168.0.164",
            "mac" : "08:ed:ed:2b:d6:4a"
        }
    }
    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "47", "dk" : "0", "dt": "act/camera/level0"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    req = Request(2)
    req.setcommand(47, CmdCode.TAKE_PICTURE, {})

    ssmate = SSMate ({}, [], "1", quelist)
    dsmate = WebCamMate(opt, devinfo, "1", quelist)
    ssmate.start ()
    dsmate.start ()
    print("mate started")
    ssmate._writereq(req)
    time.sleep(60)
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")


