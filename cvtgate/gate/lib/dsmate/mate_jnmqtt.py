#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#
# MQTT 연동을 위한 메이트

import json
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
from collections import deque

from .. import *

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class JNMQTTMate(DSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(JNMQTTMate, self).__init__(option, devinfo, coupleid, quelist)
        self._jnmqtt = SimpleJNMQTT(option, self._logger)
        self._msgq = deque()

    def connect(self):
        self._jnmqtt.connect()
        super(JNMQTTMate, self).connect()
        return True

    def close(self):
        self._jnmqtt.close()
        super(JNMQTTMate, self).close()

    def matestart(self):
        self._jnmqtt.start(self.onmsg)
        super(JNMQTTMate, self).matestart()
        self.connect()

    def matestop(self):
        self.close()
        self._jnmqtt.stop()
        super(JNMQTTMate, self).matestop()

    def doextra(self):
        self.checkmessage()
        self._jnmqtt.checkconnection()

    def checkmessage(self):
        while True:
            try:
                topic, msg = self._msgq.popleft()
            except:
                return

            tmp = topic.split('/')
            if _JNMQTT._SELF == tmp[2] and _JNMQTT._STAT == tmp[3]:
                self._logger.info("Update Mate: " + topic + msg.stringify())

            elif BlkType.isnotice(msg.gettype()):
                self._writenoti(msg)

            elif BlkType.isresponse(msg.gettype()):
                self._writeres(msg)

            elif BlkType.isobservation(msg.gettype()):
                self._writeobs(msg)

            else:
                self._logger.warn("Wrong message : " + msg.stringify())


    def onmsg(self, client, obj, blk):
        """ MQTT 로 받는 메세지는 Request """
        #print ("JNMQTTMate Received mblock '" + str(blk.payload) + "' on topic '" + blk.topic + "' with QoS " + str(blk.qos))

        msg = self._jnmqtt.getpropermsg(blk)
        if msg is None:
            self._logger.warn("The message is not proper " + str(blk))
            return None

        print ("DS JNMQTTMate Received mblock '" + str(blk.payload) + "' on topic '" + blk.topic + "' with QoS " + str(blk.qos))
        self._msgq.append((blk.topic, msg))


if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        'conn': {'host': 'farmos003.jinong.co.kr', 'port': 1883, 'keepalive': 60}, 
        'mqtt': {'svc': 'cvtgate', 'id': '#'}
    }

    devinfo = []

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = JNMQTTMate(option, devinfo, "1", quelist)
    dsmate = DSMate(option, devinfo, "1", quelist)
    ssmate.start()
    dsmate.start()

    cmd = Request(1, None)
    cmd.setcommand(12, 'on', {})

    publish.single("cvtgate/1/1/req/1", cmd.stringify(), hostname="farmos003.jinong.co.kr")
    print("published")

    time.sleep(60)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
