#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import subprocess
import traceback
import shlex

from .. import *

class _ExtProcMate(DSMate):
    def __init__(self, option, devinfo, coupleid, logger=None):
        super(_ExtProcMate, self).__init__(option, devinfo, coupleid, logger)
        self._stats = {}
        self._opids = {}
        for did in self._devinfo.getallids():
            self._stats[did] = StatCode.READY
            self._opids[did] = 0

    def execute(self, cmdline):
        cmds = shlex.split(cmdline)
        try:
            if self._option["extproc"]["stdout"] is True:
                output = subprocess.check_output(cmds)
            else:
                if subprocess.call(cmds) == 0:
                    output = None
                else:
                    self._logger.warn("fail to execute process. " + str(cmds))
        except Exception as ex:
            self._logger.warn("fail to execute process. " + str(cmdline) + " : " + str(ex))
            self._logger.warn(traceback.format_exc())

    def processrequest(self, req):
        """ implement me!! """
        pass
        """ # sample code is following....

        response = Response(req)
        cmd = blk.getcommand()
        nd = self._devinfo.finddevbyid(blk.getnodeid())
        dev = self._devinfo.finddevbyid(blk.getdevid())

        if cmd == CmdCode.EXACT_CMD:
            # MAKE_COMMAND_LINE_BY_YOURSELF
            cmdline = self._option["extproc"]["cmdfmt"] % (something....)
            devid = str(req.getdevid())
            self._opids[devid] = req.getopid()
            self._stats[devid] = StatCode.WORKING
            self.sendnotiforact(req.getnodeid(), devid)
            ret = self.execute(cmdline)
            self._stats[devid] = StatCode.READY
            self.sendnotiforact(req.getnodeid(), devid)
            # process ret
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        return response
        """

    def sendnotiforact(self, nid, devid):
        noti = Notice(nid, NotiCode.ACTUATOR_STATUS)
        print(devid, self._stats, self._opids)
        noti.setcontent(devid, {"status" : self._stats[devid].value, "opid":self._opids[devid]})
        self._writenoti(noti)

    def sendnotifornode(self, node):
        noti = Notice(node["id"], NotiCode.ACTUATOR_STATUS)
        noti.setcontent(node["id"], {"status" : StatCode.READY.value})
        for dev in node["children"]:
            noti.setcontent(dev["id"], {"status" : self._stats[dev["id"]].value, "opid":self._opids[dev["id"]]})
        self._writenoti(noti)

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE) is False:
            return 
        self._updatetime(Mate.NOTITYPE)
        for dev in self._devinfo:
            if dev["dt"] == DevType.GATEWAY:
                if "children" in dev:
                    for nd in dev["children"]:
                        print (nd) 
                        self.sendnotifornode(nd)
            elif dev["dt"] == DevType.NODE:
                self.sendnotifornode(dev)
