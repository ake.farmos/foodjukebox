#!/usr/bin/env python 
#-*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc. 
# All right reserved.
#
# Online Forecast from openweathermap
#


import requests
import json
import time

from .. import *

'''
option : {
  "params" :{
    "lat" : 37.2225719,
    "lon" : 126.822737,
    "exclude" : "current,minutely,alert,hourly",
    "units" : "metric",
    "appid" : "4bcf94a2318ec5d5fded39cbf22be946"
  }
}

devinfo = []

insert into dataindexes(id, rule_id, name, unit, sigdigit, device_id, field_id, deleted) values
(40101001, NULL, '내일 최고온도', '℃', 0, NULL, 0, 0),
(40101002, NULL, '내일 최저온도', '℃', 0, NULL, 0, 0),
(40101003, NULL, '내일 평균온도', '℃', 0, NULL, 0, 0),
(40101004, NULL, '내일 평균습도', '%', 0, NULL, 0, 0),
(40101005, NULL, '내일 평균풍향', '°', 0, NULL, 0, 0),
(40101006, NULL, '내일 평균풍속', 'm/s', 0, NULL, 0, 0),
(40101007, NULL, '내일 강수확률', '%', 0, NULL, 0, 0),
(40101008, NULL, '내일 예상적설량', 'mm', 0, NULL, 0, 0),
(40101009, NULL, '내일 예상강우량', 'mm', 0, NULL, 0, 0),
(40102001, NULL, '모레 최고온도', '℃', 0, NULL, 0, 0),
(40102002, NULL, '모레 최저온도', '℃', 0, NULL, 0, 0),
(40102003, NULL, '모레 평균온도', '℃', 0, NULL, 0, 0),
(40102004, NULL, '모레 평균습도', '%', 0, NULL, 0, 0),
(40102005, NULL, '모레 평균풍향', '°', 0, NULL, 0, 0),
(40102006, NULL, '모레 평균풍속', 'm/s', 0, NULL, 0, 0),
(40102007, NULL, '모레 강수확률', '%', 0, NULL, 0, 0),
(40102008, NULL, '모레 예상적설량', 'mm', 0, NULL, 0, 0),
(40102009, NULL, '모레 예상강우량', 'mm', 0, NULL, 0, 0),
(40103001, NULL, '3일후 최고온도', '℃', 0, NULL, 0, 0),
(40103002, NULL, '3일후 최저온도', '℃', 0, NULL, 0, 0),
(40103003, NULL, '3일후 평균온도', '℃', 0, NULL, 0, 0),
(40103004, NULL, '3일후 평균습도', '%', 0, NULL, 0, 0),
(40103005, NULL, '3일후 평균풍향', '°', 0, NULL, 0, 0),
(40103006, NULL, '3일후 평균풍속', 'm/s', 0, NULL, 0, 0),
(40103007, NULL, '3일후 강수확률', '%', 0, NULL, 0, 0),
(40103008, NULL, '3일후 예상적설량', 'mm', 0, NULL, 0, 0),
(40103009, NULL, '3일후 예상강우량', 'mm', 0, NULL, 0, 0),
(40104001, NULL, '4일후 최고온도', '℃', 0, NULL, 0, 0),
(40104002, NULL, '4일후 최저온도', '℃', 0, NULL, 0, 0),
(40104003, NULL, '4일후 평균온도', '℃', 0, NULL, 0, 0),
(40104004, NULL, '4일후 평균습도', '%', 0, NULL, 0, 0),
(40104005, NULL, '4일후 평균풍향', '°', 0, NULL, 0, 0),
(40104006, NULL, '4일후 평균풍속', 'm/s', 0, NULL, 0, 0),
(40104007, NULL, '4일후 강수확률', '%', 0, NULL, 0, 0),
(40104008, NULL, '4일후 예상적설량', 'mm', 0, NULL, 0, 0),
(40104009, NULL, '4일후 예상강우량', 'mm', 0, NULL, 0, 0),
(40105001, NULL, '5일후 최고온도', '℃', 0, NULL, 0, 0),
(40105002, NULL, '5일후 최저온도', '℃', 0, NULL, 0, 0),
(40105003, NULL, '5일후 평균온도', '℃', 0, NULL, 0, 0),
(40105004, NULL, '5일후 평균습도', '%', 0, NULL, 0, 0),
(40105005, NULL, '5일후 평균풍향', '°', 0, NULL, 0, 0),
(40105006, NULL, '5일후 평균풍속', 'm/s', 0, NULL, 0, 0),
(40105007, NULL, '5일후 강수확률', '%', 0, NULL, 0, 0),
(40105008, NULL, '5일후 예상적설량', 'mm', 0, NULL, 0, 0),
(40105009, NULL, '5일후 예상강우량', 'mm', 0, NULL, 0, 0),
(40106001, NULL, '6일후 최고온도', '℃', 0, NULL, 0, 0),
(40106002, NULL, '6일후 최저온도', '℃', 0, NULL, 0, 0),
(40106003, NULL, '6일후 평균온도', '℃', 0, NULL, 0, 0),
(40106004, NULL, '6일후 평균습도', '%', 0, NULL, 0, 0),
(40106005, NULL, '6일후 평균풍향', '°', 0, NULL, 0, 0),
(40106006, NULL, '6일후 평균풍속', 'm/s', 0, NULL, 0, 0),
(40106007, NULL, '6일후 강수확률', '%', 0, NULL, 0, 0),
(40106008, NULL, '6일후 예상적설량', 'mm', 0, NULL, 0, 0),
(40106009, NULL, '6일후 예상강우량', 'mm', 0, NULL, 0, 0),
(40107001, NULL, '7일후 최고온도', '℃', 0, NULL, 0, 0),
(40107002, NULL, '7일후 최저온도', '℃', 0, NULL, 0, 0),
(40107003, NULL, '7일후 평균온도', '℃', 0, NULL, 0, 0),
(40107004, NULL, '7일후 평균습도', '%', 0, NULL, 0, 0),
(40107005, NULL, '7일후 평균풍향', '°', 0, NULL, 0, 0),
(40107006, NULL, '7일후 평균풍속', 'm/s', 0, NULL, 0, 0),
(40107007, NULL, '7일후 강수확률', '%', 0, NULL, 0, 0),
(40107008, NULL, '7일후 예상적설량', 'mm', 0, NULL, 0, 0),
(40107009, NULL, '7일후 예상강우량', 'mm', 0, NULL, 0, 0);

insert current_observations(data_id, obs_time, nvalue, modified_time, source_id) values
(40101001, now(), 0, now(), NULL),
(40101002, now(), 0, now(), NULL),
....;

'''

class ForecastOWMate(DSMate):
    _EXTKEY = "401"
    _URL = "https://api.openweathermap.org/data/2.5/onecall"

    def reformdata(self, data):
        newdata = []
        for i in range(len(data["daily"])):
            datum = data["daily"][i]
            dt = datum["dt"]
            dataid = ForecastOWMate._EXTKEY + "{:02d}".format(i+1)

            newdata.append({"id" : dataid + "001", "value" : datum["temp"]["max"], "time" : dt})
            newdata.append({"id" : dataid + "002", "value" : datum["temp"]["min"], "time" : dt})

            value = (datum["temp"]["morn"] + datum["temp"]["day"] + datum["temp"]["night"] + datum["temp"]["eve"]) / 4
            newdata.append({"id" : dataid + "003", "value" : value, "time" : dt})

            newdata.append({"id" : dataid + "004", "value" : datum["humidity"], "time" : dt})
            newdata.append({"id" : dataid + "005", "value" : datum["wind_deg"], "time" : dt})
            newdata.append({"id" : dataid + "006", "value" : datum["wind_speed"], "time" : dt})

            newdata.append({"id" : dataid + "007", "value" : datum["pop"] * 100, "time" : dt})
            if "snow" in datum:
                newdata.append({"id" : dataid + "008", "value" : datum["snow"], "time" : dt})
            else:
                newdata.append({"id" : dataid + "008", "value" : 0, "time" : dt})
            if "rain" in datum:
                newdata.append({"id" : dataid + "009", "value" : datum["rain"], "time" : dt})
            else:
                newdata.append({"id" : dataid + "009", "value" : 0, "time" : dt})
        return newdata

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE) is False:
            return

        self._updatetime(Mate.NOTITYPE)
        res = requests.get(ForecastOWMate._URL, params=self._option['params'])
        if res.status_code != 200:
            self._logger.info ("Fail to get forecast. " + str(res.status_code))
            return 

        data = self.reformdata(res.json())

        noti = Notice(None, NotiCode.EXTERNAL_DATA)
        if noti:
            noti.setkeyvalue("data", data)
            noti.setkeyvalue("option", "updatable")

        self._writenoti(noti)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
      "params" :{
        "lat" : 37.2225719,
        "lon" : 126.822737,
        "exclude" : "current,minutely,alert,hourly",
        "units" : "metric",
        "appid" : "4bcf94a2318ec5d5fded39cbf22be946"
      }
    }

    devinfo = []

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = ForecastOWMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
