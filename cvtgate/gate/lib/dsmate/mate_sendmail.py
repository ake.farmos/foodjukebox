#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import traceback
import requests
import socket
import smtplib
import datetime


from .. import *

class SENDMAILMate(DSMate):
    def execute(self, data,receiver):
        try:
            senderid = self._option["senderid"]
            senderpw = self._option["senderpw"]
            receiver = receiver

            print (data)

            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.ehlo()
            server.starttls()
            server.login(senderid, senderpw)

            server.sendmail(senderid, receiver, data)
            print ('email sent')

        except Exception as ex:
            self._logger.warn("fail to send mail")
            self._logger.warn(traceback.format_exc())
        return None


    def sendnotiforact(self, nid, devid, stat):
        noti = Notice(nid, NotiCode.ACTUATOR_STATUS)
        noti.setcontent(devid, {"status" : stat.value})
        self._writenoti(noti)

    def processrequest(self, req):
        response = Response(req)
        cmd = req.getcommand()
        nd = self._devinfo.finddevbyid(req.getnodeid())
        dev = self._devinfo.finddevbyid(req.getdevid())

        if cmd == CmdCode.ALERT:
            # MAKE_COMMAND_LINE_BY_YOURSELF
            param = req.getparams()
#            data = {
#                "to": self._option["receiver"],
#                "from": self._option["senderid"],
#                "subject": self._option["subject"],
#                "msg": param['msg']
#            }

            data = '\r\n'.join(['To: %s' % param['receiver'],
                                'From: %s' % self._option["senderid"],
                                'Subject: %s' % param['subject'],
                                "", param['msg']])

            print (data)
            print (type(data))

            receiver = param['receiver']

            self.sendnotiforact(req.getnodeid(), req.getdevid(), StatCode.WORKING)
            ret = self.execute(data,receiver)
            self.sendnotiforact(req.getnodeid(), req.getdevid(), StatCode.READY)
            # process ret
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        return response

    def sendnotifornode(self, nd):
        noti = Notice(nd["id"], NotiCode.ACTUATOR_STATUS)
        noti.setcontent(nd["id"], {"status" : StatCode.READY.value})
        if "children" in nd:
            for dev in nd["children"]:
                noti.setcontent(dev["id"], {"status" : StatCode.READY.value})
        self._writenoti(noti)

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE) is False:
            return 

        self._updatetime(Mate.NOTITYPE)
        for dev in self._devinfo:
            if dev["dt"] == DevType.GATEWAY:
                if "children" in dev:
                    for nd in dev["children"]:
                        self.sendnotifornode(nd)
            elif dev["dt"] == DevType.NODE:
                self.sendnotifornode(dev)

if __name__ == "__main__":
    import time
    from multiprocessing import Queue

    opt = {
        "senderid": "fjbox.jinong@gmail.com",
        "senderpw": "fjbox1234%"
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : '0', "dt": "act/alarm/level1"}
            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = SSMate({}, [], None, quelist)
    dsmate = SENDMAILMate(opt, devinfo,"1", quelist)
    ssmate.start()
    dsmate.start()
    print("mate started")
    req = Request(2)
    req.setcommand(3, CmdCode.ALERT, {"msg":"alert!!", "receiver":"service.jinong@gmail.com", "subject":"test"})
    ssmate._writereq(req)
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
