#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import traceback
import requests

from .. import *

class AlertMate(DSMate):
    def execute(self, method, url, data):
        try:
            if method == "get":
                pass
            elif method == "post":
                res = requests.post(url=url, data=data)
                if res.status_code == 200:
                   self._logger.info ("called a RESTful api - " + str(res))
                   return res.json()
                else:
                   self._logger.info ("fail to process RESTful api - " + str(res))
        except Exception as ex:
            self._logger.warn("fail to access RESTful api - " + method + " : " + str(url) + " : " + str(data))
            self._logger.warn(traceback.format_exc())
        return None


    def sendnotiforact(self, nid, devid, stat):
        noti = Notice(nid, NotiCode.ACTUATOR_STATUS)
        noti.setcontent(devid, {"status" : stat.value})
        self._writenoti(noti)

    def processrequest(self, req):
        response = Response(req)
        cmd = req.getcommand()
        nd = self._devinfo.finddevbyid(req.getnodeid())
        dev = self._devinfo.finddevbyid(req.getdevid())

        if cmd == CmdCode.ALERT:
            # MAKE_COMMAND_LINE_BY_YOURSELF
            param = req.getparams()
            data = {
                "key": self._option["key"],
                "user_id": self._option["user_id"],
                "sender": self._option["sender"],
                "receiver": param['receiver'],
                "msg": param['msg'],
                "testmode_yn":"N"
            }
            
            self.sendnotiforact(req.getnodeid(), req.getdevid(), StatCode.WORKING)
            ret = self.execute("post", self._option["url"], data)
            self.sendnotiforact(req.getnodeid(), req.getdevid(), StatCode.READY)
            # process ret
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        return response

    def sendnotifornode(self, nd):
        noti = Notice(nd["id"], NotiCode.ACTUATOR_STATUS)
        noti.setcontent(nd["id"], {"status" : StatCode.READY.value})
        if "children" in nd:
            for dev in nd["children"]:
                noti.setcontent(dev["id"], {"status" : StatCode.READY.value})
        self._writenoti(noti)

    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE) is False:
            return 

        self._updatetime(Mate.NOTITYPE)
        for dev in self._devinfo:
            if dev["dt"] == DevType.GATEWAY:
                if "children" in dev:
                    for nd in dev["children"]:
                        self.sendnotifornode(nd)
            elif dev["dt"] == DevType.NODE:
                self.sendnotifornode(dev)

if __name__ == "__main__":
    import time
    from multiprocessing import Queue

    opt = {
      "key": "d7c5ijzh6o8wzfuryjk7m2w0uw19pmy9",
      "user_id": "jinong",
      "sender": "0313601970",
      "url": "https://apis.aligo.in/send/"
    }

    devinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : '0', "dt": "act/alarm/level1"}
            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = SSMate({}, [], None, quelist)
    dsmate = AlertMate(opt, devinfo,"1", quelist)
    ssmate.start()
    dsmate.start()
    print("mate started")
    req = Request(2)
    req.setcommand(3, CmdCode.ALERT, {"msg":"alert!!", "receiver":"01088802323"})
    ssmate._writereq(req)
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
