#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import os
import subprocess
import traceback
import shlex
import time
from datetime import datetime

from .. import *
from lib.dsmate.mate_extproc import _ExtProcMate

class RPICamMate(_ExtProcMate):
    def __init__(self, option, devinfo, coupleid, logger=None):
        super(RPICamMate, self).__init__(option, devinfo, coupleid, logger)
        if "prefix" in self._option["extproc"]:
            self._prefix = self._option["extproc"]["prefix"]
        else:
            self._prefix = None

    def processrequest(self, req):
        response = Response(req)
        cmd = req.getcommand()

        print ("processrequest of RPICAM")
        if cmd == CmdCode.TAKE_PICTURE:
            if self._prefix:
                fname = self._prefix + "-" + datetime.now().strftime("%Y%m%d-%H%M") + ".jpg"
            else:
                fname = devid + "-" + datetime.now().strftime("%Y%m%d-%H%M") + ".jpg"

            path = self._option["extproc"]["base"] + fname
            cmdline = self._option["extproc"]["cmdfmt"] % path
            devid = req.getdevid()
            self._stats[devid] = StatCode.WORKING
            self.sendnotiforact(req.getnodeid(), devid)
            ret = self.execute(cmdline)
            extra = {
                "deviceId": req.getdevid(), 
                "date" : datetime.now().strftime("%Y-%m-%d %H:%M:%S"), 
                "meta" : {"fname": fname}
            }
            if self.upload(Mate.IMGTYPE, path, extra):
                os.remove(fname)
            # process rep 
            self._stats[devid] = StatCode.READY
            self.sendnotiforact(req.getnodeid(), devid)
            code = ResCode.OK
        else:
            code = ResCode.FAIL
        response.setresult(code)
        self._writeres(response)
        return response

if __name__ == "__main__":
    from multiprocessing import Queue

    opt = {
        "upload" : {"URL" : "http://localhost:8081/common/v1/upload"},
        "extproc" : {
            "cmdfmt" : "raspistill -o %s",
            "stdout" : False,
            "base" : "picture/"
        }
    }
    devinfo = [
        {"id" : "1", "dk" : "1", "dt": "gw", "children" : [
            {"id" : "2", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "47", "dk" : "0", "dt": "act/camera/level0"}
            ]}
        ]}
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    req = Request(2)
    req.setcommand(47, CmdCode.TAKE_PICTURE, {})

    ssmate = SSMate ({}, [], "1", quelist)
    dsmate = RPICamMate(opt, devinfo, "1", quelist)
    ssmate.start ()
    dsmate.start ()
    print("mate started")
    ssmate._writereq(req)
    time.sleep(10)
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")


