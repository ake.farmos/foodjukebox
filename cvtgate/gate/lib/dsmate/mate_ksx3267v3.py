#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
import itertools
from enum import IntEnum
from threading import Thread, Lock
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.exceptions import ConnectionException


from .. import *

class NodeType(IntEnum):
    SENNODE = 1
    ACTNODE = 2
    INTNODE = 3
    NUTNODE = 4

class ProtoVer(IntEnum):
    KS_X_3267_2020 = 10     #표준화지원사업
    KS_X_3267_2018 = 101
    KS_X_NUTRI_2021 = 20     #양액기 신규표준
    TTA_1 = 201

class RegisterMap(object):
    _READSIZE = 100   # 필요하다면 제품에 따라 달라질 수 있음
    _ACTAREA = 'a'
    _SENAREA = 's'
    _ACTTIMING = 3    # 3 초 넘으면 리프레쉬 필요
    _SENTIMING = 30   # 30 초 넘으면 리프레쉬 필요

    def __init__(self, conn, unit, lock, size, logger):
        self._conn = conn
        self._unit = unit
        self._lock = lock
        self._logger = logger
        self._size = RegisterMap._READSIZE if size == 0 else size
        self._mem = {RegisterMap._ACTAREA : {}, RegisterMap._SENAREA : {}}    #fast access(actuator), slow access(sensor)
        self._lastaccess = {RegisterMap._ACTAREA : 0, RegisterMap._SENAREA : 0}  # _ACTAREA 만 우선 사용
        self._nactive = 0

    def up(self):
        self._nactive = self._nactive + 1

    def down(self):
        self._nactive = self._nactive - 1

    def isactive(self):
        return self._nactive > 0    

    def getvalue(self, start, length):
        for rtype in [RegisterMap._ACTAREA, RegisterMap._SENAREA]:
            for k, v in self._mem[rtype].items():
                n = start - k
                if n >= 0 and n + length <= len(v): 
                    return v[n:n+length]
        self._logger.warn("There is no register. " + str([self._unit, start, length]) + str(self._mem))
        return None

    def addregister(self, start, length, dt):
        """ add only readable registers """
        if DevType.isactuator(dt) and DevType.getdevlevel(dt) > 0: # fast access register
            rtype = RegisterMap._ACTAREA
        else:
            rtype = RegisterMap._SENAREA

        n = None
        for k, v in self._mem[rtype].items():
            if k + len(v) == start:
                v.extend([0] * length)
                n = k + len(v)
                break
        else:
            self._mem[rtype][start] = [0] * length

        if n in self._mem[rtype]:
            self._mem[rtype][k].extend(self._mem[rtype][n])
            del self._mem[rtype][n]


    def optimize(self):
        """ after addregister this function should be called. """
        for rtype in [RegisterMap._ACTAREA, RegisterMap._SENAREA]:
            newmem = {}
            for k, v in self._mem[rtype].items():
                length = len(v)
                while length > self._size:
                    newmem[k] = [0] * self._size
                    k = k + self._size
                    length = length - self._size
                newmem[k] = [0] * length
            self._mem[rtype] = newmem
        print("optimized register : ", self._mem)

    def directread(self, start, length):
        with self._lock:
            time.sleep(0.5)
            try:
                res = self._conn.read_holding_registers(start, length, unit=self._unit)
            except Exception as ex:
                self._logger.warn("fail to read holding registers. : " + str(ex))
                return None
        if res is None or res.isError():
            self._logger.warn("Fail to read register." + str(self._unit) + "," + str(start) + ":" + str(res))
            return None
        if len(res.registers) != length:
            self._logger.warn("Number of read registers is not matched." + str(self._unit) + "," + str(start) + ":" + str(length))
            return None
        print ("registers : ", start, res.registers)
        return res.registers

    def activeread(self):
        """ for notices """
        self.read(RegisterMap._ACTAREA)

    def normalread(self):
        """ for notices """
        if self.isactive():
            self.read(RegisterMap._ACTAREA)
        else:
            now = time.time()
            if now - self._lastaccess[RegisterMap._ACTAREA] > RegisterMap._ACTTIMING:
                self.read(RegisterMap._ACTAREA)

    def slowread(self):
        """ for observations """
        now = time.time()
        if now - self._lastaccess[RegisterMap._SENAREA] > RegisterMap._SENTIMING:
            self.read(RegisterMap._SENAREA)

    def read(self, rtype):
        """ 읽기의 경우 전체를 읽어들임 """
        newmem = {}
        for k in self._mem[rtype].keys():
            length = len(self._mem[rtype][k])
            ret = self.directread(k, length)
            if ret is None:
                newmem[k] = self._mem[rtype][k]
                continue
            newmem[k] = ret
        self._mem[rtype] = newmem
        self._lastaccess[rtype] = time.time()

    def write(self, start, registers):
        """ 쓰기의 경우 대기 없이 직접 씀"""
        with self._lock:
            time.sleep(0.5)
            res = self._conn.write_registers(start, registers, unit=self._unit)

        if res.isError():
            self._logger.warn("Fail to write register." + str(self._unit) + "," + str(start) + ":" + str(res))
            return ResCode.FAIL_TO_WRITE
        return ResCode.OK

class STDDevice(object):
    _KEYWORDS = {"value" : (2, "float"), "status" : (1, "status"), 
        "opid" : (1, "short"), "state-hold-time" : (2, "int"), "ratio": (1, "short"), 
        "position" : (1, "short"), "remain-time" : (2, "int"),
        "control": (1, "control"), "area" : (1, "short"), "alert" : (1, "alert"), 
        "hold-time" : (2, "int"), "operation" : (1, "operation"),
        "time" : (2, "int"), "opentime" : (1, "short"), "closetime" : (1, "short"),
        "EC": (2, "float"), "pH": (2, "float"), "on-sec" : (2, "int"),
        "start-area" : (1, "short"), "stop-area": (1, "short"), 
        "epoch" : (2, "int"), "vfloat": (2, "float"), "vint" : (1, "short")}

    _MAXOPID = 65535

    def __init__(self, devid, dk, dt, register, logger):
        self._devid = devid
        self._dk = dk
        self._dt = dt
        self._register = register
        self._logger = logger
        self._lastopid = 0
        register.addregister(dk[1], self.getsize(dk[2]), dt)
        self._logger.info("New Device " + str(devid))

    def setlastopid(self, opid):
        self._lastopid = opid

    def setregister(self, flag):
        if flag:
            self._register.up()
        else:
            self._register.down()

    def getdevicetype(self):
        return self._dt

    def getsize(self, lst):
        size =0
        for k in lst:
            if k in STDDevice._KEYWORDS:
                size = size + STDDevice._KEYWORDS[k][0]
            else:
                self._logger.warn("wrong keyword : " + str(k))
                return -1
        return size

    def setcurrent(self):
        # 시간을 세팅받을 준비가 된 노드라면 전송한다.
        if self._dt == "nd" and len(self._dk) == 5 and "epoch" in self._dk[4]:
            req = Request(self._devid)
            req.setcommand(self._devid, CmdCode.SET_CURRENT, {})
            self.processrequest(req)

    def parseregisters(self, names, values):
        idx = 0
        ret = {}
        for nm in names:
            (size, vtype) = STDDevice._KEYWORDS[nm]
            if vtype == "float":
                val = struct.unpack('f', struct.pack('HH', values[idx], values[idx+1]))[0]
            elif vtype == "int":
                val = struct.unpack('i', struct.pack('HH', values[idx], values[idx+1]))[0]
            else:
                val = values[idx]
            if nm in ["vfloat", "vint"]:
                if nm in ret:
                    ret[nm] = ret[nm] + [val]
                else:
                    ret[nm] = [val]
            else:
                ret[nm] = val
            idx = idx + size
        return ret

    def update(self):
        pass
            
    def readinfo(self, direct=False):
        size = self.getsize(self._dk[2])
        if direct:
            values = self._register.directread(self._dk[1], size)
        else:
            values = self._register.getvalue(self._dk[1], size)

        if values:
            ret = self.parseregisters(self._dk[2], values)
            print("parsed", self._dk[1], ret)
            if "opid" in ret and ret["opid"] == (self._lastopid % STDDevice._MAXOPID + 1) and ret["status"] == 0:
                # request is done now.
                self.setregister(False)
            ret["opid"] = self._lastopid
            return ret
        else:
            self._logger.info("Fail to get data.")
            return None

    def processrequest(self, request):
        if request.isontime() == False:
            self._logger.info("The request is too late. " + request.stringify())
            return ResCode.TOO_LATE

        operation = request.getcommand()
        params = request.getparams()
        params["operation"] = operation    # need to convert by case
        params["opid"] = request.getopid() % STDDevice._MAXOPID + 1 # need to convert by case
        properparams = CmdCode.getparams(operation) + ["operation", "opid"]
        registers = []

        vfidx = 0
        viidx = 0

        # SET_CURRENT 명령은 현재시간을 기록하는것이 매우 중요.
        # 그런이유로 인자를 최대한 전달 직전에 세팅함
        if operation == CmdCode.SET_CURRENT:
            params["epoch"] = int(time.time())
            #self._logger.info ("set current : " + str(params))

        for key in self._dk[4]:
            if key not in properparams:
                # key param is not used for this operation
                # However, the register should be filled.
                val = 0
            elif key in params:
                if key == 'vfloat':
                    val = params[key][vfidx]
                    vfidx = vfidx + 1
                elif key == 'vint':
                    val = params[key][viidx]
                    viidx = viidx + 1
                else:
                    val = params[key]
            else:
                self._logger.warn("Wrong Keyword : " + str(key))
                return ResCode.FAIL_WRONG_KEYWORD
                    
            if STDDevice._KEYWORDS[key][0] == 1: 
                registers.append(val)
            elif STDDevice._KEYWORDS[key][1] == "int":
                val = int(val)
                registers.extend(struct.unpack('HH', struct.pack('i', val)))
            elif STDDevice._KEYWORDS[key][1] == "float":
                val = float(val)
                registers.extend(struct.unpack('HH', struct.pack('f', val)))

        #print(self._devid, " write : ", self._dk[3], registers)
        ret = self._register.write(self._dk[3], registers)
        if ret != ResCode.OK:
            return ret

        self.setlastopid(request.getopid())
        self.setregister(True)

        return ResCode.OK

class DeviceDetector(object):
    ''' 취소가 안되는 구조라 재설계가 필요함. detect 에서 상태를 저장하고 부분작업을 지속적으로 진행해야함 '''
    _DEVINFOREG = 2            # TTA 1172 지원을 위해서 일단 2부터로 함. 7,8번의 시리얼도 읽지 않음.
    _DEVCODEREG = 101

    def __init__(self, option, logger):
        self._isdetecting = False
        self._opid = 0
        self._option = option
        self._logger = logger
        self._conn = {}
        self._detected = {}
        self._target = []
        self._tidx = 0

    def setupconn(self, conn):
        self._conn = conn
        self.canceldetection(None)
        
    def isdetecting(self):
        return self._isdetecting

    def predetect(self):
        if self._isdetecting == False:
            return None

        port, unit = self._target[self._tidx]
        tempid = port + "-" + str(unit)
        noti = Notice(None, NotiCode.DETECT_NODE_STARTED, devid=tempid) # Detection Started
        if noti:
            noti.setkeyvalue("opid", self._opid)
            return noti

        self._logger.warn ("fail to make notice for checking detectable: " + str(detected))
        return None

    def _detectresult(self, noti):
        self._tidx = self._tidx + 1
        return noti

    def detect(self):
        if self._isdetecting == False:
            return None

        print("detect ", self._tidx)
        port, unit = self._target[self._tidx]
        tempid = port + "-" + str(unit)
        res = self._conn[port].readregister(DeviceDetector._DEVINFOREG, 6, unit)
        if res is None or res.isError():
            self._logger.info ("Fail to get information from a node : " + tempid + " " + str(res))
            noti = Notice(None, NotiCode.DETECT_NO_NODE, devid=tempid) # Detection Started
            return self._detectresult(noti)

        if len(res.registers) != 6:
            self._logger.info("retry to get data since size of data is not matched. 6 " + str(len(res.registers)))
            noti = Notice(None, NotiCode.DETECT_WRONG_DEVICE, devid=tempid) # Detection Started
            return self._detectresult(noti)

        if res.registers[1] in (NodeType.SENNODE, NodeType.ACTNODE, NodeType.INTNODE): # device type
            if res.registers[3] in [ProtoVer.KS_X_3267_2018, ProtoVer.KS_X_3267_2020, ProtoVer.KS_X_NUTRI_2021]:
                info = self.detectnode(port, unit, res.registers)
                self._logger.info ("Found a node : " + str(unit) + " " + str(info))
            else:
                self._logger.info ("Protocol Version is not valid. " + str(res.registers))
                noti = Notice(None, NotiCode.DETECT_UNKNOWN_PROTOCOL_VER, devid=tempid) # unknown protocol version
                return self._detectresult(noti)
        elif res.registers[1] == NodeType.NUTNODE:
            if res.registers[3] == ProtoVer.TTA_1:
                info = self.detectnode(port, unit, res.registers)
                self._logger.info ("Found a nutrient system : " + str(unit) + " " + str(info))
            else:
                self._logger.info ("Protocol Version is not valid. " + str(res.registers))
                noti = Notice(None, NotiCode.DETECT_UNKNOWN_PROTOCOL_VER, devid=tempid) # unknown protocol version
                return self._detectresult(noti)
        else:
            self._logger.info ("Unknown node is connected. " + str(res.registers))
            noti = Notice(unit, NotiCode.DETECT_UNKNOWN_NODE, devid=tempid) # unknown device
            return self._detectresult(noti)

        if info is None:
            noti = Notice(None, NotiCode.DETECT_WRONG_DEVICE, devid=tempid) # fail to find a node
        else:
            noti = Notice(None, NotiCode.DETECT_NODE_DETECTED, devid=port, content={unit : info}) # found a node
            self._detected[port][unit] = info
        noti.setkeyvalue("opid", self._opid)
        return self._detectresult(noti)

    def detectnode(self, port, unit, registers):
        compcode = registers[0]
        nodetype = registers[1]
        nodecode = registers[2]
        size = registers[4]

        res = self._conn[port].readregister(DeviceDetector._DEVCODEREG, size, unit)
        if res is None or res.isError():
            self._logger.warn("Fail to get devices from " + str(unit) + " " + str(res))
            return None

        if len(res.registers) != size:
            self._logger.info("retry to get data since size of data is not matched. " + str(size) + " " + str(len(res.registers)))
            return None

        return {"compcode" : compcode, "nodetype" : nodetype, "nodecode" : nodecode, "devcodes": res.registers}

    def postdetect(self):
        if self._tidx >= len(self._target):
            self._isdetecting = False
            opid = self._opid
            self._opid = 0
            self._tidx = 0
            self._target = []
            self._logger.info ("finished to detect devices : " + str(self._detected))
            noti = Notice(None, NotiCode.DETECT_FINISHED) # Detection Started
            if noti:
                noti.setkeyvalue("opid", opid)
                for port, info in self._detected.items():
                    noti.setcontent(port, info)
                return noti
            self._logger.warn ("fail to make notice for detection : " + str(self._detected))
        return None

    def canceldetection(self, params):
        self._isdetecting = False
        self._opid = 0
        return ResCode.OK

    def startdetection(self, params, opid):
        if self._isdetecting == True and self._opid != opid:
            self._logger.info ("Now detection is processing.... A new detection command would be ignored.")
            return ResCode.FAIL

        self._isdetecting = True
        self._opid = opid

        ports = [params['port']] if 'port' in params and params['port'] is not None else self._conn.keys()
        self._detected = dict([(k, {}) for k in ports])

        self._target = []
        for port in ports:
            if self._conn[port].getmethod() == 'rtu':
                saddr = params['saddr'] if 'saddr' in params else 1
                eaddr = params['eaddr'] if 'eaddr' in params else 11
                for unit in range(saddr, eaddr):
                    self._target.append((port, unit))
            else:
                self._target.append((port, 1))

        self._logger.info("Start detection : " + str(self._target) + str(ports) + str(self._conn.keys()))
        self._tidx = 0

        return ResCode.OK
            
class STDConnection(object):
    def __init__(self, option, devinfo, logger):
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._name = option["name"]
        self._lock = Lock()
        self._logger = logger
        self._registers = {}
        self._devices = {}
        self._method = None
        self._conn = self.connectone(option)

        if devinfo and devinfo["dt"] == "gw":
            for nd in devinfo["children"]:
                self.setupnode(nd, option)
        elif devinfo and devinfo["dt"] == "nd":
            self.setupnode(devinfo, option)
        else:
            self._logger.warn("Device Info is not proper.")

    def getmethod(self):
        return self._method

    def connectone(self, opt):
        ret = False
        conn = None

        try:
            if opt['method'] == 'rtu':
                conn = ModbusSerialClient(method='rtu', port=opt['port'],
                        timeout=self._timeout, baudrate=opt['baudrate'])
                ret = conn.connect()
                msg = "failed to connect with rtu"
                code = NotiCode.RTU_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
                self._method = opt['method']
            elif opt['method'] == 'tcp':
                conn =  ModbusTcpClient(opt['host'], port=opt['port'], timeout=self._timeout)
                ret = conn.connect()
                msg = "failed to connect with tcp"
                code = NotiCode.TCP_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
                self._method = opt['method']
            else:
                msg = "It's a wrong connection method. " + str(opt['method'])
        except Exception as ex:
            self._logger.warn("Fail to connect MODBUS [" + str(opt) + "] : " + str(ex))
            return None

        if ret == False:
            self._logger.warn(msg)
        # 보내지 않는다. 
        #noti = Notice(None, code)
        #self._writenoti(noti)
        return conn

    def setupnode(self, node, option):
        dk = json.loads(node["dk"])
        size = 0 if "readsize" not in option else option["readsize"]
        register = RegisterMap(self._conn, dk[0], self._lock, size, self._logger)
        self._registers[node["id"]] = register

        nodedev = STDDevice(node["id"], json.loads(node["dk"]), node["dt"], register, self._logger) 
        devices = {}
        for dev in node["children"]:
            print ("dev setupnode", dev)
            devices[dev["id"]] = STDDevice(dev["id"], json.loads(dev["dk"]), dev["dt"], register, self._logger)
        self._devices[node["id"]] = (nodedev, devices)
        register.optimize()
        return (register, devices)

    def close(self):
        self._conn.close()

    def getconnection(self):
        return self._conn

    def finddevice(self, nid, devid):
        try:
            if nid == devid:
                return self._devices[nid][0]
            return self._devices[nid][1][devid]
        except:
            return None

    def getnodeids(self):
        return self._devices.keys()

    def getnode(self, nid):
        try:
            return self._devices[nid][0]
        except:
            return None

    def getnodes(self, nid):
        try:
            return self._devices[nid]
        except:
            return None

    def activeread(self, nid):
        if self._registers[nid].isactive():
            self._registers[nid].activeread()
            return True
        return False

    def normalread(self, nid):
        self._registers[nid].normalread()

    def slowread(self, nid):
        self._registers[nid].slowread()

    def processrequest(self, request):
        dev = self.finddevice(str(request.getnodeid()), str(request.getdevid()))
        if dev:
            return dev.processrequest(request)
        return ResCode.FAIL_NO_DEVICE

    def readregister(self, addr, count, unit):
        with self._lock:
            try:
                time.sleep(0.5)
                return self._conn.read_holding_registers(addr, count, unit=unit)
            except Exception as ex:
                self._logger.warn("fail to read holding registers. : " + str(ex))
                return None

class KSX3267MateV3(DSMate):
    _VERSION = "KSX3267_V3"

    def __init__(self, option, devinfo, coupleid, quelist):
        super(KSX3267MateV3, self).__init__(option, devinfo, coupleid, quelist)
        self._timeout = 3 if "timeout" not in option else option["timeout"]
        self._conn = {}        # connections by port name
        self._nidconn = {}  # connections by node ids - for performance
        self._detector = DeviceDetector(option, self._logger)
        self._logger.info("KSX3267MateV3 Started.")

    def connect(self):
        for copt in self._option["conn"]:
            dinfo = self._devinfo.findgatewaybydk(copt["name"])
            conn = STDConnection(copt, dinfo, self._logger)
            self._conn[copt["name"]] = conn
            if dinfo:
                for nid in conn.getnodeids():
                    self._nidconn[nid] = conn
                self._logger.info(copt["name"] + " makes connection " + str(conn.getnodeids()))
            else:
                self._logger.info(copt["name"] + " makes connection but no device")

        self._logger.info("Mate connected : " + str(self._nidconn))

        self._detector.setupconn(self._conn)
        super(KSX3267MateV3, self).connect()

    def close(self):
        for key, conn in self._conn.items():
            conn.close()
        super(KSX3267MateV3, self).close()

    def process(self):
        self.processrequests()
        self.processobservations()
        self.processnotices()

        if self._detector.isdetecting():
            noti = self._detector.predetect()
            if noti:
                self._writenoti(noti)
                noti = self._detector.detect()
                if noti:
                    self._writenoti(noti)
                    noti = self._detector.postdetect()
                    if noti:
                        self._writenoti(noti)

        self.deviceupdate()

    def deviceupdate(self):
        for nid, conn in self._nidconn.items():
            node, devices = conn.getnodes(nid)
            for did, dev in devices.items():
                dev.update()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE):
            for nid, conn in self._nidconn.items():
                conn.getnode(nid).setcurrent()
                conn.slowread(nid)
                node, devices = conn.getnodes(nid)
                obs = Observation(nid)
                info = node.readinfo()    
                if info:
                    obs.setobservation(nid, 0, StatCode(info["status"]))

                for did, dev in devices.items():
                    if DevType.issensor(dev.getdevicetype()):
                        info = dev.readinfo(True)    
                        if info:
                            obs.setobservation(did, info["value"], StatCode(info["status"]))
                self._writeobs(obs)
            self._updatetime(Mate.OBSTYPE)

    def processnotices(self):
        # 정기 전송과 수시 전송으로 나뉨
        if self._timetocheck(Mate.ACTNOTITYPE):
            for nid, conn in self._nidconn.items():
                if conn.activeread(nid):
                    self.sendnotice(nid, Mate.ACTNOTITYPE)
            self._updatetime(Mate.ACTNOTITYPE)
        elif self._timetocheck(Mate.NOTITYPE):
            for nid, conn in self._nidconn.items():
                conn.normalread(nid)
                self.sendnotice(nid, Mate.NOTITYPE)
            self._updatetime(Mate.NOTITYPE)
                
    def sendnotice(self, nid, ntype):
        conn = self._nidconn[nid]
        node, devices = conn.getnodes(nid)
        info = node.readinfo()    
        if info is None:
            self._logger.warn("Fail to get node information for notice.")
            return
        noti = Notice(nid, NotiCode.ACTUATOR_STATUS, nid, info)
        for did, dev in devices.items():
            if DevType.isactuator(dev.getdevicetype()):
                info = dev.readinfo(True)
                if info:
                    noti.setcontent(did, info)
        self._writenoti(noti)

    def processrequest(self, req):
        """ 하나의 요청을 처리한다. """
        print("received request", req.getdevid(), self._coupleid)
        if BlkType.isrequest(req.gettype()) is False:
            self._logger.warn("The message is not request. " + str(req.gettype()))
            return False

        response = Response(req)
        cmd = req.getcommand()
        nd = self._devinfo.finddevbyid(req.getnodeid())
        dev = self._devinfo.finddevbyid(req.getdevid())

        if req.getdevid() == self._coupleid:
            params = req.getparams()
            if cmd == CmdCode.DETECT_DEVICE:
                print("detect device")
                code = self._detector.startdetection(params, req.getopid())
            elif cmd == CmdCode.CANCEL_DETECT:
                print("cancel to detect device")
                code = self._detector.canceldetection(params)
            else:
                self._logger.warn("Unknown Error. " + str(req) + ", " + str(dev))
                code = ResCode.FAIL

        elif dev is None:
            self._logger.warn("There is no device. " + str(req.getdevid()))
            code = ResCode.FAIL_NO_DEVICE

        elif DevType.ispropercommand(dev['dt'], cmd) is False:
            self._logger.warn("The request is not proper. " + str(cmd) + " " + str(dev['dt']))
            code = ResCode.FAIL_NOT_PROPER_COMMAND

        elif DevType.isactuator(dev['dt']) or DevType.isnode(dev['dt']):
            # modbus
            nid = str(req.getnodeid())
            conn = self._nidconn[nid]
            try:
                code = conn.processrequest(req)
            except ConnectionException as ex:
                self._logger.warn(str(ex))
                self.connect()
                conn = self._nidconn[nid]
                code = conn.processrequest(req)

            if code != ResCode.OK:
                self._logger.warn("Fail to process request.")
            else:
                self.sendnotice(nid, Mate.ACTNOTITYPE)
            self._logger.info("Actuator processed : " + str(code) + str(req.getcontent()))

        elif DevType.isgateway(dev['dt']):
            self._logger.info("Gateway does not receive a request")
            code = ResCode.FAIL

        else:
            self._logger.warn("Unknown Error. " + str(req) + ", " + str(dev))
            code = ResCode.FAIL

        response.setresult(code)
        #self._logger.info("write response: " + str(response))
        self._writeres(response)
        return True #if code == ResCode.OK else False

if __name__ == "__main__":
    from multiprocessing import Queue

    isnutri = False
    quelist = [Queue(), Queue(), Queue(), Queue()]
    """
    opt = {
        'conn' : [{
            'method': 'rtu',
            'name' : 'ACM0',
            'port' : '/dev/ttyACM0',
            'baudrate' : 9600,
            'timeout': 5
        }]
    }
    """
    
    opt = {
        'conn' : [{
            'name' : 'ACM0',
            'method': 'tcp',
            'host' : '192.168.0.53',
            'port' : 502,
            'timeout': 5
        }]
    }
    
    nutriinfo = [{
        "id" : "1", "dk" : "", "dt": "gw", "children" : [{
            "id" : "101", "dk" : '[1,40201,["status"],45001,["operation","opid"]]', "dt": "nd", "children" : [
                {"id" : "102", "dk" : '[1,40211,["control","status","area","alert","opid"],45001,["operation", "opid", "control","EC","pH", "start-area", "stop-area", "on-sec"]]', "dt": "nutrient-supply/level1"},
                {"id" : "103", "dk" : '[1,40221,["value","status"]]', "dt": "sen"},
                {"id" : "104", "dk" : '[1,40231,["value","status"]]', "dt": "sen"},
                {"id" : "105", "dk" : '[1,40241,["value","status"]]', "dt": "sen"},
                {"id" : "106", "dk" : '[1,40251,["value","status"]]', "dt": "sen"},
                {"id" : "107", "dk" : '[1,40261,["value","status"]]', "dt": "sen"},
                {"id" : "109", "dk" : '[1,40271,["value","status"]]', "dt": "sen"},
                {"id" : "110", "dk" : '[1,40281,["value","status"]]', "dt": "sen"},
                {"id" : "111", "dk" : '[1,40291,["value","status"]]', "dt": "sen"},
                {"id" : "112", "dk" : '[1,40301,["value","status"]]', "dt": "sen"},
                {"id" : "113", "dk" : '[1,40311,["value","status"]]', "dt": "sen"}
            ]}
        ]}
    ]
    devinfo = [
        {"id" : "1", "dk" : "192.168.0.33:502", "dt": "gw", "children" : [
            {"id" : "2", "dk" : '[0,202,["status"]]', "dt": "nd", "children" : [
                {"id" : "3", "dk" : '[0,203,["value","status"]]', "dt": "sen"},
                {"id" : "4", "dk" : '[0,212,["value","status"]]', "dt": "sen"}
            ]}
        ]},
        {"id" : "11", "dk" : "192.168.0.53:503", "dt": "gw", "children" : [
            {"id" : "12", "dk" : '[0,201,["opid, "status"], 501,["operation", "opid"]]', "dt": "nd", "children" : [
                {"id" : "13", "dk" : '[0,203,["opid", "status", "remain-time"],503, ["operation", "opid", "hold-time"]', "dt": "act/switch/level1"},
                {"id" : "14", "dk" : '[0,267,["opid","status","remain-time"], 567,["operation", "opid", "time"]]', "dt": "act/retractable/level1"}
            ]}
        ]}
    ]
    """
        }, {
            "id" : "201", "dk" : '[2,40201,["status"],45001,["operation","opid"]]', "dt": "nd", "children" : [
                {"id" : "202", "dk" : '[2,40202,["opid","status","state-hold-time","remain-time"],40206,["operation","opid","time"]]', "dt": "act/retractable/level1"},
                {"id" : "202", "dk" : '[2,40209,["opid","status","state-hold-time","remain-time"],40213,["operation","opid","time"]]', "dt": "act/retractable/level1"},
                {"id" : "203", "dk" : '[2,40216,["value","status"]]', "dt": "sen"},
                {"id" : "204", "dk" : '[2,40219,["value","status"]]', "dt": "sen"},
                #{"id" : "203", "dk" : (2,40221,["opid","status"],45021,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "204", "dk" : (2,40231,["opid","status"],45031,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "205", "dk" : (2,40241,["opid","status"],45041,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "206", "dk" : (2,40251,["opid","status"],45051,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "207", "dk" : (2,40261,["opid","status"],45061,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "208", "dk" : (2,40271,["opid","status"],45071,["operation","opid"]), "dt": "act/switch/level0"},
                #{"id" : "209", "dk" : (2,40281,["opid","status"],45081,["operation","opid"]), "dt": "act/switch/level0"}
             ]
        }, {
            "id" : "301", "dk" : (3,40201,["opid","status"],45001,["operation","opid"]), "dt": "nd", "children" : [
                {"id" : "302", "dk" : (3,40211,["opid","status"],45011,["operation","opid"]), "dt": "act/retractable/level0"},
                {"id" : "303", "dk" : (3,40221,["opid","status"],45021,["operation","opid"]), "dt": "act/retractable/level0"},
                {"id" : "304", "dk" : (3,40231,["opid","status"],45031,["operation","opid"]), "dt": "act/retractable/level0"},
                {"id" : "305", "dk" : (3,40241,["opid","status"],45041,["operation","opid"]), "dt": "act/retractable/level0"}
            ]
        }]
    }]
    """

    if isnutri:
        kdmate = KSX3267MateV3(opt, nutriinfo, "1", quelist)
    else:
        kdmate = KSX3267MateV3(opt, devinfo, "1", quelist)

    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    kdmate.start ()
    print("mate started")

    time.sleep(10)
    req = Request(None)
    req.setcommand("1", CmdCode.DETECT_DEVICE, None)
    print("=======================================#1")
    ssmate._writereq(req)
    print("=======================================#1")

    """
    time.sleep(1)
    req = Request(None)
    req.setcommand("1", CmdCode.CANCEL_DETECT, {})
    print "=======================================#2"
    ssmate._writereq(req)
    print "=======================================#2"

    time.sleep(1)
    req = Request(None)
    req.setcommand("1", CmdCode.DETECT_DEVICE, None)
    print "=======================================#3"
    ssmate._writereq(req)
    print "=======================================#3"

    time.sleep(1)
    req = Request(None)
    req.setcommand("1", CmdCode.CANCEL_DETECT, {})
    print "=======================================#4"
    ssmate._writereq(req)
    print "=======================================#4"

    time.sleep(10)
    req = Request(201)
    req.setcommand(202, CmdCode.OPEN, {})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(201)
    req.setcommand(202, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    time.sleep(10)
    req = Request(201)
    req.setcommand(202, CmdCode.TIMED_OPEN, {"time":10})
    ssmate._writereq(req)

    time.sleep(15)
    req = Request(201)
    req.setcommand(202, CmdCode.TIMED_CLOSE, {"time":10})
    ssmate._writereq(req)

    time.sleep(5)
    req = Request(201)
    req.setcommand(202, CmdCode.OFF, {})
    ssmate._writereq(req)
    
    """

    time.sleep(60)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    kdmate.stop()
    ssmate.stop()
    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()


