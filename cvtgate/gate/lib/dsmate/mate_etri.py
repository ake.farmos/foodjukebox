#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import itertools
import json
from os import stat
from lib.dinfo import DevInfo
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import pymysql
import psycopg2
import traceback
from collections import defaultdict, deque
from enum import Enum
from itertools import groupby
import pprint
from decimal import Decimal
from lib.mate import DSMate, Logger, Mate
import re
from .. import *


# global variable for logging
logger = None


class ETRIDBConnector:
    def __init__(self, db_option) -> None:
        self.dbname = db_option["db"]
        self.username = db_option["username"]
        self.password = db_option["password"]
        self.host = db_option["host"]
        self.port = db_option.get("port", 5432)

    def connect(self):
        return psycopg2.connect(
            host=self.host,
            database=self.dbname,
            user=self.username,
            password=self.password,
            port=self.port,
        )


class ETRIDBReader:
    def __init__(self, connector: ETRIDBConnector) -> None:
        self.connector = connector

    def read(self, query, vars: tuple = None) -> tuple:
        with self.connector.connect() as conn:
            with conn.cursor() as cur:
                try:
                    cur.execute(query, vars)
                    result = cur.fetchall()
                    return result
                except:
                    error = traceback.format_exc(300)
                    global logger
                    if logger:
                        logger.error(error)
                    print(error)
        return tuple()


class ControllerDeviceReader:
    def __init__(self, reader: ETRIDBReader, zone_id: int = 1) -> None:
        self.reader = reader
        self.zone_id = zone_id

    def __readdb(self) -> tuple:
        query = """
            SELECT type, device_id, controller_id, controller_node_id, zone_id, tag, reg_dt, info->'controlType', info->'uLimit',  info->'lLimit, info'  
	        FROM tb_controller_device
            """
        return self.reader.read(query)

    def __label(self, results) -> tuple:
        return tuple(
            {
                "type": row[0],
                "device_id": row[1],
                "controller_id": row[2],
                "controller_node_id": row[3],
                "zone_id": row[4],
                "tag": row[5],
                "reg_dt": row[6],
                "control_type": row[7],
                "upper_limit": row[8],
                "lower_limit": row[9],
                "controller_device_info": json.loads(row[10]),
            }
            for row in results
        )

    def __get_zone_id(self, results):
        return tuple(r for r in results if r["zone_id"] == self.zone_id)

    def read(self):
        results = self.__readdb()
        results = self.__label(results)
        results = self.__get_zone_id(results)
        global logger
        # logger.debug("Controller device reader : " + str(results))
        return results


class AlarmReader:
    def __init__(self, reader: ETRIDBReader, before: int = 5) -> None:
        self.db_reader = reader
        self.before = before

    def read(self):
        query = """
        SELECT idx, controller_id, zone_id, collect_dt, reg_dt, info->'content' as error
        FROM public.tb_controller_alarm
        WHERE collect_dt > now() - INTERVAL '%s MINUTE';
        """
        return self.db_reader.read(query, (self.before))


class StatusReader:
    def __init__(self, reader: ETRIDBReader) -> None:
        self.reader = reader

    def __readdb(self) -> tuple:
        query = """
        SELECT controller_id, zone_id, info, collect_dt, screen_code
        FROM tb_controller_status_current 
        """
        return self.reader.read(query)

    def __label(self, results) -> tuple:
        return tuple(
            {
                "controller_id": row[0],
                "zone_id": row[1],
                "info": row[2],
                "status_collect_datetime": row[3],
                "screen_code": row[4],
            }
            for row in results
        )

    def read(self):
        results = self.__readdb()
        results = self.__label(results)
        global logger
        # logger.debug("Status reader : " + str(results))
        return results


class IrrigationStatusReader:
    def __init__(self, reader: ETRIDBReader) -> None:
        self.reader = reader

    def __readdb(self) -> tuple:
        query = """
        SELECT controller_id, info, collect_dt, screen_code
        FROM tb_irrigate_sta
        WHERE collect_dt BETWEEN NOW() - INTERVAL '1 MINUTES' AND NOW()
        """
        return self.reader.read(query)

    def __label(self, results) -> tuple:
        return tuple(
            {
                "controller_id": row[0],
                "zone_id": row[1],
                "info": row[2],
                "status_collect_datetime": row[3],
                "screen_code": row[4],
            }
            for row in results
        )

    def read(self):
        results = self.__readdb()
        results = self.__label(results)
        global logger
        # logger.debug("Irrigation Status Reader : " + str(results))
        return results


class Switch:
    @staticmethod
    def convert_to_status(status_code):
        if status_code == 1:
            return 201
        else:
            return 0

    @staticmethod
    def __get_run_status(status, screen_code_map, screen_code):
        key = screen_code_map[screen_code]
        return Switch.convert_to_status(status["info"][key])

    @staticmethod
    def __set_value(status):
        screen_code_map = {
            "ctrlHAF": "HAFRun",
            "ctrlHeatHotWater": "valveOpen",
            "ctrlCO2": "CO2Run",
        }

        if status["control_type"] == "0" and status["screen_code"] in screen_code_map:
            return {
                **status,
                "status": Switch.__get_run_status(
                    status, screen_code_map, status["screen_code"]
                ),
            }

        return status

    @staticmethod
    def parse(statuses):
        result = tuple(Switch.__set_value(d) for d in statuses)
        global logger
        # logger.debug("Switch : " + str(result))
        return result


class DefaultStatus:
    @staticmethod
    def assign(devices):
        return tuple({**d, "status": 0} for d in devices)


class Ventilator:
    @staticmethod
    def __leeStatus(curPos, tgtLee):
        if curPos > tgtLee:
            return 301
        elif curPos == tgtLee:
            return 0
        elif curPos < tgtLee:
            return 302

    @staticmethod
    def __ctnStatus(curPos, curCtn):
        if curPos > curCtn:
            return 301
        elif curPos == curCtn:
            return 0
        elif curPos < curCtn:
            return 302

    @staticmethod
    def __get_status(curVent, tgtLee, curCtn):
        first = curVent[0]
        second = curVent[0]
        return (
            {
                "device_id": first["devID"],
                "controller_node_id": first["nodeID"],
                "status": Ventilator.__leeStatus(first["curPos"], tgtLee),
                "current_position": first,
            },
            {
                "device_id": second["devID"],
                "controller_node_id": second["nodeID"],
                "status": Ventilator.__ctnStatus(second["curPos"], curCtn),
                "current_position": second,
            },
        )

    @staticmethod
    def __get_statuses(status):
        info = status["info"]
        return Ventilator.__get_status(info["curVent"], info["tgtLee"], info["curCtn"])

    @staticmethod
    def assign_status(statuses):
        ventStatuses = tuple(s for s in statuses if s["screen_code"] == "ctrlVent")
        ventStatuses = tuple(
            {**s, **d} for s in ventStatuses for d in Ventilator.__get_statuses(s)
        )
        otherStatus = tuple(s for s in statuses if s["screen_code"] != "ctrlVent")
        result = ventStatuses + otherStatus
        global logger
        # logger.debug("Ventilator : " + str(result))
        return result


class Shade:
    @staticmethod
    def __get_status_code(criActShade, curPos, targetPos):
        if criActShade == 0:
            return 0
        if curPos < targetPos:
            return 301
        elif curPos == targetPos:
            return 0
        elif targetPos < curPos:
            return 302

    @staticmethod
    def __is_shade(status):
        return status["screen_code"] == "ctrlShadeCtn"

    @staticmethod
    def __add_status_info(status):
        info = status["info"]
        state = info["curCtnState"][0]
        actShade = info["criActShade"]
        curPos = state["curPos"]
        tgtPos = state["targetPos"]
        return {
            **status,
            "status": Shade.__get_status_code(actShade, curPos, tgtPos),
            "device_id": state["devID"],
            "controller_node_id": state["nodeID"],
            "current_position": curPos,
        }

    @staticmethod
    def __add_shade_info(status):
        if Shade.__is_shade(status):
            return Shade.__add_status_info(status)
        else:
            return status

    @staticmethod
    def assign_statuses(statuses):
        return tuple(Shade.__add_shade_info(s) for s in statuses)


class SideCutton:
    @staticmethod
    def __get_status_code(curPos, targetPos):
        if curPos < targetPos:
            return 301
        elif curPos == targetPos:
            return 0
        elif targetPos < curPos:
            return 302

    @staticmethod
    def __is_side_cutton(status):
        return status["screen_code"] == "ctrlSideCtn"

    @staticmethod
    def __get_status(curCtnState_unit):
        return {
            "device_id": curCtnState_unit["devID"],
            "node_id": curCtnState_unit["nodeID"],
            "status": SideCutton.__get_status_code(
                curCtnState_unit["curPos"], curCtnState_unit["targetPos"]
            ),
            "current_position": curCtnState_unit["curPos"],
        }

    @staticmethod
    def __device_status(status):
        return tuple(
            {**status, **SideCutton.__get_status(stat)}
            for stat in status["info"]["curCtnState"]
        )

    @staticmethod
    def __collect_side_cutton(statuses):
        return tuple(s for s in statuses if SideCutton.__is_side_cutton(s))

    @staticmethod
    def __collect_non_side_cutton(statuses):
        return tuple(s for s in statuses if not SideCutton.__is_side_cutton(s))

    @staticmethod
    def assign_statuses(statuses):
        side_cuttons = SideCutton.__collect_side_cutton(statuses)
        side_cuttons = tuple(
            d for s in side_cuttons for d in SideCutton.__device_status(s)
        )
        non_side_cuttons = SideCutton.__collect_non_side_cutton(statuses)
        result = side_cuttons + non_side_cuttons
        global logger
        # logger.debug("SideCutton : " + str(result))
        return result


class Thermal:
    @staticmethod
    def __get_status_code(curPos, targetPos):
        if curPos < targetPos:
            return 301
        elif curPos == targetPos:
            return 0
        elif targetPos < curPos:
            return 302

    @staticmethod
    def __is_thermal(status):
        return status["screen_code"] == "ctrlTherCtn"

    @staticmethod
    def __get_status(curCtnState_unit):
        return {
            "device_id": curCtnState_unit["devID"],
            "node_id": curCtnState_unit["nodeID"],
            "status": Thermal.__get_status_code(
                curCtnState_unit["curPos"], curCtnState_unit["targetPos"]
            ),
            "current_position": curCtnState_unit["curPos"],
        }

    @staticmethod
    def __device_status(status):
        return tuple(
            {**status, **Thermal.__get_status(stat)}
            for stat in status["info"]["curCtnState"]
        )

    @staticmethod
    def __collect_thermal(statuses):
        return tuple(s for s in statuses if Thermal.__is_thermal(s))

    @staticmethod
    def __collect_non_thermal(statuses):
        return tuple(s for s in statuses if not Thermal.__is_thermal(s))

    @staticmethod
    def assign_statuses(statuses):
        side_cuttons = Thermal.__collect_thermal(statuses)
        side_cuttons = tuple(
            d for s in side_cuttons for d in Thermal.__device_status(s)
        )
        non_side_cuttons = Thermal.__collect_non_thermal(statuses)
        result = side_cuttons + non_side_cuttons
        global logger
        # logger.debug("SideCutton : " + str(result))
        return result


class EnvironmentValueReader:
    """
    tb_controller_environment_current table에서 controller_id,
    controller_node_id, zone_id, device_id, value, value_collect_datetime을 조회함.
    쿼리 크기에 따라서 limit을 조절할 필요도 있어보임
    """

    @staticmethod
    def __parse_key(key):
        """
        parse information key
        "SID_11_10" to (11, 10)
        return (node_id, device_id)

        Args:
            key ([type]): [description]
        """
        regex = re.compile("([S|A])ID_(\d+)_(\d+)")
        results = regex.findall(key)
        if len(results) == 0:
            return None
        device_type, node_id, device_id = results[0]

        return {
            "device_type": device_type,
            "controller_node_id": int(node_id),
            "device_id": int(device_id),
        }

    @staticmethod
    def __parse_info(info):
        return tuple(
            {**EnvironmentValueReader.__parse_key(key), "value": Decimal(value)}
            for key, value in info.items()
        )

    @staticmethod
    def __label(results):
        return tuple(
            {
                "controller_id": row[0],
                "zone_id": row[1],
                "info": row[2],
                "value_collect_datetime": row[3],
                "status": 0,
                "status_collect_datetime": row[3],
            }
            for row in results
        )

    @staticmethod
    def __parse_result(results):
        return tuple(
            {**row, **device_info}
            for row in results
            for device_info in EnvironmentValueReader.__parse_info(row["info"])
        )

    def __init__(self, reader: ETRIDBReader) -> None:
        self.reader = reader

    def __readdb(self):
        query = """
        SELECT controller_id, zone_id, info, collect_dt
        FROM tb_controller_environment_current;        
        """
        return self.reader.read(query)

    def read(self):
        results = self.__readdb()
        results = self.__label(results)
        return EnvironmentValueReader.__parse_result(results)


class IrrigationValueReader:
    """ """

    @staticmethod
    def __parse_key(key):
        """
        parse information key
        "SID_11_10" to (11, 10)
        return (node_id, device_id)

        Args:
            key ([type]): [description]
        """
        regex = re.compile("([S|A])ID_(\d+)_(\d+)")
        results = regex.findall(key)
        if len(results) == 0:
            return None
        device_type, node_id, device_id = results[0]

        return {
            "device_type": device_type,
            "controller_node_id": int(node_id),
            "device_id": int(device_id),
        }

    @staticmethod
    def __parse_info(info):
        return tuple(
            {**IrrigationValueReader.__parse_key(key), "value": Decimal(value)}
            for key, value in info.items()
        )

    @staticmethod
    def __label(results):
        return tuple(
            {
                "controller_id": row[0],
                "info": row[1],
                "value_collect_datetime": row[3],
                "status": 0,
                "status_collect_datetime": row[3],
            }
            for row in results
        )

    @staticmethod
    def __parse_result(results):
        return tuple(
            {**row, **device_info}
            for row in results
            for device_info in IrrigationValueReader.__parse_info(row["info"])
        )

    def __init__(self, reader: ETRIDBReader) -> None:
        self.reader = reader

    def __readdb(self):
        query = """
        SELECT controller_id, info, collect_dt
        FROM tb_irrigate_env
        WHERE collect_dt BETWEEN NOW() - INTERVAL '1 MINUTES' AND NOW();        
        """
        result = self.reader.read(query)
        return result

    def read(self):
        results = self.__readdb()
        results = self.__label(results)
        results = IrrigationValueReader.__parse_result(results)
        global logger
        # logger.debug("IrrigationValueReader : " + str(results))
        return results


class FarmosDBConnector:
    def __init__(self, db_option: dict, logger: Logger = None) -> None:
        self.dbname = db_option.get("db", None)
        self.username = db_option.get("username", None)
        self.password = db_option.get("password", None)
        self.host = db_option.get("host", None)
        self.port = db_option.get("port", 3306)

    def connect(self):
        try:

            conn = pymysql.connect(
                host=self.host,
                database=self.dbname,
                port=self.port,
                user=self.username,
                password=self.password,
            )
            # logger.debug(type(conn))
            return conn
        except Exception as e:
            raise e


class FarmosDBWriter:
    def __init__(self, connector: FarmosDBConnector, logger=None) -> None:
        self.connector = connector
        self.logger = logger

    def write(self, query, vars=None) -> tuple:
        global logger
        try:
            # logger.debug("FarmosDBWriter :" + str(query) + str(vars))
            conn = self.connector.connect()
            with conn.cursor() as cur:
                cur.execute(query, vars)
                result = conn.commit()
                return {"result": "OK"}
        except:
            error = traceback.format_exc(300)
            logger.error(error)
        return {"result": "Fail"}


class CurrentObservationWriter:
    def __init__(self, db_writer: FarmosDBWriter) -> None:
        self.db_writer = db_writer

    def write(self, data_id, obs_time: datetime.datetime, nvalue):
        query = """
        INSERT INTO current_observations(data_id, obs_time, nvalue)
        VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE obs_time = %s, nvalue = %s;
        """
        return self.db_writer.write(
            query, (data_id, obs_time, nvalue, obs_time, nvalue)
        )

    def multiple_write(self, rows: list):
        return [self.write(r["data_id"], r["obs_time"], r["nvalue"]) for r in rows]


class ObservationWriter:
    def __init__(self, db_writer: FarmosDBWriter) -> None:
        self.db_writer = db_writer

    def write(self, data_id, obs_time: datetime.datetime, nvalue):
        query = """
        INSERT INTO observations(data_id, obs_time, nvalue)
        VALUES(%s, %s, %s) ON DUPLICATE KEY UPDATE obs_time = %s, nvalue = %s;
        """
        return self.db_writer.write(
            query, (data_id, obs_time, nvalue, obs_time, nvalue)
        )

    def multiple_write(self, rows: list):
        return [self.write(r["data_id"], r["obs_time"], r["nvalue"]) for r in rows]


class IDConverter:
    DEVICE_DATA = 1
    USER_INPUT_DATA = 2

    CATEGORY_MODIFIER = 10_000_000
    FIELD_ID_MODIFIER = 100_000
    SAMPLE_MODIFIER = 1_000

    DEVICE_ID_MODIFIER = 100

    STATUS = 0
    VALUE = 1
    POSITION = 2

    @staticmethod
    def device_id_to_status_id(farmos_device_id):
        return (
            IDConverter.DEVICE_DATA * IDConverter.CATEGORY_MODIFIER
            + int(farmos_device_id) * IDConverter.DEVICE_ID_MODIFIER
            + IDConverter.STATUS
        )

    @staticmethod
    def device_id_to_value_id(farmos_device_id):
        return (
            IDConverter.DEVICE_DATA * IDConverter.CATEGORY_MODIFIER
            + int(farmos_device_id) * IDConverter.DEVICE_ID_MODIFIER
            + IDConverter.VALUE
        )

    @staticmethod
    def device_id_to_position_id(farmos_device_id):
        return (
            IDConverter.DEVICE_DATA * IDConverter.CATEGORY_MODIFIER
            + int(farmos_device_id) * IDConverter.DEVICE_ID_MODIFIER
            + IDConverter.POSITION
        )


class IDMapper:
    @staticmethod
    def __flatten(farmos_device_info):
        return tuple(
            {
                "farmos_node_id": node["id"],
                "farmos_node_key": node["dk"],
                "farmos_gateway_id": gateway["id"],
                "farmos_gateway_key": gateway["dk"],
                "farmos_device_id": device["id"],
                "farmos_device_key": device["dk"],
                "farmos_data_type": device["dt"],
            }
            for gateway in farmos_device_info
            for node in gateway["children"]
            for device in node["children"]
        )

    @staticmethod
    def __is_match(etri_device, farmos_device):
        match = (
            str(etri_device["controller_id"])
            == str(farmos_device["farmos_gateway_key"])
            and str(etri_device["controller_node_id"])
            == str(farmos_device["farmos_node_key"])
            and str(etri_device["device_id"]) == str(farmos_device["farmos_device_key"])
        )
        return match

    @staticmethod
    def map(etri_device_info, farmos_device_info):
        global logger
        farmos_device_info = IDMapper.__flatten(farmos_device_info)
        result = tuple(
            {**farmos_device, **etri_device}
            for etri_device in etri_device_info
            for farmos_device in farmos_device_info
            if IDMapper.__is_match(etri_device, farmos_device)
        )
        return result


class DataAppender:
    @staticmethod
    def __add_value(device_value, value):
        if device_value["type"] == "S":
            return {
                **device_value,
                "value": value["value"],
                "value_collect_datetime": value["value_collect_datetime"],
                "status_collect_datetime": value["value_collect_datetime"],
            }
        elif device_value["type"] == "A":
            return {
                **device_value,
                "value": value["value"],
                "value_collect_datetime": value["value_collect_datetime"],
            }

    @staticmethod
    def __is_match(device_info, element):
        return (
            all(k in element for k in ("controller_node_id", "device_id"))
            and device_info["controller_node_id"] == element["controller_node_id"]
            and device_info["device_id"] == element["device_id"]
        )

    @staticmethod
    def __assign_values(device_value, values):
        result = tuple(
            DataAppender.__add_value(device_value, v)
            for v in values
            if DataAppender.__is_match(device_value, v)
        )
        if 1 <= len(result):
            return result[0]
        else:
            return device_value

    @staticmethod
    def assign_value(device_status_values, values):
        return tuple(
            DataAppender.__assign_values(d, values) for d in device_status_values
        )

    @staticmethod
    def __assign_status(device_status, statuses):
        result = tuple(
            {
                **device_status,
                "status": s["status"],
                "status_collect_datetime": s["status_collect_datetime"],
            }
            for s in statuses
            if DataAppender.__is_match(device_status, s) and "status" in s
        )
        if 1 <= len(result):
            return result[0]
        else:
            return device_status

    @staticmethod
    def assign_status(device_status_values, statuses):
        return tuple(
            DataAppender.__assign_status(d, statuses) for d in device_status_values
        )


class DeviceDataCollector:
    def __init__(self, etri_db_reader: ETRIDBReader, devices) -> None:
        self.etri_db_reader = etri_db_reader
        self.devices = devices

    def __read_environment(self, devices):
        reader = EnvironmentValueReader(self.etri_db_reader)
        env_values = reader.read()
        assigned = DataAppender.assign_value(devices, env_values)
        return assigned

    def __read_irrigation_status(self, devices):
        irrigation_status_reader = IrrigationStatusReader(self.etri_db_reader)
        irrigation_status = irrigation_status_reader.read()
        devices = DataAppender.assign_status(devices, irrigation_status)
        return devices

    def __read_irrigation_value(self, devices):
        irrigation_value_reader = IrrigationValueReader(self.etri_db_reader)
        irrigation_value = irrigation_value_reader.read()
        devices = DataAppender.assign_value(devices, irrigation_value)
        return devices

    def __read_controller_status(self):
        status_reader = StatusReader(self.etri_db_reader)
        statuses_result = status_reader.read()
        vent_status_added = Ventilator.assign_status(statuses_result)
        shade_status_added = Shade.assign_statuses(vent_status_added)
        side_cutton_status_added = SideCutton.assign_statuses(shade_status_added)
        thermal_added = Thermal.assign_statuses(side_cutton_status_added)

        return thermal_added

    def __append_controller_status(self, devices):
        status = self.__read_controller_status()
        devices = DataAppender.assign_status(devices, status)
        return devices

    def collect(self):
        devices = self.devices
        devices = DefaultStatus.assign(devices)
        devices = self.__read_environment(devices)
        devices = self.__read_irrigation_status(devices)
        devices = self.__read_irrigation_value(devices)
        devices = self.__append_controller_status(devices)
        return devices


class ObservationsBuilder:
    @staticmethod
    def __get_observation(node_id, devices):
        obsblk = Observation(node_id)

        for device in devices:
            if "value" in device and "status" in device:
                value = float(device["value"])
                status = device["status"]
                device_id = device["farmos_device_id"]
                obsblk.setobservation(device_id, value, StatCode(status))
        return obsblk

    @staticmethod
    def build(value_statuses):
        global logger

        return tuple(
            ObservationsBuilder.__get_observation(node_id, devices)
            for node_id, devices in itertools.groupby(
                value_statuses, lambda x: x["farmos_node_id"]
            )
        )


class NoticesBuilder:
    @staticmethod
    def get_actuator_status(device):
        screen_code = device["screen_code"]

        if screen_code == "ctrlShadeCtn":
            return {"position": device["current_position"]}
        elif screen_code == "ctrlVent":
            return {"position": device["current_position"]}
        elif screen_code == "ctrlSideCtn":
            return {"position": device["current_position"]}
        else:
            return {}

    @staticmethod
    def build(value_statuses):
        def get_notice(node_id, devices):
            notice = Notice(node_id, NotiCode.ACTUATOR_STATUS)

            for device in devices:
                if "value" in device and "status" in device:
                    value = float(device["value"])
                    status = device["status"]
                    device_id = device["farmos_device_id"]
                    notice.setcontent(
                        device_id,
                        {
                            "status": status,
                            "opid": 0,
                            **NoticesBuilder.get_actuator_status(device),
                        },
                    )
            return notice

        global logger

        return tuple(
            get_notice(node_id, devices)
            for node_id, devices in itertools.groupby(
                value_statuses, lambda x: x["farmos_node_id"]
            )
        )


class ETRIMate(DSMate):
    def __init__(self, option, devinfo: DevInfo, coupleid, quelist):
        super(ETRIMate, self).__init__(option, devinfo, coupleid, quelist)
        global logger
        logger = self._logger
        self.etri_db_connector = ETRIDBConnector(option["db"]["source"])
        # ETRIDBConnector({"db":"pocatello", "username":"pocatello", "password":"Jinong0930#", "host":"localhost"})
        self.etri_db_reader = ETRIDBReader(self.etri_db_connector)
        zone_id = option["info"]["zone_id"]
        device_reader = ControllerDeviceReader(self.etri_db_reader, zone_id)
        etri_device_info = device_reader.read()
        self.device_map = IDMapper.map(etri_device_info, self._devinfo._devinfo)
        self.farmos_db_connector = FarmosDBConnector(option["db"]["target"])
        self.farmos_db_writer = FarmosDBWriter(self.farmos_db_connector)

        logger.debug("unmatched type")

        logger.debug(
            pprint.pformat(
                tuple(
                    (
                        data["farmos_device_id"],
                        data["tag"],
                        data["type"],
                        data["farmos_node_id"],
                    )
                    for data in self.device_map
                    if (data["farmos_data_type"] == "sen" and data["type"] == "A")
                    or (data["farmos_data_type"] == "act" and data["type"] == "S")
                )
            )
        )

    def connect(self):
        super(ETRIMate, self).connect()

    def close(self):
        super(ETRIMate, self).close()

    def matestart(self):
        super(ETRIMate, self).matestart()
        self.connect()

    def matestop(self):
        super(ETRIMate, self).matestop()

    def check_error(self):
        reader = AlarmReader(self.etri_db_reader)
        result = reader.read()
        return tuple(
            {"content_error": row["error"], "controller_id": device["error"]}
            for row in result
            for device in self.device_map
            if device["controller_id"] == row["controller_id"]
            and device["zone_id"] == row["zone_id"]
        )

    def processnotices(self):
        if not self._timetocheck(Mate.NOTITYPE):
            return

        self._updatetime(Mate.NOTITYPE)

        global logger

        collector = DeviceDataCollector(self.etri_db_reader, self.device_map)
        device_data = collector.collect()
        device_data = tuple(
            data for data in device_data if data["farmos_data_type"] == "act"
        )

        notices = NoticesBuilder.build(device_data)
        for notice in notices:
            self._writenoti(notice)
            time.sleep(2)  # for updating query

    def processobservations(self):
        if not self._timetocheck(Mate.OBSTYPE):
            return

        self._updatetime(Mate.OBSTYPE)
        global logger

        collector = DeviceDataCollector(self.etri_db_reader, self.device_map)
        device_data = collector.collect()

        device_data = tuple(
            data for data in device_data if data["farmos_data_type"] == "sen"
        )

        observations = ObservationsBuilder.build(device_data)

        for observation in observations:
            self._writeobs(observation)
            time.sleep(2)  # for updating query


def test():
    devinfo = [
        {
            "dt": "gw",
            "id": "123123",
            "dk": "1",
            "children": [
                {
                    "id": "50",
                    "dt": "nd",
                    "dk": "50",
                    "children": [
                        {"id": "100", "dt": "sen", "dk": "0"},
                        {"id": "101", "dt": "sen", "dk": "2"},
                        {"id": "102", "dt": "sen", "dk": "1"},
                        {"id": "103", "dt": "act", "dk": "100"},
                        {"id": "104", "dt": "act", "dk": "101"},
                        {"id": "100", "dt": "act", "dk": "121"},
                        {"id": "100", "dt": "act", "dk": "121"},
                    ],
                }
            ],
        },
        {
            "dt": "gw",
            "id": "123123",
            "dk": "2",
            "children": [
                {
                    "id": "60",
                    "dt": "nd",
                    "dk": "50",
                    "children": [{"id": "100", "dt": "sen", "dk": "0"}],
                }
            ],
        },
        {
            "dt": "gw",
            "id": "123123",
            "dk": "3",
            "children": [
                {
                    "id": "50",
                    "dt": "nd",
                    "dk": "50",
                    "children": [{"id": "100", "dt": "sen", "dk": "0"}],
                }
            ],
        },
    ]
    etri_db_connector = ETRIDBConnector(
        {
            "db": "pocatello",
            "username": "pocatello",
            "password": "Jinong0930#",
            "host": "localhost",
        }
    )
    etri_db_reader = ETRIDBReader(etri_db_connector)
    device_reader = ControllerDeviceReader(etri_db_reader)
    etri_device_info = device_reader.read()
    devices = IDMapper.map(etri_device_info, devinfo)
    converter = ETRIDBtoObservation(etri_db_reader, devices)
    data = converter.get_observations()
    return data


def translate_data_map():
    def translate_device(device):
        dt = "sen" if device["type"] == "S" else "act"
        return {"dk": device["device_id"], "dt": dt, "opt": {"tag": device["tag"]}}

    def translate_devices(devices):
        return [translate_device(d) for d in devices]

    def node_group(devices):
        devices = sorted(devices, key=lambda x: x["controller_node_id"])
        return [
            {"dk": k, "dt": "nd", "children": translate_devices(list(v))}
            for k, v in itertools.groupby(
                devices, key=lambda x: x["controller_node_id"]
            )
        ]

    def gateway_group(devices):
        devices = sorted(devices, key=lambda x: x["controller_id"])
        return [
            {"dk": str(k), "dt": "gw", "children": node_group(list(v))}
            for k, v in itertools.groupby(devices, key=lambda x: x["controller_id"])
        ]

    etri_db_connector = ETRIDBConnector(
        {
            "db": "pocatello",
            "username": "pocatello",
            "password": "Jinong0930#",
            "host": "localhost",
        }
    )
    etri_db_reader = ETRIDBReader(etri_db_connector)
    device_reader = ControllerDeviceReader(etri_db_reader)
    devices = device_reader.read()
    # controller_id -> type:gw
    # controller_node_id -> type:nd
    # zone -> type:couple
    # device_id -> type S or A

    devices = sorted(devices, key=lambda x: x["zone_id"])
    return tuple(
        {"key": str(k), "dt": "couple", "children": gateway_group(list(v))}
        for k, v in itertools.groupby(devices, key=lambda x: x["zone_id"])
    )


def test_insert_data(data):
    connector = FarmosDBConnector(
        {
            "db": "farmos",
            "username": "farmos",
            "password": "farmosv2@",
            "host": "127.0.0.1",
        }
    )
    writer = FarmosDBWriter(connector)
    observation_writer = ObservationWriter(writer)
    current_observation_writer = CurrentObservationWriter(writer)
    # data = {'data_id': 1, 'obs_time': datetime.datetime(2021, 8, 20, 13, 57, 8, tzinfo=datetime.timezone(datetime.timedelta(seconds=32400))), 'nvalue': Decimal('69.25')}
    results = tuple(
        observation_writer.write(
            data_id=d["data_id"], obs_time=d["obs_time"], nvalue=d["nvalue"]
        )
        for d in data
    )
    [print(r) for r in results]

    results = tuple(
        current_observation_writer.write(
            data_id=d["data_id"], obs_time=d["obs_time"], nvalue=d["nvalue"]
        )
        for d in data
    )
    [print(r) for r in results]


if __name__ == "__main__":
    data = test()
    test_insert_data(data)
    # res = translate_data_map()
    # res = json.dumps(res)
    # print(res)
