#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
from typing import Dict
from lib.mate import Logger
from lib.dsmate.mate_jnmqtt import JNMQTTMate
from lib.dsmate.mate_forwarder import MqttForwardMate
import sys
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import requests
import hashlib
import traceback
import pprint
import http.client
import requests

from collections import deque, defaultdict
from .. import *

logger: Logger = None


class SensorDataGetter:
    def __init__(self, host, mac_address, start_day, end_day, id_token) -> None:
        self.host = host
        self.end_point = (
            "/sensor-data/simple/" + mac_address + "/" + start_day + "/" + end_day
        )
        self.header = {"Content-Type": "application/json", "Authorization": id_token}

    def get(self):
        connection = http.client.HTTPConnection(self.host)
        connection.request(method="GET", url=self.end_point, headers=self.header)
        response = connection.getresponse()
        try:
            resp_data = response.read()
            return resp_data
        except:
            global logger
            error = traceback.format_exc()
            logger.debug(error)
            return {}


class TokenGetter:
    def __init__(self, host, username, password, x_api_key) -> None:
        self.host = host
        self.end_point = "/auth/signin"
        self.header = {"x-api-key": x_api_key}
        self.data = {"username": username, "password": password}

    def get(self):
        connection = http.client.HTTPConnection(self.host)
        connection.request(
            method="GET", url=self.end_point, body=self.data, headers=self.header
        )
        response = connection.getresponse()
        try:
            resp_data = response.read()
            return resp_data
        except:
            global logger
            error = traceback.format_exc()
            logger.debug(error)
            return {}


class ObservationGenerator:
    def __init__(self, pairs, message, node_id):
        """
        example :
            pairs = {"RT_root_loadcell": "1",
                     "RT_soil_WC": "3"}

        Args:
            pairs ([type]): device_id와 key 튜플 쌍
            message ([type]): [description]
        """
        self.pairs = pairs
        self.message = message
        self.node_id = node_id

    def get_observations(self):
        observation_list = []
        for element in self.message:
            obs = Observation(self.node_id)
            issuedAt = datetime.datetime.element["issuedAt"]
            time = datetime.datetime.fromtimestamp(issuedAt)
            obs.settime(time)
            sensors: dict = element["sensors"]
            sensor_map = {}
            for key, val in sensors.items():
                device_id = self.pairs[key]
                sensor_map[id] = val
                obs.setobservation(device_id, val, StatCode.READY)
            observation_list.append(obs)
        return observation_list


class IDMappingGenerator:
    @staticmethod
    def generate(devinfo: DevInfo):
        gateway = devinfo.getgw(0)
        node = gateway["children"][0]
        pairs = {}
        for device in node["children"]:
            id = device["id"]
            dk = device["dk"]
            pairs[dk] = id
        return pairs


class IoCropsMate(DSMate):
    def __init__(self, option: Dict, devinfo: DevInfo, coupleid, quelist):
        super(IoCropsMate, self).__init__(option, devinfo, coupleid, quelist)
        self.pairs = IDMappingGenerator.generate(devinfo)
        conn = option["conn"]
        self.username = conn["username"]
        self.password = conn["password"]
        self.x_api_key = conn["x-api-key"]
        self.host = conn["host"]
        self.mac_address = conn["mac_address"]
        self.token_getter = TokenGetter(
            self.host, self.username, self.password, self.x_api_key
        )
        gateway = devinfo.getgw(0)
        node = gateway["children"][0]
        self.node_id = node["id"]

    def connect(self):
        super(IoCropsMate, self).connect()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        data = self.token_getter.get()
        id_token = data["id_token"]
        sensor_data_getter = SensorDataGetter(
            self.host, self.mac_address, 0, 0, id_token
        )

        sensor_message = sensor_data_getter.get()

        obs_generator = ObservationGenerator(self.pairs, sensor_message, self.node_id)
        obs_list = obs_generator.get_observations()

        for observation in obs_list:
            self._writeobs(observation)


if __name__ == "__main__":

    devinfo = [
        {
            "id": "1",
            "dk": "1",
            "dt": "gw",
            "couple_id": "????",
            "children": [
                {
                    "id": "2",
                    "dk": "1",
                    "dt": "nd",
                    "children": [
                        {"id": "4", "dk": "RT_root_loadcell", "dt": "sen"},
                        {"id": "5", "dk": "RT_soil_WC", "dt": "sen"},
                        {"id": "6", "dk": "RT_soil_EC", "dt": "sen"},
                        {"id": "7", "dk": "RT_soil_temp", "dt": "sen"},
                        {"id": "8", "dk": "RT_soil_dp", "dt": "sen"},
                        {"id": "9", "dk": "RT_soil_bulk_EC", "dt": "sen"},
                        {"id": "10", "dk": "RT_water_out", "dt": "sen"},
                        {"id": "11", "dk": "RT_drain_rate", "dt": "sen"},
                        {"id": "12", "dk": "RT_temp", "dt": "sen"},
                        {"id": "13", "dk": "RT_hum", "dt": "sen"},
                        {"id": "14", "dk": "RT_CO2", "dt": "sen"},
                        {"id": "15", "dk": "RT_radiation", "dt": "sen"},
                    ],
                }
            ],
        },
    ]
    from multiprocessing import Queue

    option = {
        "conn": {
            "host": "apis.iocrops.com",
            "username": "username",
            "password": "password",
            "x-api-key": "VPdFsgipvx5vrmhRcq84Q7xmPq1aCx9pa5J0XLfy",
        }
    }

    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = IoCropsMate(option, devinfo, "23", quelist)
    dsmate = MqttForwardMate(option, devinfo, "23", quelist)
    ssmate.start()
    dsmate.start()

    time.sleep(50)
    for q in quelist[1:]:
        q.close()
        q.join_thread()

    dsmate.stop()
    ssmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

    print("local tested.")
