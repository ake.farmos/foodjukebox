#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc. All right reserved.
#
# GREENCS API
#


from collections import defaultdict
import requests
import json
import time
import traceback
from datetime import datetime
from decimal import *

from .. import *


class GREENCSMate(DSMate):
    def processnotices(self):
        if self._timetocheck(Mate.NOTITYPE) is False:
            return

        self._updatetime(Mate.NOTITYPE)
        processor = NodeProcessor(self, self._devinfo._devinfo)
        processor.process()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)
        processor = ObsProcessor(self, self._devinfo._devinfo)
        processor.process()


class Translator:
    @staticmethod
    def translate_status(value):
        status_list = defaultdict(
            lambda: StatCode.READY,
            {
                "기동": StatCode.WORKING,
                "정지": StatCode.READY,
                "자동": StatCode.WORKING,
                "수동": StatCode.WORKING,
            },
        )
        return status_list[value]

    @staticmethod
    def translate_value(value):

        try:
            if value.isdigit():
                return int(value)
            else:
                return float(value)
        except:
            non_numeric_map = {
                "기동": 201,
                "정지": 0,
                "자동": 201,
                "수동": 0,
                "비안옴": 0,
                "비옴": 1,
                "정상": 0,
                "정전": 1,
                "북": 0,
                "우": 90,
                "남": 180,
                "좌": 270,
            }
            if value in non_numeric_map:
                return non_numeric_map[value]
            return 0


class NodeProcessor:
    def process_device(self, id, value, notice_block: Notice):
        return notice_block.setcontent(
            id, {"opid": 0, "status": Translator.translate_status(value)}
        )

    def __init__(self, mate: GREENCSMate, devinfo: list) -> None:
        self.mate = mate
        self.devinfo = devinfo

    def process_node(self, node, results):
        notice_block = Notice(node["id"], NotiCode.ACTUATOR_STATUS)
        notice_block.settime(results["XDATETIME"][0:-1])
        devices = node["children"]
        values = tuple(
            (device["id"], value)
            for device in devices
            for key, value in results.items()
            if key == device["dk"] and device["dt"] == "act"
        )
        for id, value in values:
            self.process_device(id, value, notice_block)

        self.mate._writenoti(notice_block)

    def process_gateway(self, gateway):
        result = requests.get(gateway["url"])
        result = result.json()["fields"][0]
        node = gateway["children"][0]

        self.process_node(node, result)

    def process(self):
        try:
            [
                self.process_gateway({**gateway, **api})
                for api in self.mate._option["apis"]
                for gateway in self.mate._devinfo._devinfo
                if gateway["dk"] == api["dk"]
            ]
        except:
            error = traceback.format_exc()
            print(error)


class ObsProcessor:
    def process_device(self, id, value, observation_block):
        return observation_block.setobservation(
            id, [Translator.translate_value(value)], Translator.translate_status(value)
        )

    def __init__(self, mate: GREENCSMate, devinfo: list) -> None:
        self.mate = mate
        self.devinfo = devinfo

    def process_node(self, node, results):
        observation_block = Observation(node["id"])
        observation_block.settime(results["XDATETIME"][0:-1])
        devices = node["children"]
        values = tuple(
            (device["id"], value)
            for device in devices
            for key, value in results.items()
            if key == device["dk"] and device["dt"] == "sen"
        )

        for id, value in values:
            self.process_device(id, value, observation_block)

        self.mate._writeobs(observation_block)

    def process_gateway(self, gateway):
        result = requests.get(gateway["url"])
        result = result.json()["fields"][0]
        node = gateway["children"][0]

        self.process_node(node, result)

    def process(self):
        try:
            [
                self.process_gateway({**gateway, **api})
                for api in self.mate._option["apis"]
                for gateway in self.mate._devinfo._devinfo
                if gateway["dk"] == api["dk"]
            ]
        except:
            error = traceback.format_exc()
            print(error)


if __name__ == "__main__":
    from multiprocessing import Queue

    option = {
        "apis": [
            {
                "dk": "1",
                "url": "http://api.gcsmagma.com/gcs_fb_api.php/Get_GCS_Data/jaresb4/1",
            },
            {
                "dk": "2",
                "url": "http://api.gcsmagma.com/gcs_fb_api.php/Get_GCS_Data/jaresb4/2",
            },
        ]
    }

    devinfo = [
        {
            "id": "1",
            "dk": "1",
            "dt": "gw",
            "children": [
                {
                    "id": "2",
                    "dk": "1",
                    "dt": "nd",
                    "children": [
                        {"id": "3", "dk": "XINTEMP", "dt": "sen"},
                        {"id": "4", "dk": "XINTEMP1", "dt": "sen"},
                        {"id": "5", "dk": "XINTEMP2", "dt": "sen"},
                        {"id": "6", "dk": "XINTEMP3", "dt": "sen"},
                        {"id": "7", "dk": "XINHUM", "dt": "sen"},
                        {"id": "8", "dk": "XINHUM1", "dt": "sen"},
                        {"id": "9", "dk": "XINHUM2", "dt": "sen"},
                        {"id": "10", "dk": "XINHUM3", "dt": "sen"},
                        {"id": "11", "dk": "XGNDTEMP", "dt": "sen"},
                        {"id": "12", "dk": "XGNDHUM", "dt": "sen"},
                        {"id": "13", "dk": "XWATERTEMP", "dt": "sen"},
                        {"id": "14", "dk": "XDHUM", "dt": "sen"},
                        {"id": "15", "dk": "XCO2", "dt": "sen"},
                        {"id": "16", "dk": "XINHUM2", "dt": "sen"},
                        {"id": "17", "dk": "XOUTTEMP", "dt": "sen"},
                        {"id": "18", "dk": "XWINDDIREC", "dt": "sen"},
                        {"id": "19", "dk": "XWATERTEMP", "dt": "sen"},
                        {"id": "20", "dk": "XWINDSP", "dt": "sen"},
                        {"id": "21", "dk": "XSUNVOL", "dt": "sen"},
                        {"id": "22", "dk": "XSUNADD", "dt": "sen"},
                        {"id": "23", "dk": "XRAIN", "dt": "sen"},
                        {"id": "24", "dk": "XVENTTEMP", "dt": "sen"},
                        {"id": "25", "dk": "XHEATTEMP", "dt": "sen"},
                        {"id": "26", "dk": "XSTHUM", "dt": "sen"},
                        {"id": "27", "dk": "XABHUM", "dt": "sen"},
                        {"id": "28", "dk": "XHUMLACK", "dt": "sen"},
                        {"id": "29", "dk": "XVENTTEMP2", "dt": "sen"},
                        {"id": "30", "dk": "XCO2SET", "dt": "sen"},
                        {"id": "31", "dk": "XVENTCONT", "dt": "sen"},
                        {"id": "32", "dk": "XHEATCONT", "dt": "sen"},
                        {"id": "33", "dk": "XSKYVOL1", "dt": "sen"},
                        {"id": "34", "dk": "XSKYVOL2", "dt": "sen"},
                        {"id": "35", "dk": "XDUALVOL1", "dt": "sen"},
                        {"id": "36", "dk": "XDUALVOL2", "dt": "sen"},
                        {"id": "37", "dk": "XSIDEVOL1", "dt": "sen"},
                        {"id": "38", "dk": "XSIDEVOL2", "dt": "sen"},
                        {"id": "39", "dk": "XCUR1VOL", "dt": "sen"},
                        {"id": "40", "dk": "XCUR2VOL", "dt": "sen"},
                        {"id": "41", "dk": "XCUR3VOL", "dt": "sen"},
                        {"id": "42", "dk": "XCUR4VOL", "dt": "sen"},
                        {"id": "43", "dk": "X3WAY1VOL", "dt": "sen"},
                        {"id": "44", "dk": "X3WAY2VOL", "dt": "sen"},
                        {"id": "45", "dk": "XSPRAYRUN", "dt": "act"},
                        {"id": "46", "dk": "XCO2RUN", "dt": "act"},
                        {"id": "47", "dk": "XSPRUN", "dt": "act"},
                        {"id": "48", "dk": "XHEATERRUN", "dt": "act"},
                        {"id": "49", "dk": "XLIGHTRUN", "dt": "act"},
                        {"id": "50", "dk": "XHUNRUN", "dt": "act"},
                        {"id": "51", "dk": "XBORUN", "dt": "act"},
                        {"id": "52", "dk": "XPUMPRUN", "dt": "act"},
                        {"id": "53", "dk": "XFAN1RUN", "dt": "act"},
                        {"id": "54", "dk": "XFAN2RUN", "dt": "act"},
                        {"id": "55", "dk": "XVENTCONT2", "dt": "act"},
                        {"id": "56", "dk": "XVENTRST", "dt": "act"},
                        {"id": "57", "dk": "XVENT2RST", "dt": "act"},
                        {"id": "58", "dk": "XHEATRST", "dt": "act"},
                        {"id": "59", "dk": "XWATERTEMP2", "dt": "act"},
                        {"id": "60", "dk": "XJUNGJUN", "dt": "act"},
                        {"id": "61", "dk": "XSKYAUTO", "dt": "act"},
                        {"id": "62", "dk": "XDUALAUTO", "dt": "act"},
                        {"id": "63", "dk": "XSIDEAUTO", "dt": "act"},
                        {"id": "64", "dk": "XCUR1AUTO", "dt": "act"},
                        {"id": "65", "dk": "XCUR2AUTO", "dt": "act"},
                        {"id": "66", "dk": "XCUR3AUTO", "dt": "act"},
                        {"id": "67", "dk": "XCUR4AUTO", "dt": "act"},
                        {"id": "68", "dk": "X3WAY1AUTO", "dt": "act"},
                        {"id": "69", "dk": "X3WAY2AUTO", "dt": "act"},
                        {"id": "70", "dk": "XSPRAYAUTO", "dt": "act"},
                        {"id": "71", "dk": "XCO2AUTO", "dt": "act"},
                        {"id": "72", "dk": "XSPAUTO", "dt": "act"},
                        {"id": "73", "dk": "XHEATERAUTO", "dt": "act"},
                        {"id": "74", "dk": "XLIGHTAUTO", "dt": "act"},
                        {"id": "75", "dk": "XHUNAUTO", "dt": "act"},
                        {"id": "76", "dk": "XBOAUTO", "dt": "act"},
                        {"id": "77", "dk": "XPUMPAUTO", "dt": "act"},
                        {"id": "78", "dk": "XFAN1AUTO", "dt": "act"},
                        {"id": "79", "dk": "XFAN2AUTO", "dt": "act"},
                        {"id": "80", "dk": "XEC1", "dt": "sen"},
                        {"id": "81", "dk": "XEC2", "dt": "sen"},
                        {"id": "82", "dk": "XPH2", "dt": "act"},
                    ],
                }
            ],
        },
        {
            "id": "101",
            "dk": "2",
            "dt": "gw",
            "children": [
                {
                    "id": "102",
                    "dk": "2",
                    "dt": "nd",
                    "children": [
                        {"id": "103", "dk": "XINTEMP", "dt": "sen"},
                        {"id": "104", "dk": "XINTEMP1", "dt": "sen"},
                        {"id": "105", "dk": "XINTEMP2", "dt": "sen"},
                        {"id": "106", "dk": "XINTEMP3", "dt": "sen"},
                        {"id": "107", "dk": "XINHUM", "dt": "sen"},
                        {"id": "108", "dk": "XINHUM1", "dt": "sen"},
                        {"id": "109", "dk": "XINHUM2", "dt": "sen"},
                        {"id": "110", "dk": "XINHUM3", "dt": "sen"},
                        {"id": "111", "dk": "XGNDTEMP", "dt": "sen"},
                        {"id": "112", "dk": "XGNDHUM", "dt": "sen"},
                        {"id": "113", "dk": "XWATERTEMP", "dt": "sen"},
                        {"id": "114", "dk": "XDHUM", "dt": "sen"},
                        {"id": "115", "dk": "XCO2", "dt": "sen"},
                        {"id": "116", "dk": "XINHUM2", "dt": "sen"},
                        {"id": "117", "dk": "XOUTTEMP", "dt": "sen"},
                        {"id": "118", "dk": "XWINDDIREC", "dt": "sen"},
                        {"id": "119", "dk": "XWATERTEMP", "dt": "sen"},
                        {"id": "120", "dk": "XWINDSP", "dt": "sen"},
                        {"id": "121", "dk": "XSUNVOL", "dt": "sen"},
                        {"id": "122", "dk": "XSUNADD", "dt": "sen"},
                        {"id": "123", "dk": "XRAIN", "dt": "sen"},
                        {"id": "124", "dk": "XVENTTEMP", "dt": "sen"},
                        {"id": "125", "dk": "XHEATTEMP", "dt": "sen"},
                        {"id": "126", "dk": "XSTHUM", "dt": "sen"},
                        {"id": "127", "dk": "XABHUM", "dt": "sen"},
                        {"id": "128", "dk": "XHUMLACK", "dt": "sen"},
                        {"id": "129", "dk": "XVENTTEMP2", "dt": "sen"},
                        {"id": "130", "dk": "XCO2SET", "dt": "sen"},
                        {"id": "131", "dk": "XVENTCONT", "dt": "sen"},
                        {"id": "132", "dk": "XHEATCONT", "dt": "sen"},
                        {"id": "133", "dk": "XSKYVOL1", "dt": "sen"},
                        {"id": "134", "dk": "XSKYVOL2", "dt": "sen"},
                        {"id": "135", "dk": "XDUALVOL1", "dt": "sen"},
                        {"id": "136", "dk": "XDUALVOL2", "dt": "sen"},
                        {"id": "137", "dk": "XSIDEVOL1", "dt": "sen"},
                        {"id": "138", "dk": "XSIDEVOL2", "dt": "sen"},
                        {"id": "139", "dk": "XCUR1VOL", "dt": "sen"},
                        {"id": "140", "dk": "XCUR2VOL", "dt": "sen"},
                        {"id": "141", "dk": "XCUR3VOL", "dt": "sen"},
                        {"id": "142", "dk": "XCUR4VOL", "dt": "sen"},
                        {"id": "143", "dk": "X3WAY1VOL", "dt": "sen"},
                        {"id": "144", "dk": "X3WAY2VOL", "dt": "sen"},
                        {"id": "145", "dk": "XSPRAYRUN", "dt": "act"},
                        {"id": "146", "dk": "XCO2RUN", "dt": "act"},
                        {"id": "147", "dk": "XSPRUN", "dt": "act"},
                        {"id": "148", "dk": "XHEATERRUN", "dt": "act"},
                        {"id": "149", "dk": "XLIGHTRUN", "dt": "act"},
                        {"id": "150", "dk": "XHUNRUN", "dt": "act"},
                        {"id": "151", "dk": "XBORUN", "dt": "act"},
                        {"id": "152", "dk": "XPUMPRUN", "dt": "act"},
                        {"id": "153", "dk": "XFAN1RUN", "dt": "act"},
                        {"id": "154", "dk": "XFAN2RUN", "dt": "act"},
                        {"id": "155", "dk": "XVENTCONT2", "dt": "act"},
                        {"id": "156", "dk": "XVENTRST", "dt": "act"},
                        {"id": "157", "dk": "XVENT2RST", "dt": "act"},
                        {"id": "158", "dk": "XHEATRST", "dt": "act"},
                        {"id": "159", "dk": "XWATERTEMP2", "dt": "act"},
                        {"id": "160", "dk": "XJUNGJUN", "dt": "act"},
                        {"id": "161", "dk": "XLOGON", "dt": "act"},
                        {"id": "161", "dk": "XSKYAUTO", "dt": "act"},
                        {"id": "162", "dk": "XDUALAUTO", "dt": "act"},
                        {"id": "163", "dk": "XSIDEAUTO", "dt": "act"},
                        {"id": "164", "dk": "XCUR1AUTO", "dt": "act"},
                        {"id": "165", "dk": "XCUR2AUTO", "dt": "act"},
                        {"id": "166", "dk": "XCUR3AUTO", "dt": "act"},
                        {"id": "167", "dk": "XCUR4AUTO", "dt": "act"},
                        {"id": "168", "dk": "X3WAY1AUTO", "dt": "act"},
                        {"id": "169", "dk": "X3WAY2AUTO", "dt": "act"},
                        {"id": "170", "dk": "XSPRAYAUTO", "dt": "act"},
                        {"id": "171", "dk": "XCO2AUTO", "dt": "act"},
                        {"id": "172", "dk": "XSPAUTO", "dt": "act"},
                        {"id": "173", "dk": "XHEATERAUTO", "dt": "act"},
                        {"id": "174", "dk": "XLIGHTAUTO", "dt": "act"},
                        {"id": "175", "dk": "XHUNAUTO", "dt": "act"},
                        {"id": "176", "dk": "XBOAUTO", "dt": "act"},
                        {"id": "177", "dk": "XPUMPAUTO", "dt": "act"},
                        {"id": "178", "dk": "XFAN1AUTO", "dt": "act"},
                        {"id": "179", "dk": "XFAN2AUTO", "dt": "act"},
                        {"id": "180", "dk": "XEC1", "dt": "act"},
                        {"id": "181", "dk": "XEC2", "dt": "act"},
                        {"id": "182", "dk": "XPH2", "dt": "act"},
                    ],
                }
            ],
        },
    ]

    quelist = [Queue(), Queue(), Queue(), Queue()]
    mate = GREENCSMate(option, devinfo, "1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(300)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print("")
    except:
        pass
    quelist[0].close()
