#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc. All right reserved.
#
# Online AWS
#


import requests
import json
import time

from .. import *

'''
option : {
    "urlkey" :
            "apikey" : "4bcf94a2318ec5d5fded39cbf22be946",
            "city" : "seoul"}
}

devinfo = [{
    "id" : "1", "dk" : "", "dt": "gw", "children" : [{
        "id" : "101", "dk" : '', "dt": "nd", "children" : [
            {"id" : "102", "dk" : 'temp', "dt": "sen"}
        ]
    }]
}]

'''

class ONLINEAWSMate(DSMate):
    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        copt = self._option["urlkey"]
        api = "http://api.openweathermap.org/data/2.5/weather?q={city}&APPID={key}"
        url = api.format(city = copt["city"], key = copt["apikey"])
        r = requests.get(url)
        data = json.loads(r.text)
        k2c = lambda k: k - 273.15

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        for dev in nd["children"] :
            if "temp" in dev["dk"]:
                obs = k2c(data["main"][dev["dk"]])
            elif "wind" in dev["dk"]:
                obs = data["wind"]["speed"]
            elif "clouds" in dev["dk"]:
                obs = data["clouds"]["all"]
            else :
                obs = data["main"][dev["dk"]]

            print("obs", dev["dk"], obs)
            obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)

        self._writeobs(obsblk)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "urlkey" : {
            "apikey" : "4bcf94a2318ec5d5fded39cbf22be946",
            "city" : "seoul"}
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "humidity", "dt": "sen"},
                {"id" : "4", "dk" : "temp", "dt": "sen"},
                {"id" : "5", "dk" : "pressure", "dt": "sen"},
                {"id" : "6", "dk" : "wind", "dt": "sen"},
                {"id" : "7", "dk" : "temp_min", "dt": "sen"},
                {"id" : "8", "dk" : "clouds", "dt": "sen"},

            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = ONLINEAWSMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
