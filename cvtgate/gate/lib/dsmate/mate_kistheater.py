#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import struct
import time
import socket
import select
import traceback
import hashlib
import json
from enum import IntEnum
from threading import Thread, Lock
from pymodbus.client.sync import ModbusSerialClient
from pymodbus.client.sync import ModbusTcpClient

from .. import *

class KISTHeaterMate(DSMate):
    _SLEEP = 0.1

    def __init__(self, option, devinfo, coupleid, quelist):
        super(KISTHeaterMate, self).__init__(option, devinfo, coupleid, quelist)
        self._conn = {}
        self._lock = Lock()
        self._opids = {}
        for gw in self._devinfo:
            for nd in gw["children"]:
                for dev in nd["children"]:
                    if DevType.issensor(dev["dt"]) == False:
                        self._opids[dev["id"]] = 0

        self._logger.info("KISTHeaterMate Started.")

    def getdk(self, dev, idx):
        dk = json.loads(dev["dk"])
        return dk[idx]

    def readregister(self, conn, addr, count, unit):
        time.sleep(KISTHeaterMate._SLEEP)
        print("read register", unit, addr, count)
        with self._lock:
            try:
                return conn.read_holding_registers(addr, count, unit=unit)
            except Exception as ex:
                self._logger.warn("fail to read holding registers. : " + str(ex))
                return None

    def connectone(self, opt):
        ret = False
        conn = None

        if opt['method'] == 'rtu':
            conn =  ModbusSerialClient(method='rtu', port=opt['port'],
                    timeout=opt['timeout'], baudrate=opt['baudrate'])
            ret = conn.connect()
            msg = "failed to connect with rtu"
            code = NotiCode.RTU_CONNECTED if ret else NotiCode.RTU_FAILED_CONNECTION
        else:
            msg = "It's a wrong connection method. " + str(opt['method'])

        if ret == False:
            self._logger.warn(msg)
            noti = Notice(None, NotiCode.RTU_FAILED_CONNECTION) # detection is canceled
        else:
            noti = Notice(None, NotiCode.RTU_CONNECTED) # detection is canceled

        self._writenoti(noti)
        return conn

    def connect(self):
        print("connect : ", self._connected)
        for opt in self._option['conn']:
            conn = self.connectone(opt)
            if conn:
                self._conn[opt["name"]] = conn

        super(KISTHeaterMate, self).connect()

    def closeone(self, port):
        self._conn[port].close()

    def close(self):
        for port in list(self._conn.keys()):
            self.closeone(port)
        super(KISTHeaterMate, self).close()

    def process(self):
        self.processrequests()
        self.processobservations()
        self.processnotices()

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE):
            for gw in self._devinfo:
                for nd in gw["children"]:
                    self.sendobservation(self.readsensornodeinfo(nd))
            self._updatetime(Mate.OBSTYPE)

    def processnotices(self):
        # not use : ACTNOTITYPE
        if self._timetocheck(Mate.NOTITYPE):
            for gw in self._devinfo:
                for nd in gw["children"]:
                    self.sendnotice(self.readactnodeinfo(nd))
            self._updatetime(Mate.NOTITYPE)

    def processactrequest(self, dev, request, node):
        gw = self._devinfo.findgateway(request.getnodeid())
        unit = self.getdk(dev, 0)
        operation = request.getcommand()
        params = request.getparams()
        registers = [params["ratio"]]

        print("....... befor lock for write")
        with self._lock:
            time.sleep(KISTHeaterMate._SLEEP)
            print("....... lock for write", self.getdk(dev, 3), registers)
            res = self._conn[gw["dk"]].write_registers(self.getdk(dev, 3), registers, unit=unit)

        if res.isError():
            self._logger.warn("Fail to write a request to dev." + str(dev) + "," + str(res) + ":" + str(request))
            return ResCode.FAIL_TO_WRITE

        self._opids[dev["id"]] = request.getopid()

        msg = self.readactnodeinfo(node)
        if msg is None:
            self._logger.warn("Fail to read dev status.")
        else:
            self.sendnotice(msg)
        return ResCode.OK

    def processrequest(self, req):
        """ 하나의 요청을 처리한다. """
        print("received request", req.getdevid(), self._coupleid)
        if BlkType.isrequest(req.gettype()) is False:
            self._logger.warn("The message is not request. " + str(req.gettype()))
            return False

        response = Response(req)
        cmd = req.getcommand()
        nd = self._devinfo.finddevbyid(req.getnodeid())
        dev = self._devinfo.finddevbyid(req.getdevid())

        if dev is None:
            self._logger.warn("There is no device. " + str(req.getdevid()))
            code = ResCode.FAIL_NO_DEVICE

        elif DevType.ispropercommand(dev['dt'], cmd) is False:
            self._logger.warn("The request is not proper. " + str(cmd) + " " + str(dev['dt']))
            code = ResCode.FAIL_NOT_PROPER_COMMAND

        elif DevType.isactuator(dev['dt']) or DevType.isnode(dev['dt']):
            # modbus
            code = self.processactrequest(dev, req, nd)
            self._logger.info("Actuator processed : " + str(code))

        elif DevType.isgateway(dev['dt']):
            self._logger.info("Gateway does not receive a request")
            code = ResCode.FAIL

        else:
            self._logger.warn("Unknown Error. " + str(req) + ", " + str(dev))
            code = ResCode.FAIL

        response.setresult(code)
        self._logger.info("write response: " + str(response))
        self._writeres(response)
        return True #if code == ResCode.OK else False

    def readinfofromdev(self, conn, dev):
        res = self.readregister(conn, self.getdk(dev, 1), 1, self.getdk(dev, 0))

        if res is None:
            self._logger.warn("fail to get status from " + str(dev['dk']))
        elif res.isError():
            self._logger.warn("error at reading register from " + str(dev['dk']) + " " + str(res))
        else:
            return res.registers 
        return None

    def readinfofromactuator(self, conn, act):
        reg = self.readinfofromdev(conn, act)
        if reg:
            return {"opid" : self._opids[act["id"]], "ratio" : reg[0], "status" : StatCode.READY.value}
        else:
            return {"opid" : self._opids[act["id"]], "ratio" : None, "status" : StatCode.ERROR.value}

    def readinfofromsensor(self, conn, sen):
        reg = self.readinfofromdev(conn, sen)
        if reg:
            return {"status" : StatCode.READY.value, "value" : reg[0] / 10.0}
        else:
            return {"status" : StatCode.ERROR.value, "value" : None}

    def readsensornodeinfo(self, node):    
        ret = {"id" : node["id"], "sen" : {}, "act" : {}, "nd" : {"status":StatCode.READY.value}}
        gw = self._devinfo.findgateway(node["id"])
        conn = self._conn[gw["dk"]]
        ret["conn"] = conn

        for dev in node['children']:
            if DevType.issensor(dev["dt"]):
                info = self.readinfofromsensor(ret["conn"], dev)
                if info:
                    ret["sen"][dev["id"]] = info
                else:
                    self._logger.warn("fail to read sensor info : " + str(dev) + " however continue to read other device")
        return ret

    def readactnodeinfo(self, node):    
        ret = {"id" : node["id"], "sen" : {}, "act" : {}, "nd" : {"status":StatCode.READY.value}}
        gw = self._devinfo.findgateway(node["id"])
        conn = self._conn[gw["dk"]]
        ret["conn"] = conn

        for dev in node['children']:
            if DevType.issensor(dev["dt"]) == False:
                info = self.readinfofromactuator(ret["conn"], dev)
                if info:
                    ret["act"][dev["id"]] = info
                else:
                    self._logger.warn("fail to read actuator info : " + str(dev) + " however continue to read other device")
        return ret

    def sendobservation(self, ndinfo):
        if ndinfo is None:
            return 

        if StatCode.has_value(ndinfo["nd"]["status"]) == False:
            ndinfo["nd"]["status"] = StatCode.ERROR.value

        obsblk = Observation(ndinfo["id"])
        obsblk.setobservation(ndinfo["id"], 0, StatCode(ndinfo["nd"]["status"]))

        # do not send observation for actuator
        for devid, info in ndinfo["sen"].items():
            if StatCode.has_value(info["status"]) == False:
                info["status"] = StatCode.ERROR.value
            obsblk.setobservation(devid, self.getvalue(devid, info["value"]), StatCode(info["status"]))

        self._writeobs(obsblk)

    def sendnotice(self, ndinfo):
        if ndinfo is None:
            return

        blk = Notice(ndinfo["id"], NotiCode.ACTUATOR_STATUS, ndinfo["id"], ndinfo["nd"])
        for devid, info in ndinfo["act"].items():
            blk.setcontent(devid, info)
        self._writenoti(blk)

if __name__ == "__main__":
    from multiprocessing import Queue

    isnutri = False
    quelist = [Queue(), Queue(), Queue(), Queue()]
    opt = {
        'conn' : [{
            'method': 'rtu',
            'name': 'USB0',
            'port' : '/dev/ttyUSB0',
            'baudrate' : 9600,
            'timeout': 5
        }]
    }
    
    devinfo = [{
        "id" : "1", "dk" : "USB0", "dt": "gw", "children" : [{
            "id" : "10", "dk" : '', "dt": "nd", "children" : [
               {"id" : "11", "dk" : '[1,0,["value"]]', "dt": "sen"},
               {"id" : "12", "dk" : '[1,1,["value"]]', "dt": "sen"},
               {"id" : "13", "dk" : '[1,2,["value"]]', "dt": "sen"},
               {"id" : "21", "dk" : '[1,10,["ratio"],10,["ratio"]]', "dt": "act/switch/level104"},
               {"id" : "22", "dk" : '[1,11,["ratio"],11,["ratio"]]', "dt": "act/switch/level104"},
               {"id" : "23", "dk" : '[1,12,["ratio"],12,["ratio"]]', "dt": "act/switch/level104"}
            ]
        }]
    }]

    khmate = KISTHeaterMate(opt, devinfo, "1", quelist)
    ssmate = SSMate ({}, [], "1", quelist)

    ssmate.start ()
    khmate.start ()
    print("mate started")

    time.sleep(5)
    req = Request(10)
    req.setcommand(21, CmdCode.DIRECTIONAL_ON, {'ratio':10})
    print("=======================================#1")
    ssmate._writereq(req)
    print("=======================================#1")

    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    khmate.stop()
    ssmate.stop()
    print("mate stopped") 
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

