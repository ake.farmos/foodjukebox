#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc. All right reserved.
#
# HAMAN API
#


import requests
import json
import time

from .. import *

'''
option : {
    "urlkey" :
            "apikey" : "http://phri.iptime.org:13800/apis/com.campbellsci.cr1000/aws",
            }
}

devinfo = [{
    "id" : "1", "dk" : "", "dt": "gw", "children" : [{
        "id" : "101", "dk" : '', "dt": "nd", "children" : [
            {"id" : "102", "dk" : 'temp', "dt": "sen"}
        ]
    }]
}]

'''

class HAMANAPIMate(DSMate):
    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        copt = self._option["urlkey"]
        url = copt["apikey"]
        r = requests.get(url)
        data = json.loads(r.text)

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        try:
            for dev in nd["children"] :
                obs = data["payload"][dev["dk"]]

                print("obs", dev["dk"], obs)
                obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)
        except:
            pass

        self._writeobs(obsblk)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "urlkey" : {
             "apikey" : "http://phri.iptime.org:13800/apis/com.campbellsci.cr1000/aws",
            }
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "DV", "dt": "sen"},
                {"id" : "4", "dk" : "DT", "dt": "sen"},
                {"id" : "5", "dk" : "OAT", "dt": "sen"},
                {"id" : "6", "dk" : "ORH", "dt": "sen"},
                {"id" : "7", "dk" : "OWS", "dt": "sen"},
                {"id" : "8", "dk" : "OGS", "dt": "sen"},
                {"id" : "9", "dk" : "OWD", "dt": "sen"},
                {"id" : "10", "dk" : "OSR", "dt": "sen"},
                {"id" : "11", "dk" : "OAP", "dt": "sen"},
                {"id" : "12", "dk" : "OSM", "dt": "sen"},
                {"id" : "13", "dk" : "OCD", "dt": "sen"},
                {"id" : "14", "dk" : "ORP", "dt": "sen"}


            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = HAMANAPIMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
