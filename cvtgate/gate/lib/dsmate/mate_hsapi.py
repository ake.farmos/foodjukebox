#!/usr/bin/env python -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc. All right reserved.
#
# HS API
#


import requests
import json
import time
from datetime import datetime
from .. import *

'''
    option = {
        "urlkey" : {
            "farmID" : "173"
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "tp", "dt": "sen"},
                {"id" : "4", "dk" : "hd", "dt": "sen"},
                {"id" : "5", "dk" : "wd", "dt": "sen"},
                {"id" : "6", "dk" : "ws", "dt": "sen"},
                {"id" : "7", "dk" : "rain", "dt": "sen"},
                {"id" : "8", "dk" : "solar", "dt": "sen"},

            ]
        }]
    }]

'''

class HSAPIMate(DSMate):
    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE) is False:
            return

        self._updatetime(Mate.OBSTYPE)

        copt = self._option["urlkey"]
        today = datetime.today().strftime('%Y-%m-%d')
        api = "http://182.162.27.101:8080/getWetherMntListApi.json?frlndInnb={farmID}&mesureDt={today}"
        url = api.format(farmID = copt["farmID"], today = today)

        r = requests.get(url)
        data = json.loads(r.text)

        lastdata = data["result"][-1]
#        print lastdata

        gw = self._devinfo.getgw()
        nd = gw["children"][0]
        obsblk = Observation(nd["id"])

        for dev in nd["children"] :
            obs = lastdata[dev["dk"]]

            print("obs", dev["dk"], obs)
            obsblk.setobservation(dev["id"], self.getvalue(dev["id"], obs), StatCode.READY)

        self._writeobs(obsblk)

if __name__ == "__main__":
    from multiprocessing import Queue
    option = {
        "urlkey" : {
            "farmID" : "173"
        }
    }

    devinfo = [{
        "id" : "1", "dk" : '', "dt": "gw", "children" : [{
            "id" : "2", "dk" : '', "dt": "nd", "children" : [
                {"id" : "3", "dk" : "tp", "dt": "sen"},
                {"id" : "4", "dk" : "hd", "dt": "sen"},
                {"id" : "5", "dk" : "wd", "dt": "sen"},
                {"id" : "6", "dk" : "ws", "dt": "sen"},
                {"id" : "7", "dk" : "rain", "dt": "sen"},
                {"id" : "8", "dk" : "solrad", "dt": "sen"},

            ]
        }]
    }]

    quelist = [Queue(), Queue(), Queue(), Queue()] 
    mate = HSAPIMate(option,devinfo,"1", quelist)
    mate2 = SSMate(option, devinfo, "1", quelist)
    mate.start()
    mate2.start()
    print("mate started")
    time.sleep(10)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    mate.stop()
    mate2.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()
