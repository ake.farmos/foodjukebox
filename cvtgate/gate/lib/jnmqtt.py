#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import json
import time
import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

from .mblock import MBlock, BlkType, Observation, Notice, NotiCode

'''
option : {
    "conn" : {"host" : "dev.jinong.co.kr", "port" : 1883, "keepalive" : 60},
    "mqtt" : {"svc" : "cvtgate", "id" : "1"},
    "area" : "local"
}

devinfo : [
    {"id" : "2", "dk" : "1", "dt": "gw", "children" : [
      {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
        {"id" : "4", "dk" : "0", "dt": "sen"},
        {"id" : "5", "dk" : "1", "dt": "act"}
      ]}
    ]}
]
'''

class _JNMQTT:
    _REQ = "req"
    _RES = "res"
    _OBS = "obs"
    _NOTI = "noti"
    _SELF = "self"
    _SVR = "svr"
    _STAT = "stat"

    # as a notice
    _STAT_ON = '{"nid":null, "type":500, "content":{"code": 605, "status": 0}, "exkey":null, "extra":null}'
    _STAT_OFF = '{"nid":null, "type":500, "content":{"code": 605, "status": 91}, "exkey":null, "extra":null}'

class MQTTFilter:
    # 현재 필터는 notallow 만 지원한다.
    # Notice 의 경우에는 NotiCode 도 추가할 수 있다.
    """
    option = {"mqttfilter" : {
        "notallow": ["OBSERVATION", "NOTICE/ACTUATOR_STATUS"]
    }}
    """

    def __init__(self, option, logger):
        self._logger = logger
        self._filter = None
        if "mqttfilter" in option:
            self.setup(option["mqttfilter"])

    def setup(self, option):
        # 현재 필터는 notallow 만 지원한다.
        if "notallow" in option:
            self._filter = {}
            for key in option["notallow"]:
                items = key.split("/")
                newitems = [None, None]
                try:
                   msgtype = BlkType[items[0]]
                   if msgtype not in self._filter:
                       newitems[0] = msgtype
                except:
                   self._logger.warning("MQTT Filter option is not proper : " + key + ". It would be ignored.") 
                   continue

                if len(items) == 2 and msgtype == BlkType.NOTICE:
                   try:
                       ncode = NotiCode[items[1]]
                       newitems[1] = ncode
                   except:
                       self._logger.warning("MQTT Filter NotiCode is not proper. : " + key + ". It would be ignored.") 
                       newitems = None
                       continue

                if newitems[0] not in self._filter:
                    self._filter[newitems[0]] = {}

                if newitems[1] is not None:
                    self._filter[newitems[0]][newitems[1]] = None


    def filter(self, msg):
        if self._filter and msg.gettype() in self._filter:
            if len(self._filter[msg.gettype()]) == 0:
                #self._logger.info("A message is filtered.")
                return None
            else:
                # should be notice
                if msg.getcode() in self._filter[msg.gettype()]:
                    #self._logger.info("A Notice is filtered.")
                    return None
        return msg

class FakeJNMQTT:
    def __init__(self, option, devinfo, logger):
        self._isexecuting = False
        self._isconnected = False
        pass

    def start(self, onmsgcb = None):
        self._isexecuting = True

    def connect(self, ischecksvr):
        self._isconnected = True
        return True

    def close(self):
        pass

    def stop(self):
        self._isexecuting = False

    def writeblk(self, blk):
        return True

    def getpropermsg(self, blk):
        return blk

    def onmsg(self, client, obj, blk):
        pass

    def onclose(self, client, udata, sock):
        self._isconnected = False

class SimpleJNMQTT:
    def __init__(self, option, logger):
        self._client = None
        self._option = option
        self._logger = logger
        self._auth = None
        self._svc = self._option["mqtt"]["svc"]
        self._isexecuting = False
        self._isconnected = False
        self._newmsgcb = None
        self._filter = MQTTFilter(option, logger)

    def start(self, onmsgcb = None):
        self._isexecuting = True
        self._newmsgcb = onmsgcb

    def connect(self):
        """
            기존에는 될때까지 반복하는 방법이었으나, 
            안되는 경우 자칫하면 루프에 빠질 수 있어
            1회만 시도하는 것으로 함.
            다만, 메세지 전송시도시 연결이 안되어있다면 지속적으로 접속을 시도하는 것으로 변경함.
        """
        try:
            self._client = mqtt.Client()
            self._client.loop(.1)
            if self._newmsgcb:
                self._client.on_message = self._newmsgcb
            else:
                self._client.on_message = self.onmsg

            self._client.on_socket_close = self.onclose
            self._client.on_disconnect = self.onclose

            if self._auth:
                self._client.username_pw_set(self._auth["username"], self._auth["password"])

            if "conn" in self._option:
                conn = self._option["conn"]
            else:
                conn = self._option["mqtt"]

            self._client.connect(conn["host"], conn["port"], conn["keepalive"])
            if "ids" in self._option["mqtt"]:
                for idstr in self._option["mqtt"]["ids"]:
                    self._client.subscribe(self._svc + "/" + idstr + "/#" , 2)
            else:
                self._client.subscribe(self._svc + "/#" , 2)
            self._client.loop_start()
            self._isconnected = True
        except Exception as ex:
            self._logger.warn("fail to connect mqttserver : " + str(ex))
            self._isconnected = False
        return self._isconnected

    def close(self):
        pass

    def stop(self):
        self._isexecuting = False
        self._client.loop_stop()

    def onclose(self, client, udata, sock):
        #self._logger.warn("Disconnected with mqtt server.")
        print("Disconnected with mqtt server.")
        self._isconnected = False

    def checkconnection(self):
        if self._isconnected is False:
            # 접속 시도
            self.connect()
            if self._isconnected is False:
                # 접속 시도 했으나 실패라면 전송 실패로 기록
                self._logger.warn("fail to connect!")

    def getpropermsg(self, blk):
        tmp = blk.topic.split('/')
        msg = MBlock.load(blk.payload)
        if msg is None:
            self._logger.info("Fail to parse a message. " + str(blk.payload))
            return msg
            
        if tmp[3] == _JNMQTT._REQ and BlkType.isrequest(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._RES and BlkType.isresponse(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._NOTI and BlkType.isnotice(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._OBS and BlkType.isobservation(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._STAT and BlkType.isnotice(msg.gettype()):
            return msg

        self._logger.info("Topic is not matched. Check [3]. " + str(tmp))
        return None

    def writeblk(self, blk, dev):
        if self._filter.filter(blk) is None:
            print("**jnmqtt writeblk", msg.stringify(), "filtered")
            return False

        idx = 3
        lst = [self._svc, str(dev[2] if dev[2] else ''), str(dev[3] if dev[3] else ''), "", str(blk.getnodeid())]

        if BlkType.isobservation(blk.gettype()):
            lst[idx] = _JNMQTT._OBS
            qos = 1
        elif BlkType.isnotice(blk.gettype()):
            lst[idx] = _JNMQTT._NOTI
            qos = 1
        elif BlkType.isrequest(blk.gettype()):
            lst[idx] = _JNMQTT._REQ
            qos = 2
        elif BlkType.isresponse(blk.gettype()):
            lst[idx] = _JNMQTT._RES
            qos = 2
        else:
            # error log
            return False

        value = blk.stringify()
        try:
            print("**jnmqtt writeblk", "/".join(lst), value)
            publish.single("/".join(lst), payload=value, qos=qos, hostname=self._option["conn"]["host"], port=self._option["conn"]["port"], auth=self._auth)
            ret = True
        except Exception as ex:
            self._logger.warn("publish exception : " + str(ex) + " " + "/".join(lst) + "  " + str(value))
            ret = False
        return ret

    def onmsg(self, client, obj, blk):
        self._logger.info("Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos))
        try:
            tmp = blk.topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                if blk.payload == _JNMQTT._STAT_ON:
                    self._logger.info("The server is OK now." + tmp)
                else:
                    self._logger.warn("The server might have some problems." + tmp)

            msg = self.getpropermsg(blk)
            if msg:
                self._logger.info("proper message : " + blk.payload)
            else:
                self._logger.warn("wrong message!!")
        except Exception as ex:
            self._logger.warn("fail to call onmsg: " + str(ex) + " "  + blk.payload)

class JNMQTT:
    def __init__(self, option, devinfo, logger):
        self._client = None
        self._option = option
        self._devinfo = devinfo
        self._logger = logger
        assert self._devinfo.isok(), "Device Information is not ok."
        #self._topic = self._option["mqtt"]["svc"] + "/" + self._devinfo[0]["id"]
        self._svc = self._option["mqtt"]["svc"] + "/" + self._option["mqtt"]["id"]
        self._auth = None
        if "auth" in self._option["mqtt"]:
            self._auth = self._option["mqtt"]["auth"]
        self._ischecksvr = False
        self._isexecuting = False
        self._isconnected = False
        self._newmsgcb = None
        self._filter = MQTTFilter(option, logger)

    def start(self, onmsgcb = None):
        self._isexecuting = True
        self._newmsgcb = onmsgcb

    def connect(self, ischecksvr):
        """
            기존에는 될때까지 반복하는 방법이었으나, 
            안되는 경우 자칫하면 루프에 빠질 수 있어
            1회만 시도하는 것으로 함.
            다만, 메세지 전송시도시 연결이 안되어있다면 지속적으로 접속을 시도하는 것으로 변경함.
        """
        try:
            self._client = mqtt.Client()
            self._client.loop(.1)
            if self._newmsgcb:
                self._client.on_message = self._newmsgcb
            else:
                self._client.on_message = self.onmsg
            self._client.on_socket_close = self.onclose
            self._client.on_disconnect = self.onclose
            if self._auth:
                self._client.username_pw_set(self._auth["username"], self._auth["password"])

            conn = self._option["conn"]
            self._ischecksvr = ischecksvr

            self._client.will_set("/".join([self._svc, _JNMQTT._SELF, _JNMQTT._STAT]), _JNMQTT._STAT_OFF, 0, False)
            self._client.connect(conn["host"], conn["port"], conn["keepalive"])

            self._client.publish("/".join([self._svc, _JNMQTT._SELF, _JNMQTT._STAT]), _JNMQTT._STAT_ON)

            self._client.subscribe("/".join([self._svc, _JNMQTT._SELF, _JNMQTT._REQ]), 2) # qos = 2
            if ischecksvr:
                self._client.subscribe("/".join([self._svc, _JNMQTT._SVR, _JNMQTT._STAT]), 2) # qos = 2

            self.nodesubscribe(_JNMQTT._REQ, 2)

            self._client.loop_start()
            self._isconnected = True
        except Exception as ex:
            self._logger.warn("fail to connect mqttserver : " + str(ex))
            self._isconnected = False
        return self._isconnected

    def close(self):
        pass

    def stop(self):
        self._isexecuting = False
        self._client.publish("/".join([self._svc, _JNMQTT._SELF, _JNMQTT._STAT]), _JNMQTT._STAT_OFF)
        self._client.loop_stop()

    def writeblk(self, blk):
        if self._filter.filter(blk) is None:
            print("**jnmqtt writeblk", msg.stringify(), "filtered")
            return False

        value = blk.stringify()
        inter = _JNMQTT._SELF if blk.getnodeid() is None else ""
        node = "" if blk.getnodeid() is None else str(blk.getnodeid())

        if blk.getnodeid() is None:
            gwid = blk.getextra("gwid")
            if gwid is None:
                idx = 2
                lst = [self._svc, _JNMQTT._SELF, ""]
            else:
                idx = 3
                lst = [self._svc, str(gwid), _JNMQTT._SELF, ""]
        else:
            idx = 2
            gwid = self._devinfo.findgateway(blk.getnodeid())["id"]
            lst = [self._svc, str(gwid), "", str(blk.getnodeid())]

        if BlkType.isobservation(blk.gettype()):
            lst[idx] = _JNMQTT._OBS
            qos = 0
        elif BlkType.isnotice(blk.gettype()):
            lst[idx] = _JNMQTT._NOTI
            qos = 1
        elif BlkType.isrequest(blk.gettype()):
            lst[idx] = _JNMQTT._REQ
            qos = 2
        elif BlkType.isresponse(blk.gettype()):
            lst[idx] = _JNMQTT._RES
            qos = 2
        else:
            # error log
            return False

        if self._isconnected is False:
            # 접속 시도
            self.connect(self._ischecksvr)
            if self._isconnected is False:
                # 접속 시도 했으나 실패라면 전송 실패로 기록
                self._logger.warn("fail to publish : " + "/".join(lst) + "  " + str(value))
                return False
        try:
            print("**jnmqtt writeblk", "/".join(lst), value)
            publish.single("/".join(lst), payload=value, qos=qos, hostname=self._option["conn"]["host"], auth=self._auth)
            ret = True
        except Exception as ex:
            self._logger.warn("publish exception : " + str(ex) + " " + "/".join(lst) + "  " + str(value))
            ret = False
        return ret

    def nodesubscribe(self, tstr, qos):
        lst = [self._svc, "", tstr, ""]
        for root in self._devinfo:
            assert root["dt"] == "gw" and "children" in root and "id" in root
            lst[1] = root["id"]
            for nd in root['children']:
                lst[3] = nd["id"]
                self._client.subscribe("/".join(lst), qos) # qos = 2
                self._logger.info("subscribe : " + "/".join(lst))

    def getpropermsg(self, blk):
        tmp = blk.topic.split('/')
        msg = MBlock.load(blk.payload)
        if msg is None:
            self._logger.info("Fail to parse a message. " + str(blk.payload))
            return msg
            
        if tmp[3] == _JNMQTT._REQ and BlkType.isrequest(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._RES and BlkType.isresponse(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._NOTI and BlkType.isnotice(msg.gettype()):
            return msg
        if tmp[3] == _JNMQTT._OBS and BlkType.isobservation(msg.gettype()):
            return msg

        self._logger.info("Topic is not matched. Check [3]. " + str(tmp))
        return None

    def onmsg(self, client, obj, blk):
        self._logger.info("Received mblock '" + str(blk.payload) + "' on topic '"
              + blk.topic + "' with QoS " + str(blk.qos))
        try:
            tmp = blk.topic.split('/')

            if _JNMQTT._SVR == tmp[2] and _JNMQTT._STAT == tmp[3]:
                if blk.payload == _JNMQTT._STAT_ON:
                    self._logger.info("The server is OK now.")
                else:
                    self._logger.warn("The server might have some problems.")

            msg = self.getpropermsg(blk)
            if msg:
                self._logger.info("proper message : " + blk.payload)
            else:
                self._logger.warn("wrong message!!")
        except Exception as ex:
            self._logger.warn("fail to call onmsg: " + str(ex) + " "  + blk.payload)

    def onclose(self, client, udata, sock):
        #self._logger.warn("Disconnected with mqtt server.")
        print("Disconnected with mqtt server.")
        self._isconnected = False

if __name__ == "__main__":
    import logging

    option = {"mqttfilter" : {
        "notallow": ["OBSERVATION", "NOTICE/ACTUATOR_STATUS"]
    }}
    ft = MQTTFilter(option, logging.getLogger(''))
    obs = Observation(10)
    noti1 = Notice(10, NotiCode.ACTUATOR_STATUS)
    noti2 = Notice(10, NotiCode.EXTERNAL_DATA)

    print("Observation should be filtered.", ft.filter(obs))
    print("Notice/ACTUATOR_STATUS should be filtered.", ft.filter(noti1))
    print("Notice/EXTERANL_DATA should not be filtered.", ft.filter(noti2))


