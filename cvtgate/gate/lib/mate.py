#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

"""
    Base Mate를 정의함.
"""

import abc
# compatible with Python 2 *and* 3:
ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()})

import time
import traceback
import random
import collections
import base64
import requests
import json
import os, signal
import queue
import cProfile
import paramiko

from multiprocessing import Process
from collections import deque
from enum import Enum
from datetime import datetime, date, timedelta

from .devtype import DevType
from .dinfo import DevInfo
from .calibration import Calibrator
from .mblock import MBlock, BlkType, Observation, Request, Response, Notice, CmdCode, StatCode, ResCode

class Logger:
    def __init__(self, logq):
        self._logq = logq
        self._msgq = deque()

    def send(self, msg):
        #print (msg)
        self._msgq.append(msg)
        try:
            while True:
                old = self._msgq.popleft()
                self._logq.put(old)
        except IndexError as ex:
            pass
        except queue.Full as ex:
            self._msgq.appendleft(old)

    def metric(self, key, label, value):
        self.send(["M", key, label, value])

    def debug(self, msg):
        self.send(["L", "debug", msg])

    def info(self, msg):
        self.send(["L", "info", msg])

    def warn(self, msg):
        self.send(["L", "warn", msg])

    def error(self, msg):
        self.send(["L", "error", msg])

    def crit(self, msg):
        self.send(["L", "critical", msg])

    def heartbeat(self, pid):
        self.send(["H", pid, time.time()])

class Mate(ABC):
    """
    Mate의 기본형을 정의함.
    """
    NOTITYPE = "noti"
    ACTNOTITYPE = "actnoti"
    OBSTYPE = "obs"
    RELOADTYPE = "reload"
    IMGTYPE = "image"

    def __init__(self, option, devinfo, coupleid, quelist):
        """
        Mate 의 Constructor. option과 devinfo를 주요 입력으로 함.
        
        :param option: 작동을 위한 설정을 딕셔너리로 전달함
        :param devinfo: 처리하는 장비의 아이디를 딕셔너리 형식으로 전달함. 다음과 같은 형식임.
        id 는 장비의 아이디, dk 는 장비를 확인하기 위한 키값, dt는 장비의 타입, children은 하위 장비가 있는 경우에 하위 장비를 표현하기 위한 용도임.
        devinfo : [
            {"id" : "3", "dk" : "1", "dt": "nd", "children" : [
                {"id" : "4", "dk" : "0", "dt": "sen"},
                {"id" : "5", "dk" : "1", "dt": "sen"},
                {"id" : "6", "dk" : "2", "dt": "act"},
                {"id" : "7", "dk" : "3", "dt": "act/retractable/level0"}
            ]}
        ]
        :param coupleid: 커플아이디.
        :param quelist: 메이트가 사용할 4개의 큐 (0.log,metric, 1.request, 2.response, 3.observation,notice)
        """
        self._option = option
        print(type(self).__name__, "mate initialized. ", option)

        self._coupleid = coupleid
        self._sleep = {"time": 0.5, "obs": 50, "noti": 50, "actnoti": 2, "reload": 300} 
        if "sleep" in option:
            self._sleep.update(option["sleep"])
        self._devinfo = DevInfo(devinfo)
        self._logger = Logger(quelist[0])
        self._reqq = quelist[1]
        self._resq = quelist[2]
        self._obsq = quelist[3]
        self._p = None
        self._connected = False
        self._dayofyear = datetime.now().timetuple().tm_yday
        self._last = {"obs" : 0, "noti" : 0, "actnoti" : 0, "reload" : 0}

        if "backup" in option and "prefix" in option["backup"]:
            self._backup = True
        else:
            self._backup = False

        self._calibrator = Calibrator(option, self._logger)

    def __repr__(self):
        return "{}({},{})".format(self.__class__.__name__, str(self._option), str(self._devinfo))

    def test(self):
        fp = open("/tmp/" + str(os.getpid()) + ".log", "a")
        fp.write(str(time.time()) + "\n")
        fp.close()

    def start(self):
        """ Mate가 시작할때 호출됨 """
        self._p = Process(target=self.run)
        self._p.start()
        self._logger.info(str(self._p.pid) + " is started as a " + self.__class__.__name__)
        return True

    def stop(self):
        """ Mate가 중지될때 호출됨 """
        pid = str(self._p.pid)
        self._logger.info("sent signal to " + pid)
        self._p.terminate()
        self._logger.info(pid + " would be stopped.")

        for _ in range(5):
            if self.selfcheck() is False:
                break
            time.sleep(1)

        if self.selfcheck():
            self._logger.info("The process still remains.")
            os.kill(self._p.pid, signal.SIGKILL)

        self._p.join()
        self._logger.info(pid + " was joined.")
        self._p = None
        return True

    def selfcheck(self):
        try:
            os.kill(self._p.pid, 0)
        except OSError:
            return False
        else:
            return True

    def getpid(self):
        if self._p is None:
            return 0
        return self._p.pid

    def connect(self):
        """ Mate 를 장비 혹은 서버와 연결 시킴 """
        self._connected = True
        return True

    def close(self):
        """ Mate 를 장비 혹은 서버와 연결 해제 시킴 """
        #print(traceback.format_exc())
        self._connected = False

    def isconnected(self):
        """ Mate가 연결되어 있는지를 확인함 """
        return self._connected

    def getvalue(self, k, v):
        """ 
        센서값을 계산할때 사용함
        Calibrator를 사용하며, [converted, raw] 를 돌려줌
        설정이 없는 경우 [raw, raw] 값을 돌려줌
        """
        return [self._calibrator.calculate(k, v), v]

    def isexecuting(self):
        """ Mate가 작동중인지를 확인함 """
        return self._p is not None
        
    def _timetocheck(self, opt):
        if self._sleep[opt] == 0:
            return False
        return time.time() - self._last[opt] > self._sleep[opt]

    def _updatetime(self, opt):
        self._last[opt] = time.time()

    def process(self):
        """ Mate가 대상장비 혹은 서버와 통신을 수행하고 작업을 수행하느 함수
        DS라면 장비에서 읽어오고, SS라면 서버에서 읽어오고.. 경우에 따라 안쓸수도 있음.  """
        pass

    def backup(self, blk):
        """ 메세지 블럭을 백업함 """
        try:
            fname = self._option["backup"]["prefix"] + "-" + datetime.now().strftime("%Y%m%d") + ".bak"
            with open(fname, "a") as fp:
                fp.write(blk.stringify() + "\n")
        except Exception as ex:
            self._logger.warn("fail to backup : " + blk.stringify())

    def upload(self, ftype, fname, extra = None):
        rets = []

        if "URL" in self._option["upload"]:
            rets.append(self.uploadhttp(self._option["upload"]["URL"], ftype, fname, extra))
        elif "URLs" in self._option["upload"]:
            for url in self._option["upload"]["URLs"]:
                rets.append(self.uploadhttp(url, ftype, fname, extra))

        if "SFTP" in self._option["upload"]:
            opt = self._option["upload"]["SFTP"]
            rets.append(self.uploadsftp(opt, ftype, fname, extra))
        elif "SFTPs" in self._option["upload"]:
            for opt in self._option["upload"]["SFTPs"]:
                rets.append(self.uploadsftp(opt, ftype, fname, extra))

        return all(rets)

    def uploadhttp(self, url, ftype, fname, extra = None):
        """ HTTP로 파일을 업로드 함. 백업파일 업로드 용이지만, 그외 사진 파일 같은 다른 파일 업로드도 가능. """
        try:
            fp = open(fname, "rb")
            fdata = fp.read()
            encoded = base64.b64encode(fdata).decode('utf8')
            fp.close()
            headers = {'Content-Type': 'application/json; charset=utf-8'}
            data = {"type" : ftype, "data": encoded, "filename": fname, "meta" : extra}
            res = requests.post(url, headers=headers, data=json.dumps(data))
            if res.status_code == 200:
                return True
            self._logger.warn("fail to upload. " + fname + " : " + str(res.status_code) + " " + str(res.reason))
            return False
        except Exception as ex:
            self._logger.warn("fail to upload. " + fname + " : " + str(ex))
            return False

    def uploadsftp(self, opt, ftype, fname, extra = None):
        """ SFTP로 파일을 업로드 함. 주로 사진 업로드용 """
        try:
            if "key" in opt:
                sftp_key = paramiko.RSAKey.from_private_key_file(opt["key"])
                transport = paramiko.Transport((opt["host"], opt["port"]))
                transport.start_client(event=None, timeout=15)
                transport.get_remote_server_key()
                transport.auth_publickey(opt["user"], sftp_key, event=None)
            else:
                transport = paramiko.Transport(opt["host"], opt["port"])
                transport.connect(username = opt["user"], password = opt["passwd"])

            sftp = paramiko.SFTPClient.from_transport(transport)
            if extra is not None and 'newfname' in extra:
                newfname = opt["path"] + "/" + extra["newfname"]
            else:
                newfname = opt["path"] + "/" + fname
            ret = sftp.put(fname, newfname)
            sftp.close()
            transport.close()
            return True
        except Exception as ex:
            self._logger.warn("fail to upload. " + fname + " : " + str(ex))
            return False

    def uploadbackup(self):
        """ 백업된 파일을 업로드 함 """
        if "backup" not in self._option:
            return 

        dayofyear = datetime.now().timetuple().tm_yday
        if "upload" in self._option["backup"] and self._option["backup"]["upload"] is True and self._dayofyear != dayofyear:
            yesterday = (date.today() - timedelta(days=1)).strftime("%Y%m%d")
            fname = self._option["backup"]["prefix"] + "-" + yesterday + ".bak"
            if self.uploadhttp(Mate.OBSTYPE, fname) is True:
                self._dayofyear = dayofyear
        
    def _writeblk(self, blk, que):
        """ 메세지를 전송한다. 이 함수를 써야 백업이 제대로 이루어진다. """
        # backup
        if self._backup:
            self.backup(blk)
        try:
            que.put(blk)
        except queue.Full:
            self._logger.warn("fail to put message into queue : " + blk.stringify())

    def matestart(self):
        self._p = True
        signal.signal(signal.SIGINT, self.matesignal)
        signal.signal(signal.SIGTERM, self.matesignal)

    def matesignal(self, signum, frame):
        #self._logger.info("mate receive signal : " + str(signum))
        print(str(os.getpid()) + " mate receive signal : " + str(signum))
        self._p = None

    def matestop(self):
        pass

    def run(self):
        self.matestart()
        self._logger.info(type(self).__name__ + " mate run ... sleep : " + str(self._sleep["time"]))
        if self.isconnected() == False:
            self.connect()
        while self.isexecuting():
            try:
                while self.isexecuting() == True and self.isconnected() == False:
                    self._logger.warn("It (" + self.__class__.__name__ + ") lost a connection. : " + str(self.isconnected()))
                    if self.connect() == False:
                        self._logger.info("sleep 10 seconds and try to connect")
                        time.sleep(10)
                    else:
                        self._logger.info("reconnected!!")

                if self.isexecuting() == False:
                    break
                time.sleep(self._sleep["time"])
                self.process()
                self.uploadbackup()
                #print("heartbeat", type(self).__name__, datetime.now().strftime("%H:%M:%S"))
                self._logger.heartbeat(os.getpid())

            except Exception as ex:
                self._logger.warn("There is an exception : " + str(ex))
                self._logger.warn(traceback.format_exc())
                try:
                    self.close()
                except:
                    pass
        self.matestop()
        self._logger.info("mate stop - " + str(self._coupleid) + " : " + str(os.getpid()))

class DSMate(Mate):
    """ DSMate의 기본형임. 상속할때 _로 시작하는 메소드는 수정하지 않은 것으로 함. """
    def _readreq(self):
        try:
            req = self._reqq.get(False)
        except queue.Empty:
            return None
        except Exception as ex:
            self._logger.warn(str(ex))
            return None
        return req

    def _writeobs(self, obs):
        """ DSMate 에서 사용. 관측치를 SSMate로 전송하는 데 사용됨 """
        self._writeblk(obs, self._obsq)

    def _writenoti(self, noti):
        """ DSMate 에서 사용. 노티를 SSMate로 전송하는 데 사용됨 """
        self._writeblk(noti, self._obsq)

    def _writeres(self, res):
        """ DSMate 에서 사용. 응답을 SSMate로 전송하는 데 사용됨 """
        self._writeblk(res, self._resq)

    def process(self):
        """ 실제 처리를 수행하는 메소드로 꼭 있어야 함. """
        self.processrequests()
        self.processobservations()
        self.processnotices()
        self.doextra()
        self.reload()

    # 추가적인 일이 필요할때 사용한다.
    def doextra(self):
        pass

    def processrequest(self, req):
        pass

    def processrequests(self):
        while True:
            req = self._readreq()
            if req is None:
                break
            self.processrequest(req)

    def processobservations(self):
        if self._timetocheck(Mate.OBSTYPE):
            self._updatetime(Mate.OBSTYPE)
        pass

    def processnotices(self):
        # 명령을 처리하는 경우 정기 전송과 수시 전송으로 나누어, 명령처리 중일때 빠르게 체크할 수 있음.
        if self._timetocheck(Mate.NOTITYPE):
            self._updatetime(Mate.NOTITYPE)
        pass

    def reload(self):
        if self._timetocheck(Mate.RELOADTYPE):
            self._updatetime(Mate.RELOADTYPE)
        pass

class TestDSMate(DSMate):
    def __init__(self, option, devinfo, coupleid, quelist):
        super(TestDSMate, self).__init__(option, devinfo, coupleid, quelist)
        self.lasttm = 0

    def processobservations(self):
        if time.time() - self.lasttm > 50:
            obsblk = Observation(3)
            obsblk.setobservation(2, self.getvalue(2, random.randint(1,100)), StatCode.READY)
            self._writeobs(obsblk)
            self.lasttm = time.time()

    def processrequests(self):
        while True:
            req = self._readreq()
            if req is None:
                break

            print ("request : " + req.stringify())
            res = Response(req)
            self._writeres(res)

class SSMate(Mate):
    """ SSMate 의 기본형임. 상속할때 _로 시작하는 메소드는 수정하지 않는 것으로 함.
        매트릭 정보에 대한 책임은 SSMate 에서 진다. """
    _REQ_TIMEOUT = 30

    def __init__(self, option, devinfo, coupleid, quelist):
        super(SSMate, self).__init__(option, devinfo, coupleid, quelist)
        self._reqhistory = {}

    def _processoldrequests(self):
        current = time.time()
        for key, arg in self._reqhistory.items():
            if current - arg[0] > SSMate._REQ_TIMEOUT:
                res = Response(arg[1])
                res.setresult(ResCode.FAIL_TIMEOUT)
                self._resq.put(res)

    def _setrequestmetric(self, res):
        """ 요청 처리에 대한 메트릭을 만들어서 전달 """
        key = (res.getopid(), res.getdevid(), res.getcommand())
        print ("set request metric........................", key, self._reqhistory)
        if key in self._reqhistory:
            duration = time.time() - self._reqhistory[key][0]
            self._logger.metric("request_processing_seconds", (res.getcommand()), duration)
            del self._reqhistory[key]
        else:
            self._logger.info("A request is missing. maybe it's too late. " + str(key))

    def _writereq(self, req):
        """ SSMate 에서 사용. 명령을 DSMate로 전송하는 데 사용됨 """
        self._reqhistory[(req.getopid(), req.getdevid(), req.getcommand())] = (time.time(), req)
        print ("write request ........................", self._reqhistory)
        self._writeblk(req, self._reqq)

    def _readres(self):
        try:
            res = self._resq.get(False)
            self._setrequestmetric(res)
        except queue.Empty:
            return None
        except Exception as ex:
            self._logger.warn(str(ex))
            return None
        return res

    def _readobsnoti(self):
        try:
            msg = self._obsq.get(False)
            if BlkType.isnotice(msg.gettype()):
                self._logger.metric("notices_count", None, 1)
            elif BlkType.isobservation(msg.gettype()):
                self._logger.metric("observations_count", None, 1)
            return msg
        except queue.Empty:
            return None
        except Exception as ex:
            self._logger.warn(str(ex))
            return None

    """ 이하의 메소드들은 사용자에 의해서 수정될 수 있다. """

    def process(self):
        self.processresponses()
        self.processobsnotis()
        self.doextra()

        # 비정상적인 Request에 대한 처리 
        self._processoldrequests()
        self.processresponses()

    # 추가적인 일이 필요할때 사용한다.
    def doextra(self):
        pass

    def processresponses(self):
        while True:
            res = self._readres()
            if res is None:
                break
            self.processresponse(res)

    def processresponse(self, res):
        """ 새로 구현해야할 메소드 """
        print ("response: " + res.stringify())

    def obsnotisdone(self):
        pass

    def processobsnotis(self):
        while True:
            msg = self._readobsnoti()
            if msg is None:
                break
            self.processobsnoti(msg)
        self.obsnotisdone()

    def processobsnoti(self, msg):
        """ 새로 구현해야할 메소드 """
        print ("message : " + msg.stringify())

    def run_profile(self):
        self.matestart()
        self._logger.info(type(self).__name__ + " ssmate run ... sleep : " + str(self._sleep["time"]))
        if self.isconnected() == False:
            self.connect()
        pr = cProfile.Profile()
        pr.enable()
        while self.isexecuting():
            try:
                while self.isexecuting() == True and self.isconnected() == False:
                    self._logger.warn("It (" + self.__class__.__name__ + ") lost a connection. : " + str(self.isconnected()))
                    if self.connect() == False:
                        self._logger.info("sleep 10 seconds and try to connect")
                        time.sleep(10)
                    else:
                        self._logger.info("reconnected!!")

                if self.isexecuting() == False:
                    break
                time.sleep(self._sleep["time"])
                self.process()
                self.uploadbackup()
                #print("heartbeat", type(self).__name__, datetime.now().strftime("%H:%M:%S"))
                self._logger.heartbeat(os.getpid())

            except Exception as ex:
                self._logger.warn("There is an exception : " + str(ex))
                self._logger.warn(traceback.format_exc())
                try:
                    self.close()
                except:
                    pass
        self.matestop()
        pr.disable()
        pr.print_stats(sort='cumtime')
        self._logger.info("ssmate stop - " + str(self._coupleid) + " : " + str(os.getpid()))

class TestSSMate(SSMate):
    """ 여기에 있는 함수는 테스트용."""
    def process(self):
        """ for test """
        req = Request(3)
        req.setcommand(33, CmdCode.OFF, None)
        self._writereq(req)


if __name__ == "__main__":
    from multiprocessing import Queue
    opt = {
        "backup" : {"prefix" : "backup/BC0041", "upload": True},
        "upload" : {"URL" : "http://localhost:8081/common/v1/upload"},
        "db" : {"host" : "localhost", "user" : "farmos", "password" : "farmosv2@", "db" : "farmos"},
        "model" : {2 : "DefaultModel"}
    }
    quelist = [Queue(), Queue(), Queue(), Queue()]
    ssmate = TestSSMate({}, [], None, quelist)
    dsmate = TestDSMate(opt, [], None, quelist)
    ssmate.start()
    dsmate.start()
    print("mate started")
    time.sleep(50)
    for q in quelist[1:]:
        q.close()
        q.join_thread()
    ssmate.stop()
    dsmate.stop()
    print("mate stopped")
    try:
        while True:
            print(quelist[0].get(False))
    except:
        pass
    quelist[0].close()

