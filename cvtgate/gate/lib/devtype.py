#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2019 JiNong, Inc.
# All right reserved.
#

import time
from threading import Thread
from enum import Enum

from .calibration import Calibrator
from .mblock import CmdCode

class DevType:
    GATEWAY = "gw"
    NODE    = "nd"
    SENSOR  = "sen"
    ACTUATOR = "act"

    RETRACTABLE = "retractable"
    SWITCH = "switch"
    NUTSUPPLY = "nutrient-supply"

    MISC = "misc"
    DISPLAY = "display"     # FND
    CONTROLLER = "controller"
    TRIGGER = "trigger"
    LEVEL = "level"

    #LVLIST = ["level0", "level1", "level2", "level3"]

    DEFAULT_ACTUATOR = ["act", "switch", "level0"]

    @staticmethod
    def issameclass(devtype, cls):
        if devtype[:len(cls)] == cls:
            return True
        return False

    @staticmethod
    def isgateway(devtype):
        if devtype[:2] == DevType.GATEWAY:
            return True
        return False

    @staticmethod
    def isnode(devtype):
        if devtype[:2] == DevType.NODE:
            return True
        return False

    @staticmethod
    def issensor(devtype):
        if devtype[:3] == DevType.SENSOR:
            return True
        return False

    @staticmethod
    def isactuator(devtype):
        if devtype[:3] == DevType.ACTUATOR:
            return True
        return False

    @staticmethod
    def isswitch(devtype):
        if DevType.isactuator(devtype):
            if DevType.getdevice(devtype) == DevType.SWITCH:
                return True
        return False

    @staticmethod
    def isretractable(devtype):
        if DevType.isactuator(devtype):
            if DevType.getdevice(devtype) == DevType.RETRACTABLE:
                return True
        return False

    @staticmethod
    def isnutsupplier(devtype):
        if DevType.isactuator(devtype):
            if DevType.getdevice(devtype) == DevType.NUTSUPPLY:
                return True
        return False

    @staticmethod
    def isdisplay(devtype):
        if DevType.isactuator(devtype):
            if DevType.getmiscdevice(devtype) == DevType.DISPLAY:
                return True
        return False

    @staticmethod
    def iscontroller(devtype):
        if DevType.isactuator(devtype):
            if DevType.getmiscdevice(devtype) == DevType.CONTROLLER:
                return True
        return False

    @staticmethod
    def istrigger(devtype):
        if DevType.isactuator(devtype):
            if DevType.getmiscdevice(devtype) == DevType.TRIGGER:
                return True
        return False

    @staticmethod
    def getmiscdevice(devtype):
        tmp = devtype.split("/")
        if tmp[1] == DevType.MISC:
            return tmp[2]
        return None

    @staticmethod
    def getdevice(devtype):
        return devtype.split("/")[1]

    @staticmethod
    def getnodelevel(devtype):
        dt = devtype.split("/")
        if len(dt) == 2:
            try:
                #return DevType.LVLIST.index(dt[1])
                return int(dt[1][5:])
            except:
                return -1
        return 0

    @staticmethod
    def getdevlevel(devtype):
        dt = devtype.split("/")
        if len(dt) == 3:
            try:
                #return DevType.LVLIST.index(dt[2])
                return int(dt[2][5:])
            except:
                return -1
        return 1

    @staticmethod
    def ispropercommand(devtype, cmdcode):
        if DevType.isretractable(devtype):
            lv = DevType.getdevlevel(devtype)
            if lv >= 1 and cmdcode in [CmdCode.OFF, CmdCode.OPEN, CmdCode.CLOSE, CmdCode.TIMED_OPEN, CmdCode.TIMED_CLOSE, CmdCode.SET_TIME]:
                return True
            if lv == 2 and cmdcode in [CmdCode.POSITION]:
                return True

        if DevType.isswitch(devtype):
            lv = DevType.getdevlevel(devtype)
            if lv >= 1 and cmdcode in [CmdCode.OFF, CmdCode.ON, CmdCode.TIMED_ON]:
                return True
            if lv >= 2 and cmdcode in [CmdCode.DIRECTIONAL_ON]:
                return True
            if lv == 104 and cmdcode in [CmdCode.DIRECTIONAL_ON]:
                return True

        if DevType.isnutsupplier(devtype):
            lv = DevType.getdevlevel(devtype)
            if lv >= 1 and cmdcode in [CmdCode.OFF, CmdCode.ONCE_WATERING]:
                return True
            if lv >= 2 and cmdcode in [CmdCode.AREA_WATERING]:
                return True
            if lv >= 3 and cmdcode in [CmdCode.PARAMED_WATERING]:
                return True

        if DevType.iscontroller(devtype):
            if cmdcode in [CmdCode.SET_CONFIG]:
                return True

        if DevType.isdisplay(devtype):
            if cmdcode in [CmdCode.OFF, CmdCode.SET_CONFIG]:
                return True

        if DevType.isnode(devtype):
            if cmdcode in [CmdCode.SET_CURRENT]:
                return True

            #if DevType.getnodelevel(devtype) > 0 and cmdcode in [CmdCode.CHANGE_CONTROL]:
            if cmdcode in [CmdCode.CHANGE_CONTROL]:
                return True

        if DevType.isgateway(devtype) and cmdcode in [CmdCode.DETECT_DEVICE, CmdCode.CANCEL_DETECT]:
            return True

        print(("It's not a proper command[" + str(cmdcode) + "] for " + str(devtype)))
        return False

