#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2018 JiNong, Inc.
# All right reserved.
#

import sys
import time
import importlib
import json
import traceback
import requests

import queue 
from multiprocessing import Process, Queue
from prometheus_client import Summary, Info, Counter

from lib import *

class CoupleManager(Runner):
    def __init__(self, configfile):
        self._couples = []
        self._children = {}
        self._isrunning = False
        self._logfuncs = None
        self._metric = {
            "request_processing_seconds" : Summary("request_processing_seconds", "Time spent processing request", ["coupleid", "command"]),
            "observations_count" : Counter("observations_count", "Counter for sending observations", ["coupleid"]),
            "notices_count" : Counter("notices_count", "Counter for sending notices", ["coupleid"]),
        }

        fp = open(configfile, 'r')
        self._option = json.loads(fp.read())
        fp.close()
        if 'sleep' not in self._option:
            self._option['sleep'] = 1
        if 'iter' not in self._option:
            self._option['iter'] = 10
        if 'heartbeat' not in self._option:
            self._option['heartbeat'] = 60

    def setlogger(self, logger):
        super(CoupleManager, self).setlogger(logger)
        self._logfuncs = {
            "debug" : logger.debug,
            "info" : logger.info,
            "warn" : logger.warning,
            "error" : logger.error,
            "crit" : logger.critical
        }

    def getdname(self):
        if 'dname' in self._option:
            return self._option['dname']
        else:
            return "cpmng"

    def extractcouples(self, gate):
        if "couples" in self._option:
            couples = []
            for cp in gate["children"]:
                if cp["id"] in self._option["couples"]:
                    couples.append(cp)
        else:
            couples = gate["children"]
        return couples

    def loadcandidatesfromfile(self, fname):
        try:
            fp = open(fname, 'r')
            gate = json.loads(fp.read())
            fp.close()
            self._logger.info("configuration load from file : " + fname)
            return self.extractcouples(gate)
        except Exception as ex:
            self._logger.warning("fail to load from file : " + fname + " " + str(ex)) 
            return None

    def loadcandidates(self):
        if 'mode' in self._option and self._option['mode'] == 'local':
            return self.loadcandidatesfromfile ('conf/localcouple.json')
        else:
            try:
                headers = {'Accept': 'application/json'}
                res = requests.get(self._option["url"] + "/" + self._option["id"], headers=headers)
            except Exception as ex:
                self._logger.warning("fail to load from server: " + self._option["url"] + "/" + self._option["id"] + " " + str(ex)) 
                return self.loadcandidatesfromfile ('conf/remotecouple.json')

            if res.status_code == 200:
                gate = res.json()
                with open('conf/remotecouple.json', 'w') as fp:
                    json.dump(gate, fp)
                return self.extractcouples(gate)
            else:
                self._logger.warning("gate server error : " + str(res.status_code) + " " + self._option["url"] + "/" + self._option["id"])
                return None

    def popcouple(self, candy):
        for idx in range(len(self._couples)):
            if self._couples[idx]['id'] == candy['id']:
                return self._couples.pop(idx)
        return None

    def execute(self, candy):
        try:
            ssmate = candy['ssmate']
            dsmate = candy['dsmate']
            devinfo = candy['children']
            couple = {}
            couple['id'] = candy['id']
            couple['candy'] = json.dumps(candy)
            couple['quelist'] = [Queue(), Queue(), Queue(), Queue()]
            couple['metric'] = self._metric
        except Exception as ex:
            self._logger.warning("fail to execute a candy : " + str(ex))
            return None

        try:
            couple['ssmate'] = self.loadmate(ssmate, devinfo, candy['id'], couple['quelist'], 'lib.ssmate')
            self._logger.info("A ssmate [" + ssmate["mod"] + "/" + ssmate["class"] + "] is loading.")
        except Exception as ex:
            self._logger.warning("fail to load ssmate : " + str(ex))
            return None

        try:
            couple['dsmate'] = self.loadmate(dsmate, devinfo, candy['id'], couple['quelist'], 'lib.dsmate')
            self._logger.info("A dsmate [" + dsmate["mod"] + "/" + dsmate["class"] + "] is loading.")
        except Exception as ex:
            self._logger.warning("fail to load dsmate : " + str(ex))
            return None

        try:
            couple['ssmate'].start()
        except Exception as ex:
            self._logger.warning("fail to start ssmate : " + str(ex))
            return None
        try:
            couple['dsmate'].start()
        except Exception as ex:
            for q in couple['quelist'][1:]:
                q.close()
                q.join_thread()
            couple['ssmate'].stop()
            self._logger.warning("fail to start dsmate : " + str(ex))
            return None

        return couple

    def loadmate(self, conf, devinfo, coupleid, quelist, path):
        module = importlib.import_module(path + '.' + conf['mod'])
        class_ = getattr(module, conf['class'])
        self._logger.info("load a mate : " + str(conf['class']) + " " + str(coupleid))
        mate = class_(conf['opt'], devinfo, coupleid, quelist)
        return mate

    def stopcouple(self, couple):
        self._logger.info(couple['id'] + " stopping")
        for q in couple['quelist'][1:]:
            q.close()
            q.join_thread()
        print ("try to stop dsmate")
        couple['dsmate'].stop()
        print ("try to stop ssmate")
        couple['ssmate'].stop()
        self._logger.info(couple['id'] + " stopped")

    def stopold(self):
        for couple in self._couples:
            self.stopcouple(couple)

    def stop(self):
        self._logger.info("Couple Manager tries to stop")
        self._isrunning = False

    def processlog(self):
        for couple in self._couples:
            logq = couple['quelist'][0]
            try:
                while True:
                    msg = logq.get(False)
                    if msg[0] == "M":     # M means Metric
                        #couple[metric] .....
                        #self._logger.info ("Metric Info : " + str(msg[1:]))
                        if msg[1] == "request_processing_seconds":
                            self._metric[msg[1]].labels(couple['id'], str(msg[2])).observe(msg[3])
                        elif msg[1] == "observations_count" or msg[1] == "notices_count":
                            self._metric[msg[1]].labels(couple['id']).inc()
                        else:
                            self._logger.warning("Unknown metric arrived : " + str(msg[1:]))
                    elif msg[0] == "L":     # L means log
                        self._logfuncs[msg[1]](msg[2])
                    elif msg[0] == "H":     # H means heartbeat
                        self._children[msg[1]] = msg[2]
            except queue.Empty:
                pass
            except Exception as ex:
                self._logger.warning("There is an exception : " + str(ex))
                self._logger.warning(traceback.format_exc())

    def checkchildren(self):
        current = time.time()
        tmppid = []
        for pid, tm in self._children.items():
            if current - tm > self._option['heartbeat']: # 핫빗을 보내지 않았다면 이상이 있는 child
                self._logger.info("Mate [" + str(pid) + "] might be suspended.")
                tmppid.append(pid)

                for couple in self._couples:
                    if couple['dsmate'].getpid() == pid or couple['ssmate'].getpid() == pid:
                        self._logger.info("A couple [" + couple['id'] + "] would be stopped.")
                        tmppid.append (couple['dsmate'].getpid())
                        tmppid.append (couple['ssmate'].getpid())
                        self.stopcouple(couple)
                        self._couples.remove(couple)

        for tpid in set(tmppid):
            try:
                del self._children[tpid]
            except:
                pass

    def run(self, debug=False):
        self._isrunning = True
        i = 0
        while self._isrunning:
            if i % self._option['iter'] == 0:
                i = 1
                self.checkchildren()
                newcouples = []
                candidates = self.loadcandidates()
                if candidates is None:
                    self._logger.info("retry to load candidates..." + str(i / self._option['iter']))
                else:
                    for candy in candidates:
                        couple = self.popcouple(candy)
                        if couple is None:
                            self._logger.info("A new couple [" + candy["name"] + "] is loading.")
                            couple = self.execute(candy)
                        elif couple['candy'] != json.dumps(candy):
                            self._logger.info("Configuration of a couple [" + candy["name"] + "] is changed.")
                            self.stopcouple(couple)
                            couple = self.execute(candy)

                        if couple is not None:
                            newcouples.append(couple)
                        else:
                            self._logger.info("A couple [" + candy["name"] + "] is not loading.")
                    self.stopold()
                    self._couples = newcouples
            else:
                i = i + 1

            if debug:
                break
            self.processlog()
            time.sleep(self._option['sleep'])
        self._logger.info("Couple Manager is stopping.")
        self.stopold()
        for couple in self._couples:
            couple['quelist'][0].close()


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage : python couplemng.py [start|stop|restart|run|single]")
        sys.exit(2)

    mode = sys.argv[1]
    runner = CoupleManager('conf/cpmng.conf')
    dname = runner.getdname()
    adaemon = Daemon(dname, runner)
    if 'start' == mode:
        adaemon.start()
    elif 'stop' == mode:
        adaemon.dstop()
    elif 'restart' == mode:
        adaemon.restart()
    elif 'run' == mode:
        adaemon.run()
    elif 'single' == mode:
        adaemon.run(True)
    else:
        print("Unknown command")
        sys.exit(2)
    sys.exit(0)
