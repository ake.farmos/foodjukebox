#!/usr/bin/env python
#
# -*- coding: utf-8 -*-
#
# Copyright (c) 2020 JiNong, Inc.
# All right reserved.
#

import json
import sys
import hashlib
import requests
import re
import uuid
from pymodbus.client.sync import ModbusSerialClient


def readserial():
    try:
        conn = ModbusSerialClient(method='rtu', port='/dev/ttyS0', timeout=5, baudrate=115200)
        conn.connect()
        res = conn.read_holding_registers(0, 2, unit=1)

        if res is None or res.isError():
            sys.stderr.write("Modbus connection error.")
            return (0, 0, 0)
        else:
            text = str(res.registers[0]) + " " + str(res.registers[1])
            enc = hashlib.md5()
            enc.update(text.encode('utf-8'))
            return (res.registers[0], res.registers[1], str(enc.hexdigest()))
    except Exception as ex:
        sys.stderr.write(str(ex))
        return (0, 0, 0)


def setserial(pastserial):
    conn = ModbusSerialClient(method='rtu', port='/dev/ttyS0', timeout=5, baudrate=115200)
    conn.connect()
    res = [44064, pastserial]
    text = str(res[0]) + " " + str(res[1])
    enc = hashlib.md5()
    enc.update(text.encode('utf-8'))
    return (res[0], res[1], str(enc.hexdigest()))



def regist(model, serial, hash):
    try:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        url = 'http://fjbox.farmos.co.kr:8880/v1/fjbox'
        param = {'id' : hash, 'name' : "fjbox " + str(model) + " "  + str(serial)}
        res = requests.post(url, data=json.dumps(param), headers=headers)
        if res.status_code == 200:
            return res.json()
        else:
            sys.stderr.write(str(res.status_code) + " " + str(res.reason) + "\n")
            return None
    except Exception as ex:
        sys.stderr.write(str(ex))
        return None

def getfjboxid(hash):
    try:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        url = 'http://fjbox.farmos.co.kr:8880/v1/gate/'+hash
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            fjboxID = response.json()
            equipmentID = fjboxID['children'][0]['id']
            print (equipmentID)
        else:
            sys.stderr.write(str(response.status_code) + " " + str(response.reason) + "\n")
            return None
    except Exception as ex:
        sys.stderr.write(str(ex))
        return None


def userregist(serial):
    try:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        url = 'http://fjbox.farmos.co.kr/user'
        userid = "fjbox"+str(serial)
        passwd = hashlib.md5()
        passwd.update(userid.encode('utf-8'))
        temp = str(passwd.hexdigest())
        userpw = re.sub('[^0-9]','',temp).strip()[:6]
        param = {'id' : userid, 'pw' : userpw, "serial" : hash}
        res = requests.put(url, data=json.dumps(param), headers=headers)
        print (userid, userpw)
        if res.status_code == 200:
            return True
        else:
            sys.stderr.write(str(res.status_code) + " " + str(res.reason) + "\n")
            return None
    except Exception as ex:
        sys.stderr.write(str(ex))
        return None


def deviceregist(hash, serial):
    if int(serial) < 2000 :
        if int(serial) < 150 :
            connserver = 'fjboxtest.farmos.co.kr'
        else :
            servernum = str((int(serial) % 1000) // 15 + 1).zfill(3)
            connserver = 'fjbox'+servernum+'.farmos.co.kr'
        sshport = 2000 + int(serial) % 1000
        uiport = 8000 + int(serial) % 1000
        mqttport = 9000 + int(serial) % 1000
    else :
        servernum = str((int(serial) % 1000) // 15 + 1).zfill(3)
        connserver = 'fjbox'+servernum+'.farmos.co.kr'
        sshport = 2200 + int(serial) % 1000
        uiport = 8200 + int(serial) % 1000
        mqttport = 9200 + int(serial) % 1000

    print (connserver, sshport, uiport, mqttport)

    try:
        headers = {'Content-Type': 'application/json; charset=utf-8'}
        url = 'http://fjbox.farmos.co.kr/device'
        param = {"url" : str(connserver), "port" : str(uiport), "uuid" : "fjbox-" + str(serial), "serial" : str(hash)}
        print (param)
        res = requests.post(url, data=json.dumps(param), headers=headers)
        if res.status_code == 200:
            return True
        else:
            sys.stderr.write(str(res.status_code) + " " + str(res.reason) + "\n")
            return None
    except Exception as ex:
        sys.stderr.write(str(ex))
        return None


def makeapiconf(hash):
    file_path = "./config.json"

    data = {}
    data['templateApi'] = "http://fjbox.farmos.co.kr:8081/common/v1"
    data['cvgateIp'] = "http://fjbox.farmos.co.kr:8880/v1"
    data['backupPath'] = "../../backup"
    data['backupObs'] = "obs"
    data['backupImage'] = "image"
    data['database'] = {"user": "farmos","pass": "farmosv2@","host": "127.0.0.1","db": "farmos"}
    data['serial'] = hash

    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, indent=4)

def makecvtconf(hash):
    file_path = "./cpmng.conf"

    data = {}
    data['id'] = hash
    data['url'] = "http://fjbox.farmos.co.kr:8082/common/v1/gate"
    data['sleep'] = 1
    data['iter'] = 60

    with open(file_path, 'w') as outfile:
        json.dump(data, outfile, indent=4)


(model, serial, hash) = readserial()
if model != 0 and serial != 0:
    pass
else:
    file = open('status.txt', mode = 'r')
    pastserial = file.readlines()[10][18:]
    file.close()
    (model, serial, hash) = setserial(pastserial)


makeapiconf(hash)
makecvtconf(hash)

print (model, serial, hash)
print (regist(model, serial, hash))
print (getfjboxid(hash))
print (deviceregist(hash, serial))
print (userregist(serial))
print (uuid.getnode(), uuid.UUID(int=uuid.getnode()))

