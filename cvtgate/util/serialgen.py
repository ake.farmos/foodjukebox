#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#

import requests
import uuid
import hashlib
import json
from datetime import date

class SerialGenerator:
    """
        /var/opt/farmos.serial
    """

    SERIAL_FILE = "/var/opt/farmos.serial"

    def __init__(self):
        self.m = hashlib.md5()

    def readserial(self):
        try:
            f = open(SerialGenerator.SERIAL_FILE, "r")
            serials = f.readline().rstrip().split()
            f.close()
            return (serials[0], int(serials[1]))
        except Exception as ex:
            print ("Exception: ", ex)
            return None

    def update(self):
        self.m.update(str(uuid.getnode()).encode("utf-8"))
        return self.m.hexdigest()[:4] + "-" + self.m.hexdigest()[4:8] + "-" + self.m.hexdigest()[8:12]

    def exists(self, serial):
        # 서버랑 비교할 수 있는 코드를 넣을 수도 있을것 같기는 한데, 지금은 서버랑 비교하지는 않기로 함.
        url = "http://118.67.130.32:8080/release/" + serial
        res = requests.get(url)
        if res.status_code == 200:
            try:
                prod = res.json()
                if prod["serial_num"] == serial:
                    return True
            except Exception as ex:
                #print ("Exception: ", ex)
                pass
            return False
        raise Exception("It's impossible to compare serial key to server's")

    def register(self, serial, iteration):
        data = {
            "serial_num": serial,
            "user_id": "admin", 
            "model_name": "001001",
            "product_name": "002001",
            "product_info": {"memo": "Test"},
            "production_date": date.today().strftime("%Y-%m-%d"),
            "release_date": "",
            "company": "",
            "iteration": iteration, 
            "cvtgate_url": "http://dev.jinong.co.kr:8880/v1/gate/" + serial
        }
        try:
            url = "http://118.67.130.32:8080/release"
            res = requests.post(url, json=data)
            print (res.status_code, res.reason)
        except Exception as ex:
            print ("Exception: ", ex)
            return None
        return True

    def setserial(self, serial, iteration):
        try:
            f = open(SerialGenerator.SERIAL_FILE, "w")
            f.write(serial)
            f.write(" ")
            f.write(str(iteration))
            f.close()
            return (serial, iteration)
        except Exception as ex:
            print ("Exception: ", ex)
            return None

    def generate(self, iteration=1):
        for _ in range(iteration):
            serial = self.update() 
        if self.exists(serial) is False:
            return self.setserial(serial, iteration)
        print ("The serial ", (serial, iteration), " already exists. Try to use --iter option.")

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='make serial key for farmos device.')
    parser.add_argument('--show', help='show a serial key', action="store_true")
    parser.add_argument('--gen', help='generate a serial key', action="store_true")
    parser.add_argument('--iter', help='iteration for generating a serial key', type=int)
    parser.add_argument('--reg', help='register the serial key', action="store_true")

    args = parser.parse_args()

    sgen = SerialGenerator()
    if args.gen:
        it = args.iter if args.iter else 1
        sgen.generate(it)

    serial, iteration = sgen.readserial()
    if args.show:
        print (serial, iteration)

    if args.reg:
        sgen.register(serial, iteration)

    if args.gen is False and args.show is False and args.reg is False:
        parser.print_help()

