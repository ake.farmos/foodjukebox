#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#

import sys
import time
import json
import psutil
import pymysql
import traceback
from lib.mblock import Notice, NotiCode

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish


class FMon:
    def __init__(self, fname):
        self._auth = None
        with open(fname, "r") as fp:
            self._option = json.loads(fp.read())

        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], cursorclass=pymysql.cursors.DictCursor, charset='utf8')
        self._cur = self._conn.cursor()
        self._uuid = self.loaduuid()

    def loaduuid(self):
        query = "select uuid from gate_info"
        self._cur.execute(query, [])
        for row in self._cur.fetchall():
            return row['uuid']

    def cpu(self):
        loads = [x / psutil.cpu_count() * 100 for x in psutil.getloadavg()]
        return dict(zip(["loads_1m", "loads_5m", "loads_15m"], loads))

    def memory(self):
        vmem = psutil.virtual_memory()
        swap = psutil.swap_memory()
        return {"vmem": vmem.percent, "swap": swap.percent}

    def disk(self):
        usage = psutil.disk_usage('/')
        iocnt = psutil.disk_io_counters(perdisk=False)
        return {"disk": usage.percent, "ioread": iocnt.read_bytes / 1048576.0, "iowrite": iocnt.write_bytes / 1048576.0}

    def network(self, interface):
        if interface:
            netio = psutil.net_io_counters(pernic=True)
            return {"netsent": netio[interface].bytes_sent / 1048576.0, "netrecv": netio[interface].bytes_recv / 1048576.0}
        else:
            netio = psutil.net_io_counters(pernic=False)
            return {"netsent": netio.bytes_sent / 1048576.0, "netrecv": netio.bytes_recv / 1048576.0}

    def execute(self, interface):
        monitor = self.cpu()
        monitor.update(self.memory())
        monitor.update(self.disk())
        monitor.update(self.network(interface))
        self.process(monitor)

    def process(self, monitor):
        dataids = {"loads_1m" : 51, "loads_5m": 52, "loads_15m" : 53,
                   "vmem" : 54, "swap": 55, "disk" : 56,
                   "ioread" : 57, "iowrite" : 58, "netsent" : 59, "netrecv" : 60}

        noti = Notice(None, NotiCode.SYSTEM_MONITOR)
        for k, v in monitor.items():
            noti.setkeyvalue(k, v)
            self.writedb(dataids[k], v)
        publish.single("cvtgate/" + self._uuid + "//noti/", payload=noti.stringify(), qos=1, hostname=self._option['mqtt']['host'], auth=self._auth)
        self._conn.commit()
    
    def writedb(self, dataid, value):
        upqry = 'UPDATE current_observations SET obs_time = now(), modified_time = now(), nvalue = %s WHERE data_id = %s'
        inqry = 'INSERT INTO observations (data_id, obs_time, nvalue, source_id) VALUES (%s, now() , %s, NULL)'
        try:
            self._cur.execute(upqry, [value, dataid])
            self._cur.execute(inqry, [dataid, value])
        except Exception as ex:
            print ("Fail to get data statistics : " + str(ex))
            print (traceback.format_exc())

if __name__ == '__main__':
    fmon = FMon("conf/util.conf")
    fmon.execute("eth0")
