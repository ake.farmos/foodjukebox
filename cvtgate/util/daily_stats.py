#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#

import sys
import time
import json
import pymysql
import traceback
import argparse
from lib.mblock import Notice, NotiCode
from datetime import date, timedelta

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

class DailyStats:
    def __init__(self, fname):
        self._auth = None
        with open(fname, "r") as fp:
            self._option = json.loads(fp.read())

        copt = self._option["db"]
        self._conn = pymysql.connect(host=copt["host"], user=copt["user"],
                         password=copt["password"], db=copt["db"], 
                         cursorclass=pymysql.cursors.DictCursor, charset='utf8')
        self._cur = self._conn.cursor()

    def loaddataid(self):
        qry = "SELECT 10000000 + id * 100 + 1 as data_id from devices "\
              "where spec like '%\"Class\":\"sensor\"%' and deleted = 0" 
        try:
            self._cur.execute(qry)
            dataids = []
            for row in self._cur.fetchall():
                dataids.append(row['data_id'])
            return dataids
        except Exception as ex:
            print ("Fail to get dataids: " + str(ex))
            print (traceback.format_exc())
            return None

    def execute(self, datestr, dataids=None):
        if dataids is None:
            dataids = self.loaddataid()

        stats = self.getstats(datestr, dataids)
        if stats is None:
            # Send some warning
            print ("Fail to get stats.")
            return

        iqry = "REPLACE INTO observations(obs_time, nvalue, data_id, source_id) VALUES (%s, %s, %s, NULL)"
        uqry = "UPDATE current_observations set obs_time=%s, nvalue=%s, modified_time=now() WHERE data_id = %s"
        try:
            for param in stats:
                print("insert", param)
                self._cur.execute(iqry, param)
                if (date.today()-timedelta(days=1)).isoformat() == datestr:
                    self._cur.execute(uqry, param)
            self._conn.commit()
        except Exception as ex:
            print ("Fail to insert data statistics : " + str(ex))
            print (traceback.format_exc())
            return None
           
    def getstats(self, datestr, dataids):
        diff = self.getdiffstats(datestr, dataids)
        daily = self.getdailystats(datestr, dataids)
        if diff and daily:
            return diff + daily
        return None

    def getdiffstats(self, datestr, dataids):
        qry = "select stddev(diff) ds from ( SELECT COALESCE( ( SELECT nvalue FROM observations mi "\
              "WHERE mi.obs_time > m.obs_time and mi.data_id = %s and mi.obs_time between %s and %s "\
              "ORDER BY obs_time limit 1), nvalue) - nvalue AS diff "\
              "FROM observations m where data_id = %s and obs_time between %s and %s ORDER BY obs_time) d"

        sd = datestr + ' 00:00:00'
        ed = datestr + ' 23:59:59'
        stats = [] 
        for did in dataids:
            params = [did, sd, ed, did, sd, ed] 
            print ("diffparam", params)
            try:
                self._cur.execute(qry, params)
                for row in self._cur.fetchall(): 
                    stats.append ([datestr, row['ds'], did + 25])
            except Exception as ex:
                print ("Fail to make diff statistics : " + str(ex))
                print (traceback.format_exc())
        return stats

    def getdailystats(self, datestr, dataids):
        qry = "SELECT data_id, count(*) vc, avg(nvalue) va, max(nvalue) vx, min(nvalue) vn, std(nvalue) vs "\
              "from observations "\
              "WHERE data_id in %s and obs_time between %s and %s group by data_id"
        try:
            print ("params", datestr, dataids)
            self._cur.execute(qry, [dataids, datestr + ' 00:00:00', datestr + ' 23:59:59'])
            stats = [] 
            for row in self._cur.fetchall():
                stats.append ([datestr, row['vc'], row['data_id'] + 20])
                stats.append ([datestr, row['va'], row['data_id'] + 21])
                stats.append ([datestr, row['vx'], row['data_id'] + 22])
                stats.append ([datestr, row['vn'], row['data_id'] + 23])
                stats.append ([datestr, row['vs'], row['data_id'] + 24])
            return stats
        except Exception as ex:
            print ("Fail to make data statistics : " + str(ex))
            print (traceback.format_exc())
            return None

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='make statistics for sensor observations daily.')
    parser.add_argument('datestr', nargs='?', default=(date.today()-timedelta(days=1)).isoformat(), help='date to make statistics YYYY-MM-DD')
    parser.add_argument('--ids', metavar='DATA_ID', type=int, nargs='*', help='dataids (default: None)')

    args = parser.parse_args()
    print (args)
    dstats = DailyStats("conf/util.conf")
    dstats.execute(args.datestr, args.ids)
