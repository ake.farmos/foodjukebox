#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2021 JiNong, Inc.
# All right reserved.
#

import sys
import time
import datetime
import json
import argparse

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

import pyotp
import base64

def getotp(serial):
    otpkey = base64.b32encode(bytes(serial, 'utf-8'))
    totp = pyotp.TOTP(otpkey)
    totp.at(datetime.datetime.now())
    return totp.now()

def makemsg(svcname, serial, command):
    topic = "farmos/" + serial + "/req/" + svcname
    msg = {"cmd" : command, "key" : getotp(serial)}
    return topic, json.dumps(msg)

def publishmsg(mqttsrv, svcname, serial, command):
    topic, msg = makemsg(svcname, serial, command)
    publish.single(topic, payload=msg, qos=2, hostname=mqttsrv, port=1883)
    print("publish message", topic, msg)

def onmsg(client, obj, blk):
    print("fosmng_client received mblock '" + str(blk.payload) + "' on topic '" + blk.topic + "' with QoS " + str(blk.qos))

def subscribe(mqttsrv, svcname, serial):
    topic = "farmos/" + serial + "/res/" + svcname
    client = mqtt.Client()
    client.loop(.1)
    client.on_message = onmsg

    client.connect(mqttsrv, 1883, 10)
    client.subscribe(topic, 2)
    client.loop_start()
    return client


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='this command helps to communicate with farmos system manager.')
    parser.add_argument('mqttsrv', help='address of mqtt server. ex) farmos003.jinong.co.kr')
    parser.add_argument('svcname', help='service name. ex) cvtgate, fcore.')
    parser.add_argument('serial', help='serial number of smart link to control.')
    parser.add_argument('command', help='control command. ex) start, stop, restart.')

    args = parser.parse_args()

    if None in [args.mqttsrv, args.svcname, args.serial, args.command]:
        parser.print_help()
    else:
        client = subscribe(args.mqttsrv, args.svcname, args.serial)
        publishmsg(args.mqttsrv, args.svcname, args.serial, args.command)
        time.sleep(5);
        client.loop_stop()

