#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

function successSlack(){
    echo -e "UI update success"
    sed -i '/chat/d' pyslack.py
    echo "post_message('xoxb-1699645670614-2741113048611-IGiBCHLtN2HrRiRn6zXIjSWZ', '#fjbox', '*$fjboxname : UI update success*')" >> "pyslack.py"
    ret=$(python pyslack.py)
}


sudo forever stopall

fjboxname=$(cat /etc/hostname)

cd /home/pi/farmosv2-script

if [ -f "common_api/api/public/menu_pc.json" ] ; then
    mv common_api/api/public/menu_m.json menu_m.json
    mv common_api/api/public/menu_pc.json menu_pc.json
else
    cat << "EOF" > "menu_pc.json"
["data/reference","data/correlation","data/statistics","research","device","setting/process","setting/farm","control/auto/0","control/manual/nutrient-supply","setting/timespan","rule/template","add/template","modify/timespan"]
EOF
    cat << "EOF" > "menu_m.json"
["device/state","setting/place","setting/farm"]
EOF
fi


if [ -e opensource_ui ]; then
    rm -rf opensource_ui
fi

git clone https://gitlab.com/jinong/fjbox_opensource_ui_build.git opensource_ui

cd opensource_ui;
sudo ./install.sh
cd ..

cat /dev/null > /home/pi/farmosv2-script/updates/log/temp

sleep 2s

mv menu_m.json common_api/api/public/menu_m.json
mv menu_pc.json common_api/api/public/menu_pc.json


sudo /etc/init.d/fui start
cd /home/pi/rpi-ipset
sudo forever start app.js

cd /home/pi/farmosv2-script/updates
successSlack $fjboxname

