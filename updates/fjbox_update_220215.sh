#!/bin/bash

dirpath=`dirname $0`
echo -e $dirpath
cd $dirpath
SHELL_PATH=`pwd -P`

sed -i "s/8880/8082\/common/g" /home/pi/farmosv2-script/cvtgate/gate/conf/cpmng.conf
sed -i "s/jinong/farmos/g" /home/pi/farmosv2-script/cvtgate/gate/conf/cpmng.conf

cat <(crontab -l) <(echo '7 1 * * * cd /home/pi/farmosv2-script/scripts; ./deletephoto.sh') | crontab -

mysql -u farmos -pfarmosv2@ farmos -e "INSERT INTO core_rule_applied VALUES (19,'주기지정 사진촬영',now(),1,0,0,'{\"target\":\"field\",\"devices\":[{\"class\":\"actuator\",\"type\":\"camera/level0\",\"outputs\":\"#cam\",\"desc\":\"카메라를 선택해주세요.\",\"deviceid\":18,\"name\":\"카메라\"}],\"data\":[]}','{\"basic\":[{\"key\":\"#workperiod\",\"name\":\"촬영주기(분)\",\"value\":[30,30,30,30,3,30],\"type\":\"ts_float\",\"description\":\"촬영 주기(분)을 설정합니다.\"}],\"advanced\":[{\"key\":\"priority\",\"name\":\"우선순위\",\"value\":4,\"minmax\":[0,5],\"description\":\"룰의 우선순위\"},{\"key\":\"period\",\"name\":\"기간\",\"value\":60,\"description\":\"룰의 작동주기\"}],\"timespan\":{\"id\":4,\"used\":[true,true,true,true,true,true]}}','[]','{\"processors\":[{\"type\":\"mod\",\"mod\":\"farmos_basic.periodictakeimage\",\"outputs\":[\"#cmd\"]}]}','{\"req\":[{\"cmd\":\"#cmd\",\"pnames\":[],\"params\":[],\"targets\":[\"#cam\"]}]}',0,0,0,'스위치제어',131);"
mysql -u farmos -pfarmosv2@ farmos -e "INSERT INTO dataindexes VALUES (30190100,19,'19번 룰 1번 컨트롤 처리현황','',0,NULL,1,0);"
mysql -u farmos -pfarmosv2@ farmos -e "INSERT INTO current_observations VALUES (30190100,now(),0,now(),NULL);"


